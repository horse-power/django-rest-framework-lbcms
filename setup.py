import os
from setuptools import setup
try:
    README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()
except:
    README = ''

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

requirements = [
    'Django>=3.1.2',
    'djangorestframework>=3.12.1',
    'django-widget-tweaks>=1.4.1',
    'django-model-changes-py3>=0.14.1',
    'currencyconverter==0.13',
    'sendgrid-django==2.0.0',
    'xlwt==1.3.0',
    'xlrd==1.2.0',
    'unidecode',
    'polib>=1.1.0',
    'PyPDF2>=1.26.0',
    'requests',
    'pypugjs',
    'google-api-python-client==1.7.4',
    'jsonfield>=3.1.0',
    'django-timezone-field>=4.0',
    'django-sortedm2m==3.0.2',
    'django-storages>=1.11.1,<=1.13.2',
    'boto3>=1.9.29,<=1.13.4',
    'lxml<=4.9.4',

    'smsapi @ git+https://github.com/smsapicom/smsapi-python-client.git@c5a7aa5fbb7c932126c41d2042a12c6a57fa7cbc#egg=smsapi',
    'premailer @ git+https://github.com/gerasev-kirill/premailer.git@95a44cce222fc309aa9699ef25a4ed19f90b3e9d#egg=premailer',
    'django-rest-framework-generator @ git+https://github.com/gerasev-kirill/django-rest-framework-generator.git@v3.0.8#egg=django-rest-framework-generator',
    'django-rest-framework-loopback-js-filters @ git+https://github.com/gerasev-kirill/django-rest-framework-loopback-js-filters.git@v3.0.1#egg=django-rest-framework-loopback-js-filters',
    'django-rest-framework-angular-resource-generator @ git+https://github.com/gerasev-kirill/django-rest-framework-angular-resource-generator.git@v3.0.5#egg=django-rest-framework-angular-resource-generator',
]


setup(
    name='django-rest-framework-lbcms',
    version='3.0.0',
    packages=['drf_lbcms', 'lbcms'],
    include_package_data=True,
    license='BSD License',
    description='LbCms for django',
    long_description=README,
    author='Gerasev Kirill',
    author_email='gerasev.kirill@gmail.com',
    install_requires=requirements,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
