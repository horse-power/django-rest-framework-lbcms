try:
    from django.urls import re_path, include
except:
    # DEPRECATED: django < 2.0
    from django.conf.urls import url as re_path, include

app_name = 'lbcms'

urlpatterns = [
    re_path(r'^', include('lbcms.rest.urls'))
]
