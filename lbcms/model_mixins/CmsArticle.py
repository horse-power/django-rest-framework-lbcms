# -*- coding: utf-8 -*-
from drf_lbcms.helpers import to_url_id
from drf_lbcms.templatetags.directives.helpers import reverse

try:
    import readtime
    from pyquery import PyQuery as pq

    def text_readtime(html):
        text, img_count = readtime.utils.parse_html(pq(html))
        '''
            Время чтения нужно рассчитывать по формуле - 1 мин на 1000 символов с округлением в большую сторону, в минутах без остатка.
        '''
        t = len(text)*60.0/1000
        return (t// 60) * 60 + 60
        #return (readtime.of_html(html).seconds // 60) * 60 + 60

except:
    def text_readtime(html):
        return 0




class ModelMixin(object):
    def save(self, *args, **kwargs):
        self.readTime = {}
        for lang, html in (self.body or {}).keys():
            if not html:
                self.readTime[lang] = 0
            else:
                self.readTime[lang] = text_readtime(html)
        super(ModelMixin, self).save(*args, **kwargs)


    def get_absolute_url(self, lang=None):
        return reverse('app.articles.details', kwargs={
            'lang': lang,
            'article_url_id': self.to_url_id(lang=lang),
            'category_url_id': self.category_to_url_id(lang=lang)
        })


    def get_sitemap_item(self, lang=None, **kwargs):
        # генерируем данные для sitemap
        if not self.isPublished:
            return None
        # запрещаем также категории по gdpr и legal-info
        if self.category and self.category.identifier in ['legal-information']:
            return None
        return {
            'location': self.get_absolute_url(lang=lang),
            'changefreq': "monthly"
        }
