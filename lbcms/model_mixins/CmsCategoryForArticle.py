# -*- coding: utf-8 -*-
from drf_lbcms.helpers import to_url_id





class ModelMixin(object):
    def save(self, *args, **kwargs):
        super(ModelMixin, self).save(*args, **kwargs)

    def to_url_id(self, lang=None):
        return to_url_id(self.id, self.title)
