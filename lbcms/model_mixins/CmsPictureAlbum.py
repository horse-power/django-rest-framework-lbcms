# -*- coding: utf-8 -*-
from drf_lbcms.helpers import to_url_id



class ModelMixin(object):
    def save(self, *args, **kwargs):
        super(ModelMixin, self).save(*args, **kwargs)

    def to_url_id(self, lang=None):
        return to_url_id(self.id, self.title)

    def category_to_url_id(self, lang=None):
        if not self.category_id:
            return '-'
        return to_url_id(self.category.id, self.category.title)

    def get_absolute_url(self, lang=None):
        try:
            return reverse('app.galleries.details', kwargs={
                'lang': lang,
                'album_url_id': self.to_url_id(lang=lang),
                'category_url_id': self.category_to_url_id(lang=lang)
            })
        except:
            return reverse('app.galleries.details', kwargs={
                'lang': lang,
                'album_url_id': self.to_url_id(lang=lang)
            })
