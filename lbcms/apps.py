# -*- coding: utf-8 -*-
import sys
from django.apps import AppConfig
from django.db.utils import OperationalError, ProgrammingError
from django.utils import timezone


class LbCmsAppConfig(AppConfig):
    name = 'lbcms'
    verbose_name = 'LbCms models for django'

    def ready(self):
        if len(sys.argv) >= 2 and sys.argv[1] in ['loaddata', 'migrate', 'drf_lbcms_restore_db_and_files', 'compilemessages']:
            return

        from . import signals
        from .models import CmsCategoryForArticle

        try:
            from django.contrib.sites.models import Site
            sites = Site.objects.all()
        except:
            sites = [None]
        try:
            if not sites:
                sites = [None]
        except (OperationalError, ProgrammingError):
            return

        try:
            for site in sites:
                if not CmsCategoryForArticle.objects.filter(identifier='legal-information').count():
                    CmsCategoryForArticle.objects.create(
                        identifier='legal-information',
                        isPublished=True,
                        publicationDate=timezone.now(),
                        title={
                            'en': 'Legal information',
                            'ru': u'Юридическая информация',
                            'cz': u'Legální informace',
                            'cs': u'Legální informace',
                            'uk': u'Юридична iнформацiя'
                        }
                    )
                if not CmsCategoryForArticle.objects.filter(identifier='about-company').count():
                    CmsCategoryForArticle.objects.create(
                        identifier='about-company',
                        isPublished=False,
                        publicationDate=timezone.now(),
                        title={
                            'en': 'About company',
                            'ru': u'О компании',
                            'cz': u'O společnosti',
                            'cs': u'O společnosti',
                            'uk': u'Про компанію'
                        }
                    )
        except (OperationalError, ProgrammingError):
            pass
