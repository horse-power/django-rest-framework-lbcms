
from django.http import Http404
from drf_lbcms.exceptions import RedirectException
from drf_lbcms.views import TemplateView
from drfs import get_model




class ArticleDetailsView(TemplateView):
    template_name = 'article.pug'

    def get_articles_queryset(self):
        CmsArticle = get_model('CmsArticle.json', latest=True)
        return CmsArticle.objects.filter(isPublished=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        id = kwargs.get('article_url_id', None) or ''
        id = id.split('-')[0]
        category_id = kwargs.get('category_url_id', None) or ''
        category_id = category_id.split('-')[0]
        if not id:
            raise Http404("Article doesn't exists")
        queryset = self.get_articles_queryset()
        try:
            context['article'] = queryset.get(id=id)
        except queryset.model.DoesNotExist:
            raise Http404("Article doesn't exists")

        if category_id:
            if context['article'].category and str(context['article'].category.id) != category_id or not context['article'].category.isPublished:
                raise RedirectException('app.articles.details', params={
                    'article_url_id': kwargs['article_url_id'],
                    'category_url_id': '-'
                })

        context['all_articles'] = queryset.order_by('-publicationDate')
        if context['article'].category:
            context['all_articles_by_category'] = context['article'].category.articles_by_category.filter(isPublished=True).order_by('-publicationDate')
        return context




class ArticleListView(TemplateView):
    template_name = 'articles.pug'

    def get_articles_queryset(self):
        CmsArticle = get_model('CmsArticle.json', latest=True)
        return CmsArticle.objects.filter(isPublished=True)

    def get_categories_queryset(self):
        CmsCategoryForArticle = get_model('CmsCategoryForArticle.json', latest=True)
        return CmsCategoryForArticle.objects.filter(isPublished=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_id = kwargs.get('category_url_id', None) or ''
        category_id = category_id.split('-')[0]
        categories_queryset = self.get_categories_queryset()
        context['all_categories'] = categories_queryset.order_by('-publicationDate')
        context['category'] = None
        if category_id:
            try:
                context['category'] = categories_queryset.get(id=category_id)
            except categories_queryset.model.DoesNotExist:
                raise Http404("Category doesn't exists")


        context['all_articles'] = self.get_articles_queryset()
        if context['category']:
            context['all_articles_by_category'] = context['category'].articles_by_category.filter(isPublished=True).order_by('-publicationDate')
        return context
