# Generated by Django 3.1.2 on 2020-12-03 11:50

from django.db import migrations, models
import django.db.models.deletion
import django_model_changes.changes
import drfs.db.fields
import drfs.db.validators
import lbcms.model_mixins.CmsArticle
import lbcms.model_mixins.CmsCategoryForArticle
import lbcms.model_mixins.CmsCategoryForPictureAlbum
import lbcms.model_mixins.CmsPictureAlbum
import lbcms.model_mixins.CmsSiteReview


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('drf_lbcms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CmsCategoryForPictureAlbum',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('isPublished', models.BooleanField(default=False)),
                ('publicationDate', models.DateTimeField(blank=True, null=True)),
                ('data', drfs.db.fields.JSONField(blank=True, default={}, null=True)),
                ('identifier', models.CharField(default='', max_length=200)),
                ('title', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('description', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
            ],
            options={
                'abstract': False,
            },
            bases=(lbcms.model_mixins.CmsCategoryForPictureAlbum.ModelMixin, models.Model, django_model_changes.changes.ChangesMixin),
        ),
        migrations.CreateModel(
            name='CmsKeyword',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('isPublished', models.BooleanField(default=False)),
                ('publicationDate', models.DateTimeField(blank=True, null=True)),
                ('data', drfs.db.fields.JSONField(blank=True, default={}, null=True)),
                ('title', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('description', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('mainImg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='keywords_by_mainImg', to='drf_lbcms.l10nfile')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, django_model_changes.changes.ChangesMixin),
        ),
        migrations.CreateModel(
            name='CmsSiteReview',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('isPublished', models.BooleanField(default=True)),
                ('publicationDate', models.DateTimeField(blank=True, null=True)),
                ('importedFrom', models.CharField(blank=True, default='', max_length=100, null=True)),
                ('importedUrl', models.CharField(blank=True, default='', max_length=150, null=True)),
                ('importedIdList', drfs.db.fields.ListField(blank=True, default=[], null=True)),
                ('rating', models.FloatField(default=0)),
                ('data', drfs.db.fields.JSONField(blank=True, default={}, null=True)),
                ('title', drfs.db.fields.EmbeddedOneModel(default={}, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('body', drfs.db.fields.EmbeddedOneModel(default={}, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('description', drfs.db.fields.EmbeddedOneModel(default={}, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('mainImgUrl', drfs.db.fields.EmbeddedOneModel(blank=True, default=None, help_text='Links to external image in internet (use either mainImg field or mainImgUrl)', null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nUrl')])),
                ('mainImg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='reviews_by_mainImg', to='drf_lbcms.l10nfile')),
            ],
            options={
                'abstract': False,
            },
            bases=(lbcms.model_mixins.CmsSiteReview.ModelMixin, models.Model, django_model_changes.changes.ChangesMixin),
        ),
        migrations.CreateModel(
            name='CmsPictureAlbum',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('isPublished', models.BooleanField(default=False)),
                ('publicationDate', models.DateTimeField(blank=True, null=True)),
                ('mainImgId', models.IntegerField(blank=True, default=None, help_text='Main image for album cover(id from images[index].id)', null=True)),
                ('data', drfs.db.fields.JSONField(blank=True, default={}, null=True)),
                ('title', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('description', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='pictureAlbums_by_category', to='lbcms.cmscategoryforpicturealbum')),
                ('images', models.ManyToManyField(blank=True, related_name='pictureAlbums_by_images', to='drf_lbcms.L10nFile')),
                ('keywords', models.ManyToManyField(blank=True, related_name='pictureAlbums_by_keywords', to='lbcms.CmsKeyword')),
            ],
            options={
                'abstract': False,
            },
            bases=(lbcms.model_mixins.CmsPictureAlbum.ModelMixin, models.Model, django_model_changes.changes.ChangesMixin),
        ),
        migrations.AddField(
            model_name='cmscategoryforpicturealbum',
            name='keywords',
            field=models.ManyToManyField(blank=True, related_name='pictureAlbumCategories_by_keywords', to='lbcms.CmsKeyword'),
        ),
        migrations.AddField(
            model_name='cmscategoryforpicturealbum',
            name='mainImg',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='categoriesForPictureAlbum_by_mainImg', to='drf_lbcms.l10nfile'),
        ),
        migrations.CreateModel(
            name='CmsCategoryForArticle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('isPublished', models.BooleanField(default=False)),
                ('publicationDate', models.DateTimeField(blank=True, null=True)),
                ('data', drfs.db.fields.JSONField(blank=True, default={}, null=True)),
                ('identifier', models.CharField(default='', max_length=200)),
                ('title', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('description', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('keywords', models.ManyToManyField(blank=True, related_name='articleCategories_by_keywords', to='lbcms.CmsKeyword')),
                ('mainImg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='categoriesForArticle_by_mainImg', to='drf_lbcms.l10nfile')),
            ],
            options={
                'abstract': False,
            },
            bases=(lbcms.model_mixins.CmsCategoryForArticle.ModelMixin, models.Model, django_model_changes.changes.ChangesMixin),
        ),
        migrations.CreateModel(
            name='CmsArticle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('isPublished', models.BooleanField(default=False)),
                ('publicationDate', models.DateTimeField(blank=True, null=True)),
                ('readCount', models.IntegerField(blank=True, default=0, null=True)),
                ('readTime', drfs.db.fields.JSONField(default={})),
                ('data', drfs.db.fields.JSONField(blank=True, default={}, null=True)),
                ('protectedData', drfs.db.fields.JSONField(blank=True, default={}, null=True)),
                ('title', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('description', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('urlIdPart', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('body', drfs.db.fields.EmbeddedOneModel(blank=True, default={}, null=True, validators=[drfs.db.validators.EmbeddedValidator('L10nBaseString')])),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='articles_by_category', to='lbcms.cmscategoryforarticle')),
                ('files', models.ManyToManyField(blank=True, related_name='articles_by_files', to='drf_lbcms.L10nFile')),
                ('keywords', models.ManyToManyField(blank=True, related_name='articles_by_keywords', to='lbcms.CmsKeyword')),
                ('mainImg', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='articles_by_mainImg', to='drf_lbcms.l10nfile')),
            ],
            options={
                'abstract': False,
            },
            bases=(lbcms.model_mixins.CmsArticle.ModelMixin, models.Model, django_model_changes.changes.ChangesMixin),
        ),
    ]
