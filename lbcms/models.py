# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import drfs, os





CmsKeyword = drfs.generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsKeyword.json'
    )
)
CmsCategoryForArticle = drfs.generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsCategoryForArticle.json'
    )
)
CmsArticle = drfs.generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsArticle.json'
    )
)
CmsSiteReview = drfs.generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsSiteReview.json'
    )
)
CmsCategoryForPictureAlbum = drfs.generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsCategoryForPictureAlbum.json'
    )
)
CmsPictureAlbum = drfs.generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsPictureAlbum.json'
    )
)
