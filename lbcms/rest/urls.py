from drfs import routers
# import block
import drfs
# end import


router = routers.SimpleRouter()
# register block
router.register(r'CmsPictureAlbum', drfs.generate_viewset('CmsPictureAlbum.json'))
router.register(r'CmsCategoryForPictureAlbum', drfs.generate_viewset('CmsCategoryForPictureAlbum.json'))
router.register(r'CmsSiteReview', drfs.generate_viewset('CmsSiteReview.json'))
router.register(r'CmsKeyword', drfs.generate_viewset('CmsKeyword.json'))
router.register(r'CmsCategoryForArticle', drfs.generate_viewset('CmsCategoryForArticle.json'))
router.register(r'CmsArticle', drfs.generate_viewset('CmsArticle.json'))
# end block
urlpatterns = router.urls
