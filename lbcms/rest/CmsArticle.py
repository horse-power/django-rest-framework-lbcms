# -*- coding: utf-8 -*-
from rest_framework.response import Response

from drfs import generate_serializer, mixins
from drfs.decorators import action







class CmsArticleViewsetMixin(object):

    @action(methods=['put'], detail=True)
    def mark_as_read(self, *args, **kwargs):
        instance = self.get_object()
        instance.readCount += 1
        instance.save(update_fields=['readCount'])
        return Response()
