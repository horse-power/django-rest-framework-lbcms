install_virtualenv:
	rm -fr .virtualenv/p4 || true
	virtualenv -p python3 --system-site-packages .virtualenv/p4
	chmod +x .virtualenv/p4/bin/activate
	ln -s .virtualenv/p4/bin/activate ap4 || true
	bash -c "source ap4; pip3 install -r ./requirements.django-3.1.txt"

run_tests:
	rm ./tests/storage-tests/* || true
	#bash -c "source ap4; python3 ./manage.py test tests"
	bash -c "source ap4; python3 ./manage.py test tests.test_gettext_helpers"

run_uwsgi_test:
	bash -c "source ap4; uwsgi test-uwsgi.ini"
	rm ./tests/storage-tests/* || true


mkmigrations:
	bash -c "source ap4; python3 ./manage.py makemigrations"

mktranslations:
	bash -c "source ap4; python3 ./manage.py makemessages --locale=ru --extension=html,txt,pug,py"
	bash -c "source ap4; python3 ./manage.py makemessages --locale=cs --extension=html,txt,pug,py"
	bash -c "source ap4; python3 ./manage.py makemessages --locale=uk --extension=html,txt,pug,py"
	bash -c "source ap4; python3 ./manage.py compilemessages"


copy-to-mirror:
	mkdir -p ../drf-lbcms3
	cp -R ./drf_lbcms ../drf-lbcms3/
	cp -R ./lbcms ../drf-lbcms3/
	cp -R ./tests ../drf-lbcms3/
	cp .gitignore ../drf-lbcms3/
	cp setup.py ../drf-lbcms3/
	cp Makefile ../drf-lbcms3/
	cp manage.py ../drf-lbcms3/
	cp MANIFEST.in ../drf-lbcms3/
