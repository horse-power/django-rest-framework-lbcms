
from django.http import HttpResponse
import requests

IGNORED_HEADERS = (
    'connection', 'keep-alive', 'proxy-authenticate',
    'proxy-authorization', 'te', 'trailers', 'transfer-encoding',
    'upgrade', 'content-length', 'content-encoding'
)



class BaseBackend:
    def __init__(self, params={}):
        self.params = params
        self.session = requests.Session()

    def get_response_for_url(self, url, request=None):
        """
        Accepts a fully-qualified url.
        Returns an HttpResponse, passing through all headers and the status code.
        """
        raise NotImplementedError

    def update_url(self, url):
        """
        Accepts a fully-qualified url, or regex.
        Returns True if successful, False if not successful.
        """
        raise NotImplementedError

    def build_django_response_from_requests_response(self, response):
        r = HttpResponse(response.content)
        for k, v in response.headers.items():
            if k.lower() not in IGNORED_HEADERS:
                r[k] = v
        r['content-length'] = len(response.content)
        r.status_code = response.status_code
        return r




class PuppeteerSelfHostedBackend(BaseBackend):
    def get_response_for_url(self, url, request=None):
        url = self.params['url'] + url
        response = self.session.get(url)
        assert response.status_code < 500
        return self.build_django_response_from_requests_response(response)





# https://github.com/skoczen/django-seo-js/blob/54928764649b626111e6dbed9290ba7fb4bbff35/django_seo_js/backends/base.py
class PrerenderBackend(BaseBackend):
    """Implements the backend for prerender.io"""
    BASE_URL = "https://service.prerender.io/"
    RECACHE_URL = "https://api.prerender.io/recache"


    def get_response_for_url(self, url, request=None):
        if not url or "//" not in url:
            raise ValueError("Missing or invalid url: %s" % url)

        render_url = self.BASE_URL + url
        headers = {
            'X-Prerender-Token': self.params['token'],
        }
        if request:
            headers.update({'User-Agent': request.META.get('HTTP_USER_AGENT')})
        response = self.session.get(render_url, headers=headers, allow_redirects=False)
        assert response.status_code < 500
        djresponse = self.build_django_response_from_requests_response(response)
        if url.endswith('/404'):
            djresponse.status_code = 404
        return djresponse


    def update_url(self, url=None, regex=None):
        if not url and not regex:
            raise ValueError("Neither a url or regex was provided to update_url.")

        headers = {
            'X-Prerender-Token': self.token,
            'Content-Type': 'application/json',
        }
        data = {
            'prerenderToken': self.params['token'],
        }
        if url:
            data["url"] = url
        if regex:
            data["regex"] = regex

        response = self.session.post(self.RECACHE_URL, headers=headers, data=data)
        return response.status_code < 500


def get_prerender_backend(prerender_backend_name, params={}):
    if prerender_backend_name == 'prerender.io' and params.get('token', None):
        return PrerenderBackend(params)
    if params.get('url', None):
        return PuppeteerSelfHostedBackend(params)
    return None
