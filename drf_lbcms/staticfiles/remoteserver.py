import subprocess
import tempfile
import os

from django.core.files.storage import FileSystemStorage
from django.conf import settings as djsettings


class NotAllowed(Exception):
    pass





class StaticFilesStorageForRemoteServer(FileSystemStorage):
    def __init__(self, location=None, base_url=None, *args, **kwargs):
        if location is None:
            location = djsettings.STATIC_ROOT
        if base_url is None:
            base_url = djsettings.STATIC_URL
        super(StaticFilesStorageForRemoteServer, self).__init__(location, base_url, *args, **kwargs)
        self.ssh_hostname = getattr(djsettings, 'STATICFILES_STORAGE_SSH_HOST', None) or getattr(djsettings, 'HOSTNAME')
        self.ssh_indent_key = getattr(djsettings, 'STATICFILES_STORAGE_SSH_KEY', None)


    def _open(self, name, mode='rb'):
        raise NotAllowed


    def _run_scp(self, origin=None, destination=None, on_fail=None):
        args = ['scp']
        if self.ssh_indent_key:
            args += ['-i', self.ssh_indent_key]
        args += [
            origin,
            "root@{hostname}:{destination}".format(
                hostname=self.ssh_hostname,
                destination=destination
            )
        ]
        #print('-- ', ' '.join(args))
        proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()
        err = err.decode('utf-8')
        #print(destination, out, err)
        if "Permission denied" in err:
            if on_fail:
                on_fail()
            raise Exception(err)


    def _run_ssh_command(self, command='', on_fail=None):
        args = ['ssh']
        if self.ssh_indent_key:
            args += ['-i', self.ssh_indent_key]
        args = args + [
            '-oStrictHostKeyChecking=accept-new',
            'root@{hostname}'.format(hostname=self.ssh_hostname),
            command
        ]
        #print('-- ', ' '.join(args))
        proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()
        err = err.decode('utf-8')
        #print(out, err)
        if "Permission denied" in err:
            if on_fail:
                on_fail()
            raise Exception(err)



    def _save(self, name, content):
        full_path = self.path(name)

        if hasattr(content, 'temporary_file_path'):
            origin = content.temporary_file_path()
            delete_tmp = lambda : None
        elif hasattr(content, 'file') and hasattr(content.file, 'name') and os.path.exists(content.file.name):
            origin = content.file.name
            delete_tmp = lambda : None
        else:
            _file = None
            for chunk in content.chunks():
                if _file is None:
                    mode = 'wb' if isinstance(chunk, bytes) else 'wt'
                    _file = tempfile.NamedTemporaryFile(mode)
                _file.write(chunk)
            origin = _file.name
            def delete_tmp():
                _file.close()

        # создаем папку если нет
        self._run_ssh_command(
            command='mkdir -p {dirname}; chown -R www-data {dirname}'.format(dirname=os.path.dirname(full_path)),
            on_fail=delete_tmp
        )
        # копируем файл
        self._run_scp(origin=origin, destination=full_path, on_fail=delete_tmp)
        # устанавливаем верное разрешение
        self._run_ssh_command(
            command='chown www-data {filename}'.format(filename=full_path),
            on_fail=delete_tmp
        )
        # если все ок, то закрываем файл
        delete_tmp()
