from rest_framework import exceptions
from django.core.exceptions import FieldDoesNotExist
import json
from drfs.db.validators import EmbeddedValidator
from rest_framework import serializers

from drf_lbcms.helpers import get_site_settings
from drf_lbcms.helpers.l10nFile import get_options_from_request_data
from drf_lbcms.models import L10nFile, L10nFileLocal




class L10nFileBaseMixin(object):

    def get_model_name(self):
        return self.queryset.model.__name__


    def get_field_class(self, name):
        try:
            return self.queryset.model._meta.get_field(name).related_model
        except FieldDoesNotExist:
            return None


    def check_l10nfile_field_exists(self, field_name=None):
        model_class = self.queryset.model
        l10n_model = self.get_field_class(field_name)
        if not l10n_model or l10n_model not in [L10nFile, L10nFileLocal]:
            raise exceptions.NotAcceptable(detail="Unknown L10nFile field \""+field_name+"\" for model \""+model_class._meta.object_name+"\".")
        return True

    def get_title_description_from_request_data(self, request_data):
        validator = EmbeddedValidator('L10nBaseString')
        result = {}
        for field in ['title', 'description', 'copyright']:
            value = request_data.get(field, None)
            if isinstance(value, str):
                try:
                    value = json.loads(value)
                except:
                    continue
            if not isinstance(value, dict):
                continue
            if field == 'copyright':
                result[field] = value
                continue
            try:
                result[field] = validator.validate_data(value)
            except Exception as e:
                raise serializers.ValidationError(field + ": " + str(e))
        return result


    def get_options_from_request_data(self, request_data):
        options, inmemory_file = get_options_from_request_data(request_data)
        if not inmemory_file or options.get('saveToLocalStorage', False):
            return options, inmemory_file

        settings = get_site_settings(request=self.request)
        if not settings:
            return options, inmemory_file

        watermark_conf = getattr(settings, 'watermark', {}) or {}
        if watermark_conf.get('autoAddWatermark', False):
            if not settings.watermarkImg:
                return options, inmemory_file
            model_name = self.get_model_name()
            model_names_for_wm = watermark_conf.get('models', {}) or {}
            if model_names_for_wm.get(model_name, False):
                options['watermark'] = watermark_conf
                options['watermark']['img'] = settings.watermarkImg.file_data

        return options, inmemory_file
