# -*- coding: utf-8 -*-
import inspect
from rest_framework.response import Response
from rest_framework import exceptions
from drf_lbcms.helpers import response, field_name_to_title
from drf_lbcms.helpers.l10n import django_choices_field_to_text
from django.conf import settings
from drfs.decorators import action

from drf_lbcms.helpers.django import get_base_dir_as_string



class ExportMixin(object):
    export_error_msgs = {
        'invalid_format': "Invalid fileFormat. Allowed are 'xlsx', 'xls', 'csv'. Got '{fileFormat}'"
    }
    export_max_items = 0

    def perform_export(self, queryset, preferred_lang='en', state_name=None, **kwargs):
        # функция должна возвращать
        # массив списков в котором лежат данные
        # и словарь formatters для drf_lbcms.helpers.response.csv_to_response фукнции
        header = []
        fields = []
        choices_fields = {}
        # демо-функция экспорта
        for field in queryset.model._meta.get_fields():
            fields.append(field.name)
            header.append(field_name_to_title(field.name))
            # если поле у нас с choices, то нужен перевод в строку этого поля
            if getattr(field, 'choices', None):
                choices_fields[field.name] = field
        # для больших бд лучше проходить итератором, а не тупо выгребать все из бд
        def rows_data_generator():
            yield header
            for item in queryset.iterator():
                row = []
                for field in fields:
                    value = getattr(item, field, None)
                    if choices_fields.get(field, None):
                        value = django_choices_field_to_text(
                            choices_fields[field],
                            getattr(item, field, None),
                            preferred_lang=preferred_lang
                        )
                    row.append(value)
                yield row
        return rows_data_generator(), {}


    def perform_export_to_email(self, queryset, file_name=None, emails=[], preferred_lang='en', state_name=None, **kwargs):
        from drf_lbcms.tasks import export_file_to_email
        # находим полный путь к файлу в котором находится реальный метод self.export
        BASE_DIR = get_base_dir_as_string()
        full_path_to_method = [
            ch
            for ch in inspect.getfile(self.perform_export).replace(BASE_DIR, '').replace('.py', '').split('/')
            if ch
        ]
        mixin_class = inspect.getmodule(self.perform_export)
        for prop in dir(mixin_class):
            if 'Mixin' in prop:
                cls = getattr(mixin_class, prop)
                if getattr(cls, 'perform_export', None):
                    full_path_to_method.append(prop)
                    break
        full_path_to_method.append('perform_export')
        # отправляем celery задание на экспорт.
        # т.к. нельзя передать queryset в таск, то передаем список id-шников объектов
        export_file_to_email.delay(
            '.'.join(full_path_to_method),
            model_name=queryset.model.__name__,
            pks=list(queryset.values_list('id', flat=True)),
            preferred_lang=preferred_lang,
            file_name=file_name,
            state_name=state_name,
            request={
                'user': self.request.user.id,
                'query_params': self.request.query_params or {}
            }
        )
        return None


    @action(methods=['get'], url_path=r"export/(?P<fileFormat>[^/]+)", detail=False)
    def export(self, request, fileFormat='csv', **kwargs):
        preferredLang = request.query_params.get('preferredLang', None) or 'en'
        fileName = request.query_params.get('fileName', None) or 'Site orders'
        stateName = request.query_params.get('stateName', '')
        fileFormat = fileFormat.lower()
        if fileFormat == 'xlsx':
            fileFormat = 'xls'
        if fileFormat not in ['csv', 'xls']:
            raise exceptions.NotAcceptable(
                self.export_error_msgs['invalid_format'].format(
                    fileFormat=fileFormat
                )
            )

        queryset = self.filter_queryset(self.get_queryset())

        if self.export_max_items and queryset.count() > self.export_max_items:
            # отправляем на почту
            return Response(
                self.perform_export_to_email(
                    queryset,
                    file_name=fileName + "." + fileFormat,
                    emails=[self.request.user.email],
                    preferred_lang=preferredLang,
                    state_name=stateName
                )
            )
        # отправляем обычным методом в Response
        rows_data, formatters = self.perform_export(
            queryset,
            preferred_lang=preferredLang,
            state_name=stateName
        )
        return response.csv_to_response(
            rows_data=rows_data,
            file_name=fileName + "." + fileFormat,
            formatters=formatters
        )
