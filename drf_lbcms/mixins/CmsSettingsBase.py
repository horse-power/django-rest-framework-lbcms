# -*- coding: utf-8 -*-
import csv, datetime, six, os, uuid
from django.apps import apps
from django.core.files.storage import DefaultStorage
from django.conf import settings as djsettings
from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser

from drfs.decorators import action

from ..helpers.l10nFile import get_options_from_request_data





class CmsSettingsBase(object):

    def get_serializer_class(self):
        if not self.request or not self.request.user or not self.request.user.is_superuser:
            if hasattr(self, 'serializer_class_for_non_admin'):
                return self.serializer_class_for_non_admin
        return self.serializer_class


    @action(methods=['put'], detail=False)
    def send_test_notify(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise exceptions.NotAuthenticated()
        if not request.user.is_superuser:
            raise exceptions.NotAuthenticated(detail="Only admin can use this endpoint")

        notifyType = request.query_params.get('notifyType', None)
        to = request.query_params.get('to', None)
        if notifyType not in ['email', 'sms']:
            raise exceptions.NotAcceptable(detail="Notify type is not allowed. Use 'email' or 'sms'")
        if not to:
            raise exceptions.NotAcceptable(detail="Please provide 'to' query parameter")


        class FakeSettings(object):
            serverIntegrations = request.data.get('serverIntegrations', None) or {}

        fake_settings = FakeSettings()
        if notifyType == 'email':
            from drf_lbcms.helpers.email import get_connection
            from django.core.mail import EmailMultiAlternatives

            try:
                connection = get_connection(fake_settings, fail_silently=False)
                host = request.get_host().split(':')[0]
                if host == 'localhost' and getattr(djsettings, 'HOSTNAME', None):
                    host = djsettings.HOSTNAME
                djemail = EmailMultiAlternatives(
                    subject='Test email with backend "%s"' % fake_settings.serverIntegrations.get('emailBackend', 'noname'),
                    body='Hello <b style="color: red;">world</b>!',
                    from_email='noreply@' + host,
                    to=[to],
                    connection=connection
                )
                djemail.attach_alternative('Hello <b style="color: red;">world</b>!', "text/html")
                djemail.attach(
                    'Test file.txt',
                    'Some text line',
                    'text/plain'
                )
                djemail.send()
            except Exception as e:
                text = str(e)
                if text.startswith('(') and text.endswith(')'):
                    text = text.split("'")[1:]
                    text = "'".join(text).split("'")[:-1]
                    text = "'".join(text)
                    text = ''.join(text.split('5.7.8 '))
                return Response({'error': ' '.join(text.split('\\n'))}, 500)

        if notifyType == 'sms':
            from drf_lbcms.helpers.sms import SmsApiWrapper

            swrapper = SmsApiWrapper({
                'username': fake_settings.serverIntegrations.get('smsapicom', {}).get('username', None),
                'password': fake_settings.serverIntegrations.get('smsapicom', {}).get('password', None)
            })
            swrapper.set_default_sender_name(fake_settings.serverIntegrations.get('smsapicom', {}).get('senderName', None))
            error = swrapper.send_sms(to, 'Hello world!')
            if error:
                return Response(error, 500)

        return Response()



    @action(methods=['get'], detail=False)
    def check_dump_db_and_files_is_possible(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise exceptions.NotAuthenticated()
        if not request.user.is_superuser:
            raise exceptions.NotAuthenticated(detail="Only admin can use this endpoint")
        from drf_lbcms.management.commands.drf_lbcms_dump_db_and_files import Command

        c = Command()
        dump_args = [
            arg
            for arg in (request.query_params.get('args', None) or '').split(',') if arg in ['db', 'files']
        ]
        return Response(c.is_dump_possible(*dump_args))



    @action(methods=['get'], detail=False)
    def dump_db_and_files(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise exceptions.NotAuthenticated()
        if not request.user.is_superuser:
            raise exceptions.NotAuthenticated(detail="Only admin can use this endpoint")
        dump_args = [
            arg
            for arg in (request.query_params.get('args', None) or '').split(',') if arg in ['db', 'files']
        ]

        from drf_lbcms.management.commands.drf_lbcms_dump_db_and_files import Command

        c = Command()
        is_dump_possible = c.is_dump_possible(*dump_args)
        email = request.query_params.get('sendToEmail', None)

        if not is_dump_possible['isPossible']:
            raise exceptions.NotAcceptable(detail="Dump is not allowed")
        if is_dump_possible['emailRequired'] and not email:
            raise exceptions.NotAcceptable(detail="sendToEmail parameter required")

        if email:
            from drf_lbcms.tasks import dump_db_and_files_to_email
            dump_db_and_files_to_email.delay(
                dump_args=dump_args,
                email=email,
                preferred_lang=request.query_params.get('preferredLang', None) or 'en'
            )
            return Response({})
        response = c.handle(*dump_args, as_http_response=True)
        return response



    @action(methods=['put'], detail=False, parser_classes=[MultiPartParser])
    def restore_db_and_files(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise exceptions.NotAuthenticated()
        if not request.user.is_superuser:
            raise exceptions.NotAuthenticated(detail="Only admin can use this endpoint")
        from drf_lbcms.helpers.l10nFile import get_options_from_request_data

        options, ffile = get_options_from_request_data(request.data)
        if not ffile:
            raise exceptions.NotAcceptable(detail="Empty file in request data")
        if ffile.content_type != 'application/x-7z-compressed':
            raise exceptions.NotAcceptable(detail="Unknown file type. Allowed only 7zip archives")

        from drf_lbcms.management.commands.drf_lbcms_restore_db_and_files import Command

        c = Command()
        c.handle(in_memory_file=ffile)
        return Response({})


    @action(methods=['get'], detail=False)
    def server_info(self, request, *args, **kwargs):
        email_templates = []
        for app_name,conf in apps.app_configs.items():
            path = os.path.join(conf.path, 'templates', 'drf_lbcms', 'email')
            if os.path.exists(path) and os.path.isdir(path):
                for name in os.listdir(path):
                    if os.path.exists(os.path.join(path, name, 'mail.html')):
                        email_templates.append(name)
        # TODO: sites framework!
        return Response({
            'hostname': djsettings.HOSTNAME,
            'emailTemplates':[
                {'value': name, 'description': {'en': name}}
                for name in set(email_templates)
            ]
        })

    @action(methods=['put'], detail=False, parser_classes=[MultiPartParser])
    def parse_file(self, request, *args, **kwargs):
        from drf_lbcms.helpers.l10nFile import get_options_from_request_data

        options, ffile = get_options_from_request_data(request.data)
        if not ffile:
            raise exceptions.NotAcceptable(detail="Empty file in request data")

        data = None
        if ffile.content_type == 'text/csv':
            if six.PY2:
                reader = csv.reader(ffile)
            else:
                from io import StringIO
                csvf = StringIO(ffile.read().decode())
                reader = csv.reader(csvf)
            data = {
                "sheets": [{
                    "name": "default",
                    "rows": list(reader)
                }]
            }
        if ffile.content_type in ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']:
            import xlrd


            def cell_to_value(cell):
                if cell.ctype == xlrd.XL_CELL_EMPTY:
                    return None
                if cell.ctype == xlrd.XL_CELL_TEXT:
                    return cell.value
                if cell.ctype == xlrd.XL_CELL_NUMBER:
                    return float(cell.value)
                if cell.ctype == xlrd.XL_CELL_DATE:
                    d = xlrd.xldate_as_tuple(cell.value, reader.datemode)
                    if d[0] == 0 and d[1] == 0 and d[2] == 0:
                        # time field
                        return datetime.time(*d[3:])
                    # datetime field
                    return datetime.datetime(*d)
                if cell.ctype == xlrd.XL_CELL_BOOLEAN:
                    return bool(cell.value)
                if cell.ctype == xlrd.XL_CELL_BLANK:
                    return ''
                return cell.value

            reader = xlrd.open_workbook(file_contents=ffile.read())
            data = {"sheets": []}
            for sheetname in reader.sheet_names():
                sheet = reader.sheet_by_name(sheetname)
                data['sheets'].append({
                    "name": sheetname,
                    "rows": [
                        [
                            cell_to_value(cell)
                            for cell in row
                        ]
                        for row in sheet.get_rows()
                    ]
                })

        return Response({
            'contentType': ffile.content_type,
            'data': data
        })




    @action(methods=['get'], detail=False)
    def translate_text(self, request, *args, **kwargs):
        api_key = request.query_params.get('key', None)
        if not api_key:
            from drf_lbcms.helpers import get_site_settings
            settings = get_site_settings(request=request)
            if settings.serverIntegrations.get('useTranslations', False):
                # пока поддерживается только google translate
                api_key = settings.serverIntegrations.get('googleTranslate', {}).get('apiKey', None)
        if not api_key:
            raise exceptions.NotAcceptable(detail="Please, provide api key for google translate")
        if not request.query_params.get('source', None) or not request.query_params.get('target', None):
            raise exceptions.NotAcceptable(detail="Please, provide target and source languages")

        # pip install google-api-python-client
        from googleapiclient.discovery import build

        service = build(
            'translate',
            'v2',
            developerKey=api_key
        )
        r = service.translations().list(
          source=request.query_params['source'],
          target=request.query_params['target'],
          q=request.query_params.getlist('q', None) or [],
          format='html'
        )
        r.headers['referer'] = request.build_absolute_uri().split('?')[0]
        response = r.execute()

        return Response({
            'data': response
        })


    @action(methods=['post'], detail=False, url_path='file_for_html_editors/(?P<provider>.*)/upload', parser_classes=[MultiPartParser])
    def upload_file_for_html_editors(self, request, *args, provider=None, **kwargs):
        KNOWN_PROVIDERS = ['ckeditor4', 'ckeditor5']

        if provider not in KNOWN_PROVIDERS:
            raise exceptions.NotAcceptable(detail="Unknown provider. Use one of: %s" % ', '.join(KNOWN_PROVIDERS))

        options, inmemory_file = get_options_from_request_data(request.data)
        if not inmemory_file:
            raise exceptions.NotAcceptable(detail="No file found in request data")

        ext = os.path.splitext(inmemory_file.name)[-1]
        new_name = str(uuid.uuid1())
        storage = DefaultStorage()
        if request.user.is_authenticated:
            new_name = "%s/%s" % (request.user.id, new_name)
        new_name = new_name + ext
        storage._save(new_name, inmemory_file)

        url = storage.url(new_name)
        if url[0] == '/':
            # local
            url = request.build_absolute_uri(url)

        if provider == 'ckeditor4':
            # https://ckeditor.com/docs/ckeditor4/latest/guide/dev_file_upload.html
            return Response({
                 "uploaded": 1,
                 "fileName": inmemory_file.name,
                 "url": url
            })
        if provider == 'ckeditor5':
            # https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/simple-upload-adapter.html
            # доки врут. ckeditor5 поддерживает только большой формат ответа
            return Response({
                "urls": {
                    "default": url
                }
            })
        return Response()


    @action(methods=['post'], detail=True, url_path='brand/(?P<destination>.*)/upload', parser_classes=[MultiPartParser])
    def upload_file_for_brand(self, request, *args, destination=None, **kwargs):
        options, inmemory_file = get_options_from_request_data(request.data)
        if destination == 'logoAlt':
            destination = 'logo-alt'
        if not inmemory_file:
            raise exceptions.NotAcceptable(detail="No file found in request data")
        if destination not in ['favicon', 'logo', 'logo-alt']:
            raise exceptions.NotAcceptable(detail="Invalid destination")

        from drf_lbcms.helpers.l10nFile import resize_and_crop_img
        from django.core.files.images import ImageFile
        from PIL import Image
        from io import BytesIO
        
        # https://stackoverflow.com/questions/76616042/attributeerror-module-pil-image-has-no-attribute-antialias
        try:
            ANTIALIAS = Image.LANCZOS
        except:
            # deprecated old versions of PIL
            ANTIALIAS = Image.ANTIALIAS

        # TODO: sites framework!
        storage = DefaultStorage()

        if destination == 'favicon':
            try:
                storage.delete('client/brand/favicon.32x32.png')
            except:
                pass
            resize_and_crop_img(
                Image.open(inmemory_file),
                'client/brand/favicon.32x32.png',
                [32,32],
                format='PNG'
            )
            url = storage.url('client/brand/favicon.32x32.png')
        if destination == 'logo':
            img = Image.open(inmemory_file)
            img.thumbnail(
                (300, 150),
                ANTIALIAS
            )
            try:
                storage.delete('client/brand/logo.min.png')
            except:
                pass
            img_buffer = BytesIO()
            img.save(img_buffer, format='PNG')
            storage._save('client/brand/logo.min.png', ImageFile(img_buffer))
            url = storage.url('client/brand/logo.min.png')
        if destination == 'logo-alt':
            img = Image.open(inmemory_file)
            img.thumbnail(
                (300, 150),
                ANTIALIAS
            )
            try:
                storage.delete('client/brand/logo.alt-min.png')
            except:
                pass
            img_buffer = BytesIO()
            img.save(img_buffer, format='PNG')
            storage._save('client/brand/logo.alt-min.png', ImageFile(img_buffer))
            url = storage.url('client/brand/logo.alt-min.png')
        if url[0] == '/':
            # local
            url = request.build_absolute_uri(url)
        return Response({
            'url': url
        })
