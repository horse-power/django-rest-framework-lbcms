# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework import exceptions

from drfs.decorators import action
from drf_lbcms.models import CmsPdfData
from django.http.response import HttpResponse
from drf_lbcms.pdf_render.render import HtmlRender



def to_int(value, default_value):
    if not value:
        return default_value
    try:
        return int(value)
    except:
        pass
    return default_value



class HtmlGeneratorMixin(object):
    htmlgen_error_msgs = {
        'empty_identifier': "Please provide identifier query param for html generation",
        'identifier_doesnt_exist': "No such identifier '{identifier}' for 'CmsPdfData' model"
    }


    def get_render_printable_html_context(self, **kwargs):
        return kwargs


    @action(methods=['get'], url_path=r"render_printable_html/(?P<identifier>[^/]+)", detail=True)
    def render_printable_html(self, *args, **kwargs):
        identifier = kwargs.get('identifier', None)
        if not identifier:
            raise exceptions.NotAcceptable(self.htmlgen_error_msgs['empty_identifier'])
        try:
            pdf_data = CmsPdfData.objects.get(identifier=identifier)
        except CmsPdfData.DoesNotExist:
            raise exceptions.NotAcceptable(
                self.htmlgen_error_msgs['identifier_doesnt_exist'].format(
                    identifier=identifier
                )
            )

        copy_count = self.request.query_params.get('copyCount', None)
        preferred_lang = getattr(self.request.user, 'preferredLang', 'en') or 'en'
        if self.request.query_params.get('preferredLang', None):
            preferred_lang = self.request.query_params['preferredLang']

        context = {
            'user': self.request.user,
            'instance': self.get_object(),
            'identifier': identifier,
            'autoCloseDocument': True,
            'autoPrintDocument': True
        }
        if self.request.query_params.get('autoCloseDocument', None) in [False, 'false', '0']:
            context['autoCloseDocument'] = False
        if self.request.query_params.get('autoPrintDocument', None) in [False, 'false', '0']:
            context['autoPrintDocument'] = False

        gen = HtmlRender(
            html_data=pdf_data,
            context=self.get_render_printable_html_context(**context),
            preferred_lang=preferred_lang
        )
        return HttpResponse(
            gen.render_to_buffer(copy_count=to_int(copy_count, 1) or 1)
        )
