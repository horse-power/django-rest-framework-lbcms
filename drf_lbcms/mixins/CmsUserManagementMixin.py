# -*- coding: utf-8 -*-
from django.contrib.auth.hashers import make_password
from rest_framework.response import Response
from rest_framework import serializers as rest_serializers
from rest_framework import exceptions
from django.conf import settings as djsettings
from datetime import datetime, timedelta
from rest_framework.exceptions import ParseError, NotAcceptable
import string
from random import randint, choice as random_choice

from drf_lbcms.helpers.cms import notify_user, generate_default_notification_objs
from drf_lbcms.helpers.date import str_to_datetime
from drf_lbcms.models import CmsSmsNotification
from drfs import generate_model
from drfs.decorators import action


def add_to_serializer_allPermissions(base_serializer, additional_hidden_fields=[]):
    sfields = base_serializer.Meta.fields + ['allPermissions']
    for field in additional_hidden_fields:
        if field in sfields:
            sfields.remove(field)

    class CmsUserSerializerWithAllPermissions(base_serializer):
        allPermissions = rest_serializers.SerializerMethodField()
        def get_allPermissions(self, obj):
            if not obj.is_superuser:
                return obj.get_all_permissions()
            return []
        class Meta:
            ref_name = base_serializer.Meta.model._meta.verbose_name + 'WithAllPermissions'
            model = base_serializer.Meta.model
            fields = sfields
    return CmsUserSerializerWithAllPermissions



class IsUserTakenSerializer(rest_serializers.Serializer):
    username = rest_serializers.CharField(max_length=300, required=False)
    email = rest_serializers.CharField(max_length=300, required=False)





class CmsUserManagementMixin(object):
    management_errors = {
        'provide_sms_code_for_login': u"Please provide 'smsCode' field",
        'invalid_sms_code_for_login': u"Invalid sms code"
    }

    def perform_create(self, serializer, **kwargs):
        skipEmailSend = self.request.query_params.get('skipEmailSend', 'false') == 'true'
        password = kwargs.get('password', None) or self.request.data.get('password', '')
        kwargs['password'] = make_password(password)
        if 'is_active' not in kwargs:
            kwargs['is_active'] = True

        instance = serializer.save(**kwargs)
        if not skipEmailSend:
            notify_user(
                instance,
                identifier='cms.user.$new',
                context={'rawPassword': password, 'instance': instance}
            )
        instance.set_default_permission_group_from_user_role()
        return instance


    def perform_update(self, serializer, **kwargs):
        instance = serializer.save(**kwargs)
        instance.set_default_permission_group_from_user_role()
        return instance


    def get_serializer_class(self, full_serializer=False, additional_hidden_fields=[]):
        base_serializer = super(CmsUserManagementMixin, self).get_serializer_class()
        if not (full_serializer or (self.request and self.request.user and self.request.user.is_superuser)):
            if hasattr(self, 'user_serializer_class_for_non_admin'):
                base_serializer = self.user_serializer_class_for_non_admin
        return add_to_serializer_allPermissions(
            base_serializer,
            additional_hidden_fields=additional_hidden_fields
        )

    def get_user_serializer_class(self):
        return add_to_serializer_allPermissions(
            super(CmsUserManagementMixin, self).get_user_serializer_class(),
            additional_hidden_fields=['user_permissions', 'groups']
        )

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class(
            full_serializer=kwargs.pop('full_serializer', False),
            additional_hidden_fields=kwargs.pop('additional_hidden_fields', [])
        )
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)


    def perform_login(self, serializer):
        data = self.request.data
        user = serializer.validated_data['user']
        # проверяем, если требуется ли дополнительная защита логина
        if not getattr(user, 'loginProtection', None):
            return
        generate_default_notification_objs()
        if getattr(user, 'loginProtection', None) == 's':
            # защита по смс
            # проверяем можем ли мы вообще отослать смс с сервера
            if getattr(djsettings, 'DRF_LBCMS', None):
                if not djsettings.DRF_LBCMS.get('send_notify_sms', True):
                    return
            # проверяем, есть ли номера телефонов у пользователя
            phones = []
            for p in user.accountData.get('phones', None) or []:
                if p and (p.get('val', None) or p.get('value', None)) and p.get('type', None) == 'm':
                    phones.append(p)
            if not phones:
                # номеров телефона у него нет
                return

            # если у указан смс, то сверяемся
            if data.get('smsCode', None):
                now = datetime.now()
                smsProtection = (user.hiddenAccountData or {}).get('smsProtection', None) or {}
                sended = str_to_datetime(smsProtection.get('sended', None))
                smsCode = data['smsCode'].replace(' ', '').replace('-', '')
                if now-timedelta(minutes=3) <= sended <= now  and smsCode == smsProtection.get('smsCode', None):
                    # логин прошел успешно
                    user.hiddenAccountData['smsProtection'] = {}
                    user.save()
                    return
                # отправляем ошибку
                raise ParseError(detail={
                    'protection': 'sms',
                    'username': user.username,
                    'detail': self.management_errors['invalid_sms_code_for_login']
                })

            # мы должны отсылать пользователю смс, также в запросе к серверу пользователь
            # не предоставил smsCode. сверяемся, можем ли мы отправить ему смс с кодом
            CmsSettingsModel = generate_model('CmsSettings.json')
            settings = CmsSettingsModel.objects.all()[0]
            integrations_settings = getattr(settings, 'serverIntegrations', {}) or {}
            if not integrations_settings.get('useSmsNotifications', False):
                return
            # мы можем отослать смс. проверяем, есть ли нужный id уведомлений
            notifications = CmsSmsNotification.objects.filter(identifier='cms.user.$verificationCodeForLogin', send=True)
            if not notifications:
                return
            # генерируем код и отсылаем смс
            user.hiddenAccountData['smsProtection'] = {
                'smsCode': "".join(random_choice(string.digits) for x in range(randint(4, 4))),
                'sended': datetime.now()
            }
            user.save()
            notifications[0].send_user_notification(user, context={
                'user': user
            })
            # отсылаем пользователю специальную ошибку
            raise NotAcceptable(detail={
                'protection': 'sms',
                'username': user.username,
                'detail': self.management_errors['provide_sms_code_for_login']
            })



    def perform_reset_password(self, user, token):
        preferred_lang = getattr(user, 'preferredLang', None) or 'en'
        # сохраняем timestamp когда дали возможность сбрасывать пароль
        user.hiddenAccountData['resetPassword'] = datetime.now().isoformat()
        user.save()
        reset_password_href = "/{lang}/resetPassword/?access_token={token}&username={username}&control={control}".format(
            lang=preferred_lang,
            token=str(token),
            username=user.username,
            control=user.hiddenAccountData['resetPassword']
        )
        generate_default_notification_objs()
        notify_user(
            user,
            identifier='cms.user.$resetPassword',
            context={'resetPasswordHref': reset_password_href, 'instance': user}
        )

    def perform_set_password(self, user, new_password):
        control = self.request.query_params.get('control', None)
        if user.hiddenAccountData.get('resetPassword', None) == control and control:
            user.set_password(new_password)
            del user.hiddenAccountData['resetPassword']
            user.save()
        else:
            raise exceptions.NotAcceptable("Invalid 'control' value")


    @action(methods=['put'], detail=True)
    def force_set_password(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not request.data.get('password', None):
            return Response({}, 401)
        instance = self.get_object()
        allowed = request.user.is_superuser
        if hasattr(self, 'can_force_set_password'):
            allowed = self.can_force_set_password(request=request, user=instance)
        if allowed:
            instance.set_password(request.data['password'])
            instance.save()
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        return Response({}, 403)


    @action(methods=['get'], detail=False)
    def me(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return Response({}, 401)
        serializer = self.get_serializer(
            request.user,
            partial=True,
            full_serializer=True,
            additional_hidden_fields=['password', 'user_permissions', 'groups', 'auth_token', 'hiddenAccountData'],
        )
        return Response(serializer.data)


    def perform_update_me(self, serializer):
        return serializer.save()


    @action(methods=['put'], detail=False)
    def update_me(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return Response({}, 401)
        partial = request.GET.get('partialUpdate', False)
        serializer = self.get_serializer(
            request.user,
            data=self.request.data,
            partial=bool(partial),
            full_serializer=True,
            additional_hidden_fields=['username', 'password', 'user_permissions', 'groups', 'auth_token', 'hiddenAccountData']
        )
        serializer.is_valid(raise_exception=True)
        instance = self.perform_update_me(serializer)
        serializer = self.get_serializer(
            instance,
            partial=True,
            full_serializer=True,
            additional_hidden_fields=['password', 'user_permissions', 'groups', 'auth_token', 'hiddenAccountData']
        )
        return Response(serializer.data)


    @action(methods=['put'], detail=False)
    def is_user_taken(self, request, *args, **kwargs):
        serializer = IsUserTakenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = (serializer.data.get('username', None) or '').strip()
        email = (serializer.data.get('email', None) or '').strip()

        if not username and not email:
            return Response({'isTaken': False})
        queryset = super(CmsUserManagementMixin, self).filter_queryset(
            super(CmsUserManagementMixin, self).get_queryset(),
            **kwargs
        )
        if username:
            queryset = queryset.filter(username=username)
        if email:
            queryset = queryset.filter(email=email)
        return Response({
            'isTaken': queryset.exists()
        })
