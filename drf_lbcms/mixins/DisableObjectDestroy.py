# -*- coding: utf-8 -*-






class DisableObjectDestroy(object):
    def get_queryset(self, skip_exclude_by_isdeleted=False, **kwargs):
        queryset = super(DisableObjectDestroy, self).get_queryset()
        if skip_exclude_by_isdeleted:
            return queryset
        return queryset.exclude(isDeleted=True)


    def perform_destroy(self, instance):
        instance.isDeleted = True
        instance.save()
