from drf_lbcms.helpers import get_current_site
try:
    from django.contrib.sites.models import Site
except ImportError:
    Site = None


'''
    фильтруем только если есть нужное поле у модели И если этот пакет подключен к приложению
'''

if Site:
    class FilterQuerysetBySiteMixin(object):
        def get_queryset(self, *args, **kwargs):
            queryset = super(FilterQuerysetBySiteMixin, self).get_queryset(*args, **kwargs)
            try:
                queryset.model._meta.get_field('_site')
            except:
                return queryset
            # если сайт не нашли, то игнорим фильтрацию (для старых библиотек)
            site = get_current_site(self.request)
            if site is not None:
                return queryset.filter(_site=site)
            return queryset

        def perform_create(self, serializer):
            site = get_current_site(self.request)
            if site is not None:
                return serializer.save(_site=site)
            return serializer.save()

else:
    class FilterQuerysetBySiteMixin(object):
        pass
