# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework import exceptions
from rest_framework.parsers import MultiPartParser
from drfs.decorators import action
from django.core.exceptions import ObjectDoesNotExist

from .l10nFileBase import L10nFileBaseMixin





class L10nFileListMixin(L10nFileBaseMixin):

    def get_l10nfile_list_data(self, objects=None):
        from ..serializers import L10nFile as L10nFileSerializer
        ser = L10nFileSerializer(objects.order_by('id'), many=True)
        return ser.data


    @action(methods=['post'], url_path="fileList/(?P<forModelField>[^/.]+)/upload", detail=True, parser_classes=[MultiPartParser])
    def upload_file_to_list(self, request, *args, **kwargs):
        forModelField = kwargs.get('forModelField', None)
        self.check_l10nfile_field_exists(
            field_name=forModelField
        )
        instance = self.get_object()
        fileList = getattr(instance, forModelField)
        options, ffile = self.get_options_from_request_data(request.data)
        data = self.get_title_description_from_request_data(request.data)
        forIdInList = options.get('forIdInList', -1)

        if not ffile:
            raise exceptions.NotAcceptable(detail="Can't find field with file in formdata.")

        L10nFile = self.get_field_class(forModelField)
        meta = {
            'size': getattr(ffile, 'size', 0) or getattr(ffile, '_size', 0),
            'originalName': ffile._name,
            'type': ffile.content_type
        }

        try:
            l10nFile = fileList.get(id=forIdInList)
        except ObjectDoesNotExist:
            l10nFile = None
            forIdInList = -1

        if forIdInList < 0:
            l10nFile = L10nFile(
                file_data=ffile,
                meta_data=meta,
                title=data.get('title', None) or {},
                description=data.get('description', None) or {},
                copyright=data.get('copyright', None) or {}
            )
            l10nFile.save(options=options)
            fileList.add(l10nFile)
            instance.save()
        else:
            l10nFile.delete_file_data()
            l10nFile.file_data = ffile
            l10nFile.meta_data = meta
            for k,v in data.items():
                setattr(l10nFile, k, v)
            l10nFile.save(options=options)

        return Response(self.get_l10nfile_list_data(
            objects=fileList.all()
        ))



    @action(methods=['put'], url_path="fileList/(?P<forModelField>[^/.]+)/update", detail=True)
    def update_files_inside_list(self, request, *args, **kwargs):
        forModelField = kwargs.get('forModelField', None)
        self.check_l10nfile_field_exists(
            field_name=forModelField
        )
        instance = self.get_object()
        fileList = getattr(instance, forModelField)
        for obj_data in request.data:
            if obj_data.get('id', None) is None:
                continue
            try:
                l10nFile = fileList.get(id=obj_data['id'])
            except ObjectDoesNotExist:
                l10nFile = None
            obj_data = self.get_title_description_from_request_data(obj_data)
            for k,v in obj_data.items():
                setattr(l10nFile, k, v)
            if obj_data:
                l10nFile.save(ignore_processing=True)

        return Response(self.get_l10nfile_list_data(objects=fileList.all()))


    @action(methods=['patch'], url_path="fileList/(?P<forModelField>[^/.]+)/delete", detail=True)
    def delete_files_inside_list(self, request, *args, **kwargs):
        forModelField = kwargs.get('forModelField', None)
        self.check_l10nfile_field_exists(
            field_name=forModelField
        )
        instance = self.get_object()
        fileList = getattr(instance, forModelField)

        to_delete = [
            obj['id']
            for obj in request.data if obj.get('id', None)
        ]
        fileList.filter(id__in=to_delete).delete()
        return Response(self.get_l10nfile_list_data(
            objects=fileList.all()
        ))
