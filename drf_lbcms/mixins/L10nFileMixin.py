# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework import exceptions
from rest_framework.parsers import MultiPartParser
import json
from drfs.decorators import action

from .l10nFileBase import L10nFileBaseMixin






class L10nFileMixin(L10nFileBaseMixin):


    def get_l10nfile_data(self, obj=None):
        from ..serializers import L10nFile as L10nFileSerializer
        ser = L10nFileSerializer(obj)
        return ser.data


    @action(methods=['post'], url_path="file/(?P<forModelField>[^/.]+)/upload", detail=True, parser_classes=[MultiPartParser])
    def upload_file(self, request, *args, **kwargs):
        forModelField = kwargs.get('forModelField', None)
        self.check_l10nfile_field_exists(
            field_name=forModelField
        )
        instance = self.get_object()

        if request.data.get('linkTo', None):
            '''
                привязывание уже существующего файла из массива к другому полю
            '''
            try:
                linkTo = json.loads(request.data['linkTo'])
            except:
                raise exceptions.ValidationError("Invalid 'linkTo' data")
            self.check_l10nfile_field_exists(linkTo.get('field', None))
            if not linkTo.get('id', None):
                raise exceptions.ValidationError("Invalid 'linkTo' data")
            l10nFile = getattr(instance, linkTo['field']).filter(id=linkTo['id']).first()
            if l10nFile:
                # снимаем дубликат с файла
                l10nFile.id = None
                l10nFile.save(ignore_processing=True)
                # сохраняем файл
                setattr(instance, forModelField, l10nFile)
                instance.save()
            return Response(self.get_l10nfile_data(obj=getattr(instance, forModelField)))



        oldFfile = getattr(instance, forModelField, None)
        options, ffile = self.get_options_from_request_data(request.data)
        data = self.get_title_description_from_request_data(request.data)
        if not options and hasattr(self, 'l10nfile_default_options'):
            options = self.l10nfile_default_options.get(forModelField, None) or {}

        if not ffile:
            raise exceptions.NotAcceptable(detail="Can't find field with file in formdata.")


        L10nFile = self.get_field_class(forModelField)
        meta = {
            'size': getattr(ffile, 'size', 0) or getattr(ffile, '_size', 0),
            'originalName': ffile._name,
            'type': ffile.content_type
        }
        l10nFile = L10nFile(
            file_data=ffile,
            meta_data=meta,
            title=data.get('title', None) or {},
            description=data.get('description', None) or {},
            copyright=data.get('copyright', None) or {}
        )
        l10nFile.save(options=options)
        setattr(instance, forModelField, l10nFile)
        instance.save()
        if oldFfile:
            oldFfile.delete_file_data()
            oldFfile.delete()

        return Response(self.get_l10nfile_data(obj=l10nFile))



    @action(methods=['put'], url_path="file/(?P<forModelField>[^/.]+)/update", detail=True)
    def update_file(self, request, *args, **kwargs):
        forModelField = kwargs.get('forModelField', None)
        self.check_l10nfile_field_exists(
            field_name=forModelField
        )
        instance = self.get_object()
        l10nFile = getattr(instance, forModelField)
        if not l10nFile:
            return Response(None)

        data = self.get_title_description_from_request_data(request.data)
        for k,v in data.items():
            setattr(l10nFile, k, v)
        l10nFile.save(update_fields=['title', 'description', 'copyright'], ignore_processing=True)

        return Response(self.get_l10nfile_data(obj=l10nFile))


    @action(methods=['patch'], url_path="file/(?P<forModelField>[^/.]+)/delete", detail=True)
    def delete_file(self, request, *args, **kwargs):
        forModelField = kwargs.get('forModelField', None)
        self.check_l10nfile_field_exists(
            field_name=forModelField
        )
        instance = self.get_object()
        oldFfile = getattr(instance, forModelField)
        if oldFfile:
            setattr(instance, forModelField, None)
            instance.save()
            oldFfile.delete_file_data()
            oldFfile.delete()

        return Response(None, 204)
