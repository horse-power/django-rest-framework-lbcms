# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework import exceptions
from drfs.decorators import action
import urllib.parse

from drf_lbcms.models import CmsPdfData, CmsMailNotification
from drf_lbcms.pdf_render.render import PdfRender
from drf_lbcms.helpers.response import sanitize_filename
from drf_lbcms.helpers import get_current_site

from django.shortcuts import redirect



def to_int(value, default_value):
    if not value:
        return default_value
    try:
        return int(value)
    except:
        pass
    return default_value




class PdfGeneratorMixin(object):
    pdfgen_error_msgs = {
        'empty_identifier': "Please provide identifier query param for pdf generation",
        'identifier_doesnt_exist': "No such identifier '{identifier}' for 'CmsPdfData' model",
        'emailidentifier_doesnt_exist': "No such identifier '{identifier}' for 'CmsMailNotification' model"
    }


    def get_render_pdf_context(self, **kwargs):
        return kwargs


    @action(methods=['get'], url_path=r"render_pdf/(?P<identifier>.*)", trailing_slash='', detail=True)
    def render_pdf(self, request, *args, **kwargs):
        identifier = kwargs.get('identifier', None)
        if not identifier:
            raise exceptions.NotAcceptable(self.pdfgen_error_msgs['empty_identifier'])
        identifier = identifier.split('/')
        redirect_to_filename = False
        if len(identifier) == 1:
            redirect_to_filename = True
        elif len(identifier) == 2 and identifier[1] == '':
            redirect_to_filename = True
        identifier = identifier[0]
        try:
            pdf_data = CmsPdfData.objects.get(identifier=identifier)
        except CmsPdfData.DoesNotExist:
            raise exceptions.NotAcceptable(
                self.pdfgen_error_msgs['identifier_doesnt_exist'].format(
                    identifier=identifier
                )
            )

        copy_count = self.request.query_params.get('copyCount', None)
        auto_download = self.request.query_params.get('autoDownload', 'true') or 'true'
        preferred_lang = getattr(self.request.user, 'preferredLang', 'en') or 'en'
        if self.request.query_params.get('preferredLang', None):
            preferred_lang = self.request.query_params['preferredLang']

        context = {
            'user': self.request.user,
            'instance': self.get_object(),
            'identifier': identifier
        }
        gen = PdfRender(
            pdf_data=pdf_data,
            context=self.get_render_pdf_context(**context),
            preferred_lang=preferred_lang
        )
        if redirect_to_filename:
            filename = sanitize_filename(gen.get_filename())
            url = request.build_absolute_uri()
            parts = url.split('?')
            if parts[0][-1] == '/':
                url = parts[0] + filename
            else:
                url = parts[0] + '/' + filename
            if len(parts) > 1:
                url = url + '?' + parts[1]
            '''
            parts = url.split(identifier)
            url = parts[0] + identifier + "/" + filename
            if '?' in parts[-1]:
                url = url + '?' + parts[-1].split('?')[-1]
            '''
            return redirect(url)
        return gen.render_to_response(
            copy_count=to_int(copy_count, 1) or 1,
            auto_download=auto_download in ['true', 'True', '1']
        )


    def get_user_for_render_pdf_to_user_email(self, user):
        return user

    @action(methods=['get'], url_path=r"render_pdf_to_user_email/(?P<identifier>[^/]+)", detail=True)
    def render_pdf_to_user_email(self, request, *args, **kwargs):
        identifier = kwargs.get('identifier', None)
        emailIdentifier = request.query_params.get('emailIdentifier', identifier)
        if not identifier:
            raise exceptions.NotAcceptable(self.pdfgen_error_msgs['empty_identifier'])

        site = get_current_site(request)
        try:
            if site:
                pdf_data = CmsPdfData.objects.filter(_site=site).get(identifier=identifier)
            else:
                pdf_data = CmsPdfData.objects.get(identifier=identifier)
        except CmsPdfData.DoesNotExist:
            raise exceptions.NotAcceptable(
                self.pdfgen_error_msgs['identifier_doesnt_exist'].format(
                    identifier=identifier
                )
            )
        try:
            if site:
                mail_notify_instance = CmsMailNotification.objects.filter(_site=site).get(identifier=emailIdentifier)
            else:
                mail_notify_instance = CmsMailNotification.objects.get(identifier=emailIdentifier)
        except:
            raise exceptions.NotAcceptable(
                self.pdfgen_error_msgs['emailidentifier_doesnt_exist'].format(
                    identifier=emailIdentifier
                )
            )

        file_name = request.query_params.get('fileName', None) or 'file'
        if '.pdf' not in file_name:
            file_name = file_name + '.pdf'
        copy_count = self.request.query_params.get('copyCount', None)
        preferred_lang = getattr(request.user, 'preferredLang', 'en') or 'en'
        if request.query_params.get('preferredLang', None):
            preferred_lang = request.query_params['preferredLang']

        user = self.get_user_for_render_pdf_to_user_email(self.request.user)
        context = {
            'user': user,
            'instance': self.get_object()
        }

        gen = PdfRender(
            pdf_data=pdf_data,
            context=self.get_render_pdf_context(**context),
            preferred_lang=preferred_lang
        )
        mail_notify_instance.send_user_notification(
            user,
            context={
                'user': user,
                'instance': self.get_object()
            },
            preferred_lang=preferred_lang,
            attachments=[{
                'name': file_name,
                'content': gen.render_to_buffer(copy_count=to_int(copy_count, 1) or 1),
                'mime': 'application/pdf'
            }]
        )
        return Response()
