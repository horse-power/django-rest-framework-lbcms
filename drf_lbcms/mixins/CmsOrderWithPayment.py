# -*- coding: utf-8 -*-
from rest_framework.response import Response
from django.template import Template, Context

from django.urls import reverse

from drfs import generate_serializer, mixins
from drfs.decorators import action

try:
    from payments import RedirectNeeded
except:
    RedirectNeeded = None


def render_to_string(html, context={}):
    t = Template(html)
    c = Context(context)
    return t.render(c)



class CmsOrderWithPaymentMixin(object):

    @action(methods=['get'], detail=True)
    def get_payment_provider_html_form(self, request, pk=None):
        instance = self.get_object().payment
        data = {}
        try:
            form = instance.get_form(data=request.data or None)
            data['html'] = render_to_string(
                """
                <form action="{{ form.action }}" method="{{ form.method }}" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded">
                    {{ form.as_p }}
                    <input id="submitButtonForDjangoPayment"
                        type="submit"
                        value="{% verbatim %}{{BUTTON_NAME}}{% endverbatim %}"
                        class="{% verbatim %}{{BUTTON_CLASS}}{% endverbatim %}"
                    />
                </form>
                """,
                context={
                    'form': form,
                    'payment': instance
                }
            )
        except RedirectNeeded as redirect_to:
            data['redirectTo'] = reverse(str(redirect_to))
        return Response(data)



    @action(methods=['get'], detail=True)
    def get_payment_provider_status(self, request, pk=None):
        instance = self.get_object().payment
        return Response({'status': instance.status})



    def perform_regenerate_payment(self, order=None):
        if not order:
            return False
        payment = order.payment
        old_id = payment.id
        payment.id = None
        payment.save()
        payment.__class__.objects.filter(id=old_id).delete()
        order.payment = payment
        order.save(update_fields=['payment'])
        return True


    @action(methods=['put'], detail=True)
    def regenerate_payment(self, *args, **kwargs):
        instance = self.get_object()
        if not instance.payment:
            raise exceptions.NotAcceptable(detail="No paymnet for this order to regenerate")
        result = self.perform_regenerate_payment_for_order(self, order=instance)
        return Response({'isRegenerated': bool(result)})
