# -*- coding: utf-8 -*-
from rest_framework import exceptions
from drf_lbcms.helpers import response, field_name_to_title
from drf_lbcms.helpers.l10n import django_choices_field_to_text
from drfs.decorators import action





class ExportByIdMixin(object):
    export_error_msgs = {
        'invalid_format': "Invalid fileFormat. Allowed are 'xlsx', 'xls', 'csv'. Got '{fileFormat}'"
    }

    def perform_export_object(self, instance, preferred_lang='en'):
        # функция должна возвращать
        # массив списков в котором лежат данные
        # и словарь formatters для drf_lbcms.helpers.response.csv_to_response фукнции
        rows_data = [[]]
        fields = []
        choices_fields = {}
        # демо-функция экспорта
        for field in instance.__class__._meta.get_fields():
            fields.append(field.name)
            rows_data[0].append(field_name_to_title(field.name))
            # если поле у нас с choices, то нужен перевод в строку этого поля
            if getattr(field, 'choices', None):
                choices_fields[field.name] = field
        rows_data.append([])
        for field in fields:
            value = getattr(instance, field, None)
            if choices_fields.get(field, None):
                value = django_choices_field_to_text(
                    choices_fields[field],
                    getattr(instance, field, None),
                    preferred_lang=preferred_lang
                )
            rows_data[-1].append(value)
        return rows_data, {}


    @action(methods=['get'], url_path=r"export/(?P<fileFormat>[^/]+)", detail=True)
    def export_by_id(self, request, pk=None, fileFormat='csv', **kwargs):
        preferredLang = request.query_params.get('preferredLang', None) or 'en'
        fileFormat = fileFormat.lower()
        if fileFormat == 'xlsx':
            fileFormat = 'xls'
        if fileFormat not in ['csv', 'xls']:
            raise exceptions.NotAcceptable(
                self.export_error_msgs['invalid_format'].format(
                    fileFormat=fileFormat
                )
            )

        instance = self.get_object()
        fileName = request.query_params.get('fileName', None) or instance.__class__.__name__
        rows_data, formatters = self.perform_export_object(
            instance,
            preferred_lang=preferredLang
        )
        return response.csv_to_response(
            rows_data=rows_data,
            file_name=fileName + "." + fileFormat,
            formatters=formatters
        )
