# -*- coding: utf-8 -*-
from django.db.models import fields as djfields
from django.db.models import Q

INTEGER_FIELDS = (
    djfields.AutoField,
    djfields.IntegerField,
    djfields.BigIntegerField,
    djfields.PositiveIntegerField,
    djfields.PositiveSmallIntegerField,
    djfields.SmallIntegerField
)
FLOAT_FIELDS = (
    djfields.FloatField,
    djfields.DecimalField
)



class QuickFilterMixin(object):
    def get_fields_for_quick_filter(self, model):
        definition = getattr(model, 'DRFS_MODEL_DEFINITION', None)
        field_names = []
        field_types = {}
        options = {}
        def get_names():
            names = []
            for field in model._meta.get_fields():
                # отключаем поиск по зависимым таблицам
                if getattr(field, 'related_model', None):
                    continue
                # если поле у нас с choices, то отключаем быстрый поиск по нему
                if not getattr(field, 'choices', None):
                    names.append(field.name)
            return names

        if definition:
            if definition.get('viewset', {}).get('quickFilter', {}).get('fields', None):
                field_names = definition['viewset']['quickFilter']['fields']
            if definition.get('viewset', {}).get('quickFilter', {}).get('additionalFields', None):
                field_names = get_names() + definition['viewset']['quickFilter']['additionalFields']
            if definition.get('viewset', {}).get('quickFilter', {}).get('forceDistinct', False):
                options['forceDistinct'] = True
        else:
            field_names = get_names()
        # теперь находим типы полей
        for field in model._meta.get_fields():
            if field.name not in field_names:
                continue
            if isinstance(field, INTEGER_FIELDS):
                field_types[field.name] = int
            elif isinstance(field, FLOAT_FIELDS):
                field_types[field.name] = float
        return field_names, field_types, options


    def get_queryset(self, *args, **kwargs):
        queryset = super(QuickFilterMixin, self).get_queryset(*args, **kwargs)
        search_term = self.request.query_params.get('quickFilter', None)
        if not search_term:
            return queryset

        field_names, field_types, options = self.get_fields_for_quick_filter(queryset.model)
        q = Q()
        for name in field_names:
            kw = {}
            fn = field_types.get(name, None)
            if not fn:
                kw[name+'__icontains'] = search_term
            else:
                try:
                    kw[name] = fn(search_term)
                except:
                    continue
            q |= Q(**kw)

        if options.get('forceDistinct', False) and field_names:
            return queryset.filter(q).distinct()
        return queryset.filter(q)
