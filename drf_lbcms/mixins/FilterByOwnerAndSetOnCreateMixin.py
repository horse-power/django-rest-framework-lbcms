# -*- coding: utf-8 -*-





class FilterByOwnerAndSetOnCreateMixin(object):
    def get_queryset(self, skip_filter_by_owner=False, **kwargs):
        queryset = super(FilterByOwnerAndSetOnCreateMixin, self).get_queryset()
        if skip_filter_by_owner:
            return queryset
        if self.request.user.is_authenticated:
            return queryset.filter(owner=self.request.user)
        return queryset.none()


    def perform_create(self, serializer, **kwargs):
        if self.request.user.is_authenticated:
            kwargs['owner'] = self.request.user
        else:
            kwargs['owner'] = None
        return serializer.save(**kwargs)
