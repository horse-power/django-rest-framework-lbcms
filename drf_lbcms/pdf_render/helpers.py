# -*- coding: utf-8 -*-
import os, base64, six
from django.conf import settings as djsettings
from django.template import Template
from drf_lbcms.helpers.html import Font


CURRENT_DIR = os.path.dirname(__file__)
RESOURCE_TEMPLATE_DIR = os.path.join(CURRENT_DIR, 'resources')

with open(os.path.join(RESOURCE_TEMPLATE_DIR, 'pdf', 'base.html')) as f:
    PDF_BASE_HTML = Template(f.read())

with open(os.path.join(RESOURCE_TEMPLATE_DIR, 'pdf', 'part.html')) as f:
    PDF_PART_HTML = Template(f.read())

with open(os.path.join(RESOURCE_TEMPLATE_DIR, 'html', 'base.html')) as f:
    HTML_BASE_HTML = Template(f.read())

with open(os.path.join(RESOURCE_TEMPLATE_DIR, 'html', 'part.html')) as f:
    HTML_PART_HTML = Template(f.read())

if hasattr(djsettings, 'DRF_LBCMS') and djsettings.DRF_LBCMS.get('use_wkhtml', False):
    with open(os.path.join(RESOURCE_TEMPLATE_DIR, 'bootstrap-wkhtml.css')) as f:
        BOOTSTRAP_CSS = f.read()
else:
    with open(os.path.join(RESOURCE_TEMPLATE_DIR, 'bootstrap-base.css')) as f:
        BOOTSTRAP_CSS = f.read()


def font_to_base64(filepath):
    ext = os.path.splitext(filepath)[-1].lower()
    with open(filepath) as f:
        data = f.read()
    prefix = ''
    if ext == '.ttf':
        prefix = 'data:font/truetype;base64,'
    elif ext == '.otf':
        prefix = 'data:font/opentype;base64,'
    elif ext == '.woff':
        prefix = 'data:application/x-font-woff;base64,'
    elif ext == '.woff2':
        prefix = 'data:application/font-woff2;base64,'
    else:
        return None
    return prefix + base64.b64encode(data)
    # return "'"+ prefix + base64.b64encode(data) + "'"




def wkhtml_to_pdf(html, *args, **kwargs):
    import tempfile, subprocess, os

    f, tmpfilename_in = tempfile.mkstemp()
    f, tmpfilename_header = tempfile.mkstemp()
    f, tmpfilename_footer = tempfile.mkstemp()
    f, tmpfilename_out = tempfile.mkstemp()
    os.remove(tmpfilename_in)
    os.remove(tmpfilename_header)
    os.remove(tmpfilename_footer)
    process_args = ['wkhtmltopdf', '--enable-local-file-access']

    tmpfilename_in += '.html'
    with open(tmpfilename_in, 'w') as f:
        f.write(html)

    # колонтитул-ы
    if kwargs.get('header', None):
        tmpfilename_header += '.html'
        with open(tmpfilename_header, 'w') as f:
            f.write(kwargs['header'])
        process_args.append('--header-html')
        process_args.append(tmpfilename_header)
        process_args.append('-T')
        process_args.append('%smm' % kwargs['headerHeight'])
    else:
        # размеры отступа, для отсутствующего header
        process_args.append('-T')
        process_args.append('10mm')
    if kwargs.get('footer', None):
        tmpfilename_footer += '.html'
        with open(tmpfilename_footer, 'w') as f:
            f.write(kwargs['footer'])
        process_args.append('--footer-html')
        process_args.append(tmpfilename_footer)
        process_args.append('-B')
        process_args.append('%smm' % kwargs['footerHeight'])
    else:
        # размеры отступа, для отсутствующего footer
        process_args.append('-B')
        process_args.append('10mm')

    # рендерим pdf
    process_args.append(tmpfilename_in)
    process_args.append(tmpfilename_out)
    p = subprocess.Popen(
        process_args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    out, err = p.communicate()
    # print out, err
    with open(tmpfilename_out, 'rb') as f:
        data = f.read()
    os.remove(tmpfilename_in)
    if kwargs.get('header', None):
        os.remove(tmpfilename_header)
    if kwargs.get('footer', None):
        os.remove(tmpfilename_footer)
    os.remove(tmpfilename_out)
    return data


def wkhtml_to_image(html, *args, **kwargs):
    import tempfile, subprocess, os

    f, tmpfilename_in = tempfile.mkstemp()
    f, tmpfilename_header = tempfile.mkstemp()
    f, tmpfilename_footer = tempfile.mkstemp()
    f, tmpfilename_out = tempfile.mkstemp()
    os.remove(tmpfilename_in)
    os.remove(tmpfilename_header)
    os.remove(tmpfilename_footer)
    process_args = ['wkhtmltoimage', '--enable-local-file-access']
    if kwargs.get('width', False):
        width = kwargs['width']
        if not isinstance(width, six.string_types) or not isinstance(width, six.text_type):
            width = str(width)
        process_args += [
            "--disable-smart-width",
            "--width",
            width
        ]

    tmpfilename_out += '.png'
    tmpfilename_in += '.html'
    with open(tmpfilename_in, 'w') as f:
        f.write(html)

    # колонтитул-ы
    '''
    if kwargs.get('header', None):
        tmpfilename_header += '.html'
        with open(tmpfilename_header, 'w') as f:
            f.write(kwargs['header'])
        process_args.append('--header-html')
        process_args.append(tmpfilename_header)
        process_args.append('-T')
        process_args.append('%smm' % kwargs['headerHeight'])
    else:
        # размеры отступа, для отсутствующего header
        process_args.append('-T')
        process_args.append('10mm')
    if kwargs.get('footer', None):
        tmpfilename_footer += '.html'
        with open(tmpfilename_footer, 'w') as f:
            f.write(kwargs['footer'])
        process_args.append('--footer-html')
        process_args.append(tmpfilename_footer)
        process_args.append('-B')
        process_args.append('%smm' % kwargs['footerHeight'])
    else:
        # размеры отступа, для отсутствующего footer
        process_args.append('-B')
        process_args.append('10mm')
    '''

    # рендерим png
    process_args.append(tmpfilename_in)
    process_args.append(tmpfilename_out)
    p = subprocess.Popen(
        process_args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    out, err = p.communicate()
    # print out, err
    with open(tmpfilename_out, 'rb') as f:
        data = f.read()
    os.remove(tmpfilename_in)
    if kwargs.get('header', None):
        os.remove(tmpfilename_header)
    if kwargs.get('footer', None):
        os.remove(tmpfilename_footer)
    os.remove(tmpfilename_out)
    return data


FONTS = {}

for font_name in os.listdir(os.path.join(RESOURCE_TEMPLATE_DIR, 'fonts')):
    _p = os.path.join(RESOURCE_TEMPLATE_DIR, 'fonts', font_name)
    if os.path.isdir(_p):
        font = Font(RESOURCE_TEMPLATE_DIR, name=font_name)
        FONTS[font_name] = font.getCss()
