# -*- coding: utf-8 -*-
from django.template import Context, Template, TemplateSyntaxError
from django.conf import settings as djsettings
from django.utils.safestring import mark_safe
import os, base64, six
from io import BytesIO
try:
    from PyPDF2 import PdfFileReader, PdfFileWriter
except ImportError:
    from pyPdf import PdfFileReader, PdfFileWriter

from drf_lbcms.helpers import get_site_settings
from drf_lbcms.helpers.l10n import l10n_filter
from drf_lbcms.helpers.response import file_to_response
from drf_lbcms.helpers.html import img_to_base64, unescape_html
from . import helpers






def l10ntext_to_template(textObj, lang):
    template = l10n_filter(textObj, lang)
    if not template:
        return None
    template = "{% language '"+lang+"' %}{% localize on %}" + img_to_base64(template) + "{% endlocalize %}{% endlanguage %}"
    try:
        template = Template(
            "{% load lbcms %}{% load l10n %}" + template
        )
    except TemplateSyntaxError:
        template = Template(
            unescape_html("{% load lbcms %}{% load l10n %}" + template)
        )
    return template



def html_to_pdf(html, *args, **kwargs):
    if hasattr(djsettings, 'DRF_LBCMS') and djsettings.DRF_LBCMS.get('use_wkhtml', False):
        # рендерим новым способом, который не настолько плохо генерит pdf
        return helpers.wkhtml_to_pdf(html, *args, **kwargs)
    # рендерим по-старому
    from easy_pdf.rendering import html_to_pdf as easy_html_to_pdf
    # если у нас есть колонтитул-ы, то нужно их отрендерить как обычный html
    if kwargs.get('header', None):
        html = kwargs.pop('header') + "<div class='clearfix'></div>" + html
        kwargs.pop('headerHeight')
    if kwargs.get('footer', None):
        html = html + "<div class='clearfix'></div>" + kwargs.pop('footer')
        kwargs.pop('footerHeight')
    return easy_html_to_pdf(html, *args, **kwargs)

def html_to_image(html, *args, **kwargs):
    if hasattr(djsettings, 'DRF_LBCMS') and djsettings.DRF_LBCMS.get('use_wkhtml', False):
        # рендерим новым способом, который не настолько плохо генерит pdf
        return helpers.wkhtml_to_image(html, *args, **kwargs)
    raise NotImplementedError


def html_to_html(html, *args, **kwargs):
    if kwargs.get('asPng', False):
        kwargs = kwargs.copy()
        kwargs['asPng'] = False
        body_start = html.split('<body>')[0]
        html = html.replace(body_start, '').split('</body>')[0]

        result = [body_start + "<body>"]
        if kwargs.get('width', False):
            result.append("<div style='margin: 0 auto; width: {width}px;'>".format(width=kwargs['width']))
        for frame in html.split('<div class="pagebreak">'):
            if frame.startswith("&nbsp;</div>") or frame.startswith("</div>"):
                frame = "<div style='display: none;'>" + frame
            if not frame.startswith('<body>'):
                frame = "<body>" + frame
            img = html_to_image(
                "{start}{frame}</body>".format(start=body_start, frame=frame),
                **kwargs
            )
            if six.PY2:
                img = base64.b64encode(img)
            else:
                img = base64.b64encode(img).decode('utf-8')
            result.append("<img src='data:image/png;base64,{img}' />".format(img=img))
        if kwargs.get('width', False):
            result.append("</div>")
        result.append("</body>")
        result = ''.join(result)
        return result.encode()


    if kwargs.get('header', None):
        iframe = """
            <iframe frameborder="0" style="width: 100%; padding: 0px; height: {minHeight}cm;"
                    class='html-header'
                    src="data:text/html;base64,{html}">
            </iframe>
        """.format(html=base64.b64encode(kwargs['header']), minHeight=kwargs['headerHeight'] / 10.0)
        html = html.split('<body>')
        html[0] += iframe
        html = '<body>'.join(html)
    if kwargs.get('footer', None):
        iframe = """
            <iframe frameborder="0" style="width: 100%; padding: 0px; height: {minHeight}cm;"
                    class='html-footer'
                    src="data:text/html;base64,{html}">
            </iframe>
        """.format(html=base64.b64encode(kwargs['header']), minHeight=kwargs['footerHeight'] / 10.0)
        html = html.split('</body>')
        html[-1] += iframe
        html = '</body>'.join(html)
    return html.encode()




class PdfData(object):
    title = {}
    content = {}
    margins = {}




class BaseRender(object):
    GENERATED_FILE_EXT = '.base'
    DEFAULT_PAGE_MARGIN = 0

    def __init__(self, settings=None, context=None, preferred_lang='en', **kwargs):
        self.context = context or {}
        self.template = l10ntext_to_template(
            settings.content,
            preferred_lang
        )
        if not self.template:
            self.template = Template("")
        if not settings.title:
            settings.title = {'en': 'File'}
        self.title = Template(l10n_filter(settings.title, preferred_lang))
        self.title = self.title.render(Context(context))
        self.header = None
        self.footer = None
        if hasattr(settings, 'runningTitles') and settings.runningTitles.get('isHeaderActive', False):
            self.header = l10ntext_to_template(
                settings.runningTitles.get('header', {}),
                preferred_lang
            )
            self.headerHeight = settings.runningTitles.get('headerHeight', 0)
        if hasattr(settings, 'runningTitles') and settings.runningTitles.get('isFooterActive', False):
            self.footer = l10ntext_to_template(
                settings.runningTitles.get('footer', {}),
                preferred_lang
            )
            self.footerHeight = settings.runningTitles.get('footerHeight', 0)
        self.fonts = getattr(settings, 'fonts', {})
        self.kwargs = kwargs

        if not isinstance(self.context, Context):
            self.context = Context(self.context)
        if not self.context.get('lang', None):
            self.context['lang'] = preferred_lang

        cmsSettings = self.context.get('cmsSettings', None) or self.context.get('settings', None)
        if not cmsSettings:
            self.context['settings'] = get_site_settings()
        else:
            self.context['settings'] = cmsSettings


        self.context['SITE_DOMAIN'] = self.context.get('SITE_DOMAIN', None) or getattr(djsettings, 'SITE_DOMAIN', None) or self.context.get('CMS_BASE_URL', None)
        self.context['margins'] = {
            'left': str(settings.margins.get('left', self.DEFAULT_PAGE_MARGIN)) + 'cm',
            'right': str(settings.margins.get('right', self.DEFAULT_PAGE_MARGIN)) + 'cm',
            'top': str(settings.margins.get('top', self.DEFAULT_PAGE_MARGIN)) + 'cm',
            'bottom': str(settings.margins.get('bottom', self.DEFAULT_PAGE_MARGIN)) + 'cm',
        }

    def get_filename(self, filename=None):
        if not filename:
            filename = self.title or 'File'
        ext = os.path.splitext(filename)[-1].lower()
        if ext != self.GENERATED_FILE_EXT:
            filename += self.GENERATED_FILE_EXT
        return filename


    def _render(self, html_template_name='base', **context):
        raise NotImplementedError

    def _render_to_buffer(self, copy=0, total_copy_count=1):
        raise NotImplementedError

    def render_to_buffer(self, copy_count=1):
        raise NotImplementedError


    def render_to_file(self, filepath, copy_count=1):
        buf = self.render_to_buffer(copy_count=copy_count)
        with open(filepath, 'wb') as f:
            f.write(buf)


    def render_to_response(self, filename=None, copy_count=1, auto_download=True):
        buf = self.render_to_buffer(copy_count=copy_count)
        if not filename:
            filename = self.title or 'File'
        ext = os.path.splitext(filename)[-1].lower()
        if ext != self.GENERATED_FILE_EXT:
            filename += self.GENERATED_FILE_EXT
        return file_to_response(file_data=buf, file_name=filename, as_bytes=True, auto_download=auto_download)





class PdfRender(BaseRender):
    GENERATED_FILE_EXT = '.pdf'
    DEFAULT_PAGE_MARGIN = 2.5

    def __init__(self, pdf_data=None, context=None, preferred_lang='en', **kwargs):
        super(PdfRender, self).__init__(settings=pdf_data, context=context, preferred_lang=preferred_lang, **kwargs)


    def _render(self, html_template_name='base', **context):
        pdf_fonts = ''
        last_font_name = ''
        for font_name, v in self.fonts.items():
            if v and helpers.FONTS.get(font_name, None):
                last_font_name = font_name
                pdf_fonts += helpers.FONTS[font_name]
        if not pdf_fonts:
            pdf_fonts = helpers.FONTS['Roboto']
            last_font_name = 'Roboto'

        context['BOOTSTRAP_CSS'] = helpers.BOOTSTRAP_CSS
        context['FONTS'] = helpers.FONTS
        context['PDF_FONTS'] = pdf_fonts
        context['LAST_FONT_NAME'] = last_font_name
        if html_template_name == 'base':
            html = helpers.PDF_BASE_HTML.render(Context(context))
        else:
            html = helpers.PDF_PART_HTML.render(Context(context))
        # теперь по 2 кругу проходимся по html, чтоб заменить заново картинки,
        # если они появились
        return img_to_base64(
            html,
            skip_external=hasattr(djsettings, 'DRF_LBCMS') and djsettings.DRF_LBCMS.get('use_wkhtml', False)
        )


    def _render_to_buffer(self, copy=0, total_copy_count=1):
        context = self.context
        context['COPY_INDEX'] = copy
        context['TOTAL_COPY_COUNT'] = total_copy_count or 1
        content = self.template.render(context)
        html = self._render(
            html_template_name='base',
            content=content,
            title=self.title,
            margins=self.context['margins']
        )
        kwargs = self.kwargs
        # колонтитул-ы
        if self.header and self.headerHeight:
            kwargs['header'] = self._render(
                html_template_name='part',
                content=self.header.render(self.context)
            )
            kwargs['headerHeight'] = self.headerHeight
        if self.footer and self.footerHeight:
            kwargs['footer'] = self._render(
                html_template_name='part',
                content=self.footer.render(self.context)
            )
            kwargs['footerHeight'] = self.footerHeight
        return html_to_pdf(
            html,
            'utf-8',
            **kwargs
        )


    def render_to_buffer(self, copy_count=1):
        copy_count = copy_count or 1
        if copy_count == 1:
            return self._render_to_buffer(copy=0)
        # рендерим каждую пдф-ку отдельно, после чего объединяем их
        writer = PdfFileWriter()
        for c in range(copy_count):
            reader = PdfFileReader(
                BytesIO(self._render_to_buffer(copy=c, total_copy_count=copy_count))
            )
            for n in range(reader.getNumPages()):
                writer.addPage(reader.getPage(n))
        buf = BytesIO()
        writer.write(buf)
        return buf.getvalue()




class HtmlRender(BaseRender):
    GENERATED_FILE_EXT = '.html'
    DEFAULT_PAGE_MARGIN = 0

    def __init__(self, html_data=None, context=None, preferred_lang='en', **kwargs):
        super(HtmlRender, self).__init__(settings=html_data, context=context, preferred_lang=preferred_lang, **kwargs)


    def _render(self, html_template_name='base', **context):
        context['BOOTSTRAP_CSS'] = helpers.BOOTSTRAP_CSS
        context['autoCloseDocument'] = self.context.get('autoCloseDocument', True)
        context['autoPrintDocument'] = self.context.get('autoPrintDocument', True)
        if html_template_name == 'base':
            html = helpers.HTML_BASE_HTML.render(Context(context))
        else:
            html = helpers.HTML_PART_HTML.render(Context(context))
        # теперь по 2 кругу проходимся по html, чтоб заменить заново картинки,
        # если они появились
        return img_to_base64(
            html,
            skip_external=True
        )


    def _render_to_buffer(self, copy=0, total_copy_count=1):
        context = self.context
        context['COPY_INDEX'] = copy
        context['TOTAL_COPY_COUNT'] = total_copy_count or 1
        content = self.template.render(context)
        html = self._render(
            html_template_name='base',
            content=content,
            title=self.title,
            margins=self.context['margins']
        )
        kwargs = self.kwargs
        # колонтитул-ы
        if self.header and self.headerHeight:
            kwargs['header'] = self._render(
                html_template_name='part',
                content=self.header.render(self.context)
            )
            kwargs['headerHeight'] = self.headerHeight
        if self.footer and self.footerHeight:
            kwargs['footer'] = self._render(
                html_template_name='part',
                content=self.footer.render(self.context)
            )
            kwargs['footerHeight'] = self.footerHeight
        return html_to_html(html, **kwargs)


    def render_to_buffer(self, copy_count=1):
        copy_count = copy_count or 1
        if copy_count == 1:
            return self._render_to_buffer(copy=0)

        iframes = []
        for c in range(copy_count):
            frame = self._render_to_buffer(copy=0).split("<body>")
            del frame[0]
            frame = '<body>'.join(frame).split("</body")
            del frame[-1]
            iframes.append('</body>'.join(frame))
            if c != copy_count-1:
                iframes.append('<div class="pagebreak"></div>')

        html = self._render(
            html_template_name='base',
            content=mark_safe("\n".join(iframes)),
            title=self.title,
            margins=self.context['margins']
        )
        return html_to_html(html, **self.kwargs)
