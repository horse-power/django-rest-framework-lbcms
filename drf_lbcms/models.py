# -*- coding: utf-8 -*-
import os
from django.db import models
from django.dispatch import receiver
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import models as auth_models
from django.conf import settings
from timezone_field import TimeZoneField

from drfs import generate_model
from drfs.db.fields import ListField, JSONField

from .helpers import l10nFile as l10nFileHelpers

#------------------------------------------------------------------------
#
#           files
#
#------------------------------------------------------------------------

class L10nFileAbstract(models.Model):
    meta_data = JSONField(default={})
    copyright = JSONField(default={})
    title = JSONField(default={})
    description = JSONField(default={})
    thumbs = JSONField(default=[])

    def resolve_file_uri(self, thumb_size=None, force_cms_storage=None, force_cms_db_bucket=None):
        from drf_lbcms.helpers.l10nFile import resolve_l10nfile_uri
        return resolve_l10nfile_uri(
            str(self.file_data),
            thumb_size=thumb_size,
            force_cms_storage=force_cms_storage,
            force_cms_db_bucket=force_cms_db_bucket
        )

    def resolve_file_orig_name(self):
        if not self.file_data:
            return ''
        return self.meta['originalName']

    def delete_file_data(self):
        if not self.file_data:
            return
        if getattr(settings, 'DRF_LBCMS', {}).get('disable_l10nfile_delete', False):
            return
        l10nFileHelpers.delete_l10nFile(self)
        try:
            self.file_data.delete(save=False)
        except AttributeError:
            pass

    def save(self, *args, **kwargs):
        options = kwargs.get('options', {})
        if 'options' in kwargs:
            del kwargs['options']

        if self.file_data and not kwargs.get('ignore_processing', False):
            l10nFileHelpers.process_l10nFile(self, options=options)
            super(L10nFileAbstract, self).save(*args, **kwargs)
        else:
            if 'ignore_processing' in kwargs:
                del kwargs['ignore_processing']
            super(L10nFileAbstract, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class L10nFile(L10nFileAbstract):
    file_data = models.FileField()


class L10nFileLocal(L10nFileAbstract):
    file_data = models.FileField(storage=FileSystemStorage(
        location=settings.MEDIA_ROOT
    ))


@receiver(models.signals.pre_delete, sender=L10nFile)
@receiver(models.signals.pre_delete, sender=L10nFileLocal)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `L10nFile` object is deleted.
    """
    if getattr(settings, 'DRF_LBCMS', {}).get('disable_l10nfile_delete', False):
        return
    instance.delete_file_data()




#------------------------------------------------------------------------
#
#           translations
#
#------------------------------------------------------------------------


class CmsTranslations(models.Model):
    lang = models.CharField(max_length=8, default='en')
    data = JSONField(default={})
    msgs = ListField(default=[])
    isPublished = models.BooleanField(default=False)
    publicationDate = models.DateTimeField(default=None, null=True)
    created = models.DateTimeField(auto_now=True)
    _msgsCache = JSONField(default={})

    def save(self, *args, **kwargs):
        from .helpers import gettext
        i = 0
        for msg in self.data.get('msgs', []) or []:
            self.data['msgs'][i]['id'] = str(i)
            i += 1
        self._msgsCache = gettext.model_to_gettext_json(self)[self.lang]
        for key in list(self._msgsCache.keys()):
            if key == self._msgsCache[key]:
                del self._msgsCache[key]
        super(CmsTranslations, self).save(*args, **kwargs)




#------------------------------------------------------------------------
#
#           default models
#
#------------------------------------------------------------------------

CmsPdfData = generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsPdfData.json'
    )
)

CmsMailNotification = generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsMailNotification.json'
    )
)

CmsSmsNotification = generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsSmsNotification.json'
    )
)

CmsCeleryResult = generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsCeleryResult.json'
    )
)

CmsCurrencyData = generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsCurrencyData.json'
    )
)

CmsGdprRequest = generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsGdprRequest.json'
    )
)



#------------------------------------------------------------------------
#
#           abstract models
#
#------------------------------------------------------------------------


CmsSettingsAbstract = generate_model(
    os.path.join(
        os.path.dirname(__file__),
        'models.json',
        'CmsSettingsAbstract.json'
    )
)


class CmsUserAbstract(auth_models.AbstractUser):
    userRole = models.CharField(max_length=2, default='u', choices=[
        ('u', 'Regular user'),
        ('a', 'Admin')
    ])
    preferredLang = models.CharField(max_length=2, default='ru', blank=True)
    firstName = models.CharField(max_length=40, default='', blank=True)
    middleName = models.CharField(max_length=40, default='', blank=True)
    lastName = models.CharField(max_length=60, default='', blank=True)
    name = models.CharField(max_length=142, default='', blank=True)
    fullName = models.CharField(max_length=150, default='', blank=True)
    isGdprProtected = models.BooleanField(default=False)
    loginProtection = models.CharField(max_length=2, default='', choices=[
        ["", "Without protection (preferred)"],
        ["s", "With sms protection"],
        ["c", "With captcha"]
    ])
    timezone = TimeZoneField(default='Europe/Prague')
    accountData = JSONField(default={})
    hiddenAccountData = JSONField(default={})

    def save(self, *args, **kwargs):
        name = []
        if getattr(self, 'firstName', None):
            name.append(self.firstName)
        if getattr(self, 'middleName', None):
            name.append(self.middleName)
        if getattr(self, 'lastName', None):
            name.append(self.lastName)
        self.name = ' '.join(name)
        self.fullName = self.name or self.username
        self.is_superuser = self.userRole == 'a'
        if 'userRole' in kwargs.get('update_fields', []):
            kwargs['update_fields'].append('is_superuser')
        super(CmsUserAbstract, self).save(*args, **kwargs)


    def set_default_permission_group_from_user_role(self):
        if self.is_superuser:
            self.groups.set(self.groups.all().exclude(name__startswith="Permissions for '"))
            return
        from django.contrib.auth.models import Group
        group, created = Group.objects.get_or_create(name="Permissions for '%s'" % self.get_userRole_display())
        if not self.groups.filter(id=group.id).count():
            self.groups.set(self.groups.all().exclude(name__startswith="Permissions for '"))
        self.groups.add(group)


    def phones_to_string(self):
        accountData = self.accountData or {}
        phones = accountData.get('phones', None) or []
        return u', '.join([
            p.get('val', None) or p.get('value', None)
            for p in phones if p.get('val', None) or p.get('value', None)
        ])

    def __unicode__(self):
        return self.fullName or self.name or self.username or str(self.id)

    def __str__(self):
        return self.fullName or self.name or self.username or str(self.id)

    class Meta:
        abstract = True
