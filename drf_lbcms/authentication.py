from rest_framework.authentication import TokenAuthentication, BaseAuthentication, get_authorization_header
from django.contrib.auth import get_user_model
#from django.contrib.auth import login



class QueryStringTokenAuthentication(TokenAuthentication):
    def authenticate(self, request):
        token = request.query_params.get('access_token', None)
        if not token:
            return None
        return self.authenticate_credentials(token)

    def authenticate_header(self):
        return 'Token in query string'



class PermanentApiKeyAuthentication(BaseAuthentication):
    def authenticate(self, request):
        from django.conf import settings
        api_key = get_authorization_header(request) or request.query_params.get('api_key', None)

        if not api_key or api_key != getattr(settings, 'PERMANENT_PUBLIC_API_KEY', None):
            return None
        if hasattr(settings, 'PERMANENT_PUBLIC_API_USER'):
            UserModel = get_user_model()
            user = UserModel.objects.get(username=settings.PERMANENT_PUBLIC_API_USER)
            #login(request, user)
            return (user, None)
        return None
