from __future__ import absolute_import, unicode_literals
from django.conf import settings
from django.core.files.storage import DefaultStorage
from django.core.files.base import ContentFile
from datetime import datetime, timedelta
from collections import namedtuple

import drfs, six, os, uuid
from drf_lbcms.helpers import run_viewset_method, fs as fs_helpers
from drf_lbcms.helpers.response import csv_to_response, sanitize_filename
from drf_lbcms.helpers.cms import generate_default_notification_objs

from .models import CmsMailNotification
from .decorators import shared_task





@shared_task
def export_file_to_email(full_path_to_method, request=None, model_name=None, pks=[], preferred_lang='en', file_name=None, state_name=None):
    p = full_path_to_method.split('.')
    string_path_to_viewset = '.'.join(p[:-1])
    method_name = p[-1]
    model_class = drfs.get_model(model_name+'.json')
    rows_data, formatters = run_viewset_method(
        string_path_to_viewset, method_name, request,
        queryset=model_class.objects.filter(pk__in=pks),
        preferred_lang=preferred_lang,
        state_name=state_name
    )
    response = csv_to_response(rows_data=rows_data, file_name=file_name, formatters=formatters)
    file_content = response.content
    request = request or {}
    user = request.get('user', None)
    if isinstance(user, six.integer_types) or isinstance(user, six.text_type):
        from django.contrib.auth import get_user_model
        UserModel = get_user_model()
        user = UserModel.objects.get(id=user)

    storage = DefaultStorage()
    name = storage.save(sanitize_filename(file_name), ContentFile(file_content))
    try:
        cms_mailnotify_instance = CmsMailNotification.objects.get(identifier='cms.user.$exportExcelFile')
    except:
        return
    file_url = storage.url(name)
    if 'https://' not in file_url and 'http://' not in file_url:
        if getattr(settings, 'PLATFORM', 'dev') == 'dev':
            file_url = "http://localhost:8000%s" % file_url
        else:
            file_url = "https://%s%s" % (settings.HOSTNAME, file_url)
    cms_mailnotify_instance.send_user_notification(
        user,
        context={'fileName': file_name, 'fileUrl': file_url},
        preferred_lang=preferred_lang
    )



@shared_task
def dump_db_and_files_to_email(dump_args=[], email=None, preferred_lang='en'):
    if not email:
        return
    generate_default_notification_objs()
    try:
        cms_mailnotify_instance = CmsMailNotification.objects.get(identifier='cms.settings.$dumpDbAndFiles')
    except:
        return

    storage = DefaultStorage()
    # удаляем старые дампы
    try:
        for od in storage.listdir('dump/')[0]:
            for of in storage.listdir(os.path.join('dump', od))[-1]:
                storage.delete(os.path.join('dump', od, of))
            storage.delete(os.path.join('dump', od))
    except:
        pass




    from drf_lbcms.management.commands.drf_lbcms_dump_db_and_files import Command
    from .decorators import call_with_timeout

    c = Command()
    dump_file = c.handle(*dump_args)
    destination_file = os.path.join("dump", str(uuid.uuid1()), os.path.basename(dump_file))

    if c.is_default_storage_local():
        fs_helpers.mkdir(os.path.join(settings.MEDIA_ROOT, os.path.dirname(destination_file)))


    CHUNK_SIZE = 1048576
    with storage.open(destination_file, 'wb') as destination:
        with open(dump_file, 'rb') as f:
            for chunk in iter(lambda: f.read(CHUNK_SIZE), b''):
                destination.write(chunk)

    if dump_file.startswith("/"):
        os.remove(dump_file)


    file_url = storage.url(destination_file)
    if 'https://' not in file_url and 'http://' not in file_url:
        if getattr(settings, 'PLATFORM', 'dev') == 'dev':
            file_url = "http://localhost:8000%s" % file_url
        else:
            file_url = "https://%s%s" % (settings.HOSTNAME, file_url)

    delete_timeout = timedelta(hours=2)

    # отсылаем письмо с дампом
    User = namedtuple('CmsUser', 'email')
    cms_mailnotify_instance.send_user_notification(
        User(email),
        context={'fileUrl': file_url, 'deleteAt': datetime.now() + delete_timeout},
        preferred_lang=preferred_lang
    )

    # TODO: сделать удаление архива
    #if getattr(settings, 'PLATFORM', 'dev') != 'dev':
    #    # отложенное удаление файла
    #    @call_with_timeout(timeout=delete_timeout.total_seconds())
    #    def delete_dump_file():
    #        storage.delete(destination_file)

    #    delete_dump_file()
