# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.forms import SetPasswordForm as _SetPasswordForm



class ResetPasswordForm(_SetPasswordForm):
    def save(self, commit=True):
        # обнуляем значение контрольного параметра, чтоб ссылкой могли воспользоваться только из письма
        if 'resetPassword' in self.user.hiddenAccountData:
            del self.user.hiddenAccountData['resetPassword']
        # сохраняем пароль
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save()
        return self.user


class ResetPasswordAndPhonesForm(ResetPasswordForm):
    phones = forms.CharField(required=False, max_length=300, label=_('Phones'))

    def save(self, commit=True):
        self.user = super(ResetPasswordAndPhonesForm, self).save(commit=False)
        new_phones = self.cleaned_data['phones'].split(',')
        phones = []

        for p in new_phones:
            if p.strip():
                phones.append({
                    'type': 'm',
                    'value': p.strip()
                })
        for p in self.user.accountData.get('phones', None) or []:
            if p and p.get('type', None) != 'm' and (p.get('val', None) or p.get('value', None)):
                phones.append({
                    'type': p['type'],
                    'value': p.get('val', None) or p.get('value', None)
                })
        self.user.accountData['phones'] = phones
        if commit:
            self.user.save()
        return self.user
