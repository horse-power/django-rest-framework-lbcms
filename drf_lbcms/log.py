from django.utils.log import AdminEmailHandler  as AdminEmailHandlerBase




class AdminEmailHandler(AdminEmailHandlerBase):

    def connection(self):
        from drf_lbcms.helpers.email import get_connection
        from drf_lbcms.helpers import get_site_settings
        return get_connection(get_site_settings(), fail_silently=True)
