# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.conf import settings as djsettings
from django.utils.deprecation import MiddlewareMixin
from django.http import HttpResponsePermanentRedirect, HttpResponseRedirect
from django import shortcuts
from django.urls import reverse, resolve

from django.conf.urls.i18n import is_language_prefix_patterns_used
from django.urls import get_script_prefix, is_valid_path
from django.utils import translation
from django.utils.cache import patch_vary_headers
from django.utils.deprecation import MiddlewareMixin

import re, six
import logging
import json
import warnings

from .helpers import get_site_settings
from .web_prerenders import get_prerender_backend
from .exceptions import RedirectException


if six.PY2:
    import urllib
    from urlparse import urlparse

    def parse_qs(url):
        parsed = urlparse.urlparse(url)
        return urlparse.parse_qs(parsed.query)

    def unquote(url):
        return urllib.unquote(url).decode('utf8')
else:
    from urllib.parse import unquote
    from urllib.parse import urlparse

    def parse_qs(url):
        parsed = urlparse(url)
        return urlparse.parse_qs(parsed.query)


logger = logging.getLogger(__name__)
IGNORED_HEADERS = (
    'connection', 'keep-alive', 'proxy-authenticate',
    'proxy-authorization', 'te', 'trailers', 'transfer-encoding',
    'upgrade', 'content-length', 'content-encoding'
)


def request_should_be_ignored(request, langs):
    if request.path == '/':
        return False
    for lang in langs:
        if request.path.startswith('/%s/' % lang):
            return False
    return True





class DisableCSRF(MiddlewareMixin):
    def process_request(self, request):
        request.csrf_processing_done = True
        request._dont_enforce_csrf_checks = True


class LbCmsQueryParamsMiddleware(MiddlewareMixin):
    def process_request(self, request):
        query = request.GET or request.POST or getattr(request, 'query_params', None) or {}
        _where = query.get('where', None)
        _filter = query.get('filter', None)
        request.lbquery_params = {}

        if _where and _filter:
            pass
        elif _filter:
            try:
                _filter = json.loads(_filter)
            except:
                pass
        elif _where:
            try:
                _where = json.loads(_where)
                _filter = {'where': _where}
            except:
                pass
        _filter = _filter or {}
        if not isinstance(_filter, dict):
            return
        _filter['where'] = _filter.get('where', {})
        _filter['fields'] = _filter.get('fields', {})
        request.lbquery_params = _filter




class CrawlerRenderMiddleware(MiddlewareMixin):
    def __init__(self, *args, **kwargs):
        super(CrawlerRenderMiddleware, self).__init__(*args, **kwargs)

    def build_django_response_from_requests_response(self, response):
        r = HttpResponse(response.content)
        for k, v in response.headers.items():
            if k.lower() not in IGNORED_HEADERS:
                r[k] = v
        r['content-length'] = len(response.content)
        r.status_code = response.status_code
        return r

    def process_request(self, request):
        if request_should_be_ignored(request, djsettings.APP_CONTEXT.get('CMS_ALLOWED_LANGS', ['en'])):
            return

        if "HTTP_USER_AGENT" not in request.META:
            return

        settings = None
        if hasattr(request, 'siteSettings'):
            settings = request.siteSettings
        settings = settings or get_site_settings(request)
        if not settings or not settings.serverIntegrations.get('usePrerender', False):
            return

        params = settings.serverIntegrations.get('prerender', {})
        regex_str = params.get('userAgentRegex', None) or "yandex|WhatsApp|Viber|Google-Site-Verification|google-site-verification|googlebot|baiduspider|facebookexternalhit|twitterbot|rogerbot|linkedinbot|embedly|quora\ link\ preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator"
        regex_str = ".*?(%s)" % regex_str
        user_agent_regex = re.compile(regex_str, re.IGNORECASE)

        if not user_agent_regex.match(request.META["HTTP_USER_AGENT"]):
            return

        prerender_backend = get_prerender_backend(settings.serverIntegrations.get('prerenderBackend', ''), params=params)
        if not prerender_backend:
            return

        try:
            return prerender_backend.get_response_for_url(request.build_absolute_uri(), request=request)
        except:
            return





class UrlRedirectMiddleware(MiddlewareMixin):
    """
    This middleware lets you match a specific url and redirect the request to a
    new url.
    """

    def get_url_domain(self, url):
        return '{uri.scheme}://{uri.netloc}'.format(uri=urlparse(url))

    def normalize_url(self, url, domain=None):
        if url[0]!='/' and not url.startswith('https://') and not url.startswith('http://'):
            url = "http://" + url
        if domain and url.startswith('/'):
            url = domain + url
        params = {}
        if '?' in url:
            params = parse_qs(url.split('?')[-1])
            url = url.split('?')[0]
        search = []
        # сортируем по ключам и значениям, чтоб получить действительно
        # нормализированный url
        keys = list(params.keys())
        keys.sort()
        for k in keys:
            search.append(k+"="+params[k])
        if search:
            return url + '?' + "&".join(search)
        return url

    def process_exception(self, request, exception):
        if isinstance(exception, RedirectException):
            if isinstance(exception.params, dict):
                return shortcuts.redirect(exception.url, **exception.params)
            return shortcuts.redirect(exception.url)


    def process_request(self, request):
        settings = None
        custom_url_redirect = getattr(self, 'custom_url_redirect', None)
        if hasattr(request, 'siteSettings'):
            settings = request.siteSettings
        if not settings:
            settings = get_site_settings(request)
        if not settings and not custom_url_redirect:
            return
        siteRedirections = getattr(settings, 'siteRedirections', None) or []

        # пытаемся найти правило редиректа
        current_url = unquote(request.build_absolute_uri())
        domain = self.get_url_domain(current_url)

        if custom_url_redirect:
            to_url = custom_url_redirect(current_url, domain=domain)
            if to_url:
                return HttpResponsePermanentRedirect(to_url)

        if siteRedirections:
            for r in siteRedirections:
                if not r.get('fromUrl', None) or not r.get('toUrl', None):
                    continue
                from_url = self.normalize_url(r['fromUrl'], domain)
                to_url = self.normalize_url(r['toUrl'], domain)
                if from_url == current_url and to_url != from_url:
                    if r.get('redirectCode', u'301') == '301':
                        return HttpResponsePermanentRedirect(to_url)
                    if r.get('redirectCode', u'301') == u'302':
                        return HttpResponseRedirect(to_url)
                    return

        # определяем, включен ли нужный нам язык
        language_from_path = translation.get_language_from_path(request.path_info)
        if not language_from_path:
            return

        allowed_langs = getattr(settings, 'cmsLangCodes', None) or []
        if language_from_path in allowed_langs:
            return

        url = current_url.replace(domain + '/', '')
        parts = url.split('/')

        url_name = resolve(request.path_info).url_name
        if url_name == 'app.index' or len(parts) == 2:
            translation.activate(djsettings.LANGUAGE_CODE)
            return HttpResponseRedirect(reverse(url_name))
        warnings.warn("UrlRedirectMiddleware: Unknown language in url or other error")
        return HttpResponseRedirect('/404')



class CmsSettingsMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request.siteSettings = get_site_settings(request)





# создано для обхода 9-ти летнего бага
# https://code.djangoproject.com/ticket/17734#comment:2

class LocaleMiddleware(MiddlewareMixin):
    """
    Parse a request and decide what translation object to install in the
    current thread context. This allows pages to be dynamically translated to
    the language the user desires (if the language is available).
    """
    response_redirect_class = HttpResponseRedirect

    def process_request(self, request):
        urlconf = getattr(request, 'urlconf', djsettings.ROOT_URLCONF)
        i18n_patterns_used, prefixed_default_language = is_language_prefix_patterns_used(urlconf)
        language = translation.get_language_from_request(request, check_path=i18n_patterns_used)
        language_from_path = translation.get_language_from_path(request.path_info)
        if not language_from_path and i18n_patterns_used and not prefixed_default_language:
            language = djsettings.LANGUAGE_CODE
        translation.activate(language)
        request.LANGUAGE_CODE = translation.get_language()

    def process_response(self, request, response):
        language = translation.get_language()
        language_from_path = translation.get_language_from_path(request.path_info)
        urlconf = getattr(request, 'urlconf', djsettings.ROOT_URLCONF)
        i18n_patterns_used, prefixed_default_language = is_language_prefix_patterns_used(urlconf)
        redirect_needed = response.status_code == 404
        if not redirect_needed:
            if not request.path_info:
                redirect_needed = True
            else:
                href = request.path_info.split('?')[0].split('#')[0]
                redirect_needed = href in ['', '/']

        if (redirect_needed and not language_from_path and
                i18n_patterns_used and prefixed_default_language):
            # Maybe the language code is missing in the URL? Try adding the
            # language prefix and redirecting to that URL.
            language_path = '/%s%s' % (language, request.path_info)
            path_valid = is_valid_path(language_path, urlconf)
            path_needs_slash = (
                not path_valid and (
                    djsettings.APPEND_SLASH and not language_path.endswith('/') and
                    is_valid_path('%s/' % language_path, urlconf)
                )
            )
            content_type = 'text/html'
            if hasattr(response, '_headers') and 'content-type' in response._headers:
                content_type = response._headers['content-type'][-1]

            if (path_valid or path_needs_slash) and ('html' in content_type or 'text' in content_type):
                script_prefix = get_script_prefix()
                # Insert language after the script prefix and before the
                # rest of the URL
                language_url = request.get_full_path(force_append_slash=path_needs_slash).replace(
                    script_prefix,
                    '%s%s/' % (script_prefix, language),
                    1
                )
                return self.response_redirect_class(language_url)

        if not (i18n_patterns_used and language_from_path):
            patch_vary_headers(response, ('Accept-Language',))
        if hasattr(response, 'headers'):
            # django >= 3.2.x
            response.headers.setdefault('Content-Language', language)
        else:
            response.setdefault('Content-Language', language)
        return response
