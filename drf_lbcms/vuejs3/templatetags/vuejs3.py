import os, json
from django import template
from django.conf import settings


register = template.Library()


def _get_file_ext(filename):
    if not filename:
        return ''
    ext = os.path.splitext(filename)[-1].lower()
    if ext.startswith('.'):
        return ext[1:]
    return ext


manifests = {}
class WebpackFile:
    filename = None
    relation = None
    def __init__(self, filename=None, relation=None):
        self.filename = filename
        self.relation = relation

    def with_prefix(self, prefix=''):
        data = {'relation': self.relation, 'name': self.filename, 'tag': 'link'}
        if prefix:
            data['name'] = os.path.join(prefix, self.filename)
        if self.relation == 'module':
            data['tag'] = 'script'
        elif not self.relation and _get_file_ext(self.filename) == 'css':
            data['relation'] = 'stylesheet'
        return data


def normalize_manifest(manifest):
    normalized = {}

    for k,v in manifest.items():
        if not v.get('isEntry', False):
            normalized[k] = WebpackFile(filename=v['file'])
            continue
        normalized[k] = []
        for im in v.get('imports', []):
            normalized[k].append(WebpackFile(
                filename=manifest.get(im, {}).get('file', None) or im,
                relation='modulepreload'
            ))
        for im in v.get('css', []):
            normalized[k].append(WebpackFile(
                filename=manifest.get(im, {}).get('file', None) or im
            ))
        normalized[k].append(WebpackFile(
            filename=v.get('file', None),
            relation='module'
        ))
    return normalized


BUILD_DATA = getattr(settings, 'BUILD_DATA', {'date': None})

@register.inclusion_tag('vuejs3/vuejs_manifest.html', takes_context=True)
def vuejs_manifest(context, filepath, blockname='index.html', prefix='', types=[]):
    if not filepath in manifests:
        with open(os.path.join(settings.BASE_DIR, filepath)) as f:
            manifests[filepath] = normalize_manifest(json.load(f))
    if blockname not in manifests[filepath]:
        return {}
    files = manifests[filepath][blockname]
    if not isinstance(files, list):
        files = [files]

    if types:
        files = [
            f
            for f in files if _get_file_ext(f.filename) in types
        ]

    suffix = ''
    if BUILD_DATA.get('date', None):
        suffix = '?build=%s' % BUILD_DATA['date']
    return {
        'files': [
            wf.with_prefix(prefix=prefix)
            for wf in files
        ],
        'prefix': prefix or 'prefix',
        'suffix': suffix
    }



@register.inclusion_tag('vuejs3/pwa_metadata.html', takes_context=True)
def vuejs_pwa_metadata(context):
    return {'PWA': settings.PWA_APP_CONFIG}
