{% load lbcms %}
let MAX_CACHE_TIME = 5 * 24 * 60 * 60; // 5 days
let PRECACHE_URLS = {{ PWA.precacheUrls | json }};
let PRECACHE_FILES = [
    {% for name in PWA.precacheFiles %}"{% static name%}",{% endfor %}
];

importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.1.1/workbox-sw.js');
try{
    process = process || {};
} catch {
    process = {env: {}};
}


self.addEventListener('message', (event) => {
    if (event.data && event.data.type === 'SKIP_WAITING') {
        self.skipWaiting();
    }
});


/**
 * The precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
let precache = PRECACHE_URLS.concat(PRECACHE_FILES).map(function(url){
    return {url: url, revision: "{{PWA.precacheRevision}}"}
});
workbox.precaching.precacheAndRoute(precache, {});


//workbox.routing.registerRoute(new workbox.routing.NavigationRoute(workbox.precaching.createHandlerBoundToURL("index.html")));


workbox.routing.registerRoute(/^https:\/\/fonts\.(?:googleapis|gstatic)\.com\/.*/i, new workbox.strategies.CacheFirst({
    "cacheName": "google-fonts",
    plugins: [new workbox.expiration.ExpirationPlugin({
        maxEntries: 4,
        maxAgeSeconds: 31536000,
        purgeOnQuotaError: true
    })]
}), 'GET');
workbox.routing.registerRoute(/\.(?:eot|otf|ttc|ttf|woff|woff2|font.css)$/i, new workbox.strategies.StaleWhileRevalidate({
    "cacheName": "static-font-assets",
    plugins: [new workbox.expiration.ExpirationPlugin({
        maxEntries: 4,
        maxAgeSeconds: MAX_CACHE_TIME * 2,
        purgeOnQuotaError: true
    })]
}), 'GET');
workbox.routing.registerRoute(/\.(?:jpg|jpeg|gif|png|svg|ico|webp)$/i, new workbox.strategies.StaleWhileRevalidate({
    "cacheName": "static-image-assets",
    plugins: [new workbox.expiration.ExpirationPlugin({
        maxEntries: 64,
        maxAgeSeconds: MAX_CACHE_TIME * 2,
        purgeOnQuotaError: true
    })]
}), 'GET');
//workbox.routing.registerRoute(/\.(?:js)$/i, new workbox.strategies.StaleWhileRevalidate({
workbox.routing.registerRoute(({url}) => {
        return url.pathname.endsWith('.js') && !(
            url.pathname.endsWith('serviceworker.js') ||
            url.origin === 'https://maps.googleapis.com'
        )
    }, new workbox.strategies.StaleWhileRevalidate({
    "cacheName": "static-js-assets",
    plugins: [new workbox.expiration.ExpirationPlugin({
        maxEntries: 320,
        maxAgeSeconds: MAX_CACHE_TIME,
        purgeOnQuotaError: true
    })]
}), 'GET');

workbox.routing.registerRoute(/\.(?:css|less)$/i, new workbox.strategies.StaleWhileRevalidate({
    "cacheName": "static-style-assets",
    plugins: [new workbox.expiration.ExpirationPlugin({
        maxEntries: 32,
        maxAgeSeconds: MAX_CACHE_TIME,
        purgeOnQuotaError: true
    })]
}), 'GET');

workbox.routing.registerRoute(/\.(?:json|xml|csv)$/i, new workbox.strategies.NetworkFirst({
    "cacheName": "static-data-assets",
    plugins: [new workbox.expiration.ExpirationPlugin({
        maxEntries: 32,
        maxAgeSeconds: MAX_CACHE_TIME,
        purgeOnQuotaError: true
    })]
}), 'GET');

workbox.routing.registerRoute(/\/api\/.*$/i, new workbox.strategies.NetworkFirst({
    "cacheName": "apis",
    "networkTimeoutSeconds": 10,
    plugins: [new workbox.expiration.ExpirationPlugin({
        maxEntries: 16,
        maxAgeSeconds: 86400,
        purgeOnQuotaError: true
    })]
}), 'GET');

//workbox.routing.registerRoute(/.*/i, new workbox.strategies.NetworkFirst({
//    "cacheName": "others",
//    "networkTimeoutSeconds": 10,
//    plugins: [new workbox.expiration.ExpirationPlugin({
//        maxEntries: 32,
//        maxAgeSeconds: 86400,
//        purgeOnQuotaError: true
//    })]
//}), 'GET');

let devFilesRe = /\.(?:ts|scss|sass|vue|pug)$/i;
workbox.routing.registerRoute(
    function({request, url}){
        if (request.mode === 'navigate' && url.pathname.startsWith('/admin')){
            return false
        }
        return devFilesRe.test(url.pathname) || url.pathname.includes('@vite')
    },
    new workbox.strategies.NetworkFirst({
        "cacheName": "others",
        "networkTimeoutSeconds": 10,
        plugins: [new workbox.expiration.ExpirationPlugin({
            maxEntries: 320,
            maxAgeSeconds: MAX_CACHE_TIME,
            purgeOnQuotaError: true
        })]
    }),
    'GET'
);
