from django.conf import settings as djsettings
from drf_lbcms.views import TemplateView
import copy

PWA_CONFIG = None

class PwaView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        global PWA_CONFIG
        if not PWA_CONFIG:
            PWA_CONFIG = copy.deepcopy(djsettings.PWA_APP_CONFIG)
            PWA_CONFIG['isDebugMode'] = djsettings.PWA_APP_CONFIG.get('isDebugMode', False) or djsettings.DEBUG
            PWA_CONFIG['precacheUrls'] = PWA_CONFIG.get('precacheUrls', None) or []
            PWA_CONFIG['precacheFiles'] = PWA_CONFIG.get('precacheFiles', None) or []
            PWA_CONFIG['precacheRevision'] = PWA_CONFIG.get('precacheRevision', None) or 'v1'
        context['PWA'] = PWA_CONFIG
        return context



class PwaManifestView(PwaView):
    template_name = 'vuejs3/pwa_manifest.json'
    content_type = 'application/json'



class ServiceWorkerView(PwaView):
    template_name = 'vuejs3/serviceworker.js'
    content_type = 'application/javascript'
