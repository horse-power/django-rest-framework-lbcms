from rest_framework import serializers
from drfs import generate_serializer


def generate_serializer_with_company_name(model_class, use_isPublic=False):
    BaseSerializer = generate_serializer(model_class)

    class Serializer(BaseSerializer):
        _companyName = serializers.SerializerMethodField()
        def get__companyName(self, obj):
            if use_isPublic and hasattr(obj, 'isPublic') and obj.isPublic:
                return None
            if getattr(obj, 'company_id', None):
                return obj.company.name
            return None

        class Meta:
            model = model_class
            fields = BaseSerializer.Meta.fields + ['_companyName']

    return type(model_class.__name__ + 'Serializer', (Serializer,), {})
