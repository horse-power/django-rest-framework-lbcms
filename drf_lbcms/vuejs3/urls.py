from django.conf.urls import  include, url

from . import views


urlpatterns = [
    url(r'^manifest.json', views.PwaManifestView.as_view(), name='vuejs-pwa-manifest'),
    url(r'^serviceworker.js', views.ServiceWorkerView.as_view(), name='vuejs-pwa-serviceworker')
]
