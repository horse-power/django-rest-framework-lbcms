from django.apps import AppConfig




class Vue3AppConfig(AppConfig):
    name = 'drf_lbcms.vuejs3'
    verbose_name = 'Vuejs3 for django'

    def ready(self):
        pass
