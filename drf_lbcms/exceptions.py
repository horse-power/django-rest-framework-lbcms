from rest_framework.exceptions import APIException


class RedirectException(Exception):
    def __init__(self, url, params={}):
        self.url = url
        self.params = params or None


class ObjectNotFoundException(APIException):
    status_code = 404
    default_detail = "Object not found"
