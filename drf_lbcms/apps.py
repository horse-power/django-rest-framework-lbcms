import sys
from django.apps import AppConfig
from django.conf import settings

if getattr(settings, 'CELERY_BROKER_URL', None) == 'drf-lbcms://uwsgi-spooler':
    try:
        from uwsgidecorators import spool
    except:
        pass



class LbCmsAppConfig(AppConfig):
    name = 'drf_lbcms'
    verbose_name = 'LbCms for django'

    def ready(self):
        if len(sys.argv) >= 2 and sys.argv[1] in ['loaddata', 'migrate', 'drf_lbcms_restore_db_and_files', 'compilemessages']:
            return

        from django.contrib.auth.models import Permission
        from django.contrib.contenttypes.models import ContentType
        from django.contrib.auth import get_user_model
        from django.contrib.auth.models import Group
        from django.templatetags import static as original_static_templatetags
        from .templatetags.base_lbcms import do_static

        from . import signals

        # вот такими говно-хаками приходится переопределять встроенные теги
        if hasattr(original_static_templatetags, 'register'):
            original_static_templatetags.register.tag('static', do_static)



        if getattr(settings, 'CELERY_BROKER_URL', None) == 'drf-lbcms://uwsgi-spooler' and 'uwsgi' in sys.argv:
            from django.apps import apps
            for app in apps.get_app_configs():
                try:
                    __import__(app.name + '.tasks')
                except ModuleNotFoundError:
                    pass

        try:
            from site_settings.models import CmsSettings
            content_type = ContentType.objects.get_for_model(CmsSettings)
        except:
            return

        if not Permission.objects.filter(codename='view_glossary', content_type=content_type).count():
            Permission.objects.create(
                codename='view_glossary',
                name='Can view glossary',
                content_type=content_type,
            )
        if not Permission.objects.filter(codename='view_adminpage', content_type=content_type).count():
            Permission.objects.create(
                codename='view_adminpage',
                name='Can view admin page',
                content_type=content_type,
            )

        CmsUser = get_user_model()
        try:
            for choice in CmsUser._meta.get_field('userRole').choices:
                if choice[0] not in ['admin', 'a']:
                    group, created = Group.objects.get_or_create(name="Permissions for '%s'" % choice[-1])
        except:
            return
