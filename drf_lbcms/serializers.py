from rest_framework import serializers
from django.conf import settings
from .models import L10nFile as L10nFileModelClass, L10nFileLocal as L10nFileLocalModelClass
from .models import CmsTranslations as CmsTranslationsModelClass





class L10nFileBase(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    meta = serializers.SerializerMethodField()
    copyright = serializers.SerializerMethodField()
    thumbs = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()

    def get_meta(self, obj):
        return obj.meta_data

    def get_copyright(self, obj):
        return obj.copyright or {}

    def get_thumbs(self, obj):
        return obj.thumbs

    def get_title(self, obj):
        return obj.title

    def get_description(self, obj):
        return obj.description

    def get_name(self, obj):
        name = str(obj.file_data)
        name = name.replace(settings.MEDIA_ROOT, '')
        if not name:
            return None
        if name[0] == '/':
            name = name[1:]
        return name



class L10nFile(L10nFileBase):
    class Meta:
        model = L10nFileModelClass
        fields = ('id', 'name', 'title', 'description', 'meta', 'copyright', 'thumbs')



class L10nFileLocal(L10nFileBase):
    class Meta:
        model = L10nFileLocalModelClass
        fields = ('id', 'name', 'title', 'description', 'meta', 'copyright', 'thumbs')


class CmsTranslations(serializers.ModelSerializer):
    class Meta:
        model = CmsTranslationsModelClass
        fields = ('id', 'data', 'msgs', 'lang')



class SerializerWithCurrentUser(serializers.ModelSerializer):
    currentUser = serializers.SerializerMethodField()

    def get_currentUser(self, obj):
        request = self.context.get('request', None)
        if not request:
            return {}
        if request.user.is_authenticated:
            data = {
                'id': request.user.pk,
                'username': request.user.username,
                'userRole': request.user.userRole,
                'mainImg': None
            }
            if hasattr(request.user, 'privateData'):
                data['privateData'] = request.user.privateData or {}
            if hasattr(request.user, 'accountData'):
                data['accountData'] = request.user.accountData or {}
            if hasattr(request.user, 'mainImg') and request.user.mainImg:
                data['mainImg'] = L10nFile(request.user.mainImg).data
            if request.user.is_superuser:
                return data
            data['allPermissions'] = request.user.get_all_permissions()
            return data
        return {}
