# -*- coding: utf-8 -*-
#from rest_framework.response import Response

#from drfs import generate_serializer, mixins
#from drfs.decorators import action







class CmsCurrencyDataViewsetMixin(object):
    def get_queryset(self):
        queryset = super(CmsCurrencyDataViewsetMixin, self).get_queryset()
        if not self.request:
            return queryset.none()
        userRole = getattr(self.request.user, 'userRole', None)
        if userRole not in ['a', 'admin']:
            return queryset.none()
        return queryset
