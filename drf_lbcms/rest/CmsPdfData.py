# -*- coding: utf-8 -*-
import os
from rest_framework.response import Response
from django.conf import settings as djsettings
from drfs.decorators import action
from drf_lbcms.helpers.django import get_base_dir_as_string

from .serializers import CmsPdfDataSerializer




class CmsPdfDataViewsetMixin(object):
    serializer_class = CmsPdfDataSerializer

    @action(methods=['put'], detail=False)
    def validate(self, request, *args, **kwargs):
        '''
            Валидирует шаблоны
        '''
        from .serializers import TemplateFieldsValidator
        v = TemplateFieldsValidator(['title', 'content', 'runningTitles.header', 'runningTitles.footer'])
        v(request.data)
        return Response()



    @action(methods=['get'], detail=True)
    def get_initial(self, request, *args, **kwargs):
        '''
            позволяет получить начальные данные для шаблонов (ака, восстанавливать их тексты, если они были сломаны пользователем при редактировании)
        '''
        instance = self.get_object()
        '''
        if instance.identifier.startswith('custom'):
            # это шаблон, который может редактировать только пользователь.
            # берем данные из базового уведомления
            identifier = '|'.join(instance.identifier.split('|')[1:])
            try:
                base_instance = self.queryset.get(identifier=identifier)
            except self.queryset.DoesNotExist:
                base_instance = None
            if base_instance:
                return Response({
                    'title': base_instance.title,
                    'content': base_instance.content,
                    'runningTitles': base_instance.runningTitles
                })
        '''
        # пытаемся найти начальные данные в папке с проектом
        BASE_DIR = get_base_dir_as_string()
        fpath = os.path.join(BASE_DIR, 'resources', 'pdf', instance.identifier, 'description.json')
        if os.path.isfile(fpath):
            from .helpers import load_notify
            data = load_notify(os.path.join(BASE_DIR, 'resources', 'pdf', instance.identifier))
            return Response({
                'title': data.get('title', {}),
                'content': data.get('content', {}),
                'runningTitles': data.get('runningTitles', {})
            })
        return Response({
            'title': {},
            'content': {},
            'runningTitles': {}
        })
