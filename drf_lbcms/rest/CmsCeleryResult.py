# -*- coding: utf-8 -*-
from django.db.models import Q
#from rest_framework.response import Response
#from drfs.decorators import action

#from drfs import generate_serializer, mixins




class CmsCeleryResultViewsetMixin(object):
    def get_queryset(self):
        queryset = super(CmsCeleryResultViewsetMixin, self).get_queryset()
        if not self.request or not self.request.user.is_authenticated:
            return queryset.none()
        return queryset.filter(
            Q(ownerId=self.request.user.id) | Q(ownerId=0)
        )
