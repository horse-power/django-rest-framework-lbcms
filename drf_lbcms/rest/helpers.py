# -*- coding: utf-8 -*-
import os, json, io


def load_notify(dir_path):
    with open(os.path.join(dir_path, 'description.json')) as f:
        data = json.load(f)

    for filename in os.listdir(dir_path):
        if filename == 'description.json':
            continue
        if os.path.splitext(filename)[-1].lower() not in ['.txt', '.html']:
            continue
        file_path = os.path.join(dir_path, filename)
        if not os.path.isfile(file_path):
            continue
        with io.open(file_path, mode="r", encoding="utf-8") as f:
            content = f.read()
        if content and content[-1]=='\n':
            content = content[:-1]
        name = filename.split('.')
        data[name[0]] = data.get(name[0], {})
        data[name[0]][name[1]] = content
    return data
