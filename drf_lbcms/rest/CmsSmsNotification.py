# -*- coding: utf-8 -*-
import os
from rest_framework.response import Response
from django.conf import settings as djsettings
from django.db.models import Q


from drfs.decorators import action


from drf_lbcms.helpers.django import get_base_dir_as_string
from .serializers import CmsSmsNotificationSerializer, CmsSmsNotificationCustomSerializer






class CmsSmsNotificationViewsetMixin(object):
    serializer_class = CmsSmsNotificationSerializer

    def get_queryset(self):
        queryset = super(CmsSmsNotificationViewsetMixin, self).get_queryset()
        user = getattr(self.request, 'user', None)
        if user.is_superuser:
            return queryset.filter(owner=None)
        if user.is_authenticated:
            return queryset.filter(Q(owner=self.request.user.id) | Q(owner=None))
        return  queryset.filter(owner=None)


    def get_serializer_class(self):
        user = getattr(self.request, 'user', None)
        userRole = getattr(user, 'userRole', None)
        if userRole in ['a', 'admin', None]:
            return self.serializer_class
        return CmsSmsNotificationCustomSerializer


    @action(methods=['put'], detail=False)
    def validate(self, request, *args, **kwargs):
        '''
            Валидирует шаблоны в уведомлении
        '''
        from .serializers import TemplateFieldsValidator
        v = TemplateFieldsValidator(['body', 'echoBody'])
        v(request.data)
        return Response()


    @action(methods=['get'], detail=True)
    def get_initial(self, request, *args, **kwargs):
        '''
            позволяет получить начальные данные для уведомлений (ака, восстанавливать их тексты, если они были сломаны пользователем при редактировании)
        '''
        instance = self.get_object()
        if instance.identifier.startswith('custom'):
            # это уведомление, которое может редактировать только пользователь.
            # берем данные из базового уведомления
            identifier = '|'.join(instance.identifier.split('|')[1:])
            try:
                base_instance = self.queryset.get(identifier=identifier)
            except self.queryset.DoesNotExist:
                base_instance = None
            if base_instance:
                return Response({
                    'body': base_instance.body,
                    'echoBody': base_instance.echoBody
                })
        # пытаемся найти начальные данные в папке с проектом
        BASE_DIR = get_base_dir_as_string()
        fpath = os.path.join(BASE_DIR, 'resources', 'mail', instance.identifier, 'description.json')
        if os.path.isfile(fpath):
            from .helpers import load_notify
            data = load_notify(os.path.join(BASE_DIR, 'resources', 'mail', instance.identifier))
            return Response({
                'body': data.get('body', {}),
                'echoBody': data.get('echoBody', {}),
            })
        return Response({
            'body': {},
            'echoBody': {}
        })
