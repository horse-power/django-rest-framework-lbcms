# -*- coding: utf-8 -*-
import os
from rest_framework.response import Response
from rest_framework.exceptions import NotAcceptable
from django.conf import settings as djsettings
from django.core.exceptions import ValidationError
from django.db.models import Q

from drfs.decorators import action

from drf_lbcms.helpers import get_current_site
from drf_lbcms.exceptions import ObjectNotFoundException
from drf_lbcms.helpers.l10nFile import get_options_from_request_data
from drf_lbcms.helpers import get_current_site
from drf_lbcms.helpers.django import get_base_dir_as_string

from .serializers import CmsMailNotificationSerializer, CmsMailNotificationCustomSerializer



def get_object_or_404(queryset, *args, **kwargs):
    try:
        return queryset.get(*args, **kwargs)
    except queryset.model.DoesNotExist:
        raise ObjectNotFoundException(detail='No %s matches the given query.' % queryset.model._meta.object_name)



class FakeUser(object):
    def __init__(self, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)






class CmsMailNotificationViewsetMixin(object):
    serializer_class = CmsMailNotificationSerializer

    def get_queryset(self):
        queryset = super(CmsMailNotificationViewsetMixin, self).get_queryset()
        user = getattr(self.request, 'user', None)
        if user.is_superuser:
            return queryset.filter(owner=None)
        if user.is_authenticated:
            return queryset.filter(Q(owner=self.request.user.id) | Q(owner=None))
        return  queryset.filter(owner=None)


    def get_serializer_class(self):
        user = getattr(self.request, 'user', None)
        userRole = getattr(user, 'userRole', None)
        if userRole in ['a', 'admin', None]:
            return self.serializer_class
        return CmsMailNotificationCustomSerializer


    def get_send_context(self, **kwargs):
        return kwargs


    @action(methods=['put'], url_path='send/(?P<identifier>.+)', detail=False)
    def send(self, request, identifier, *args, **kwargs):
        if identifier.endswith('/'):
            identifier = identifier[:-1]
        if identifier.startswith('cms.') or not identifier:
            raise NotAcceptable(details="Identifier '%s' is not allowed" % identifier)
        data = request.data.copy()
        data['identifier'] = identifier
        instance = get_object_or_404(
            self.filter_queryset(self.get_queryset()),
            identifier=identifier
        )
        user = FakeUser(
            email=data.get('email', None),
            preferredLang=data.get('lang', None) or data.get('preferredLang', None) or 'en',
            _site=get_current_site(request)
        )
        instance.send_user_notification(
            user,
            context=self.get_send_context(**data)
        )
        return Response(
            getattr(instance, 'formResponseSuccess', None) or {}
        )



    @action(methods=['put'], url_path='send_multipart/(?P<identifier>.+)', detail=False)
    def send_multipart(self, request, identifier, *args, **kwargs):
        if identifier.startswith('cms.') or not identifier:
            raise NotAcceptable(details="Identifier '%s' is not allowed" % identifier)

        options, _files = get_options_from_request_data(request.data, allow_multiple=True)
        # генерируем чистый контекст, без файлов
        data = {}
        for k,v in request.data.items():
            if k not in _files:
                data[k] = v
        data['identifier'] = identifier
        # генерируем файлы для отправки
        attachments = [
            {
                'name': ffile._name,
                'mime': ffile.content_type,
                'content': ffile.read()
            }
            for k,ffile in (_files or {}).items()
        ]

        instance = get_object_or_404(
            self.filter_queryset(self.get_queryset()),
            identifier=identifier
        )
        user = FakeUser(
            email=data.get('email', None),
            preferredLang=data.get('lang', None) or data.get('preferredLang', None) or 'en',
            _site=get_current_site(request)
        )
        instance.send_user_notification(
            user,
            context=self.get_send_context(**data),
            attachments=attachments
        )
        return Response(
            getattr(instance, 'formResponseSuccess', None) or {}
        )


    @action(methods=['put'], detail=False)
    def validate(self, request, *args, **kwargs):
        '''
            Валидирует шаблоны в уведомлении
        '''
        from .serializers import TemplateFieldsValidator
        v = TemplateFieldsValidator(['subject', 'body', 'echoBody', 'echoSubject'])
        v(request.data)
        return Response()



    @action(methods=['get'], detail=True)
    def get_initial(self, request, *args, **kwargs):
        '''
            позволяет получить начальные данные для уведомлений (ака, восстанавливать их тексты, если они были сломаны пользователем при редактировании)
        '''
        instance = self.get_object()
        if instance.identifier.startswith('custom'):
            # это уведомление, которое может редактировать только пользователь.
            # берем данные из базового уведомления
            identifier = '|'.join(instance.identifier.split('|')[1:])
            try:
                base_instance = self.queryset.get(identifier=identifier)
            except self.queryset.DoesNotExist:
                base_instance = None
            if base_instance:
                return Response({
                    'subject': base_instance.subject,
                    'body': base_instance.body,
                    'echoSubject': base_instance.echoSubject,
                    'echoBody': base_instance.echoBody
                })
        # пытаемся найти начальные данные в папке с проектом
        BASE_DIR = get_base_dir_as_string()
        fpath = os.path.join(BASE_DIR, 'resources', 'mail', instance.identifier, 'description.json')
        if os.path.isfile(fpath):
            from .helpers import load_notify
            data = load_notify(os.path.join(BASE_DIR, 'resources', 'mail', instance.identifier))
            return Response({
                'subject': data.get('subject', {}),
                'body': data.get('body', {}),
                'echoSubject': data.get('echoSubject', {}),
                'echoBody': data.get('echoBody', {}),
            })
        return Response({
            'subject': {},
            'body': {},
            'echoSubject': {},
            'echoBody': {}
        })
