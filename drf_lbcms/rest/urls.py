from drfs import routers
import drfs
# import block
# end import
from drf_lbcms.models import CmsTranslations, CmsMailNotification, CmsSmsNotification, CmsPdfData
from django.contrib.auth.models import Group
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from .serializers import GroupSerializer

DRF_LBCMS = getattr(settings, 'DRF_LBCMS', None) or {}
if DRF_LBCMS.get('CmsSmsNotification', {}).get('mixin', None):
    raise ImproperlyConfigured("Do not use 'DRF_LBCMS.CmsSmsNotification.mixin' use 'DRF_LBCMS.builtin_models.CmsSmsNotification.viewset_mixins' instead!")
if DRF_LBCMS.get('CmsMailNotification', {}).get('mixin', None):
    raise ImproperlyConfigured("Do not use 'DRF_LBCMS.CmsMailNotification.mixin' use 'DRF_LBCMS.builtin_models.CmsMailNotification.viewset_mixins' instead!")
models_conf = DRF_LBCMS.get('builtin_models', None) or {}

router = routers.SimpleRouter()
# register block
router.register(r'CmsCeleryResult', drfs.generate_viewset('CmsCeleryResult.json'))
router.register(r'CmsCurrencyData', drfs.generate_viewset('CmsCurrencyData.json'))
router.register(r'CmsGdprRequest', drfs.generate_viewset('CmsGdprRequest.json'))

router.register(r'CmsPdfData', drfs.generate_viewset(
    CmsPdfData,
    mixins=models_conf.get('CmsPdfData', {}).get('viewset_mixins', None)
))
router.register(r'CmsSmsNotification', drfs.generate_viewset(
    CmsSmsNotification,
    mixins=models_conf.get('CmsSmsNotification', {}).get('viewset_mixins', None)
))
router.register(r'CmsMailNotification', drfs.generate_viewset(
    CmsMailNotification,
    mixins=models_conf.get('CmsMailNotification', {}).get('viewset_mixins', None)
))
router.register(r'CmsTranslations', drfs.generate_viewset(
    CmsTranslations,
    mixins=["drf_lbcms.rest.CmsTranslations.CmsTranslationsViewsetMixin"],
    acl=[{
            "principalId": "$everyone",
            "property": ".+",
            "permission": "DENY"
        },{
            "principalId": "$everyone",
            "property": [
                "get_by_lang_code_for_gettext",
                "retrieve"
            ],
            "permission": "ALLOW"
        },{
            "principalId": "$djangoPermissions",
            "requiredCodename": "{app_label}.view_{model_name}",
            "property": [
                "list",
                "find_one",
                "count"
            ],
            "permission": "ALLOW"
        },{
            "principalId": "$djangoPermissions",
            "requiredCodename": "{app_label}.change_{model_name}",
            "property": [
                "update",
                "partial_update",
                "download_po_file",
                "upload_po_file"
            ],
            "permission": "ALLOW"
        },{
            "principalId": "$admin",
            "property": ".+",
            "permission": "ALLOW"
        }
    ])
)


router.register(r'GroupPermissions', drfs.generate_viewset(
    Group,
    serializer_class=GroupSerializer,
    mixins=["drf_lbcms.rest.PermissionGroup.PermissionGroupViewsetMixin"],
    acl=[{
            "principalId": "$everyone",
            "property": ".+",
            "permission": "DENY"
        },{
            "principalId": "$djangoPermissions",
            "requiredCodename": "auth.view_permission",
            "property": [
                "list",
                "find_one",
                "retrieve",
                "list_all_permissions",
                "count"
            ],
            "permission": "ALLOW"
        },{
            "principalId": "$djangoPermissions",
            "requiredCodename": "auth.add_permission",
            "property": "create",
            "permission": "ALLOW"
        },{
            "principalId": "$djangoPermissions",
            "requiredCodename": "auth.change_permission",
            "property": [
                "update",
                "partial_update"
            ],
            "permission": "ALLOW"
        },{
            "principalId": "$djangoPermissions",
            "requiredCodename": "auth.delete_permission",
            "property": "destroy",
            "permission": "ALLOW"
        },{
            "principalId": "$admin",
            "property": ".+",
            "permission": "ALLOW"
        }
    ])
)
# end block

urlpatterns =  router.urls
