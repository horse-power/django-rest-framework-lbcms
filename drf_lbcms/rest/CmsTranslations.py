# -*- coding: utf-8 -*-
import polib
from rest_framework.response import Response
from rest_framework import exceptions
from drfs.decorators import action
from datetime import datetime


from drf_lbcms.helpers import gettext
from drf_lbcms.helpers.l10nFile import get_options_from_request_data
from drf_lbcms.helpers.response import file_to_response


def normalize_lang_code(code):
    if code == 'ua':
        code = 'uk'
    if code == 'cz':
        code = 'cs'
    return code.split(':')[0]





class CmsTranslationsViewsetMixin(object):
    @action(methods=['get'], detail=False)
    def get_by_lang_code_for_gettext(self, request, *args, **kwargs):
        code = request.query_params.get('code', '')
        code = code.split('.')[0]
        if '"' in code:
            code = code.strip('"')
        if "'" in code:
            code = code.strip("'")
        if not self.queryset.filter(lang=code).count():
            return Response({})
        instance = self.queryset.filter(lang=code)[0]
        code = code.split(':')[0]
        data = {}
        data[code] = instance._msgsCache or {}
        return Response(data)


    def retrieve(self, request, *args, **kwargs):
        if kwargs['pk'] == 'get_by_lang_code_for_gettext':
            return self.get_by_lang_code_for_gettext(request, *args, **kwargs)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


    def get_object(self):
        code = self.request.query_params.get('code', None) or ''
        code = code.split('.')[0]
        if self.queryset.filter(lang=code).count():
            return self.queryset.filter(lang=code)[0]
        return super(CmsTranslationsViewsetMixin, self).get_object()



    @action(methods=['post'], detail=True)
    def upload_po_file(self, request, *args, **kwargs):
        instance = self.get_object()
        options, ffile = get_options_from_request_data(request.data)
        if not ffile:
            raise exceptions.NotAcceptable(detail="Can't find field with file in formdata.")


        content = ffile.read()
        if isinstance(content, bytes):
            content = content.decode('utf-8')
        catalog = polib._pofile_or_mofile(content, 'pofile')
        if not catalog.metadata or catalog.metadata.get('Language', None) != normalize_lang_code(instance.lang):
            raise exceptions.NotAcceptable(detail="The language code in the file does not match this translation")


        msgs = gettext.pot_file_to_json(catalog, options={'withReferences': True, 'withMsgStr': True})['msgs']

        for item in msgs:
            for i in range(len(instance.msgs)):
                msg = instance.msgs[i]
                if msg['msgId'] == item['msgId'] and \
                        msg.get('msgCtxt', '') == item.get('msgCtxt', '') and \
                        msg.get('msgIdPlural', '') == item.get('msgIdPlural', ''):
                    instance.msgs[i]['msgStr'] = item['msgStr']

        instance.save()
        instance.isPublished = True
        instance.publicationDate = instance.publicationDate or datetime.now()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)



    @action(methods=['get'], detail=True)
    def download_po_file(self, request, *args, **kwargs):
        instance = self.get_object()
        lang = normalize_lang_code(instance.lang)

        po = polib.POFile()
        po.metadata = {
            'Project-Id-Version': '1.0',
            'POT-Creation-Date': '',
            'PO-Revision-Date': '',
            'Last-Translator': 'Automatically generated',
            'Language-Team': 'none',
            'Language': lang,
            'MIME-Version': '1.0',
            'Content-Type': 'text/plain; charset=utf-8',
            'Content-Transfer-Encoding': '8bit',
            'Plural-Forms': gettext.get_plural_form(lang),
            'X-Generator': 'drf-lbcms'
        }

        for msg in instance.msgs:
            entry = {
                'msgid': msg['msgId'],
            }
            if msg.get('msgIdPlural', ''):
                entry['msgid_plural'] = msg['msgIdPlural']
            if msg.get('msgCtxt', ''):
                entry['msgctxt'] = msg['msgCtxt']
            if msg.get('hidden', False):
                entry['obsolete'] = True

            msgstr = msg.get('msgStr', [])
            if len(msgstr) > 1:
                entry['msgstr_plural'] = {}
                for i in range(len(msgstr)):
                    entry['msgstr_plural'][i] = msgstr[i]
            if msgstr:
                entry['msgstr'] = msgstr[0]
            else:
                entry['msgstr'] = ''
            # правим бажное поведение дебильного poedit
            if entry.get('msgid_plural', None):
                if not isinstance(entry.get('msgstr_plural', None), dict):
                    entry['msgstr_plural'] = {0: entry.get('msgstr', None) or ''}
                if 'msgstr' in entry:
                    del entry['msgstr']

            po.append(polib.POEntry(
                **entry,
                occurrences=[
                    ref.split(':')
                    for ref in msg.get('references', [])
                ]
            ))

        return file_to_response(
            file_data=po.__unicode__(),
            file_name=lang + '.po'
        )
