# -*- coding: utf-8 -*-
from django.db.models import Q
from django.core.files.base import ContentFile

import string, random, json
from datetime import datetime
from rest_framework.response import Response
from rest_framework import exceptions

from drfs import generate_serializer
from drfs.decorators import action
from drf_lbcms.helpers.cms import generate_default_notification_objs


class FakeUser(object):
    def __init__(self, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)



def code_generator(size=6, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def get_field_names_for_search(model, gdprRequest):
    fields = []
    # находим самую длинную строку для поиска
    max_length = max([
        len(o)
        for o in gdprRequest.emails + gdprRequest.phones
    ])
    for field in model._meta.fields:
        if field.__class__.__name__ in ['CharField', 'StringField', 'JSONField', 'EmbeddedOneModel', 'EmbeddedManyModel']:
            if field.max_length and field.max_length < max_length:
                # пропускаем поля, в которые точно не влезут искаемые значения
                continue
            if field.choices:
                # в таких полях уж точно нечего искать
                continue
            fields.append(field)
        if field.__class__.__name__ == 'EmailField':
            fields.append(field)
    return fields


def collect_info_for_request(gdrpRequest):
    # собираем из всех моделей данные согласно email, и телефону
    from django.apps import apps
    from django.contrib.auth import get_user_model

    BANNED_MODELS = [
        'cmsuser', 'cmssettings', 'cmstranslations', 'cmspdfdata',
        'cmspdfdata_files', 'cmsmailnotification', 'cmssmsnotification',
        'cmsceleryresult', 'cmscurrencydata', 'cmsgdprrequest'
    ]
    CmsUser = get_user_model()
    models = []
    # собираем модели из приложения, которые относятся к цмс и которые могут содержать данные
    for app, mdict in apps.all_models.items():
        for name, m in mdict.items():
            if name.startswith('cms') and name not in BANNED_MODELS:
                models.append(m)

    q = Q()
    for e in gdrpRequest.emails:
        q |= Q(email=e)
    for f in CmsUser._meta.fields:
        if f.name == 'accountData':
            for p in gdrpRequest.phones:
                q |= Q(accountData__icontains=p)
            break

    info = {
        'CmsUser': CmsUser.objects.filter(q)
    }

    search_data = gdrpRequest.emails + gdrpRequest.phones

    # обыскиваем модели по полям
    for model in models:
        q = Q()
        for field in model._meta.fields:
            if field.__class__.__name__ in ['CharField', 'StringField', 'JSONField', 'EmbeddedOneModel', 'EmbeddedManyModel']:
                for value in search_data:
                    if field.max_length and field.max_length < len(value):
                        # пропускаем поля, в которые точно не влезут искаемые значения
                        continue
                    if field.choices:
                        # в таких полях уж точно нечего искать
                        continue
                    _kwargs = {}
                    _kwargs[field.name+'__icontains'] = value
                    q |= Q(**_kwargs)
            if field.__class__.__name__ == 'EmailField':
                for value in gdrpRequest.emails:
                    _kwargs = {}
                    _kwargs[field.name] = value
                    q |= Q(**_kwargs)
        if q:
            info[model.__name__] = model.objects.filter(q)
    return info


def dump_queryset(queryset):
    model = queryset.model
    serializer = generate_serializer(model)
    ser = serializer(queryset, many=True)
    return ser.data







class CmsGdprRequestViewsetMixin(object):
    def get_queryset(self):
        queryset = super(CmsGdprRequestViewsetMixin, self).get_queryset()
        if not self.request:
            return queryset.none()
        return queryset

    def get_field_class(self, name):
        model_class = self.queryset.model
        for field in model_class._meta.get_fields():
            if field.name == name:
                return field.related_model
        return None



    @action(methods=['put'], detail=True)
    def send_verification_code(self, request, *args, **kwargs):
        from drf_lbcms.helpers.cms import notify_user
        data = request.data or {}
        instance = self.get_object()
        if not data.get('email', None) and not data.get('phone', None):
            raise exceptions.NotAcceptable()
        if instance.status != 'n':
            raise exceptions.NotAcceptable()

        instance.verificationCodes = instance.verificationCodes or {}
        generate_default_notification_objs()
        if data.get('email', None):
            user = FakeUser(email=data['email'], preferredLang=instance.preferredLang)
            code = code_generator()
            instance.verificationCodes[data['email']] = code
            notify_user(
                user,
                identifier='cms.gdprRequest.$verificationCode',
                context={
                    'code': code
                }
            )
        elif data.get('phone', None):
            user = FakeUser(accountData={'phones': [{'value': data['phone'], 'type': 'm'}]}, preferredLang=instance.preferredLang)
            code = code_generator()
            instance.verificationCodes[data['phone']] = code
            notify_user(
                user,
                identifier='cms.gdprRequest.$verificationCode',
                context={
                    'code': code
                }
            )
        instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)



    @action(methods=['put'], detail=True)
    def verify_code(self, request, *args, **kwargs):
        data = request.data or {}
        instance = self.get_object()
        if not data.get('email', None) and not data.get('phone', None):
            raise exceptions.NotAcceptable()
        if not data.get('code', None):
            raise exceptions.NotAcceptable()
        if instance.status != 'n':
            raise exceptions.NotAcceptable()

        generate_default_notification_objs()
        is_valid = False
        if data.get('email', None):
            if instance.verificationCodes.get(data['email']) == data['code']:
                instance.emails = instance.emails or []
                if data['email'] not in instance.emails:
                    instance.emails.append(data['email'])
                del instance.verificationCodes[data['email']]
                is_valid = True
        elif data.get('phone', None):
            if instance.verificationCodes.get(data['phone']) == data['code']:
                instance.phones = instance.phones or []
                if data['phone'] not in instance.phones:
                    instance.phones.append(data['phone'])
                del instance.verificationCodes[data['phone']]
                is_valid = True
        instance.save()
        return Response({'validated': is_valid})



    @action(methods=['put'], detail=True)
    def send_request(self, request, *args, **kwargs):
        from django.contrib.auth import get_user_model
        from drf_lbcms.helpers.cms import notify_user

        CmsUser = get_user_model()
        instance = self.get_object()
        identifier = "cms.gdprRequest.$%sRequest" % {
            'i': 'information',
            'a': 'anonimization',
            'd': 'deletion'
        }[instance.type]
        generate_default_notification_objs()

        q = Q(is_staff=True)
        for f in CmsUser._meta.fields:
            if f.name == 'userRole':
                q |= Q(userRole__in=['a', 'admin'])

        for admin in CmsUser.objects.filter(q):
            notify_user(
                admin,
                identifier=identifier,
                context={'instance': instance}
            )
        serializer = self.get_serializer(instance)
        return Response(serializer.data)



    @action(methods=['put'], detail=True)
    def collect_user_data(self, request, *args, **kwargs):
        from drf_lbcms.helpers.l10n import dump_json
        instance = self.get_object()
        # собираем инфу о пользователе
        info = collect_info_for_request(instance)
        data = {}
        has_data = False
        for k,v in info.items():
            data[k] = dump_queryset(v)
            # нам нужно проконтролировать что мы хоть что-то нашли
            has_data = has_data or bool(data[k])
        # загружаем ее в гуглесторадж
        if instance.infoFile:
            # удаляем старый файл
            instance.infoFile.delete_file_data()
            instance.infoFile.delete()
        # загружаем новый файл, если данные не пусты
        if has_data:
            L10nFile = self.get_field_class('infoFile')
            # генерируем файл
            data = dump_json(data)
            ffile = ContentFile(data)
            ffile.name = 'info-file-'+code_generator()+'.json'
            meta = {
                'size': ffile.size,
                'originalName': ffile.name,
                'type': 'application/json'
            }
            instance.infoFile = L10nFile(file_data=None, meta_data=meta)
            instance.infoFile.file_data = ffile
            instance.infoFile.save()
            instance.save()
        if request.query_params.get('sendEmail', None) in ['true', True]:
            # отправляем письма
            from drf_lbcms.helpers.cms import notify_user
            identifier = 'cms.gdprRequest.$informationResponse'
            instance.status = 'a'
            if not has_data:
                identifier = 'cms.gdprRequest.$emptyResponse'
                instance.status = 'd'
            for email in instance.emails:
                user = FakeUser(email=email, preferredLang=instance.preferredLang)
                notify_user(
                    user,
                    identifier=identifier,
                    context={
                        'instance': instance,
                        'now': datetime.now()
                    }
                )
            instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


    @action(methods=['put'], detail=True)
    def anonimize_user_data(self, request, *args, **kwargs):
        instance = self.get_object()
        # ищем данные
        info = collect_info_for_request(instance)
        search_data = list(set(instance.emails + instance.phones))
        for modelName, queryset in info.items():
            if modelName == 'CmsUser':
                for item in queryset.iterator():
                    item.isGdprProtected = True
                    item.save()
                continue
            fields = get_field_names_for_search(queryset.model, instance)
            # проходимся по каждому полю и заменяем строки
            for item in queryset.iterator():
                for field in fields:
                    data = getattr(item, field.name)
                    if isinstance(data, (dict, list)):
                        data = json.dumps(data)
                        for value in search_data:
                            if '@' in value:
                                replace = "protected-by-gdpr@"+ value.split('@')[-1]
                            else:
                                replace = "+phone-protected-by-gdpr"
                            data = replace.join(data.split(value))
                        data = json.loads(data)
                    else:
                        for value in search_data:
                            if '@' in value:
                                replace = "protected-by-gdpr@"+ value.split('@')[-1]
                            else:
                                replace = "+phone-protected-by-gdpr"
                            data = replace.join(data.split(value))
                    setattr(item, field.name, data)
                item.save()

        if request.query_params.get('sendEmail', None) in ['true', True]:
            # отправляем письма
            from drf_lbcms.helpers.cms import notify_user
            instance.status = 'a'
            instance.save()

            for email in instance.emails:
                user = FakeUser(email=email, preferredLang=instance.preferredLang)
                notify_user(
                    user,
                    identifier='cms.gdprRequest.$anonimizationResponse',
                    context={
                        'instance': instance
                    }
                )
        serializer = self.get_serializer(instance)
        return Response(serializer.data)



    @action(methods=['put'], detail=True)
    def delete_user_data(self, request, *args, **kwargs):
        instance = self.get_object()
        # ищем данные
        info = collect_info_for_request(instance)
        search_data = list(set(instance.emails + instance.phones))
        for modelName, queryset in info.items():
            if modelName == 'CmsUser':
                for item in queryset.iterator():
                    item.isGdprProtected = True
                    item.save()
                continue
            queryset.delete()

        if request.query_params.get('sendEmail', None) in ['true', True]:
            # отправляем письма
            from drf_lbcms.helpers.cms import notify_user
            instance.status = 'a'
            instance.save()

            for email in instance.emails:
                user = FakeUser(email=email, preferredLang=instance.preferredLang)
                notify_user(
                    user,
                    identifier='cms.gdprRequest.$deletionResponse',
                    context={
                        'instance': instance
                    }
                )
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
