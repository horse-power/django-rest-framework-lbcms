from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.contrib.auth.models import Permission, Group
from drfs import generate_serializer
from drf_lbcms.helpers.templates import validate_template


BaseCmsMailNotificationSerializer = generate_serializer('CmsMailNotification.json')
BaseCmsSmsNotificationSerializer = generate_serializer('CmsSmsNotification.json')
BaseCmsPdfDataSerializer = generate_serializer('CmsPdfData.json')

BasePermissionSerializer = generate_serializer(Permission, visible_fields=['id', 'name', 'codename', 'content_type'])
GroupSerializer = generate_serializer(Group, visible_fields=['id', 'name', 'permissions'])



class PermissionSerializer(BasePermissionSerializer):
    content_type = serializers.SerializerMethodField()

    def get_content_type(self, obj):
        return obj.content_type.app_label




class TemplateFieldsValidator(object):
    def __init__(self, fields):
        self.fields = fields

    def __get_value_by_field_name(self, obj, field):
        for f in field.split('.'):
            obj = obj.get(f, None) or {}
        return obj

    def __call__(self, obj):
        obj = obj or {}
        errors = {}
        for field in self.fields:
            data = self.__get_value_by_field_name(obj, field)
            for k,v in data.items():
                valid, e = validate_template(v or '')
                if not valid:
                    errors[field] = errors.get(field, {})
                    errors[field][k] = e
        if errors:
            raise ValidationError(errors)




class CmsMailNotificationSerializer(BaseCmsMailNotificationSerializer):
    class Meta:
        model = BaseCmsMailNotificationSerializer.Meta.model
        fields = BaseCmsMailNotificationSerializer.Meta.fields
        validators = getattr(BaseCmsMailNotificationSerializer.Meta, 'validators', []) + [
            TemplateFieldsValidator(['subject', 'body', 'echoBody', 'echoSubject'])
        ]



class CmsMailNotificationCustomSerializer(CmsMailNotificationSerializer):
    variables = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    send = serializers.SerializerMethodField()
    sendEcho = serializers.SerializerMethodField()

    def _get_parent(self, obj):
        if '|' not in obj.identifier:
            return obj
        identifier = obj.identifier.split('|')[1:]
        identifier = '|'.join(identifier)
        parents = BaseCmsMailNotificationSerializer.Meta.model.objects.filter(identifier=identifier)
        if parents:
            return parents[0]
        return None

    def get_variables(self, obj):
        parent = self._get_parent(obj)
        if not parent:
            return []
        return parent.variables or []

    def get_description(self, obj):
        parent = self._get_parent(obj)
        if not parent:
            return {}
        return parent.description or {}

    def get_send(self, obj):
        parent = self._get_parent(obj)
        if not parent:
            return True
        return parent.send

    def get_sendEcho(self, obj):
        parent = self._get_parent(obj)
        if not parent:
            return True
        return parent.sendEcho


    class Meta:
        model = CmsMailNotificationSerializer.Meta.model
        validators = CmsMailNotificationSerializer.Meta.validators
        fields = CmsMailNotificationSerializer.Meta.fields + [
            'variables'
        ]





class CmsSmsNotificationSerializer(BaseCmsSmsNotificationSerializer):
    class Meta:
        model = BaseCmsSmsNotificationSerializer.Meta.model
        fields = BaseCmsSmsNotificationSerializer.Meta.fields
        validators = getattr(BaseCmsSmsNotificationSerializer.Meta, 'validators', []) + [
            TemplateFieldsValidator(['subject', 'body', 'echoBody', 'echoSubject'])
        ]



class CmsSmsNotificationCustomSerializer(CmsSmsNotificationSerializer):
    variables = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    send = serializers.SerializerMethodField()
    sendEcho = serializers.SerializerMethodField()


    def _get_parent(self, obj):
        if '|' not in obj.identifier:
            return obj
        identifier = obj.identifier.split('|')[1:]
        identifier = '|'.join(identifier)
        parents = BaseCmsSmsNotificationSerializer.Meta.model.objects.filter(identifier=identifier)
        if not parents:
            return None
        return parents[0]

    def get_variables(self, obj):
        parent = self._get_parent(obj)
        if not parent:
            return []
        return parent.variables or []

    def get_description(self, obj):
        parent = self._get_parent(obj)
        if not parent:
            return {}
        return parent.description or {}

    def get_send(self, obj):
        parent = self._get_parent(obj)
        if not parent:
            return True
        return parent.send

    def get_sendEcho(self, obj):
        parent = self._get_parent(obj)
        if not parent:
            return True
        return parent.sendEcho

    class Meta:
        model = CmsSmsNotificationSerializer.Meta.model
        validators = CmsSmsNotificationSerializer.Meta.validators
        fields = CmsSmsNotificationSerializer.Meta.fields + [
            'variables'
        ]


class CmsPdfDataSerializer(BaseCmsPdfDataSerializer):
    class Meta:
        model = BaseCmsPdfDataSerializer.Meta.model
        fields = BaseCmsPdfDataSerializer.Meta.fields
        validators = getattr(BaseCmsPdfDataSerializer.Meta, 'validators', []) + [
            TemplateFieldsValidator(['title', 'content', 'runningTitles.header', 'runningTitles.footer'])
        ]
