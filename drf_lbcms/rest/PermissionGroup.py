
from rest_framework.response import Response
from django.contrib.auth.models import Permission
from django.db.models import Q
from drfs.decorators import action
from .serializers import PermissionSerializer



class PermissionGroupViewsetMixin(object):

    @action(methods=['get'], detail=False)
    def list_all_permissions(self, *args, **kwargs):
        permissions = Permission.objects.exclude(
            #Q(content_type__app_label__in=['auth', 'contenttypes', 'sites', 'sessions', 'authtoken', 'admin', 'dj_gmap']) |
            Q(content_type__app_label__in=['contenttypes', 'sites', 'sessions', 'authtoken', 'admin', 'dj_gmap']) |
            Q(content_type__app_label__in=['drf_lbcms'], content_type__model__in=['l10nfilelocal', 'l10nfile', 'cmsceleryresult']) |
            Q(codename__in=['delete_cmssettings', 'add_cmssettings', 'view_cmssettings'])
        )
        serializer = PermissionSerializer(permissions, many=True)
        return Response(serializer.data)
