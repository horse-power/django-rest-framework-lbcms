from drfs.schemas import AutoSchema as BaseAutoSchema


def field_from_serializer(serializer, field_name):
    for field in serializer.fields.values():
        if field.field_name == field_name:
            return field
    return None


class AutoSchema(BaseAutoSchema):

    def map_serializer(self, serializer):
        from .serializers import L10nFileBase

        result = super().map_serializer(serializer)
        if isinstance(serializer, L10nFileBase):
            return {
                "$ref": "#/components/schemas/node_modules/vue3-drf-lbcms/CmsApp/interfaces/L10nFile"
            }

        return result

