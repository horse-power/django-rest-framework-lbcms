# -*- coding: utf-8 -*-
from django.views.generic import TemplateView as BaseTemplateView
from django.views.generic.edit import FormView
from django.views.i18n import JavaScriptCatalog as BaseJavaScriptCatalog
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from django.conf import settings as djsettings
from django.contrib.auth import get_user_model
from django.utils import translation
from django.utils.translation import get_language
from django.utils.translation.trans_real import DjangoTranslation


from rest_framework.authtoken.models import Token
from rest_framework import viewsets
from rest_framework.settings import api_settings as drf_api_settings
from collections import OrderedDict


from drfs.decorators import action
from drfs import get_model

from .helpers.bulk import Bulk
from .helpers import get_site_settings, get_current_site
from .forms import ResetPasswordForm, ResetPasswordAndPhonesForm




_bulk_instance = None
class HelpersView(viewsets.ViewSet):
    schema = drf_api_settings.DEFAULT_SCHEMA_CLASS(
        tags=['Helpers']
    )
    def get_bulk_instance(self):
        global _bulk_instance
        if not _bulk_instance:
            _bulk_instance = Bulk()
            _bulk_instance.disabled_rest_points.append('Helpers')
        return _bulk_instance

    @action(methods=['post'], detail=False)
    def post_bulk(self, request, *args, **kwargs):
        bulk = self.get_bulk_instance()
        return bulk.bulk_view(request, *args, **kwargs)

    @action(methods=['get'], detail=False)
    def bulk(self, request, *args, **kwargs):
        bulk = self.get_bulk_instance()
        return bulk.bulk_view(request, *args, **kwargs)





class TemplateView(BaseTemplateView):
    def get_default_site(self):
        return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site'] = get_current_site(self.request) or self.get_default_site()
        context['settings'] = get_site_settings(site=context['site'])
        context['request'] = self.request
        context['state'] = {}
        if self.request.resolver_match:
            context['state']['current'] = {
                'name': self.request.resolver_match.url_name
            }
        if hasattr(djsettings, 'APP_CONTEXT'):
            context.update(djsettings.APP_CONTEXT)
        if 'LANGUAGE_CODE' not in context:
            context['LANGUAGE_CODE'] = translation.get_language()
        return context




class RobotsTxtView(View):
    def get(self, request, *args, **kwargs):
        settings = get_site_settings(request=request)
        text = getattr(settings, 'robotsTxt', '')
        return HttpResponse(text, content_type="text/plain")





class ResetPasswordAndPhonesView(FormView):
    '''
        для этой вьюхи нужно указывать template_name, success_url, dead_link_url
    '''
    def get_user(self, request):
        # пытаемся найти пользователя, которому нужно сбросить пароль
        if not request.user or not request.GET.get('control', None):
            return None
        user = None
        if not request.user.is_anonymous:
            user = request.user
        else:
            # пытаемся найти реального пользователя
            CmsUser = get_user_model()
            try:
                user = CmsUser.objects.get(username=request.GET.get('username'))
                token, created = Token.objects.get_or_create(user=user)
                if str(token) != request.GET.get('access_token'):
                    user = None
            except:
                pass

        hiddenAccountData = getattr(user, 'hiddenAccountData', None) or {}
        if hiddenAccountData.get('resetPassword', None) == request.GET.get('control'):
            # мы таки нашли нужного пользователя
            return user
        return None


    def get(self, request, *args, **kwargs):
        self.user = self.get_user(request)
        if not self.user:
            # подходящего пользователя нет, редиректим
            return HttpResponseRedirect(self.dead_link_url)
        return super(ResetPasswordAndPhonesView, self).get(request, *args, **kwargs)


    def post(self, request, *args, **kwargs):
        self.user = self.get_user(request)
        if not self.user:
            # подходящего пользователя нет, редиректим
            return HttpResponseRedirect(self.dead_link_url)
        return super(ResetPasswordAndPhonesView, self).post(request, *args, **kwargs)


    def get_initial(self):
        phones = []
        for p in self.user.accountData.get('phones', None) or []:
            if p and p.get('type', None) == 'm' and (p.get('value', None) or p.get('val', None)):
                phones.append(p.get('value', None) or p.get('val', None))
        return {
            'phones': ', '.join(phones)
        }


    def get_form_class(self):
        if getattr(self.user, 'loginProtection', None) == 's':
            # даем возможность и сменить телефоны
            return ResetPasswordAndPhonesForm
        return ResetPasswordForm

    def get_form(self, form_class=None):
        """
        Returns an instance of the form to be used in this view.
        """
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.user, **self.get_form_kwargs())

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.user = form.save(commit=True)
        return super(ResetPasswordAndPhonesView, self).form_valid(form)




class SitemapView(TemplateView):
    template_name = 'drf_lbcms/sitemap.xml'
    content_type = 'text/xml'

    def get_queryset_for_model(self, model):
        return model.objects.all()

    def get_context_data(self, **kwargs):
        context = super(SitemapView, self).get_context_data(**kwargs)
        # выгребаем все возможные модели, которые мы можем использовать для sitemap
        models = []
        for name in getattr(djsettings, 'DRF_LBCMS', {}).get('models_for_sitemap', []):
            try:
                models.append(get_model(name, latest=True))
            except:
                pass
        context['domain'] = self.request.build_absolute_uri('/')[:-1]
        root_hreflang = OrderedDict()
        initial_lang = translation.get_language()
        if len(context['settings'].cmsLangCodes) > 1:
            for lang in context['settings'].cmsLangCodes or []:
                root_hreflang[lang] = "/{lang}/".format(lang=lang)
        context['items'] = [
            {
                'location': "/{lang}/".format(lang=djsettings.APP_CONTEXT['CMS_DEFAULT_LANG']),
                'changefreq': "weekly",
                'priority': "1",
                'hreflang': root_hreflang
            }
        ]

        def add_url_from_item(item, method):
            if method == 'get_sitemap_items':
                urls = getattr(item, method)(
                    langs=context['settings'].cmsLangCodes,
                    defaultLang=djsettings.APP_CONTEXT['CMS_DEFAULT_LANG']
                ) or []
                context['items'] += urls
                return

            translation.activate(djsettings.APP_CONTEXT['CMS_DEFAULT_LANG'])
            url = getattr(item, method)(lang=djsettings.APP_CONTEXT['CMS_DEFAULT_LANG']) or {}
            url['hreflang'] = {}
            # теперь выгребаем hreflang-для этого же ресурса
            for lang in context['settings'].cmsLangCodes or []:
                if lang == djsettings.APP_CONTEXT['CMS_DEFAULT_LANG']:
                    continue
                # если метод хоть что-то вернул, то отображаем его в sitemap
                translation.activate(lang)
                lang_url = getattr(item, method)(lang=lang)
                if lang_url:
                    url['hreflang'][lang] = lang_url['location']

            if url.get('location', None):
                url['hreflang'][djsettings.APP_CONTEXT['CMS_DEFAULT_LANG']] = url['location']
            elif not url['hreflang']:
                return
            else:
                keys = list(url['hreflang'].keys())
                url['location'] = url['hreflang'][keys[0]]

            if len(url['hreflang'].keys()) == 1:
                # у нас только на 1 языке страница, так что игнорим hreflang
                url['hreflang'] = {}
            context['items'].append(url)


        for model in models:
            # ищем все методы у объекта, которые могут создать sitemap
            methods = [
                m
                for m in dir(model) if m.startswith('get_sitemap_item')
            ]
            if not methods:
                continue
            # выгребаем url-ы для sitemap
            for item in self.get_queryset_for_model(model).iterator():
                for m in methods:
                    add_url_from_item(item, m)

        translation.activate(initial_lang)
        return context




class RssView(TemplateView):
    template_name = 'drf_lbcms/rss.xml'
    content_type = 'application/xml'

    def get_queryset_for_model(self, model):
        return model.objects.all()

    def get_allowed_models(self):
        # выгребаем все возможные модели, которые мы можем использовать для rss
        models = []
        allowed = self.request.GET.get('models', "").split(',')
        for name in getattr(djsettings, 'DRF_LBCMS', {}).get('models_for_rss', []):
            if allowed and name not in allowed:
                continue
            try:
                models.append(get_model(name))
            except:
                pass
        return models

    def get_context_data(self, **kwargs):
        context = super(RssView, self).get_context_data(**kwargs)
        context['selfFullUrl'] = self.request.build_absolute_uri()
        context['domain'] = self.request.build_absolute_uri('/')[:-1]
        context['items'] = []

        lang = self.request.GET.get('language', None)
        if lang not in context['settings'].cmsLangCodes:
            if context['settings'].cmsLangCodes:
                lang = context['settings'].cmsLangCodes[0]
            else:
                lang = djsettings.APP_CONTEXT['CMS_DEFAULT_LANG']
        context['LANGUAGE_CODE'] = lang


        for model in self.get_allowed_models():
            # ищем все методы у объекта, которые могут создать sitemap
            methods = [
                m
                for m in dir(model) if m.startswith('get_rss_item')
            ]
            if not methods:
                continue
            # выгребаем url-ы для rss
            for item in self.get_queryset_for_model(model).iterator():
                for m in methods:
                    rss_item = getattr(item, m)(lang=lang, defaultLang=djsettings.APP_CONTEXT['CMS_DEFAULT_LANG'])
                    if rss_item:
                        context['items'].append(rss_item)

        return context





class JavaScriptCatalog(BaseJavaScriptCatalog):
    def get(self, request, *args, **kwargs):
        locale = request.GET.get('currentLanguage', None) or get_language()
        domain = kwargs.get("domain", self.domain)
        # If packages are not provided, default to all installed packages, as
        # DjangoTranslation without localedirs harvests them all.
        packages = kwargs.get("packages", "")
        packages = packages.split("+") if packages else self.packages
        paths = self.get_paths(packages) if packages else None
        self.translation = DjangoTranslation(locale, domain=domain, localedirs=paths)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
