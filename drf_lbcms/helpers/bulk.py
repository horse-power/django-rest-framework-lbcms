from django.test import RequestFactory
from django.contrib.auth.models import AnonymousUser
from rest_framework.response import Response
from rest_framework import exceptions
import json, re


from drf_ng_generator import schemas
from drf_ng_generator.converter import SchemaConverter
from drf_ng_generator.helpers import resolve_api_callback_by_name

try:
    from drf_lbcms.middleware import LbCmsQueryParamsMiddleware
except ImportError:
    class LbCmsQueryParamsMiddleware:
        def __init__(self, *args, **kwargs):
            pass
        def __call__(self, x):
            return x


class Bulk(object):
    disabled_rest_points = []
    allowed_http_methods = [None, 'get', 'head', 'options']
    error_msgs = {
        'malformed_json': "Malformed json string for query param '{property}'",
        'invalid_type': "Query param '{property}' should be <type 'dict'>, got - {type}",
        'no_such_rest_point': "Model(rest point) not found: '{rest_point}'",
        'not_in_allowed_http_methods': "You can't get data from rest point  '{rest_point}.{rest_action}', "+\
            "http method for this action is '{method}' "+\
            "and is not in allowed methods {allowed_methods}",
        'no_such_rest_action': "Rest action '{rest_action}' for point '{rest_point}' doesn't exists"
    }

    def __init__(self):
        generator = schemas.SchemaGenerator()
        schema = generator.get_schema(x_django_only=True)
        converter = SchemaConverter()
        self.rest_schema, base_api_url = converter.convert(schema, ignore_viewset_names=True)
        self.factory = RequestFactory()
        self.lb_middleware = LbCmsQueryParamsMiddleware(lambda x: x)

    def validate_value(self, value):
        try:
            bulk_query = json.loads(value or '{}')
        except:
            raise exceptions.ParseError(self.error_msgs['malformed_json'].format(
                property='data'
            ))

        if not isinstance(bulk_query, dict):
            raise exceptions.ParseError(self.error_msgs['invalid_type'].format(
                property='data',
                type=type(bulk_query)
            ))

        for rest_point, operations in bulk_query.items():
            if rest_point not in self.rest_schema:
                raise exceptions.NotAcceptable(
                    detail=self.error_msgs['no_such_rest_point'].format(
                        rest_point=rest_point
                    ),
                    code=500
                )
            if not isinstance(operations, dict):
                raise exceptions.NotAcceptable(self.error_msgs['invalid_type'].format(
                    property='data.'+rest_point,
                    type=type(operations)
                ))
        return bulk_query


    def resolve_api_callback_by_name(self, viewset_name, action_name):
        return resolve_api_callback_by_name(self.rest_schema, viewset_name, action_name)


    def create_fake_request(self, request, raw_url, method, query_params):
        fake_request_kwargs = {}
        url = raw_url
        url_params = re.findall(r":([A-Za-z0-9_]+)\/", raw_url)

        for p in url_params:
            if p in query_params:
                fake_request_kwargs[p] = query_params[p]

        if 'id' in fake_request_kwargs and 'pk' not in fake_request_kwargs:
            fake_request_kwargs['pk'] = fake_request_kwargs['id']

        for param_name, param_value in query_params.items():
            if not isinstance(param_value, str):
                param_value = str(param_value)
            url = re.sub(':'+param_name, param_value, url)

        # dump params to string
        for k,v in query_params.items():
            query_params[k] = json.dumps(v)

        fake_request = getattr(self.factory, method or 'get')(
            url,
            query_params
        )
        fake_request.user = getattr(request, 'user', AnonymousUser())
        fake_request._user = getattr(request, 'user', AnonymousUser())
        fake_request.get_host = request.get_host
        fake_request = self.lb_middleware(fake_request)

        if hasattr(request, '_request'):
            for prop in dir(request._request):
                value = getattr(request._request, prop)
                if not hasattr(fake_request, prop) and not callable(value):
                    try:
                        setattr(fake_request, prop, value)
                    except:
                        pass

        return fake_request, fake_request_kwargs


    def bulk_view(self, request, *args, **kwargs):
        bulk_query = self.validate_value(request.GET.get('data', None))
        res_data = {}
        res_error = {}

        for rest_point, operations in list(bulk_query.items()):
            if rest_point in self.disabled_rest_points:
                continue
            for action_name, params in list(operations.items()):
                if not isinstance(params, dict):
                    params = {}
                callback, raw_url, method = self.resolve_api_callback_by_name(rest_point, action_name)

                if method not in self.allowed_http_methods:
                    raise exceptions.NotAcceptable(self.error_msgs['not_in_allowed_http_methods'].format(
                        rest_point=rest_point,
                        rest_action=action_name,
                        method=method,
                        allowed_methods=self.allowed_http_methods
                    ))

                if not callback:
                    res_error[rest_point] = res_error.get(rest_point, {})
                    res_error[rest_point][action_name] = self.error_msgs['no_such_rest_action'].format(
                        rest_point=rest_point,
                        rest_action=action_name
                    )
                    continue

                fake_request, fake_request_kwargs = self.create_fake_request(
                    request,
                    raw_url,
                    method,
                    params
                )
                response = callback(fake_request, *[], **fake_request_kwargs)
                res_data[rest_point] = res_data.get(rest_point, {})
                res_data[rest_point][action_name] = response.data

        return Response({
            "data": res_data,
            "error": res_error
        }, status=200)
