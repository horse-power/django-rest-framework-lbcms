from django.conf import settings

def get_base_dir_as_string():
    if isinstance(settings.BASE_DIR, str):
        return settings.BASE_DIR
    return str(settings.BASE_DIR)
