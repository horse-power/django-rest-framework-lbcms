import shutil, os, sys, errno, six
from subprocess import Popen, PIPE


IS_TESTING_MODE = sys.argv[1:2] == ['test']



class ShellResponse:
    code = 0
    out = ''
    err = ''


def run_shell_command(command, args=[], shell=False):
    response = ShellResponse()
    arguments = list(args or [])
    arguments.insert(0, command)
    p = Popen(arguments, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=shell)
    response.out, response.err = p.communicate()
    response.code = p.returncode
    if not six.PY2:
        response.err = response.err.decode('utf-8')
        response.out = response.out.decode('utf-8')
    return response


def delete_file_or_dir(path, ignore_errors=True):
    if os.path.isfile(path):
        try:
            os.remove(path)
        except OSError:
            pass
    else:
        shutil.rmtree(path, ignore_errors=ignore_errors)



def mkdir(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise



def chown(path, user=None):
    if IS_TESTING_MODE:
        return
    from pwd import getpwnam
    from grp import getgrnam
    try:
        user_id = getpwnam(user)[2]
        group_id = getgrnam(user)[2]
    except:
        user_id = None
        group_id = None
    if user_id != None and group_id != None:
        os.chown(path, user_id, group_id)
