# -*- coding: utf-8 -*-
import mimetypes, os
import xlwt, csv
import datetime
import six
from django.http import HttpResponse
from wsgiref.util import FileWrapper
from unidecode import unidecode
from io import StringIO, BytesIO

try:
    # Python 2 forward compatibility
    range = xrange
except NameError:
    pass


UGLY_MIME_TYPES = {
    '.pdf': 'application/pdf'
}

def sanitize_filename(filename):
    filename = u'' + filename
    # убирает символы, которые могут не дать скачать файл для пользователя
    bad_chars = [
        '<', '>', ':', '"', "'", '/', '\\', '|', '?', '*',
        '\t', '\n', '\r', '\0', '%', '@', '!', '&', '=', '#', '^', ','
    ]
    for i in bad_chars:
        filename = filename.replace(i, '_', 10)
    filename = filename.translate(',/\\+.')
    filename = '_'.join(filename.split(' '))
    return unidecode(filename)




def file_to_response(file_pointer=None, file_data=None, file_name=None, as_bytes=False, auto_download=True):
    ext = os.path.splitext(file_name)[-1].lower()
    mime = mimetypes.guess_extension(ext)
    file_name = sanitize_filename(file_name)
    if not mime:
        mime = UGLY_MIME_TYPES.get(ext, None)
    disposition = 'attachment'
    if not auto_download:
        disposition = 'inline'

    if file_pointer:
        response = HttpResponse(content_type=mime)
        if auto_download:
            response['Content-Disposition'] = '%s; filename=%s' % (disposition, file_name)
        file_pointer.save(response)
    elif file_data:
        if as_bytes:
            file_data = BytesIO(file_data)
        else:
            file_data = StringIO(''+file_data)
        response = HttpResponse(FileWrapper(file_data), content_type=mime)
        if auto_download:
            response['Content-Disposition'] = '%s; filename=%s' % (disposition, file_name)

    return response




def csv_to_response(rows_data=None, file_name=None, formatters={}):
    file_ext = os.path.splitext(file_name)[-1].lower()
    formatters = formatters or {}
    if file_ext not in ['.csv', '.xls']:
        raise Exception(
            "Unknown file type {type} for csv_to_response function. Allowed are: csv, xls".format(
                type = file_ext[1:]
        ))
    file_name = sanitize_filename(file_name)
    array_formatter_separator = ', '
    footer = {}
    footer_settings = formatters.get('FOOTER', None)
    column_settings = formatters.get('COLUMN', None) or {}
    cell_settings = formatters.get('CELL', None) or {}
    # даем возможность менять стиль клеток
    def dummy_cell_pipe(value, row=None, column=None):
        return value
    cell_settings['valuePipe'] = cell_settings.get('valuePipe', None) or dummy_cell_pipe

    if formatters.get('None', None) == None:
        formatters['None'] = ''
    if formatters.get('Date', None) == None:
        formatters['Date'] = lambda d: d.strftime('%d/%m/%Y')
    if formatters.get('Datetime', None) == None:
        formatters['Datetime'] = lambda d: d.strftime('%d/%m/%Y, %H:%M')
    if formatters.get('int', None) == None:
        formatters['int'] = lambda _int: _int
    #    formatters['int'] = lambda _int: str(_int)
    if formatters.get('float', None) == None:
        formatters['float'] = lambda _float: round(_float, 2)
    #    formatters['float'] = lambda _float: "%.2f" % _float
    if formatters.get('bool', None) == None:
        formatters['bool'] = lambda _bool: str(_bool)
    if formatters.get('dict', None) == None:
        formatters['dict'] = lambda _dict: str(_dict)
    if isinstance(formatters.get('array', None), str):
        array_formatter_separator = formatters['array']
        formatters['array'] = None
    if formatters.get('array', None) == None:
        def array_formatter(arr):
            res = []
            for o in arr:
                res.append(normalize_cell(o))
            return u'' + array_formatter_separator.join(res)
        formatters['array'] = array_formatter


    def normalize_cell(o):
        if o == None:
            return formatters['None']
        if isinstance(o, datetime.datetime):
            return formatters['Datetime'](o)
        if isinstance(o, datetime.date):
            return formatters['Date'](o)
        if isinstance(o, bool):
            return formatters['bool'](o)
        if isinstance(o, six.integer_types):
            return formatters['int'](o)
        if isinstance(o, float):
            return formatters['float'](o)
        if isinstance(o, dict):
            return formatters['dict'](o)
        if isinstance(o, (list, tuple)):
            return formatters['array'](o)
        return str(o)



    def add_row_to_footer(row, rowIndex):
        if not footer_settings:
            return
        if rowIndex < footer_settings.get('startRowIndex', 0):
            return
        for k,v in footer_settings.get('columns', {}).items():
            if k in column_settings.get('hidden', []):
                continue
            if k not in footer:
                footer[k] = []
            if len(row) >= k:
                footer[k].append(row[k])


    def get_footer_row():
        if not footer_settings:
            return []
        indexes = list(footer_settings.get('formatters', {}).keys()) + list(footer_settings.get('columns', {}).keys())
        indexes = list(set(indexes))
        indexes.sort()
        row = [None] * (max(indexes) + 1)

        for j in indexes:
            if j in column_settings.get('hidden', []):
                continue
            formatter_str = footer_settings.get('formatters', {}).get(j, None) or "%s"
            if '%' not in formatter_str:
                row[j] = formatter_str
            else:
                footer_cell_fn = footer_settings.get('columns', {}).get(j, None)
                if not footer_cell_fn:
                    footer_cell_fn = lambda x: None
                if footer_cell_fn == sum:
                    # декорируем sum, т.к. она не умеет слаживать None c числами
                    footer_cell_fn = lambda lst: sum([e for e in lst if e])
                value = footer_cell_fn(footer.get(j, None))
                row[j] = formatter_str % normalize_cell(value)
        return row

    def get_cell_value_len(value):
        if isinstance(value, six.string_types) or isinstance(value, six.text_type):
            return len(value)
        return len(str(value))

    style_cache = {}
    def get_xls_cell_style(style_text):
        if not style_cache.get(style_text, None):
            style_cache[style_text] = xlwt.easyxf(style_text)
        return style_cache[style_text]


    if file_ext == '.csv':
        response = HttpResponse(content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=%s' % file_name
        writer = csv.writer(response)
        i = 0
        for row in rows_data:
            add_row_to_footer(row, i)
            i += 1
            _row = []
            j = 0
            for cell in row:
                if j in column_settings.get('hidden', []):
                    j += 1
                    continue
                cell = cell_settings['valuePipe'](cell, row=i, column=j)
                if isinstance(cell, dict) and '$value' in cell:
                    cell = cell['$value']
                if not isinstance(cell, six.text_type):
                    cell = normalize_cell(cell)
                _row.append(cell)
                j += 1
            writer.writerow(_row)
        footer_row = get_footer_row()
        if footer_row:
            row = []
            for j in range(len(footer_row)):
                if j not in column_settings.get('hidden', []):
                    row.append(footer_row[j])
            writer.writerow(row)
        return response

    elif file_ext == '.xls':
        wb = xlwt.Workbook()
        ws = wb.add_sheet('Sheet')
        i = 0
        for row in rows_data:
            add_row_to_footer(row, i)
            j = 0
            realJ = 0
            for cell in row:
                if j in column_settings.get('hidden', []):
                    j += 1
                    continue
                cell = cell_settings['valuePipe'](cell, row=i, column=j)
                if isinstance(cell, dict) and '$style' in cell and '$value' in cell and len(cell.keys()) == 2:
                    cell_style = get_xls_cell_style(cell['$style'])
                    cell = cell['$value']
                else:
                    cell_style = None
                if not isinstance(cell, six.text_type):
                    cell = normalize_cell(cell)
                if six.PY2 and isinstance(cell, str):
                    cell = u"" + cell
                if cell_style:
                    ws.write(i, realJ, cell, style=cell_style)
                else:
                    ws.write(i, realJ, cell)
                cwidth = ws.col(realJ).width
                if (get_cell_value_len(cell)*256) > cwidth:
                    #(Modify column width to match biggest data in that column)
                    width = len(cell)*256
                    if width >= 65536:
                        width = 65535
                    ws.col(realJ).width = width
                j += 1
                realJ += 1
            i+=1

        footer_row = get_footer_row()
        if footer_row:
            j = 0
            realJ = 0
            for cell in footer_row:
                if j in column_settings.get('hidden', []):
                    j += 1
                    continue
                cell = cell_settings['valuePipe'](cell, row=i, column=j)
                if isinstance(cell, dict) and '$style' in cell and '$value' in cell and len(cell.keys()) == 2:
                    cell_style = get_xls_cell_style(cell['$style'])
                    cell = cell['$value']
                else:
                    cell_style = None
                if cell_style:
                    ws.write(i, realJ, cell, style=cell_style)
                else:
                    ws.write(i, realJ, cell)
                j += 1
                realJ += 1

        response = HttpResponse(content_type="application/ms-excel")
        response['Content-Disposition'] = 'attachment; filename=%s' % file_name
        wb.save(response)
        return response
