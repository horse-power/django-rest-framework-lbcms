# -*- coding: utf-8 -*-
import datetime, re
from currency_converter import CurrencyConverter, RateNotFoundError
from django.utils.formats import number_format
from django.utils.encoding import force_text
from django.conf import settings
from decimal import Decimal


CONVERTER = None


def convert_ecb(value, from_currency, to_currency, date=None):
    global CONVERTER

    if isinstance(date, datetime.datetime):
        date = date.date()
    if not isinstance(date, datetime.date):
        date = datetime.datetime.now().date()

    c = CONVERTER or CurrencyConverter('http://www.ecb.int/stats/eurofxref/eurofxref-hist.zip')
    if c.bounds[to_currency][-1] < date:
        CONVERTER = CurrencyConverter('http://www.ecb.int/stats/eurofxref/eurofxref-hist.zip')
        c = CONVERTER
    # по выходным банки не обновляют значения по валютам
    # берем последнее доступное значение
    if c.bounds[to_currency][-1] < date:
        date = c.bounds[to_currency][-1]
    # конвертируем и округляем результат.
    # из-за хранения дробного числа в виде степени двойки,
    # имеет смысл хранить результат в Decimal
    try:
        converted_value = c.convert(value, from_currency, to_currency, date=date)
        rate_from = c._get_rate(from_currency, date)
        rate_to = c._get_rate(to_currency, date)
    except RateNotFoundError:
        # если не можем перевести валюту нужным днем, то отнимаем по 1 дню
        # до победного конца
        date = date - datetime.timedelta(days=1)
        converted_value, rate_from, rate_to = convert_ecb(
            value,
            from_currency=from_currency,
            to_currency=to_currency,
            date=date
        )
    return converted_value, rate_from, rate_to


def convert_db(value, from_currency, to_currency, date=None):
    from drf_lbcms.models import CmsCurrencyData

    if isinstance(date, datetime.datetime):
        date = date.date()
    if not isinstance(date, datetime.date):
        date = datetime.datetime.now().date()

    cdata = CmsCurrencyData.objects.filter(date__lte=date)
    if from_currency != 'EUR':
        kw = {}
        kw[from_currency] = None
        cdata = cdata.exclude(**kw)
    if to_currency != 'EUR':
        kw = {}
        kw[to_currency] = None
        cdata = cdata.exclude(**kw)
    if not cdata.count():
        raise RateNotFoundError('{0} has no rate for {1}'.format(to_currency, date))

    def get_rate(currencyData, _currency):
        if _currency == 'EUR':
            return 1
        return getattr(currencyData, _currency)

    for c in cdata.order_by('-date').iterator():
        rate_from = get_rate(c, from_currency)
        rate_to = get_rate(c, to_currency)
        if rate_to > 0 and rate_from > 0:
            break
    if not rate_to:
        raise RateNotFoundError('{0} has no rate for {1}'.format(to_currency, date))
    if not rate_from:
        raise RateNotFoundError('{0} has no rate for {1}'.format(from_currency, date))
    converted_value = float(value) / rate_from * rate_to
    return converted_value, rate_from, rate_to




def convert(value, from_currency, to_currency, date=None, currencyData={}, precision=None):
    if precision is None:
        precision = 3

    if not value:
        return 0
    if from_currency == to_currency:
        return round(value, precision)

    if currencyData == None:
        currencyData = {}

    if isinstance(date, datetime.datetime):
        date = date.date()
    if not isinstance(date, datetime.date):
        date = datetime.datetime.now().date()

    def get_rate(_currency):
        if _currency == 'EUR':
            return 1
        return currencyData[date.isoformat()].get(_currency, None) or 0

    if date.isoformat() in currencyData.keys():
        rate_from = get_rate(from_currency)
        rate_to = get_rate(to_currency)
        if rate_from > 0 and rate_to > 0:
            converted_value = float(value) / rate_from * rate_to
            return round(converted_value, precision)

    data_source = getattr(settings, 'DRF_LBCMS', {}).get('CURRENCY_DATA_SOURCE', None) or getattr(settings, 'DRF_LBCMS', {}).get('currency_data_source', None)
    if data_source == 'db':
        converted_value, rate_from, rate_to = convert_db(
            value,
            from_currency,
            to_currency, date=date
        )
    else:
        converted_value, rate_from, rate_to = convert_ecb(
            value,
            from_currency,
            to_currency,
            date=date
        )
    if not currencyData.get(date.isoformat, {}).get(from_currency, None) and not currencyData.get(date.isoformat, {}).get(to_currency, None):
        cache = currencyData.get(date.isoformat(), None) or {}
        cache[from_currency] = rate_from
        cache[to_currency] = rate_to
        currencyData[date.isoformat()] = cache
    return round(converted_value, precision)







def human_currency(value, from_currency=None, to_currency=None, fraction_size=2, cms_settings=None, user=None, date=None):
    #hcConf = cmsFiltersConfig.humanCurrency or {}
    if getattr(cms_settings, 'defaultCurrency', None) and not from_currency:
        from_currency = cms_settings.defaultCurrency
    if not from_currency:
        raise Exception("Please provide 'from_currency' value")
    if not to_currency:
        to_currency = from_currency
    from_currency = from_currency.upper()
    to_currency = to_currency.upper()
    hcConf = {
        'useConverter': True
    }

    def convert_value():
        preferred_currency = to_currency or getattr(user,'preferred_currency', None)
        if not hcConf['useConverter']:
            return {
                'symbol': preferred_currency,
                'value': value
            }
        if preferred_currency == from_currency:
            return {
                'symbol': preferred_currency,
                'value': value
            }
        if preferred_currency:
            # валюта выбранная пользователем
            # тут нужно сконвертировать цену
            cms_converter = getattr(cms_settings, 'defaultCurrencyConverter', {}) or {}
            # если указано в админке как конвертировать валюту
            if cms_converter.get(from_currency+"."+to_currency, None):
                k = float(cms_converter[from_currency+"."+to_currency])
                return {
                    'symbol': preferred_currency,
                    'value': k*value
                }
            # иначе тянем из банка
            return {
                'symbol': preferred_currency,
                'value': convert(value, from_currency, preferred_currency, date=date)
            }
    def intcomma(v):
        """
        Converts an integer to a string containing commas every three digits.
        For example, 3000 becomes '3,000' and 45000 becomes '45,000'.
        # from django.contrib.humanize
        """
        if settings.USE_L10N:
            try:
                if not isinstance(v, (float, Decimal)):
                    v = int(v)
            except (TypeError, ValueError):
                return intcomma(v)
            else:
                return number_format(v, force_grouping=True)
        orig = force_text(v)
        new = re.sub(r"^(-?\d+)(\d{3})", r'\g<1>,\g<2>', orig)
        if orig == new:
            return new
        else:
            return intcomma(new)

    if not value:
        return ''

    info = convert_value()
    value = info['value']

    # теперь занимаемся форматированием
    if info['symbol'] == 'CZK':
        info['symbol'] = u'Kč'
    elif info['symbol'] == 'USD':
        info['symbol'] = u'$'
    elif info['symbol'] == 'EUR':
        info['symbol'] = u'€'

    if value % 1 == 0:
        fraction_size = 0
    value = round(value, fraction_size)

    if info['symbol'] == u'$':
        return u'$' + intcomma(value)
    else:
        return intcomma(value) + ' ' + info['symbol']
