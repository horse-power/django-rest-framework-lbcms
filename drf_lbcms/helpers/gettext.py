# -*- coding: utf-8 -*-
import polib
import glob
import os
import six


def collect_po_files(dir_path):
    po_files = []
    if not os.path.isdir(dir_path):
        return po_files
    for f in os.listdir(dir_path):
        fpath = os.path.join(dir_path, f)
        if os.path.isdir(fpath):
            po_files += collect_po_files(fpath)
        elif os.path.splitext(f)[-1] in ['.po', '.pot']:
            po_files.append(fpath)
    return po_files


def make_unsafe_string(string):
    unsafe = u""
    for ch in string:
        try:
            unsafe += ch.encode('unicode', 'xmlcharrefreplace')
        except:
            unsafe += ch
    return unsafe



def _is_expression(text):
    if not text.startswith('{{') or not text.endswith('}}'):
        return False
    return text.count('{{') == 1 and text.count('}}') == 1



def _fix_references(references):
    result = []
    for r in references:
        if "auto_constants.js" in r:
            r = r.split('/')
            r[1] = 'auto_constants'
            r = '/'.join(r[1:])
        result.append(r)
    return result



def _msg_get_id(item):
    id = item['msgId']
    if item.get('msgCtxt', None):
        id += item['msgCtxt']
    return id



def _normalize_translation_msgs(msgs):
    normalized = []
    ids = []
    for m in msgs:
        id = _msg_get_id(m)
        if id in ids:
            continue
        ids.append(id)
        m['references'] = m.get('references', None) or []
        normalized.append(m)
    return normalized




def pot_to_json(filePath, options={}):
    '''
        преобразовывает *.pot файлы в CmsTranslations-подобные объекты
        @param {string | list<string>} путь к *.pot файлу или список путей
            (можно передавать glob-паттерны)
        @returns {dict} CmsTranslations-подобный объект
    '''
    if isinstance(filePath, six.string_types) or isinstance(filePath, six.text_type):
        filePath = [filePath]

    po_files = []

    for d in filePath:
        po_files += collect_po_files(d)
    po_files = list(set(po_files))

    if not po_files:
        for f in filePath:
            po_files += glob.glob(f)

    pot_data = []
    for f in po_files:
        ext = os.path.splitext(f)[-1].lower()
        if ext in ['.pot', '.po']:
            pot_data.append(
                polib.pofile(f)
            )

    msgs = []
    for catalog in pot_data:
        res = pot_file_to_json(catalog, options)
        msgs += res['msgs']
    return {
        'msgs': _normalize_translation_msgs(msgs)
    }




def pot_file_to_json(catalog, options={}):
    msgs = []
    for item in catalog:
        if _is_expression(item.msgid):
            continue
        obj = {'msgId': make_unsafe_string(item.msgid), 'msgStr': []}
        if item.msgid_plural:
            obj['msgIdPlural'] = make_unsafe_string(item.msgid_plural)
        if item.msgctxt:
            obj['msgCtxt'] = item.msgctxt
        if item.comment:
            obj['msgComm'] = ' '.join(item.comment.split('\n'))
        if options.get('withReferences', False):
            obj['references'] = [
                ':'.join(o)
                for o in item.occurrences or []
            ]
        if options.get('withMsgStr', False):
            if item.msgstr_plural:
                obj['msgStr'] = [
                    make_unsafe_string(item.msgstr_plural[m])
                    for m in item.msgstr_plural
                ]
            else:
                obj['msgStr'] = [make_unsafe_string(item.msgstr)]
        msgs.append(obj)
    return {'msgs': msgs}




def merge_translation_msgs(originalMsgs, newMsgs):
    def update_property(_original, _new, field):
        if _original.get(field, None) and _new.get(field, None):
            _original[field] = _new[field]
        elif _original.get(field, None) and not _new.get(field, None):
            del _original[field]
        elif not _original.get(field, None) and _new.get(field, None):
            _original[field] = _new[field]
        return _original

    allowedIds = [
        _msg_get_id(item)
        for item in newMsgs
    ]
    existedIds = []
    processedNewMsgs = {}

    for item in newMsgs:
        id = _msg_get_id(item)
        processedNewMsgs[id] = item

    i = 0
    for item in originalMsgs:
        id = _msg_get_id(item)
        if id not in allowedIds:
            originalMsgs[i]['hidden'] = True
        else:
            existedIds.append(id)
            originalMsgs[i]['hidden'] = False
            for prop in ['msgIdPlural', 'msgCtxt', 'msgComm']:
                originalMsgs[i] = update_property(
                    originalMsgs[i],
                    processedNewMsgs[id],
                    prop
                )
            originalMsgs[i]['msgStr'] = originalMsgs[i].get('msgStr', [])
            originalMsgs[i]['references'] = _fix_references(
                processedNewMsgs[id].get('references', None) or []
            )
        i += 1
    for id in allowedIds:
        if id not in existedIds:
            originalMsgs.append(processedNewMsgs[id])
            originalMsgs[-1]['hidden'] = False

    return _normalize_translation_msgs(originalMsgs)




def model_to_gettext_json(modelInstance):
    def getTr(msg):
        if msg.get('msgIdPlural', None):
            return msg.get('msgStr')
        if len(msg.get('msgStr', [])):
            return msg['msgStr'][0]
        return None

    def isTrEmpty(tr):
        if isinstance(tr, six.text_type):
            return len(tr) == 0
        if isinstance(tr, (list, tuple)):
            for o in tr:
                if len(o):
                    return False
        True

    gjson = {}
    for msg in modelInstance.msgs:
        if not msg.get('msgId', None):
            continue
        tr = ""
        if msg['msgId'] in gjson:
            tr = gjson[msg['msgId']]
        if msg.get('msgIdPlural', None) or msg.get('msgCtxt', None):
            if isinstance(tr, six.text_type) and len(tr):
                tr = {"":tr}
            elif not isinstance(tr, dict):
                tr = {}
        _tr = getTr(msg)
        if isTrEmpty(_tr):
            continue
        if msg.get('msgCtxt', None):
            tr[msg['msgCtxt']] = _tr
        else:
            if _tr is None:
                continue
            if isinstance(tr, six.text_type):
                tr = _tr
            else:
                tr[''] = _tr
        gjson[msg['msgId']] = tr

    data = {}
    data[modelInstance.lang] = gjson
    return data




def generate_gettext_dump_and_upload(cmsTranslateInstance):
    from django.core.files.storage import default_storage
    from django.core.files.base import ContentFile
    from . import l10n

    gettext_json = model_to_gettext_json(cmsTranslateInstance)
    gettext_dump = l10n.dump_json(gettext_json)
    filename = "languages-" + (cmsTranslateInstance.lang or '--') + ".json"
    try:
        default_storage.delete(filename)
    except:
        pass
    try:
        default_storage.save(filename, ContentFile(gettext_dump))
    except:
        print("!!! Failed to upload translation file")


def get_plural_form(lang_code):
    # https://docs.translatehouse.org/projects/localization-guide/en/latest/l10n/pluralforms.html
    forms = {
        "ach": "nplurals=2; plural=(n > 1);",
        "af": "nplurals=2; plural=(n != 1);",
        "ak": "nplurals=2; plural=(n > 1);",
        "am": "nplurals=2; plural=(n > 1);",
        "an": "nplurals=2; plural=(n != 1);",
        "anp": "nplurals=2; plural=(n != 1);",
        "ar": "nplurals=6; plural=(n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5);",
        "arn": "nplurals=2; plural=(n > 1);",
        "as": "nplurals=2; plural=(n != 1);",
        "ast": "nplurals=2; plural=(n != 1);",
        "ay": "nplurals=1; plural=0;",
        "az": "nplurals=2; plural=(n != 1);",
        "be": "nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);",
        "bg": "nplurals=2; plural=(n != 1);",
        "bn": "nplurals=2; plural=(n != 1);",
        "bo": "nplurals=1; plural=0;",
        "br": "nplurals=2; plural=(n > 1);",
        "brx": "nplurals=2; plural=(n != 1);",
        "bs": "nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);",
        "ca": "nplurals=2; plural=(n != 1);",
        "cgg": "nplurals=1; plural=0;",
        "cs": "nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;",
        "csb": "nplurals=3; plural=(n==1) ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;",
        "cy": "nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;",
        "da": "nplurals=2; plural=(n != 1);",
        "de": "nplurals=2; plural=(n != 1);",
        "doi": "nplurals=2; plural=(n != 1);",
        "dz": "nplurals=1; plural=0;",
        "el": "nplurals=2; plural=(n != 1);",
        "en": "nplurals=2; plural=(n != 1);",
        "eo": "nplurals=2; plural=(n != 1);",
        "es": "nplurals=2; plural=(n != 1);",
        "es_AR": "nplurals=2; plural=(n != 1);",
        "et": "nplurals=2; plural=(n != 1);",
        "eu": "nplurals=2; plural=(n != 1);",
        "fa": "nplurals=2; plural=(n > 1);",
        "ff": "nplurals=2; plural=(n != 1);",
        "fi": "nplurals=2; plural=(n != 1);",
        "fil": "nplurals=2; plural=(n > 1);",
        "fo": "nplurals=2; plural=(n != 1);",
        "fr": "nplurals=2; plural=(n > 1);",
        "fur": "nplurals=2; plural=(n != 1);",
        "fy": "nplurals=2; plural=(n != 1);",
        "ga": "nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : (n>2 && n<7) ? 2 :(n>6 && n<11) ? 3 : 4;",
        "gd": "nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;",
        "gl": "nplurals=2; plural=(n != 1);",
        "gu": "nplurals=2; plural=(n != 1);",
        "gun": "nplurals=2; plural=(n > 1);",
        "ha": "nplurals=2; plural=(n != 1);",
        "he": "nplurals=2; plural=(n != 1);",
        "hi": "nplurals=2; plural=(n != 1);",
        "hne": "nplurals=2; plural=(n != 1);",
        "hr": "nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);",
        "hu": "nplurals=2; plural=(n != 1);",
        "hy": "nplurals=2; plural=(n != 1);",
        "ia": "nplurals=2; plural=(n != 1);",
        "id": "nplurals=1; plural=0;",
        "is": "nplurals=2; plural=(n%10!=1 || n%100==11);",
        "it": "nplurals=2; plural=(n != 1);",
        "ja": "nplurals=1; plural=0;",
        "jbo": "nplurals=1; plural=0;",
        "jv": "nplurals=2; plural=(n != 0);",
        "ka": "nplurals=1; plural=0;",
        "kk": "nplurals=2; plural=(n != 1);",
        "kl": "nplurals=2; plural=(n != 1);",
        "km": "nplurals=1; plural=0;",
        "kn": "nplurals=2; plural=(n != 1);",
        "ko": "nplurals=1; plural=0;",
        "ku": "nplurals=2; plural=(n != 1);",
        "kw": "nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n == 3) ? 2 : 3;",
        "ky": "nplurals=2; plural=(n != 1);",
        "lb": "nplurals=2; plural=(n != 1);",
        "ln": "nplurals=2; plural=(n > 1);",
        "lo": "nplurals=1; plural=0;",
        "lt": "nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);",
        "lv": "nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);",
        "mai": "nplurals=2; plural=(n != 1);",
        "me": "nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;",
        "mfe": "nplurals=2; plural=(n > 1);",
        "mg": "nplurals=2; plural=(n > 1);",
        "mi": "nplurals=2; plural=(n > 1);",
        "mk": "nplurals=2; plural= n==1 || n%10==1 ? 0 : 1; Can’t be correct needs a 2 somewhere",
        "ml": "nplurals=2; plural=(n != 1);",
        "mn": "nplurals=2; plural=(n != 1);",
        "mni": "nplurals=2; plural=(n != 1);",
        "mnk": "nplurals=3; plural=(n==0 ? 0 : n==1 ? 1 : 2);",
        "mr": "nplurals=2; plural=(n != 1);",
        "ms": "nplurals=1; plural=0;",
        "mt": "nplurals=4; plural=(n==1 ? 0 : n==0 || ( n%100>1 && n%100<11) ? 1 : (n%100>10 && n%100<20 ) ? 2 : 3);",
        "my": "nplurals=1; plural=0;",
        "nah": "nplurals=2; plural=(n != 1);",
        "nap": "nplurals=2; plural=(n != 1);",
        "nb": "nplurals=2; plural=(n != 1);",
        "ne": "nplurals=2; plural=(n != 1);",
        "nl": "nplurals=2; plural=(n != 1);",
        "nn": "nplurals=2; plural=(n != 1);",
        "no": "nplurals=2; plural=(n != 1);",
        "nso": "nplurals=2; plural=(n != 1);",
        "oc": "nplurals=2; plural=(n > 1);",
        "or": "nplurals=2; plural=(n != 1);",
        "pa": "nplurals=2; plural=(n != 1);",
        "pap": "nplurals=2; plural=(n != 1);",
        "pl": "nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);",
        "pms": "nplurals=2; plural=(n != 1);",
        "ps": "nplurals=2; plural=(n != 1);",
        "pt": "nplurals=2; plural=(n != 1);",
        "pt_BR": "nplurals=2; plural=(n > 1);",
        "rm": "nplurals=2; plural=(n != 1);",
        "ro": "nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);",
        "ru": "nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);",
        "rw": "nplurals=2; plural=(n != 1);",
        "sah": "nplurals=1; plural=0;",
        "sat": "nplurals=2; plural=(n != 1);",
        "sco": "nplurals=2; plural=(n != 1);",
        "sd": "nplurals=2; plural=(n != 1);",
        "se": "nplurals=2; plural=(n != 1);",
        "si": "nplurals=2; plural=(n != 1);",
        "sk": "nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;",
        "sl": "nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);",
        "so": "nplurals=2; plural=(n != 1);",
        "son": "nplurals=2; plural=(n != 1);",
        "sq": "nplurals=2; plural=(n != 1);",
        "sr": "nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);",
        "su": "nplurals=1; plural=0;",
        "sv": "nplurals=2; plural=(n != 1);",
        "sw": "nplurals=2; plural=(n != 1);",
        "ta": "nplurals=2; plural=(n != 1);",
        "te": "nplurals=2; plural=(n != 1);",
        "tg": "nplurals=2; plural=(n > 1);",
        "th": "nplurals=1; plural=0;",
        "ti": "nplurals=2; plural=(n > 1);",
        "tk": "nplurals=2; plural=(n != 1);",
        "tr": "nplurals=2; plural=(n > 1);",
        "tt": "nplurals=1; plural=0;",
        "ug": "nplurals=1; plural=0;",
        "uk": "nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);",
        "ur": "nplurals=2; plural=(n != 1);",
        "uz": "nplurals=2; plural=(n > 1);",
        "vi": "nplurals=1; plural=0;",
        "wa": "nplurals=2; plural=(n > 1);",
        "wo": "nplurals=1; plural=0;",
        "yo": "nplurals=2; plural=(n != 1);",
        "zh": "nplurals=2; plural=(n > 1);"
    }

    if lang_code in forms:
        return forms[lang_code]
    lang_code = lang_code.split('_')[0].split('-')[0]
    if lang_code in forms:
        return forms[lang_code]

    return ''
