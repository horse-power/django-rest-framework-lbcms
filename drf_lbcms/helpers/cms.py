# -*- coding: utf-8 -*-
import os, json
from django.conf import settings as djsettings
from drfs import get_model
from django.core.exceptions import FieldDoesNotExist




CMS_SETTINGS_SITE_FIELD_NAME = None
try:
    CmsSettings = get_model('CmsSettings')
except:
    CmsSettings = None

try:
    if CmsSettings._meta.get_field('_site'):
        CMS_SETTINGS_SITE_FIELD_NAME = '_site'
except FieldDoesNotExist:
    try:
        if CmsSettings._meta.get_field('site'):
            CMS_SETTINGS_SITE_FIELD_NAME = 'site'
    except FieldDoesNotExist:
        pass


def get_site_settings(site):
    if not CmsSettings:
        return None
    kwargs = {}
    if CMS_SETTINGS_SITE_FIELD_NAME:
        kwargs[CMS_SETTINGS_SITE_FIELD_NAME] = site
    try:
        return CmsSettings.objects.filter(**kwargs)[0]
    except:
        return None


def is_notify_identifier_enabled(default_settings, user_settings, identifier):
    identifier = identifier.split('|')[-1]
    # если нет объекта 'enabled' в настройках сайта, то считаем, что все уведомления
    # разрешены
    if 'enabled' not in default_settings:
        return True
    enabled = default_settings['enabled'] or []
    # админ не разрешил этот тип уведомлений для пользователя
    if identifier not in enabled:
        return False
    # админ разрешил этот тип уведомлений и пользователь не отключил его
    if user_settings.get(identifier, True):
        return True
    # админ разрешил этот тип уведомлений, но пользователь отключил его
    return False



def notify_user(user, context={}, identifier=None, preferred_lang=None, attachments=None, email_connection=None):
    from drf_lbcms.models import CmsSmsNotification, CmsMailNotification

    if isinstance(identifier, (list, tuple)):
        identifiers = identifier
    else:
        identifiers = [identifier]
    main_mail_notify_instance = None
    main_sms_notify_instance = None
    mail_notify_instance = None
    sms_notify_instance = None
    site = getattr(user, '_site', None)
    # находим основной ("последний в списке") шаблон уведомлений
    try:
        main_mail_notify_instance = CmsMailNotification.objects.get(identifier=identifiers[-1], _site=site)
    except CmsMailNotification.DoesNotExist:
        pass
    try:
        main_sms_notify_instance = CmsSmsNotification.objects.get(identifier=identifiers[-1], _site=site)
    except CmsSmsNotification.DoesNotExist:
        pass
    # теперь пытаемся искать с начала
    for identifier in identifiers:
        try:
            mail_notify_instance = CmsMailNotification.objects.get(identifier=identifier, _site=site)
            # мы нашли доступный email-шаблон. теперь перезаписываем свойства из основного
            if main_mail_notify_instance:
                for prop in ['send', 'sendEcho', 'emailTemplateName', 'fromEmail']:
                    setattr(
                        mail_notify_instance,
                        prop,
                        getattr(main_mail_notify_instance, prop, None)
                    )
            break
        except CmsMailNotification.DoesNotExist:
            pass
    for identifier in identifiers:
        try:
            sms_notify_instance = CmsSmsNotification.objects.get(identifier=identifier, _site=site)
            # мы нашли доступный email-шаблон. теперь перезаписываем свойства из основного
            if main_sms_notify_instance:
                for prop in ['send', 'sendEcho']:
                    setattr(
                        sms_notify_instance,
                        prop,
                        getattr(main_sms_notify_instance, prop, None)
                    )
            break
        except CmsSmsNotification.DoesNotExist:
            pass

    settings = get_site_settings(site=site)
    mail_enabled = True
    sms_enabled = True


    if settings and user:
        # проверяем разрешил ли пользователь отправку сообщений для этого
        # идентификатора
        default_notify_settings = getattr(settings, 'userNotifySettings', None) or {}
        user_notify_settings = getattr(user, 'userNotifySettings', None) or {}
        user_role = getattr(user, 'userRole', None) or 'u'
        mail_enabled = is_notify_identifier_enabled(
            default_notify_settings.get('email', {}).get(user_role, None) or {},
            user_notify_settings.get('email', None) or {},
            identifier
        )
        sms_enabled = is_notify_identifier_enabled(
            default_notify_settings.get('sms', {}).get(user_role, None) or {},
            user_notify_settings.get('sms', None) or {},
            identifier
        )
    if mail_notify_instance and mail_enabled:
        mail_notify_instance.send_user_notification(
            user=user,
            context=context,
            preferred_lang=preferred_lang,
            attachments=attachments,
            connection=email_connection
        )
    if sms_notify_instance and sms_enabled:
        sms_notify_instance.send_user_notification(
            user=user,
            context=context,
            preferred_lang=preferred_lang
        )


def generate_default_notification_objs():
    from drf_lbcms.models import CmsSmsNotification, CmsMailNotification
    with open(os.path.join(os.path.dirname(__file__), 'default-notifications.json')) as f:
        notifications = json.load(f)

    for k,v in notifications['email'].items():
        if not CmsMailNotification.objects.filter(identifier=k).count():
            data = v
            data['identifier'] = k
            data['fromEmail'] = data['fromEmail'].replace(
                '{{HOSTNAME}}',
                getattr(djsettings, 'HOSTNAME', 'noname.com')
            )
            if 'variables' in data:
                del data['variables']
            CmsMailNotification.objects.create(**data)
    for k,v in notifications['sms'].items():
        if not CmsSmsNotification.objects.filter(identifier=k).count():
            data = v
            data['identifier'] = k
            if 'variables' in data:
                del data['variables']
            CmsSmsNotification.objects.create(**data)
