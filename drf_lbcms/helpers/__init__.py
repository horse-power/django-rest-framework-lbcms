import requests, json
import mimetypes, os
import re, io, six
from drfs import get_model
from django.conf import settings
from django.core.exceptions import FieldDoesNotExist
from .l10n import _slugify as slugify, l10n_filter

try:
    from django.contrib.sites.shortcuts import get_current_site as _get_current_site
    from django.contrib.sites.models import Site

    def get_current_site(request):
        try:
            return _get_current_site(request)
        except:
            return None
except:
    def get_current_site(request):
        return None



def get_site_settings(request=None, site=None):
    CmsSettings = get_model('CmsSettings', latest=True)
    if not request:
        return CmsSettings.objects.all().first()
    if site is None and request:
        site = get_current_site(request)
    if site is None:
        return CmsSettings.objects.all().first()
    return CmsSettings.objects.filter(_site=site).first()



def download_file(url):
    r = requests.get(url)
    if r.status_code != 200:
        return None, {}
    try:
        size = int(r.headers["Content-Length"])
    except:
        size = 0
    try:
        size = int(size)
    except:
        size = 0
    try:
        ext = mimetypes.guess_extension(r.headers['Content-Type'])
    except:
        ext = 'unknown'
    basename = os.path.basename(url).split('?')[0].split('#')[0]
    if os.path.splitext(basename)[-1] != basename and os.path.splitext(basename)[-1]:
        ext = ''
    return r._content, {
        'originalName': basename + ext,
        'size': size,
        'type': r.headers['Content-Type']
    }



def dump_to_file(obj, file_path, indent=4, cls=None):
    with io.open(file_path, 'w', encoding='utf8') as f:
        data = json.dumps(obj, indent=indent, ensure_ascii=False)
        f.write(six.text_type(data))


def model_class_has_field(model_class, field_name):
    if not model_class or not field_name:
        return False
    try:
        model_class._meta.get_field(field_name)
        return True
    except FieldDoesNotExist:
        return False


def split_camel_case_text(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]


def field_name_to_title(text):
    text = ' '.join(split_camel_case_text(text))
    text = text.capitalize()
    return ' '.join(text.split('_'))


def to_url_id(id, title=None, alt_title=None, lang=None):
    if not id:
        return '-'

    title = l10n_filter(title, lang or settings.APP_CONTEXT['CMS_DEFAULT_LANG']).strip() or \
            l10n_filter(alt_title, lang or settings.APP_CONTEXT['CMS_DEFAULT_LANG']).strip()
    if not title:
        return "{id}".format(id)
    return "{id}-{title}".format(
        id=id,
        title=slugify(title).lower()
    )



def run_viewset_method(string_path_to_viewset, method_name, json_request, *args, **kwargs):
    from django.contrib import auth
    from django.contrib.auth.models import AnonymousUser
    from django.test import RequestFactory
    import drfs

    user = json_request.get('user', None)
    if isinstance(user, (int, str)):
        UserModel = auth.get_user_model()
        user = UserModel.objects.get(pk=user)
    viewset = drfs.helpers.import_class(string_path_to_viewset)()
    method = getattr(viewset, method_name)

    request = RequestFactory().get('')
    request.user = user or AnonymousUser()
    request._user = request.user
    request.query_params = json_request.get('query_params', None) or {}

    viewset.request = request
    return method(*args, **kwargs)



def is_ngrok_url(url):
    if not url:
        return False

    from urllib.parse import urlparse
    domain = urlparse(url).netloc

    return '.ngrok.io' in domain or \
            '.ngrok-free.app' in domain or \
            '.ngrok.app' in domain