# -*- coding: utf-8 -*-
import os, math, json
from django.conf import settings as djsettings
from PIL import Image
from django.core.files.images import ImageFile
import uuid, six
from django.core.files.uploadedfile import InMemoryUploadedFile, TemporaryUploadedFile

if six.PY2:
    from cStringIO import StringIO as BytesIO
else:
    from io import BytesIO

ALLOWED_IMG_EXT_FOR_WEBP = ['.gif', '.png', '.jpg', '.jpeg', '.apng']
DEFAULT_IMG_CONF = getattr(djsettings, 'DRF_GENERATOR', {}).get('l10nfile_img_conf', None) or getattr(djsettings, 'DRF_LBCMS', {}).get('l10nfile_img_conf', None) or {
    "maxWidth": 2000,
    "maxHeight": 2000,
    "quality": 70,
    "skipGif": True,
    "thumbSizes": ["70x70", "350x350", "440x250"],
    "webpQuality": {
        "png": 97,
        "jpg": 78,
        "default": 78
    }
}
# https://stackoverflow.com/questions/76616042/attributeerror-module-pil-image-has-no-attribute-antialias
try:
    ANTIALIAS = Image.LANCZOS
except:
    # deprecated old versions of PIL
    ANTIALIAS = Image.ANTIALIAS



def save_image_as_webp(img, filename, storage, is_gif=False):
    if not DEFAULT_IMG_CONF.get('useWebp', False):
        return
    img_buffer = BytesIO()
    ext = os.path.splitext(filename)[-1].lower()
    webpQuality = DEFAULT_IMG_CONF.get('webpQuality', None) or {}
    quality = webpQuality.get(ext[1:], None) or webpQuality.get('default', None) or 90
    if is_gif:
        img.save(
            img_buffer, quality=quality, format='WEBP',
            save_all=True, duration=img.info['duration'], loop=img.info['loop']
        )
    else:
        minimize_size = False
        if '.thumb.' not in filename:
            minimize_size = True
        img.save(img_buffer, quality=quality, format='WEBP', minimize_size=minimize_size, method=4)
    storage._save(os.path.splitext(filename)[0]+'.webp', ImageFile(img_buffer))


def draw_watermark_on_img(img, watermark, position, img_size={}, watermark_size={}):
    """
        Накладывает поверху картинки ватермарк. Ватермарк всегда накладывается размером
        не более 1/4 картинки

        @type img: PIL.Image
        @param img: оригинальная картинка
        @type watermark: PIL.Image
        @param watermark: ватермарк
        @type position: str
        @param position: расположение ватермарка в ['top left', 'top right', 'bottom left', 'bottom right', 'center']
        @type img_size: dict
        @param img_size: размеры оригинальной картинки в формате {'width': X, 'height': Y}
        @type watermark_size: dict
        @param img_size: размеры ватермарка в формате {'width': X, 'height': Y}

        @rtype: PIL.Image
        @return: картинку с наложенным ватермарком
    """
    r = {}
    if img_size['width']/2.0 <= watermark_size['width']:
        r['width'] = int(img_size['width']/2.0)
    if img_size['height']/2.0 <= watermark_size['height']:
        r['height'] = int(img_size['height']/2)
    if r:
        if not r.get('height', None):
            r['height'] = watermark_size['height'] * r['width'] / watermark_size['width']
        if not r.get('width', None):
            r['width'] = watermark_size['width'] * r['height'] / watermark_size['height']

        watermark.thumbnail(
            (r['width'], r['height']),
            ANTIALIAS
        )
        w_width, w_height = watermark.size
        watermark_size = {
            'width': w_width,
            'height': w_height
        }
    pos = (0, 0)

    if position == 'top left':
        pos = (0, 0)
    elif position == 'top right':
        pos = (img_size['width'] - watermark_size['width'], 0)
    elif position == 'bottom left':
        pos = (0, img_size['height'] - watermark_size['height'])
    elif position == 'bottom right':
        pos = (
            img_size['width'] - watermark_size['width'],
            img_size['height'] - watermark_size['height']
        )
    elif position == 'center':
        pos = (
            int((img_size['width'] - watermark_size['width']) /2),
            int((img_size['height'] - watermark_size['height']) /2)
        )

    # HATE!!! HATE!!!!!!
    if img.mode != 'RGBA':
        img = img.convert('RGBA')
    if watermark.mode != 'RGBA':
        watermark = watermark.convert('RGBA')
    layer_with_watermark = Image.new('RGBA', img.size)
    layer_with_watermark.paste(watermark, pos)
    img.alpha_composite(layer_with_watermark)
    return img




def resize_and_crop_img(img, modified_path, size, quality=90, storage=None, format='JPEG'):
    if not storage:
        from django.core.files.storage import default_storage as storage
    # Get current and desired ratio for the images
    img_ratio = img.size[0] / float(img.size[1])
    ratio = size[0] / float(size[1])
    # The image is scaled/cropped vertically or horizontally depending on the
    # ratio
    if ratio > img_ratio:
        img = img.resize(
            (
                size[0],
                int(size[0] * img.size[1] / img.size[0])
            ),
            ANTIALIAS
        )
        box = (
            0,
            int((img.size[1] - size[1]) / 2),
            img.size[0],
            int((img.size[1] + size[1]) / 2)
        )
        img = img.crop(box)
    elif ratio < img_ratio:
        img = img.resize(
            (
                int(size[1] * img.size[0] / img.size[1]),
                size[1]
            ),
            ANTIALIAS
        )
        box = (
            int((img.size[0] - size[0]) / 2),
            0,
            int((img.size[0] + size[0]) / 2),
            img.size[1]
        )
        img = img.crop(box)
    else:
        img = img.resize((size[0], size[1]), ANTIALIAS)
        # If the scale is the same, we do not need to crop
    if img.mode == 'RGBA' and format=='JPEG':
        # fix black background for PNG->JPEG convert
        background = Image.new('RGBA', img.size, (255,255,255))
        background.paste(img, img)
        img = background

    if format == 'JPEG':
        img = img.convert("RGB")
    img_buffer = BytesIO()
    img.save(img_buffer, quality=quality, format=format)
    storage._save(modified_path, ImageFile(img_buffer))
    save_image_as_webp(img, modified_path, storage)
    return img



def process_l10nFile(l10nFile, options={}):
    """
        google storage не позволит нам сохранять нормально названия файлов с unicode
        поэтому заменяем названия на uuid1
    """
    storage = l10nFile.file_data.storage
    file_name = os.path.splitext(l10nFile.file_data.name)
    file_name = str(uuid.uuid1()) + file_name[-1]
    l10nFile.file_data.name = file_name

    if 'image' not in l10nFile.meta_data['type'] or l10nFile.meta_data['type'].startswith('image/svg'):
        return

    if 'maxWidth' not in options:
        options['maxWidth'] = DEFAULT_IMG_CONF['maxWidth']
    if 'maxHeight' not in options:
        options['maxHeight'] = DEFAULT_IMG_CONF['maxHeight']
    if 'quality' not in options:
        options['quality'] =  DEFAULT_IMG_CONF['quality']
    if 'skipGif' not in options:
        options['skipGif'] = DEFAULT_IMG_CONF['skipGif']
    if 'thumbSizes' not in options:
        options['thumbSizes'] = DEFAULT_IMG_CONF['thumbSizes']

    if six.PY2:
        img = Image.open(l10nFile.file_data)
    else:
        img = Image.open(l10nFile.file_data.file)
    ext = os.path.splitext(l10nFile.meta_data['originalName'])[-1].lower()
    img_width, img_height = img.size
    size = {
        'width': img_width,
        'height': img_height
    }
    if options['skipGif'] and ext=='.gif':
        pass
    else:
        # ресайзим картинку в ограничивающие размеры
        # если картинка - не gif
        if img_width > options['maxWidth']:
            size['width'] = options['maxWidth']
        if img_height > options['maxHeight']:
            size['height'] = options['maxHeight']

        img.thumbnail(
            (size['width'], size['height']),
            ANTIALIAS
        )
        img_width, img_height = img.size
        size = {
            'width': img_width,
            'height': img_height
        }
        img_buffer = BytesIO()
        img.save(img_buffer, quality=options['quality'], format=img.format)
        save_image_as_webp(img, file_name, storage, is_gif=ext=='.gif')
        l10nFile.file_data = ImageFile(img_buffer)
        l10nFile.file_data.name = file_name

    l10nFile.meta_data['width'] = size['width']
    l10nFile.meta_data['height'] = size['height']


    """
        Генерация превью картинки.
        Функция вырезает по центру область в отмасшатабированной
        картинке до размеров thumbSize.

        thumbSize    in    [  70, {width:90, height:120}, "200x130"  ]
    """
    tslist = []
    thumbSizeList = options['thumbSizes']
    for thumbSize in thumbSizeList:
        if isinstance(thumbSize, int):
            # размер квадрата --  70 (в px)
            thumbSize = {
                'width': thumbSize,
                'height': thumbSize
            }
        elif isinstance(thumbSize, six.text_type):
            # размер прямоугольника -- "200x100" (в px)
            thumbSize = {
                'width': int(thumbSize.split('x')[0]),
                'height': int(thumbSize.split('x')[1])
            }
        tslist.append(thumbSize)

    thumbSizeList = tslist
    thumbNames = []
    localFilePath = str(l10nFile.file_data)


    for thumbSize in thumbSizeList:
        thumb = img
        if isinstance(thumbSize, six.string_types):
            thumbSize = {
                'width': int(thumbSize.split('x')[0]),
                'height': int(thumbSize.split('x')[-1])
            }
        name = str(thumbSize['width'])+"x"+str(thumbSize['height'])
        filename = localFilePath+".thumb."+name+".jpg"
        thumbNames.append(name)
        resize_and_crop_img(
            thumb,
            filename,
            (thumbSize['width'], thumbSize['height']),
            quality=87, storage=storage
        )
        if thumbSize['width'] > 80 or thumbSize['height'] > 80:
            save_image_as_webp(img, file_name, storage, is_gif=ext=='.gif')

    l10nFile.thumbs = thumbNames
    if options.get('watermark', None):
        img_format_orig = img.format
        img_mode_orig = img.mode
        watermark = Image.open(options['watermark']['img'])
        w_width, w_height = watermark.size
        img = draw_watermark_on_img(
            img,
            watermark,
            options['watermark'].get('position', None),
            img_size=size,
            watermark_size={
                'width': w_width,
                'height': w_height
            }
        )
        # теперь нужно вернуть картинку к старому формату, до того как мы накладывали
        # ватермарк
        if img.mode != img_mode_orig:
            img = img.convert(img_mode_orig)
        img_buffer = BytesIO()
        img.save(img_buffer, quality=options['quality'], format=img_format_orig)
        save_image_as_webp(img, file_name, storage, is_gif=ext=='.gif')
        l10nFile.file_data = ImageFile(img_buffer)
        l10nFile.file_data.name = file_name




def delete_l10nFile(l10nFile):
    path = str(l10nFile.file_data)
    storage = l10nFile.file_data.storage
    if not storage:
        return
    for thumb in l10nFile.thumbs or []:
        filePath = path+".thumb."+thumb+".jpg"
        try:
            storage.delete(filePath)
        except:
            pass
        filePath = path+".thumb."+thumb+".webp"
        try:
            storage.delete(filePath)
        except:
            pass





def get_options_from_request_data(fields, allow_multiple=False):
    """
        На вход подается объект request.data
        --
        возвращает (dict, InMemoryUploadedFile)
    """
    allowedOptions = ['maxWidth', 'maxHeight', 'skipGif', 'saveToLocalStorage',
                      'quality', 'thumbSizes', 'forIdInList', 'addWatermark']
    options = {}
    _files = {}

    for key in fields:
        value = fields[key]
        if isinstance(value, InMemoryUploadedFile) or isinstance(value, TemporaryUploadedFile):
            _files[key] = value
            continue
        if key.startswith('options_'):
            key = key.replace('options_', '')
            if key not in allowedOptions:
                continue
            if isinstance(value, list):
                if not value:
                    continue
                value = value[0]
            if key.find('max') > -1 or key.find('quality') > -1:
                try:
                    value = int(value)
                    options[key] = value
                except:
                    continue
            if key in ['skipGif', 'saveToLocalStorage', 'addWatermark']:
                if value == 'true':
                    options[key] = True
                else:
                    options[key] = False
            if key == 'thumbSizes':
                try:
                    options['thumbSizes'] = json.loads(value)
                except:
                    pass
            if key == 'forIdInList':
                try:
                    options[key] = int(value)
                except:
                    pass
    # поддержка старого типа загрузки файла (по одному) и нового (по несколько файлов)
    if allow_multiple:
        return options, _files
    if len(_files.keys()) == 0:
        return options, None
    key = list(_files.keys())[0]
    return options, _files[key]




def resolve_l10nfile_uri(file_name, thumb_size=None, force_cms_storage=None, force_cms_db_bucket=None):
    cms_storage = force_cms_storage or djsettings.APP_CONTEXT.get('CMS_STORAGE', {
        'type': 'local'
    })
    cms_db_bucket = force_cms_db_bucket or djsettings.APP_CONTEXT['CMS_DB_BUCKET']
    if file_name[0] == '/':
        file_name = file_name[1:]

    # возвращаем путь к файлу внутри bucket-a
    if thumb_size:
        url = resolve_l10nfile_uri(file_name, force_cms_storage=cms_storage, force_cms_db_bucket=cms_db_bucket)
        if not url:
            return url
        if thumb_size.find('x') == -1:
            thumb_size = thumb_size + "x" + thumb_size
        return "{path}.thumb.{thumb_size}.jpg".format(
            path=url,
            thumb_size=thumb_size
        )
    # обычный файл
    if cms_storage['type'] in ['local', 'localStorage', '']:
        # если сохранялись на локальной фс, то ссылаемся на url сервера cms
        # или
        # файлы сохраненные на сервере во временной папке (напр. watermark)
        cms_base_url = cms_storage.get('server', None) or ''
        if not cms_base_url and djsettings.PLATFORM != 'dev':
            cms_base_url = "https://" + djsettings.HOSTNAME
        return u"{CMS_BASE_URL}/storage/lbcms-container-{CMS_DB_BUCKET}/{fileName}".format(
            CMS_BASE_URL=cms_base_url,
            CMS_DB_BUCKET=cms_db_bucket,
            fileName=file_name
        )
    if cms_storage['type'] in ['amazon', 'google', 'digitaloceanSpaces']:
        # файлы сохраненные на s3 у aws или в гугле
        return u"{CMS_STORAGE_server}/lbcms-container-{CMS_DB_BUCKET}/{fileName}".format(
            CMS_STORAGE_server=cms_storage['server'],
            CMS_DB_BUCKET=cms_db_bucket,
            fileName=file_name
        )
    return ''
