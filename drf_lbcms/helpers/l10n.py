# -*- coding: utf-8 -*-
import json



def _slugify(text):
    text = '-'.join(text.split(': '))
    table = {ord(char): None for char in ";/?:@&=+$|,!{}\^[]`<>#%"}
    table[ord(' ')] = ord('-')
    return text.translate(table)



def l10n_filter(obj, lang, slugify=False):
    def tr():
        if not obj:
            return obj
        if obj.get(lang, None):
            return obj[lang]
        if lang == 'cs' and obj.get('cz', None):
            return obj['cz']
        if obj.get('en', None):
            return obj['en']
        keys = obj.keys()
        if not keys:
            return ''
        return obj[list(keys)[0]]

    text = tr()
    if not text:
        return ''
    if not slugify:
        return text or ''
    return _slugify(text)




def dump_json(data, indent=0, separators=None):
    class JSONEncoder(json.JSONEncoder):
        def default(self, o):
            if o and (hasattr(o, '__proxy__') or hasattr(o, '_proxy____args')):
                return str(o)
            return super().default(o)

    if separators:
        return json.dumps(data, indent=indent, ensure_ascii=False, separators=separators, cls=JSONEncoder)
    else:
        return json.dumps(data, indent=indent, ensure_ascii=False, cls=JSONEncoder)
    '''
    if six.PY2:
        if indent:
            return json.dumps(data, indent=indent).decode('unicode-escape').encode('utf8')
        return json.dumps(data).decode('unicode-escape').encode('utf8')
    if indent:
        return json.dumps(data, indent=indent)
    return json.dumps(data)
    '''


def django_choices_field_to_text(field, value, preferred_lang='en'):
    text = value
    # сначала просто пытаемся перевести по списку поле в текст
    for ch in getattr(field, 'choices', []):
        if isinstance(ch, list):
            if ch[0] == value:
                text = ch[1]
                break
        else:
            continue
    model = field.model
    model_definition = getattr(model, 'DRFS_MODEL_DEFINITION', None)
    if not model_definition:
        return text
    # если есть объект DRFS_MODEL_DEFINITION у модели. то велика вероятность
    # нахождения там переводов строк для поля
    for field_name, params in model_definition.get('properties', {}).items():
        if field_name == field.name and params.get('choices_l10n', None):
            return l10n_filter(
                params['choices_l10n'].get(value, {}),
                preferred_lang
            ) or text
    return text



dump_gettext_json = dump_json
