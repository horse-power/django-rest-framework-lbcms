# -*- coding: utf-8 -*-
from django.template import Template, TemplateSyntaxError
from django.template.base import DebugLexer, Parser
import re
from .html import unescape_html




class TemplateForValidation(Template):
    def compile_nodelist(self):
        """
            переопределяем этот метод таким образом, чтоб джанга всегда выдывала сведения о ошибках
            парсинга в расширенном (отладочном) виде
        """
        lexer = DebugLexer(self.source)
        tokens = lexer.tokenize()
        parser = Parser(
            tokens, self.engine.template_libraries, self.engine.template_builtins,
            self.origin,
        )

        try:
            return parser.parse()
        except Exception as e:
            e.template_debug = self.get_exception_info(e, e.token)
            e.template_debug['source_lines'] = [
                list(l)
                for l in e.template_debug['source_lines']
            ]
            raise


def validate_template(text):
    if '<style' in text:
        text = re.sub(
            re.compile('<style(.|\n)*?</style>'),
            '',
            text
        )
    text = "{% load lbcms %}{% load l10n %}{% load tz %}" + text
    text = unescape_html(text)
    errors = []
    if text.count('{{') != text.count('}}'):
        errors.append({
            'type': 'general',
            'description': {
                'message': 'Found impaired {{ and }} ("{{" - %s, "}}" - %s count)' % (text.count('{{'), text.count('}}'))
            }
        })

    if text.count('{%') != text.count('%}'):
        errors.append({
            'type': 'general',
            'description': {
                'message': 'Found impaired {% and %} ("{%" - ' + str(text.count('{%') - 3) +', "%}" - ' + str(text.count('%}') - 3) + ' count)'
            }
        })
    text = text.split('\n')
    cleared = []
    for c in text:
        if c:
            cleared.append(c)
    text = '\n'.join(cleared)
    try:
        TemplateForValidation(text)
    except TemplateSyntaxError as e:
        errors.append({
            'type': 'parsing',
            'description': e.template_debug
        })
    return not bool(errors), errors
