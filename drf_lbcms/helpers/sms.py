# -*- coding: utf-8 -*-
import sys
from .html import unescape_html

IS_TESTING_MODE = sys.argv[1:2] == ['test']


if IS_TESTING_MODE:
    outbox = []
    class CacheObj:
        def __init__(self, **kwargs):
            for k,v in kwargs.items():
                setattr(self, k, v)




class SmsApiWrapper(object):
    def __init__(self, credentials):
        from smsapi.client import SmsAPI
        self.api = SmsAPI()
        self.api.set_username(credentials['username'])
        self.api.set_password(credentials['password'])
        self.sender_name = ''

    def set_default_sender_name(self, name):
        self.sender_name = name

    def send_sms(self, to, text, sender=None):
        from smsapi.responses import ApiError
        sender = sender or self.sender_name
        self.api.service('sms').action('send')
        self.api.set_content(text)
        self.api.set_to([to])
        self.api.set_from(sender)
        if IS_TESTING_MODE:
            # делаем mock для тестов
            global outbox
            outbox.append(CacheObj(
                body=text,
                to=[to],
                from_phone=sender
            ))
            return
        try:
            result = self.api.execute()
            return False
        except ApiError as e:
            print('SMS send error: %s - %s' % (e.code, e.message))
            return {
                "error": '%s - %s' % (e.code, e.message),
                "text": text,
                "phone": to
            }
