# -*- coding: utf-8 -*-
import smtplib, threading, sendgrid
from django.conf import settings as djsettings
from django.core.mail.message import sanitize_address

from sgbackend import SendGridBackend as SendGridBackendBase
from django.core.mail.backends.smtp import EmailBackend as EmailBackendBase
from .html import unescape_html, html_to_text

import logging
logger = logging.getLogger(__name__)




def inline_css(text='', body_only=False):
    try:
        from premailer import Premailer
    except ImportError:
        return text
    first_line = ''
    if text.startswith('{%'): # load directives
        text = text.split('\n')
        first_line = text[0]
        text = '\n'.join(text[1:])
    try:
        text = Premailer(strip_important=False, disable_validation=True, disable_link_rewrites=True, remove_classes=False).transform(
            text,
            pretty_print=False,
            export_method='xml'
        )
    except:
        return text
    if body_only:
        text = text.split('<body>')[-1].split('</body')[0]
    return first_line + text


'''
    бекэнд на основе сенгрида
'''
class DrfSendGridClient(sendgrid.SendGridClient):
    def send(self, *args, **kwargs):
        if not self.username and not self.password:
            logger.warn("Sendgrid key is missing for SendGridBackend! Ignoring 'send' function for mail")
            return
        return super(DrfSendGridClient, self).send(*args, **kwargs)

class SendGridBackend(SendGridBackendBase, EmailBackendBase):
    def __init__(self, fail_silently=False, api_key=None, **kwargs):
        super(EmailBackendBase, self).__init__(fail_silently=fail_silently, **kwargs)
        #super(SendGridBackend, self).__init__(fail_silently=fail_silently, **kwargs)
        self.sg = DrfSendGridClient(
            api_key,
            raise_errors=not fail_silently
        )

'''
    бекэнд на основе gmail
'''
class GmailBackend(EmailBackendBase):
    def __init__(self, fail_silently=False, username=None, password=None, **kwargs):
        super(GmailBackend, self).__init__(fail_silently=fail_silently)
        self.host = 'smtp.gmail.com'
        self.port = 465
        self.username = username
        self.password = password
        self.timeout = kwargs.get('timeout', None)
        self.use_tls = None
        self.use_ssl = True
        self.ssl_keyfile = None
        self.ssl_certfile = None
        self.connection = None
        self._lock = threading.RLock()


'''
    бекэнд на основе Amazon SES
'''
try:
    import django_amazon_ses, boto3

    class AmazonSesBackend(django_amazon_ses.EmailBackend):
        def __init__(self, fail_silently=False,
                     aws_access_key_id=None, aws_secret_access_key=None, aws_region_name=None, aws_email_from=None,
                     **kwargs):
            super(AmazonSesBackend, self).__init__(fail_silently=fail_silently)
            self.default_from_email = aws_email_from
            self.conn = boto3.client(
                "ses",
                aws_access_key_id=aws_access_key_id,
                aws_secret_access_key=aws_secret_access_key,
                region_name=aws_region_name,
            )

        def _send(self, email_message):
            # если указан email, с которого нужно отправлять, то делаем замену.
            # амазон не даст отправлять от левых доменов/адресов.
            if self.default_from_email:
                if '@' in self.default_from_email:
                    email_message.from_email = self.default_from_email
                else:
                    # заменяем домен
                    email_message.from_email = email_message.from_email.split('@')[0] + '@' + self.default_from_email
            return super(AmazonSesBackend, self)._send(email_message)

except ImportError:
    from django.core.exceptions import ImproperlyConfigured

    class AmazonSesBackend(object):
        def __init__(self, *args, **kwargs):
            raise ImproperlyConfigured("Please install 'django-amazon-ses' pip package on the server!")



'''
    other
'''

class OtherBackend(EmailBackendBase):
    def __init__(self, *args, **kwargs):
        self.default_from_email = kwargs.pop('default_from_email', None)
        super(OtherBackend, self).__init__(*args, **kwargs)

    def _send(self, email_message):
        """A helper method that does the actual sending."""
        if not email_message.recipients():
            return False
        encoding = email_message.encoding or djsettings.DEFAULT_CHARSET
        if email_message.content_subtype != 'html' and '<body' in email_message.body and '</body>' in email_message.body:
            email_message.body = html_to_text(email_message.body)

        from_email = sanitize_address(
            self.default_from_email or email_message.from_email,
            encoding
        )
        recipients = [sanitize_address(addr, encoding) for addr in email_message.recipients()]
        message = email_message.message()
        try:
            self.connection.sendmail(from_email, recipients, message.as_bytes(linesep='\r\n'))
        except smtplib.SMTPException:
            if not self.fail_silently:
                raise
            return False
        return True




def get_connection(settings, fail_silently=True):
    serverIntegrations = settings.serverIntegrations or {}
    if not serverIntegrations.get('emailBackend', None):
        # берем бекэнд по умолчанию из django.conf.settings
        return
    if serverIntegrations['emailBackend'] == 'sendgrid':
        return SendGridBackend(
            fail_silently=fail_silently,
            api_key=serverIntegrations.get('sendgrid', {}).get('apiKey', None)
        )
    if serverIntegrations['emailBackend'] == 'gmail':
        return GmailBackend(
            fail_silently=fail_silently,
            username=serverIntegrations.get('gmail', {}).get('username', None),
            password=serverIntegrations.get('gmail', {}).get('password', None)
        )
    if serverIntegrations['emailBackend'] == 'amazonSes':
        return AmazonSesBackend(
            fail_silently=fail_silently,
            aws_access_key_id=serverIntegrations.get('amazonSes', {}).get('accessKeyId', None),
            aws_secret_access_key=serverIntegrations.get('amazonSes', {}).get('secretAccessKey', None),
            aws_region_name=serverIntegrations.get('amazonSes', {}).get('regionName', None),
            aws_email_from=serverIntegrations.get('amazonSes', {}).get('defaultFrom', None)
        )
    if serverIntegrations['emailBackend'] == 'regular':
        return OtherBackend(
            fail_silently=fail_silently,
            host=serverIntegrations.get('smtp', {}).get('host', None) or 'localhost',
            port=serverIntegrations.get('smtp', {}).get('port', None) or 25,
            username=serverIntegrations.get('smtp', {}).get('username', None) or '',
            password=serverIntegrations.get('smtp', {}).get('password', None) or '',
            use_tls=serverIntegrations.get('smtp', {}).get('useTls', False),
            default_from_email=serverIntegrations.get('smtp', {}).get('defaultFrom', None)
        )
