# -*- coding: utf-8 -*-

import os, six, base64
from django.conf import settings as djsettings
import logging
logger = logging.getLogger(__name__)

from .django import get_base_dir_as_string


try:
    # Python >= 3.5
    from html import unescape
    from html.parser import HTMLParser

    def unescape_html(text):
        return unescape(text)

except ImportError:
    try:
        # Python > 2.6 and < 3.0
        from HTMLParser import HTMLParser
    except ImportError:
        # Python >= 3.0 and < 3.5
        from html.parser import HTMLParser

    def unescape_html(text):
        h = HTMLParser()
        return h.unescape(text)




try:
    import html2text

    def html_to_text(html='', use_md=False):
        if not html:
            return ''
        h = html2text.HTML2Text()
        h.ignore_links = False
        h.body_width = 0
        h.inline_links = True
        h.wrap_links = False
        h.skip_internal_links = True
        h.ignore_images = True
        h.single_line_break = True

        if not use_md:
            h.strong_mark = ''
            h.emphasis_mark = ''

        text = h.handle(html).strip()
        #disable_email_trim_index = text.rfind('#')
        #if disable_email_trim_index > -1 and text[disable_email_trim_index-1] == '\n':
        #    text = text[:disable_email_trim_index].strip()
        lines = ""
        for line in text.split('\n'):
            if line.startswith('| '):
                line = line[2:]
            lines += line + '\n'
        return lines.strip()

except ImportError:
    def html_to_text(html=''):
        logger.warn("Install html2text to use this function")
        return html




def download_file(url, skip_external=False):
    if url[0] == '/':
        # файл закопан на фс
        MEDIA_URL = djsettings.MEDIA_URL
        if MEDIA_URL[0] != '/':
            MEDIA_URL = "/%s" % MEDIA_URL
        filename = url.replace(MEDIA_URL, '')
        filepath = os.path.join(djsettings.MEDIA_ROOT, filename)
        # медиа
        if os.path.isfile(filepath):
            # считываем файл
            with open(filepath, 'rb') as f:
                data = f.read()
            return data
        # статика
        BASE_DIR = get_base_dir_as_string()
        filepath = os.path.join(BASE_DIR, filename[1:])
        if os.path.isfile(filepath):
            # считываем файл
            with open(filepath, 'rb') as f:
                data = f.read()
            return data
        return None
    # нам попался url на внешний ресурс.
    if skip_external:
        return None

    import requests
    r = requests.get(url, stream=True)
    if r.status_code not in [200, 201, 301, 302]:
        return None
    return r.content


def img_to_base64(text, skip_external=False):
    if '<img' not in text:
        return text
    imgs = {}

    class ImgHTMLParser(HTMLParser):
        def handle_starttag(self, tag, attrs):
            # выгребаем все внешние ссылки на картинки
            if tag != 'img':
                return
            for attr in attrs:
                if attr and attr[0] == 'src' and len(attr) > 1 and attr[1]:
                    if attr[1].startswith('/') or attr[1].startswith('http://') or attr[1].startswith('https://'):
                        imgs[attr[1]] = None


    parser = ImgHTMLParser()
    parser.feed(text)
    files = {}
    for url, v in imgs.items():
        #print 'DOWNLOAD', url
        data = download_file(url, skip_external=skip_external)
        if data:
            ext = os.path.splitext(url)[-1].lower()
            prefix = None
            if ext == '.png':
                prefix = 'data:image/png;base64,'
            elif ext == '.gif':
                prefix = 'data:image/gif;base64,'
            elif ext in ['.jpg', 'jpeg']:
                prefix = 'data:image/jpg;base64,'
            else:
                continue
            if six.PY2:
                files[url] = prefix + base64.b64encode(data)
            else:
                files[url] = prefix + base64.b64encode(data).decode('utf-8')
    # теперь заменяем все вхождения строк
    for url, data in files.items():
        srcdata = "src='%s'" % data
        text = srcdata.join(
            text.split("src='%s'" % url)
        )
        text = srcdata.join(
            text.split('src="%s"' % url)
        )
    return text




class Font(object):
    def __init__(self, base_dir, name=None):
        self.name = name
        self.base_dir = base_dir

    def getWeight(self, weight):
        W = {
            'bold': 700,
            'italic': 400,
            'light': 300,
            'regular': 400
        }
        return W.get(weight, 400)

    def getCss(self):
        files = os.listdir(os.path.join(self.base_dir, 'fonts', self.name))
        css = ""
        for f in files:
            _p = os.path.join(self.base_dir, 'fonts', self.name, f)
            if os.path.isfile(_p) and os.path.splitext(_p)[-1] == '.ttf':
                bname = os.path.splitext(os.path.basename(_p))[0]
                weight = bname.split('-')[1].lower()
                style = 'normal'
                if weight.lower() == 'italic':
                    style = 'italic'
                weight = self.getWeight(weight)
                css += """
                @font-face {
                    font-family: """+self.name+""";
                    src: url("""+_p+""");
                    font-style: """+style+""";
                    font-weight: """+str(weight)+""";
                }
                """
        return css
