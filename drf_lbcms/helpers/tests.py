
from datetime import date as real_date
from datetime import datetime as real_datetime
from datetime import time as real_time
from django.test import TestCase
from django.conf import settings as djsettings
from rest_framework.test import APIRequestFactory
import json, tempfile, shutil
import six


from . import date as date_helpers


# http://tech.yunojuno.com/mocking-dates-with-django




class FakeNow(object):
    def __init__(self, datetime, fn):
        self.datetime = datetime
        self.fn = fn

    def __enter__(self, datetime, fn):
        from mock import patch

        datetime = self.datetime
        if isinstance(datetime, str):
            if 'T' in datetime:
                datetime = date_helpers.str_to_datetime(datetime)
            else:
                datetime = date_helpers.str_to_date(datetime)
        if isinstance(datetime, real_date):
            datetime = real_datetime.combine(datetime, real_time())

        class _FakeDate(real_date):
            """A fake replacement for datetime.date."""
            class FakeDateType(type):
                "Used to ensure the FakeDate returns True to function calls."
                def __instancecheck__(self, instance):
                    return isinstance(instance, real_date)

            # this forces the FakeDate to return True to the isinstance date check
            __metaclass__ = FakeDateType
            @classmethod
            def today(cls):
                return datetime.date()


        class _FakeDatetime(real_datetime):
            """A fake replacement for datetime.datetime."""
            class FakeDatetimeType(type):
                "Used to ensure the FakeDatetime returns True to function calls."
                def __instancecheck__(self, instance):
                    return isinstance(instance, real_datetime)

            # this forces the FakeDatetime to return True to the isinstance date check
            __metaclass__ = FakeDatetimeType
            @classmethod
            def now(cls):
                return datetime

        @patch('datetime.date', _FakeDate)
        @patch('datetime.datetime', _FakeDatetime)
        def wrapper(*args, **kwargs):
            return self.fn(*args, **kwargs)

        return wrapper



    def __exit__(self, type, value, traceback):
        pass



def is_file(f):
    if six.PY2:
        return isinstance(f, file)
    # python3
    from io import IOBase
    return isinstance(f, IOBase)


def is_multipart_data(data):
    for k,v in data.items():
        if is_file(v):
            return True
    return False



class APIRequestFactoryWrapper:
    def __init__(self):
        self.f = APIRequestFactory()

    def get(self, url, user=None):
        request = self.f.get(url)
        if user:
            request.user = user
            request._user = user
        return request

    def post(self, url, data={}, format='json', user=None):
        if is_multipart_data(data):
            format = 'multipart'
        if format == 'json':
            request = self.f.post(url, json.dumps(data), content_type='application/json')
        elif format == 'multipart':
            request = self.f.post(url, data, format='multipart')
        if user:
            request.user = user
            request._user = user
        return request

    def put(self, url, data={}, format='json', user=None):
        if is_multipart_data(data):
            format = 'multipart'
        if format == 'json':
            request = self.f.put(url, json.dumps(data), content_type='application/json')
        elif format == 'multipart':
            request = self.f.put(url, data, format='multipart')
        if user:
            request.user = user
            request._user = user
        return request

    def delete(self, url, data={}, format='json', user=None):
        request = self.f.delete(url)
        if user:
            request.user = user
            request._user = user
        return request


    def getFileAsFormData(self, filepath):
        return open(filepath, 'rb')


class DrfLbcmsTestCase(TestCase):
    use_tmp_media_root = False


    def setup_test_environment(self):
        "Create temp directory and update MEDIA_ROOT and default storage."
        super(DrfLbcmsTestCase, self).setup_test_environment()
        if self.use_tmp_media_root:
            djsettings._original_media_root = djsettings.MEDIA_ROOT
            djsettings._original_file_storage = djsettings.DEFAULT_FILE_STORAGE
            self._temp_media = tempfile.mkdtemp()
            djsettings.MEDIA_ROOT = self._temp_media
            djsettings.DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'

    def teardown_test_environment(self):
        "Delete temp storage."
        super(DrfLbcmsTestCase, self).teardown_test_environment()
        if self.use_tmp_media_root:
            shutil.rmtree(self._temp_media, ignore_errors=True)
            djsettings.MEDIA_ROOT = djsettings._original_media_root
            del djsettings._original_media_root
            djsettings.DEFAULT_FILE_STORAGE = djsettings._original_file_storage
            del djsettings._original_file_storage

    def setUp(self):
        from drf_lbcms.management.commands.drf_lbcms_upload_resources_to_db import Command

        self.maxDiff = None
        self.factory = APIRequestFactoryWrapper()

        if self.default_site_settings:
            from site_settings.models import CmsSettings
            self.settings = CmsSettings.objects.create(**self.default_site_settings)
        c = Command()
        c.handle()
        if hasattr(self, 'setUpData'):
            self.setUpData()


    def getUserRole(self, user):
        if not user:
            return None
        return getattr(user, 'userRole', None)
