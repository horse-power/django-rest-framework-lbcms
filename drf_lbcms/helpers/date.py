import datetime, warnings
from django.utils.dateparse import (
    parse_date, parse_datetime, parse_duration, parse_time,
)
from django.utils.duration import duration_string
from django.core import exceptions
from django.conf import settings
from django.utils import timezone
try:
    # DEPRECATED
    from django.utils.translation import ugettext_lazy as _
except ImportError:
    # django >= 4.0.0
    from django.utils.translation import gettext_lazy as _

ERROR_MSGS = {
    'invalid_date_format': _("'%(value)s' value has an invalid date format. It must be "
                 "in YYYY-MM-DD format."),
    'invalid_date': _("'%(value)s' value has the correct format (YYYY-MM-DD) "
                      "but it is an invalid date."),
    'invalid_datetime': _("'%(value)s' value has the correct format "
                      "(YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]) "
                      "but it is an invalid date/time."),
    'invalid_time_format': _("'%(value)s' value has an invalid format. It must be in "
                     "HH:MM[:ss[.uuuuuu]] format."),
    'invalid_time': _("'%(value)s' value has the correct format "
                          "(HH:MM[:ss[.uuuuuu]]) but it is an invalid time.")
}



def str_to_date(value):
    if value is None:
        return value
    if isinstance(value, datetime.datetime):
        if settings.USE_TZ and timezone.is_aware(value):
            # Convert aware datetimes to the default time zone
            # before casting them to dates (#17742).
            default_timezone = timezone.get_default_timezone()
            value = timezone.make_naive(value, default_timezone)
        return value.date()
    if isinstance(value, datetime.date):
        return value

    try:
        parsed = parse_date(value)
        if parsed is not None:
            return parsed
    except ValueError:
        raise exceptions.ValidationError(
            ERROR_MSGS['invalid_date'],
            code='invalid_date',
            params={'value': value},
        )

    raise exceptions.ValidationError(
        ERROR_MSGS['invalid_date_format'],
        code='invalid',
        params={'value': value},
    )



def str_to_datetime(value):
    if value is None:
        return value
    if isinstance(value, datetime.datetime):
        return value
    if isinstance(value, datetime.date):
        value = datetime.datetime(value.year, value.month, value.day)
        if settings.USE_TZ:
            # For backwards compatibility, interpret naive datetimes in
            # local time. This won't work during DST change, but we can't
            # do much about it, so we let the exceptions percolate up the
            # call stack.
            warnings.warn("drf_lbcms.helpers.date.str_to_datetime received a naive datetime "
                          "(%s) while time zone support is active." %
                          (value,),
                          RuntimeWarning)
            default_timezone = timezone.get_default_timezone()
            value = timezone.make_aware(value, default_timezone)
        return value

    try:
        parsed = parse_datetime(value)
        if parsed is not None:
            return parsed
    except ValueError:
        raise exceptions.ValidationError(
            ERROR_MSGS['invalid_datetime'],
            code='invalid_datetime',
            params={'value': value},
        )

    try:
        parsed = parse_date(value)
        if parsed is not None:
            return datetime.datetime(parsed.year, parsed.month, parsed.day)
    except ValueError:
        raise exceptions.ValidationError(
            ERROR_MSGS['invalid_date'],
            code='invalid_date',
            params={'value': value},
        )

    raise exceptions.ValidationError(
        ERROR_MSGS['invalid_date_format'],
        code='invalid',
        params={'value': value},
    )


def str_to_time(value):
    if value is None:
        return None
    if isinstance(value, datetime.time):
        return value
    if isinstance(value, datetime.datetime):
        # Not usually a good idea to pass in a datetime here (it loses
        # information), but this can be a side-effect of interacting with a
        # database backend (e.g. Oracle), so we'll be accommodating.
        return value.time()

    try:
        parsed = parse_time(value)
        if parsed is not None:
            return parsed
    except ValueError:
        raise exceptions.ValidationError(
            ERROR_MSGS['invalid_time'],
            code='invalid_time',
            params={'value': value},
        )

    raise exceptions.ValidationError(
        ERROR_MSGS['invalid_time_format'],
        code='invalid',
        params={'value': value},
    )


def str_to_duration(value):
    if value is None:
        return None
    if isinstance(value, datetime.timedelta):
        return value
    return parse_duration(value)



def duration_to_str(value):
    if value is None:
        return '00'
    return duration_string(value)
