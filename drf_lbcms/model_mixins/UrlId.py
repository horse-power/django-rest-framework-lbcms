# -*- coding: utf-8 -*-
from django.conf import settings as djsettings
from django.utils import translation
from drf_lbcms.helpers import slugify, to_url_id



class ModelMixin(object):
    def save(self, *args, **kwargs):
        if not djsettings.DRF_LBCMS.get('seo', {}).get('useUrlIdPart', False):
            super(ModelMixin, self).save(*args, **kwargs)
            return
        if not self.title or kwargs.get('raw', False):
            super(ModelMixin, self).save(*args, **kwargs)
            return
        self.urlIdPart = self.urlIdPart or {}
        for k,v in self.title.items():
            v = self.urlIdPart.get(k, None) or v or ''
            self.urlIdPart[k] = slugify(v).lower().replace('--', '-')
        super(ModelMixin, self).save(*args, **kwargs)


    def to_url_id(self, lang=None):
        lang = lang or translation.get_language()
        if not djsettings.DRF_LBCMS.get('seo', {}).get('useUrlIdPart', False):
            return to_url_id(self.id, title=self.title, lang=lang)
        return to_url_id(self.id, title=self.urlIdPart, alt_title=self.title, lang=lang)


    def category_to_url_id(self, lang=None):
        if not self.category_id:
            return '-'
        lang = lang or translation.get_language()
        if not djsettings.DRF_LBCMS.get('seo', {}).get('useUrlIdPart', False):
            return to_url_id(self.category.id, self.category.title, lang=lang)
        return to_url_id(self.category.id, title=self.category.urlIdPart, alt_title=self.category.title, lang=lang)
