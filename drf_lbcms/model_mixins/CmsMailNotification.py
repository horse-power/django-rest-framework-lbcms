# -*- coding: utf-8 -*-
from django.template import Template, Context
from django.template.loader import get_template
from django.conf import settings as djsettings
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth import get_user_model
from django.db.models import Q

from drf_lbcms.helpers.email import inline_css, get_connection
from drf_lbcms.helpers.l10n import l10n_filter
from drf_lbcms.helpers.html import unescape_html




class ModelMixin(object):
    def save(self, *args, **kwargs):
        self._cache = {
            'emailBody': {}
        }
        try:
            main_template_html = get_template('drf_lbcms/email/'+self.emailTemplateName+'/mail.html').template.source
        except:
            main_template_html = "{{TEMPLATE|safe}}"
        for prop in ['body', 'echoBody']:
            self._cache[prop] = {}
            body = getattr(self, prop, {})
            for k,v in body.items():
                self._cache[prop][k] = inline_css(
                    text=unescape_html(main_template_html.replace("{{TEMPLATE | safe}}", v) \
                                                         .replace("{{ TEMPLATE | safe }}", v)
                                                         .replace("{{ TEMPLATE|safe }}", v)
                                                         .replace("{{TEMPLATE|safe}}", v)
                ))
        super(ModelMixin, self).save(*args, **kwargs)




    def send_user_notification(self, user, context={}, preferred_lang=None, attachments=None, connection=None):
        if getattr(djsettings, 'DRF_LBCMS', None):
            if not djsettings.DRF_LBCMS.get('send_notify_email', True):
                print('ignore by settings')
                return
        # если уведомления отключены, то дальнейший код можно и не выполнять
        site = getattr(user, '_site', None)
        settings = context.get('settings', None) or \
                   context.get('cmsSettings', None)

        if not settings:
            from drf_lbcms.helpers import get_site_settings
            settings = get_site_settings(site=site)

        integrations_settings = getattr(settings, 'serverIntegrations', {}) or {}
        if not integrations_settings.get('useEmailNotifications', False):
            return


        CmsUser = get_user_model()
        connection = connection or get_connection(settings, fail_silently=True)
        preferred_lang = preferred_lang or getattr(user, 'preferred_lang', None) or getattr(user, 'preferredLang', None) or 'en'
        attachments = attachments or []

        if not self._cache or not self._cache.get('emailBody', None):
            self.save()


        def send_email(emails, subject, body, timezone=None, lang=None):
            subject = subject or "Notification"
            body = body or ("Please set body content in admin interface for notification mail with identifier = "+self.identifier)
            if lang:
                body = "{% language '"+lang+"' %}{% localize on %}" + body + "{% endlocalize %}{% endlanguage %}"
            # поддержка часовых поясов
            timezone = timezone or getattr(djsettings, 'DRF_LBCMS', {}).get('timezone_for_templates', None)
            timezone = str(timezone)
            if timezone in ['UTC', 'None']:
                body = "{% load lbcms %}{% load l10n %}\n" + body
            else:
                body = "{% load lbcms %}{% load l10n %}\n{% load tz %}{% timezone '"+timezone+"' %} {% localtime on %}"+body+" {% endlocaltime %}{% endtimezone %}"
            _context = Context(context)

            for e in emails:
                t = Template(subject)
                _context['email'] = _context.get('email', e)
                _subject = t.render(_context)
                # теперь полностью рендерим письмо
                t = Template(body)
                _context['emailSubject'] = _subject
                _body = t.render(_context)
                # отправляем письмо
                djemail = EmailMultiAlternatives(
                    subject=_subject,
                    body=_body,
                    from_email=self.fromEmail or 'noreply@mail.com',
                    to=[e],
                    connection=connection
                )
                djemail.attach_alternative(_body, "text/html")
                for attachment in attachments:
                    djemail.attach(
                        attachment.get('name', 'Noname'),
                        attachment.get('content', 'Nocontent'),
                        attachment.get('mime', 'text/plain')
                    )
                djemail.send()

        # набиваем контекст нужными переменными
        context['user'] = context.get('user', user)
        context['settings'] = settings
        context['LANGUAGE_CODE'] = context.get('lang', None) or preferred_lang
        context['PLATFORM'] = getattr(djsettings, 'PLATFORM', 'prod')
        context['CMS_DB_BUCKET'] = getattr(djsettings, 'DJANGO_GCS_BUCKET', '')
        HOSTNAME = getattr(djsettings, 'HOSTNAME', 'noname.com')
        if 'http://' not in HOSTNAME and 'https://' not in HOSTNAME:
            context['CMS_BASE_URL'] = 'http://' + HOSTNAME
        else:
            context['CMS_BASE_URL'] = HOSTNAME
        if site:
            context['SITE_DOMAIN'] = site.domain
        else:
            context['SITE_DOMAIN'] = context.get('SITE_DOMAIN', None) or getattr(djsettings, 'SITE_DOMAIN', None) or context['CMS_BASE_URL']


        if self.sendEcho:
            # send mails to admins
            if self.to:
                send_email(
                    self.to,
                    l10n_filter(self.echoSubject, preferred_lang),
                    l10n_filter(self._cache['echoBody'], preferred_lang),
                    lang=preferred_lang
                )
            else:
                for u in CmsUser.objects.filter(Q(userRole='a') | Q(userRole='admin')):
                    if u.email:
                        send_email(
                            [u.email],
                            l10n_filter(self.echoSubject, preferred_lang),
                            l10n_filter(self._cache['echoBody'], preferred_lang),
                            timezone=getattr(u, 'timezone', None),
                            lang=preferred_lang
                        )

        if self.send and getattr(user, 'email', None):
            # send mail to user
            send_email(
                [user.email],
                l10n_filter(self.subject, preferred_lang),
                l10n_filter(self._cache['body'], preferred_lang),
                timezone=getattr(user, 'timezone', None),
                lang=preferred_lang
            )


    def __str__(self):
        return self.identifier
