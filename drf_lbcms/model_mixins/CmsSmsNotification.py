# -*- coding: utf-8 -*-
from django.db.models import Q
from django.conf import settings as djsettings
from django.template import Template, Context
from django.contrib.auth import get_user_model

from drf_lbcms.helpers.l10n import l10n_filter
from drf_lbcms.helpers.sms import SmsApiWrapper
from drf_lbcms.helpers.html import unescape_html
import six




class ModelMixin(object):
    def save(self, *args, **kwargs):
        super(ModelMixin, self).save(*args, **kwargs)


    def send_user_notification(self, user, context={}, preferred_lang=None):
        if getattr(djsettings, 'DRF_LBCMS', None):
            if not djsettings.DRF_LBCMS.get('send_notify_sms', True):
                return

        # если уведомления отключены, то дальнейший код можно и не выполнять
        site = getattr(user, '_site', None)
        settings = context.get('settings', None) or \
                   context.get('cmsSettings', None)

        if not settings:
            from drf_lbcms.helpers import get_site_settings
            settings = get_site_settings(site=site)

        integrations_settings = getattr(settings, 'serverIntegrations', {}) or {}
        if not integrations_settings.get('useSmsNotifications', False):
            return

        CmsUser = get_user_model()
        preferred_lang = preferred_lang or getattr(user, 'preferred_lang', None) or getattr(user, 'preferredLang', None) or 'en'
        smsapi = None


        def send_sms(user, body, phones=None, timezone=None, lang=None):
            if not phones:
                accountData = getattr(user, 'accountData', {}) or {}
                if not isinstance(accountData, dict):
                    raise Exception("Invalid accountData for user {username}. Expect <type 'dict'>, got {type}".format(
                        username=user.username,
                        type=type(accountData)
                    ))

                phones = accountData.get('phones', []) or []

            if not phones or not isinstance(phones, list):
                return

            body = body or ("No body content for notification sms with identifier = "+self.identifier)
            if lang:
                body = "{% language '"+lang+"' %}{% localize on %}" + body + "{% endlocalize %}{% endlanguage %}"
            body = unescape_html(body)
            # поддержка часовых поясов
            timezone = timezone or getattr(djsettings, 'DRF_LBCMS', {}).get('timezone_for_templates', None)
            timezone = str(timezone)
            if timezone in ['UTC', 'None']:
                body = "{% load lbcms %}{% load l10n %}" + body
            else:
                body = "{% load lbcms %}{% load l10n %}{% load tz %}{% timezone '"+timezone+"' %}{% localtime on %}"+body+"{% endlocaltime %}{% endtimezone %}"

            errors = []
            for p in phones:
                if isinstance(p, six.string_types) or isinstance(p, six.text_type):
                    p = {'value': p, 'type': 'm'}
                if not isinstance(p, dict):
                    continue
                # поддержка старых версий
                p['value'] = p.get('value', None) or p.get('val', None)
                if not p.get('value', None):
                    continue
                if not p.get('type', 'm') == 'm':
                    continue
                c = Context(context)
                c['phone'] = p['value']
                t = Template(body)
                # отсылаем сообщение
                # print 'SEND ', self.identifier, p['value']
                err = smsapi.send_sms(p['value'], t.render(c))
                if err:
                    errors.append(err)
            return errors


        # набиваем контекст нужными переменными
        context['user'] = context.get('user', user)
        context['settings'] = settings
        context['LANGUAGE_CODE'] = context.get('lang', None) or preferred_lang
        context['CMS_DB_BUCKET'] = getattr(djsettings, 'DJANGO_GCS_BUCKET', '')
        HOSTNAME = getattr(djsettings, 'HOSTNAME', 'noname.com')
        if 'http://' not in HOSTNAME and 'https://' not in HOSTNAME:
            context['CMS_BASE_URL'] = 'http://' + HOSTNAME
        else:
            context['CMS_BASE_URL'] = HOSTNAME
        if site:
            context['SITE_DOMAIN'] = site.domain
        else:
            context['SITE_DOMAIN'] = context.get('SITE_DOMAIN', None) or getattr(djsettings, 'SITE_DOMAIN', None) or context['CMS_BASE_URL']


        sms_settings = integrations_settings.get('smsapicom', {}) or {}
        smsapi = SmsApiWrapper(credentials={
            'username': sms_settings.get('username', ''),
            'password': sms_settings.get('password', '')
        })
        smsapi.set_default_sender_name(sms_settings.get('senderName'))


        if self.sendEcho:
            if self.to:
                send_sms(
                    None,
                    l10n_filter(self.echoBody, preferred_lang),
                    phones=[
                        {'value':p, 'type': 'm'}
                        for p in self.to
                    ],
                    lang=preferred_lang
                )
            else:
                for u in CmsUser.objects.filter(Q(userRole='a') | Q(userRole='admin')):
                    send_sms(
                        u,
                        l10n_filter(self.echoBody, preferred_lang),
                        timezone=getattr(u, 'timezone', None),
                        lang=preferred_lang
                    )

        if self.send:
            # send sms to user
            errors = send_sms(
                user,
                l10n_filter(self.body, preferred_lang),
                timezone=getattr(user, 'timezone', None),
                lang=preferred_lang
            )
            if not errors:
                return
            return errors[0]
