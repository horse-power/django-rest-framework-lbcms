# -*- coding: utf-8 -*-
from drfs.generators.model import DjangoOrmModelGenerator
from drfs.generators.serializer import DjangoRestSerializerGenerator
from .models import L10nFile, L10nFileLocal
from .serializers import L10nFile as L10nFileSerializer, L10nFileLocal as L10nFileLocalSerializer



class LbCmsModelGenerator(DjangoOrmModelGenerator):
    def build_field__hasMany(self, name, params):
        is_sorted = params.get('sorted', False)
        field_class, field_args, field_kwargs = super(LbCmsModelGenerator, self).build_field__hasMany(name, params)

        if is_sorted:
            from sortedm2m.fields import SortedManyToManyField
            field_class = SortedManyToManyField

        return field_class, field_args, field_kwargs




class LbCmsSerializerGenerator(DjangoRestSerializerGenerator):
    def to_serializer(self):
        if self.model_class == L10nFile:
            return L10nFileSerializer
        if self.model_class == L10nFileLocal:
            return L10nFileLocalSerializer
        return super(LbCmsSerializerGenerator, self).to_serializer()
