import sys

default_app_config = 'drf_lbcms.apps.LbCmsAppConfig'



__gmap_api_key = None
def get_googlemap_api_key(request=None, site=None):
    global __gmap_api_key
    if __gmap_api_key:
        return __gmap_api_key
    if len(sys.argv) >= 2 and sys.argv[1] in ['loaddata', 'migrate']:
        return

    from drf_lbcms.helpers import get_site_settings
    settings = get_site_settings(request=request, site=site)
    if not settings:
        return None
    __gmap_api_key = settings.serverIntegrations.get('googleMaps', {}).get('apiKeyServer', None) or \
                     settings.serverIntegrations.get('googleMaps', {}).get('apiKey', None)
    return __gmap_api_key
