from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string
from django.conf import settings
import os

from drf_ng_generator import schemas
from drf_ng_generator.converter import SchemaConverter

text_pairs = [
    [
    """/:forModelField/upload/",
                    method: "POST",""",
    """/:forModelField/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: \"POST\","""
    ],[
    """/:destination/upload/",
                    method: "POST",""",
    """/:destination/upload/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: \"POST\","""
    ],[
    """Multipart": {""",
    """Multipart": {
                    headers: {
                        'Content-Type': undefined
                    },"""
    ],[
    """/import_xls/\",\n                    method: \"POST\"""",
    """/import_xls/",
                    headers: {
                        'Content-Type': undefined
                    },
                    method: \"POST\",
                    isArray: true"""
    ],[
    """"restoreDbAndFiles": {\n                    url: urlBase + "/CmsSettings/restore_db_and_files/",""",
    """"restoreDbAndFiles": {
                    url: urlBase + "/CmsSettings/restore_db_and_files/",
                    headers: {
                        'Content-Type': undefined
                    },"""
    ],[
    """"parseFile": {\n                    url: urlBase + "/CmsSettings/parse_file/",""",
    """"parseFile": {
                    url: urlBase + "/CmsSettings/parse_file/",
                    headers: {
                        'Content-Type': undefined
                    },"""
    ],[
"""
    .factory('lbAuth', function() {
        var lbAuth, load, props, propsPrefix, save;
        props = ['accessTokenId', 'currentUserId', 'rememberMe'];
        propsPrefix = '$lb$';
        lbAuth = function() {
            var self;
            self = this;
            props.forEach(function(name) {
                self[name] = load(name);
            });
            this.currentUserData = null;
        };
        save = function(storage, name, value) {
            var key;
            key = propsPrefix + name;
            if (value === null) {
                value = '';
            }
            storage[key] = value;
        };
        load = function(name) {
            var key;
            key = propsPrefix + name;
            if (localStorage){
               return localStorage[key] || sessionStorage[key] || null;
            }
            return sessionStorage[key] || null;
        };
        lbAuth.prototype.save = function() {
            var self, storage;
            self = this;
            storage = this.rememberMe ? localStorage : sessionStorage;
            if (!localStorage && !storage){
                console.warn('lbAuth: localStorage is unavailable, using sessionStorage');
                storage = sessionStorage;
            }
            props.forEach(function(name) {
                save(storage, name, self[name]);
            });
        };
        lbAuth.prototype.setUser = function(accessTokenId, userId, userData) {
            this.accessTokenId = accessTokenId;
            this.currentUserId = userId;
            this.currentUserData = userData;
        };
        lbAuth.prototype.clearUser = function() {
            this.accessTokenId = null;
            this.currentUserId = null;
            this.currentUserData = null;
        };
        lbAuth.prototype.clearStorage = function() {
            props.forEach(function(name) {
                save(sessionStorage, name, null);
                if (localStorage){
                    save(localStorage, name, null);
                }
            });
        };
        return new lbAuth;
    })""",
"""
    .factory('lbAuth', ["$injector", function($injector) {
        var lbAuth, load, props, propsPrefix, save, storage, useGdprStorage = false;
        props = ['accessTokenId', 'currentUserId', 'rememberMe'];
        propsPrefix = '$lb$';
        if ($injector.has('$gdprStorage')){
            storage = $injector.get('$gdprStorage');
            useGdprStorage = true;
        } else {
            storage = localStorage;
        }
        lbAuth = function() {
            var self;
            self = this;
            props.forEach(function(name) {
                self[name] = load(name);
            });
            this.currentUserData = null;
        };
        save = function(name, value) {
            if (useGdprStorage){
                storage[name] = value;
            } else {
                var key;
                key = propsPrefix + name;
                if (value === null) {
                    value = '';
                }
                storage[key] = value;
            }
        };
        load = function(name) {
            var key;
            if (useGdprStorage){
                return storage[name] || null;
            } else {
                key = propsPrefix + name;
                return storage[key] || null;
            }
        };
        lbAuth.prototype.save = function() {
            var self;
            self = this;
            props.forEach(function(name) {
                save(name, self[name]);
            });
            if (useGdprStorage){
                storage.$apply(true);
            }
        };
        lbAuth.prototype.setUser = function(accessTokenId, userId, userData) {
            this.accessTokenId = accessTokenId;
            this.currentUserId = userId;
            this.currentUserData = userData;
        };
        lbAuth.prototype.clearUser = function() {
            this.accessTokenId = null;
            this.currentUserId = null;
            this.currentUserData = null;
        };
        lbAuth.prototype.clearStorage = function() {
            props.forEach(function(name) {
                save(name, null);
            });
        };
        return new lbAuth;
    }])"""
    ]
]


def fix_js_resource(fpath):
    with open(fpath) as f:
        data = f.read()

    for pair in text_pairs:
        data = data.split(pair[0])
        data = pair[1].join(data)

    data += """

angular.module('lbServices').factory('LoopBackAuth', [
    'lbAuth',
    function(lbAuth){
        return lbAuth;
    }
]);

angular.module('facebook', []).provider('Facebook', function(){
    this.init = angular.noop;
    this.$get = function(){
        return {
            init: angular.noop
        }
    }
});
"""
    with open(fpath, 'w') as f:
        f.write(data)






class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument('filepath', nargs='+', type=str)


    def handle(self, *args, **options):
        for fpath in options.get('filepath', []):
            ext = os.path.splitext(fpath)[-1].lower()
            if ext not in ['.coffee', '.js']:
                fpath += '.js'
                ext = '.js'

            if ext == '.js':
                fix_js_resource(fpath)
