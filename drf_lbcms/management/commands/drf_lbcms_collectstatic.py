import os, fnmatch, six
from multiprocessing import Pool
from collections import OrderedDict
from django.core.management.base import CommandError
from django.contrib.staticfiles.management.commands.collectstatic import Command as BaseCommand
from django.contrib.staticfiles.finders import get_finders
from django.conf import settings

from drf_lbcms.helpers import l10nFile
from drf_lbcms.helpers.django import get_base_dir_as_string
from PIL import Image




def exec_command(params):
    command = Command()
    command.restore_options(params['options'])
    handler = getattr(command, params['handler'])
    handler(*params.get('args', []), **params.get('kwargs', {}))




class Command(BaseCommand):
    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '-o', '--only', action='append', default=[],
            dest='only_patterns', metavar='PATTERN',
            help="Collect only files or directories matching this glob-style "
                 "pattern. Use multiple times to add more.",
        )
        parser.add_argument('--webp', type=str)
        parser.add_argument('--skip-delete', dest='skip_delete', type=str)


    def set_options(self, **options):
        """
        Set instance variables based on an options dict
        """
        super(Command, self).set_options(**options)
        self.only_patterns = options.get('only_patterns', None) or []
        self.use_webp = options.get('webp', 'false') == 'true'
        self.skip_delete = options.get('skip_delete', 'false') == 'true'

    def dump_options(self):
        dump = {}
        for prop in ['interactive', 'verbosity', 'symlink', 'clear', 'dry_run', 'ignore_patterns', 'post_process', 'only_patterns', 'use_webp', 'skip_delete']:
            dump[prop] = getattr(self, prop, None)
        return dump

    def restore_options(self, options):
        for k,v in options.items():
            setattr(self, k, v)


    def is_file_ignored(self, fullpath):
        for p in self.ignore_patterns:
            if fnmatch.fnmatch(fullpath, p):
                return True
        for p in self.only_patterns:
            if not fnmatch.fnmatch(fullpath, p):
                return True
        return False


    def collect(self):
        """
        Perform the bulk of the work of collectstatic.

        Split off from handle() to facilitate testing.
        """
        if self.symlink and not self.local:
            raise CommandError("Can't symlink to a remote destination.")

        if self.clear:
            self.clear_dir('')

        if self.symlink:
            #handler = self.link_file
            handler_name = 'link_file'
        else:
            #handler = self.copy_file
            handler_name = 'copy_file'

        found_files = OrderedDict()
        files_for_multiprocessing = []
        dumped_options = self.dump_options()

        for finder in get_finders():
            for path, storage in finder.list(self.ignore_patterns):
                # Prefix the relative path if the source storage contains it
                if getattr(storage, 'prefix', None):
                    prefixed_path = os.path.join(storage.prefix, path)
                else:
                    prefixed_path = path
                source_path = storage.path(prefixed_path)
                if self.is_file_ignored(source_path):
                    continue

                if prefixed_path not in found_files:
                    found_files[prefixed_path] = (storage, path)
                    files_for_multiprocessing.append({
                        'options': dumped_options,
                        'handler': handler_name,
                        'args': [path, prefixed_path, storage]
                    })
                    if handler_name == 'link_file':
                        self.symlinked_files.append(prefixed_path)
                    else:
                        self.copied_files.append(prefixed_path)
                    #handler(path, prefixed_path, storage)
                else:
                    self.log(
                        "Found another file with the destination path '%s'. It "
                        "will be ignored since only the first encountered file "
                        "is collected. If this is not what you want, make sure "
                        "every static file has a unique path." % prefixed_path,
                        level=1,
                    )


        pool = Pool(processes=10)
        pool.map(exec_command, files_for_multiprocessing)

        # Here we check if the storage backend has a post_process
        # method and pass it the list of modified files.
        if self.post_process and hasattr(self.storage, 'post_process'):
            processor = self.storage.post_process(found_files,
                                                  dry_run=self.dry_run)
            for original_path, processed_path, processed in processor:
                if isinstance(processed, Exception):
                    self.stderr.write("Post-processing '%s' failed!" % original_path)
                    # Add a blank line before the traceback, otherwise it's
                    # too easy to miss the relevant part of the error message.
                    self.stderr.write("")
                    raise processed
                if processed:
                    self.log("Post-processed '%s' as '%s'" %
                             (original_path, processed_path), level=1)
                    self.post_processed_files.append(original_path)
                else:
                    self.log("Skipped post-processing '%s'" % original_path)

        return {
            'modified': self.copied_files + self.symlinked_files,
            'unmodified': self.unmodified_files,
            'post_processed': self.post_processed_files,
        }


    def copy_file(self, path, prefixed_path, source_storage):
        """
        Attempt to copy ``path`` with storage
        """
        # Skip this file if it was already copied earlier
        if prefixed_path in self.copied_files:
            return self.log("Skipping '%s' (already copied earlier)" % path)
        # Delete the target file if needed or break
        if not self.skip_delete:
            if not self.delete_file(path, prefixed_path, source_storage):
                return
        # The full path of the source file
        source_path = source_storage.path(path)
        # Finally start copying
        if self.dry_run:
            self.log("Pretending to copy '%s'" % source_path, level=1)
        else:
            build_data = getattr(settings, 'BUILD_DATA', None) or {}
            BASE_DIR = get_base_dir_as_string()
            with source_storage.open(path) as source_file:
                if source_path.startswith('/var/app/client') or source_path.startswith(os.path.join(BASE_DIR, 'client')):
                    prefixed_path = "client/" + prefixed_path
                if source_path.startswith('/var/app/bower_components') or source_path.startswith(os.path.join(BASE_DIR, 'bower_components')):
                    prefixed_path = "bower_components/" + prefixed_path
                if build_data.get('staticFilesPrefix', None) and build_data['staticFilesPrefix'] != 'client':
                    prefixed_path = build_data['staticFilesPrefix'] + '/' + prefixed_path
                #pool.apply_async(self.storage.save, [prefixed_path, source_file])
                self.log("Copying '%s' to '%s'" % (source_path, prefixed_path), level=1)
                try:
                    self.storage.save(prefixed_path, source_file)
                except:
                    self.storage.save(prefixed_path, source_file)

            if self.use_webp:
                ext = os.path.splitext(prefixed_path)[-1].lower()
                if ext in l10nFile.ALLOWED_IMG_EXT_FOR_WEBP:
                    with source_storage.open(path) as source_file:
                        self.log("Generating wepb file for '%s'" % prefixed_path, level=1)
                        if six.PY2:
                            img = Image.open(source_file)
                        else:
                            img = Image.open(source_file.file)
                        l10nFile.save_image_as_webp(
                            img,
                            prefixed_path,
                            self.storage,
                            is_gif=ext=='.gif'
                        )
        self.copied_files.append(prefixed_path)
