import shlex
import subprocess

from django.core.management.base import BaseCommand
from django.utils import autoreload
try:
    import celery
except ImportError:
    celery = None


def restart_celery():
    cmd = 'pkill -9 celery'
    subprocess.call(shlex.split(cmd))
    cmd = 'celery --app www_site worker --task-events -l info'
    subprocess.call(shlex.split(cmd))


class Command(BaseCommand):

    def handle(self, *args, **options):
        if not celery:
            print("ERROR: Celery is not installed. Use 'pip install celery'")
            return
        print('Starting celery worker with autoreload...')
        if hasattr(autoreload, 'run_with_reloader'):
            autoreload.run_with_reloader(restart_celery)
            return
        # old versions
        autoreload.main(restart_celery)
