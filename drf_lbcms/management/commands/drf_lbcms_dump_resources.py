# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os
from drf_lbcms.models import CmsSmsNotification, CmsMailNotification, CmsPdfData
from drf_lbcms.helpers import dump_to_file
from drf_lbcms.helpers.django import get_base_dir_as_string



def dump_notify(obj, dir_path, as_text=False):
    data = {
        'description': obj.description or {},
        'send': obj.send,
        'sendEcho': obj.sendEcho
    }
    if getattr(obj, 'subject', None):
        data['subject'] = obj.subject
    if getattr(obj, 'echoSubject', None):
        data['echoSubject'] = obj.echoSubject
    dump_to_file(data, os.path.join(dir_path, 'description.json'))

    for lang, body in obj.body.items():
        if not body:
            continue
        if as_text:
            f = open(os.path.join(dir_path, 'body.'+lang+'.txt'), 'w')
        else:
            f = open(os.path.join(dir_path, 'body.'+lang+'.html'), 'w')
        f.write(body)
        f.close()
    for lang, body in obj.echoBody.items():
        if not body:
            continue
        if as_text:
            f = open(os.path.join(dir_path, 'echoBody.'+lang+'.txt'), 'w')
        else:
            f = open(os.path.join(dir_path, 'echoBody.'+lang+'.html'), 'w')
        f.write(body)
        f.close()



def dump_pdf(obj, dir_path, as_text=False):
    data = {
        'description': obj.description or {},
        'title': obj.title,
    }
    dump_to_file(data, os.path.join(dir_path, 'description.json'))

    for lang, body in obj.content.items():
        if not body:
            continue
        if as_text:
            f = open(os.path.join(dir_path, 'body.'+lang+'.txt'), 'w')
        else:
            f = open(os.path.join(dir_path, 'body.'+lang+'.html'), 'w')
        f.write(body)
        f.close()

    for t in ['header', 'footer']:
        if not obj.runningTitles.get(t, None):
            continue
        for lang, body in obj.runningTitles[t].items():
            if not body:
                continue
            if as_text:
                f = open(os.path.join(dir_path, t+'.'+lang+'.txt'), 'w')
            else:
                f = open(os.path.join(dir_path, t+'.'+lang+'.html'), 'w')
            f.write(body)
            f.close()







class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        BASE_DIR = get_base_dir_as_string()

        for item in CmsMailNotification.objects.all():
            if item.identifier.startswith('cms.gdprRequest'):
                continue
            dir_path = os.path.join(BASE_DIR, 'resources', 'mail', item.identifier)
            if not os.path.isdir(dir_path):
                os.makedirs(dir_path)
            dump_notify(item, dir_path)


        for item in CmsSmsNotification.objects.all():
            if item.identifier.startswith('cms.gdprRequest'):
                continue
            dir_path = os.path.join(BASE_DIR, 'resources', 'sms', item.identifier)
            if not os.path.isdir(dir_path):
                os.makedirs(dir_path)
            dump_notify(item, dir_path, as_text=True)

        for item in CmsPdfData.objects.all():
            dir_path = os.path.join(BASE_DIR, 'resources', 'pdf', item.identifier)
            if not os.path.isdir(dir_path):
                os.makedirs(dir_path)
            dump_pdf(item, dir_path)
