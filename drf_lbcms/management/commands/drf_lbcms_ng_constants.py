from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string
from django.apps import apps
from timezone_field import TimeZoneField
import os, re, json
from drf_lbcms.helpers.l10n import dump_json
from collections import OrderedDict




def fix_choices(choices):
    if not isinstance(choices[0], tuple) and not isinstance(choices[0], list):
        if isinstance(choices[0], tuple) or isinstance(choices[0], list):
            choices = [
                list(c)
                for c in choices
            ]
        else:
            choices = [
                [c,c]
                for c in choices
            ]
    choices = list(choices)
    i = 0
    for ch in choices:
        ch = list(ch)
        ch[0] = json.dumps(ch[0], separators=(',', ':'))
        choices[i] = ch
        i += 1
    return choices

def to_underscope(text):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', text)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument('filepath', nargs='+', type=str)
        parser.add_argument('modulename', type=str)
        parser.add_argument('--ignoretranslation=', type=str)
        parser.add_argument('--template=', type=str)


    def handle(self, *args, **options):
        ignore_angular_tr_for_models = options.get('--ignoretranslation=', "") or options.get('ignoretranslation=', "") or ''
        ignore_angular_tr_for_models = ignore_angular_tr_for_models.split(',')
        template = options.get('--template=', '') or options.get('template=', '') or 'angular1'
        template_path = 'drf_lbcms/commands/ng_constants'
        if template == 'vue3':
            template_path = 'drf_lbcms/commands/vue3_constants'
        elif template != 'angular1':
            template_path = template

        models = OrderedDict()
        file_path = options.get('filepath', [])[0]
        ext = os.path.splitext(file_path)[-1].lower()
        if ext not in ['.js', '.json', '.ts']:
            file_path += '.js'
            ext = '.js'

        for model in sorted(apps.get_models(), key=lambda m: m.__name__):
            model_name = to_underscope(model.__name__).upper()
            for f in sorted(model._meta.fields, key=lambda x: x.name):
                if f.__class__ == TimeZoneField:
                    continue
                if f.choices:
                    if not models.get(model_name, None):
                        models[model_name] = []
                    models[model_name].append({
                        'name': f.name,
                        'choices': fix_choices(f.choices),
                        'ignoreTranslate': model.__name__ in ignore_angular_tr_for_models
                    })

        def collect_embedded(embedded_filepath, prefix=None):
            if os.path.isdir(embedded_filepath):
                for name in os.listdir(embedded_filepath):
                    collect_embedded(
                        os.path.join(embedded_filepath, name),
                        prefix=to_underscope(os.path.basename(embedded_filepath))
                    )
                return
            if not os.path.isfile(embedded_filepath):
                return
            with open(embedded_filepath) as f:
                model = json.load(f)
            model_name = to_underscope(model['name']).upper()
            if prefix:
                model_name = prefix.upper() + '_' + model_name
            for name, params in model.get('properties', {}).items():
                if params.get('choices', None):
                    if not models.get(model_name, None):
                        models[model_name] = []
                    models[model_name].append({
                        'name': name,
                        'choices': fix_choices(params['choices']),
                        'ignoreTranslate': model['name'] in ignore_angular_tr_for_models
                    })

        for app in apps.get_app_configs():
            d = os.path.join(app.path, 'embedded_models.json')
            if not os.path.isdir(d):
                continue
            for name in os.listdir(d):
                collect_embedded(os.path.join(d, name) )

        #
        model_list = [
            c
            for c in apps.get_models() if getattr(c, 'DRFS_MODEL_DEFINITION', {})
        ]
        representations = OrderedDict()
        for m in sorted(model_list, key=lambda x: x.__name__):
            r = m.DRFS_MODEL_DEFINITION.get('representation', None) or {}
            html = r.get('html', None) or r.get('str', None) or None
            placeholder = r.get('placeholder', None)
            name = r.get('name', None)
            if not html:
                for property, params in m.DRFS_MODEL_DEFINITION.get('properties', {}).items():
                    if property in ['title', 'name'] and params.get('type', None) == 'string':
                        html = "{{$item.%s}}" % property
                        break
            if not html:
                rels = m.DRFS_MODEL_DEFINITION.get('relations', {})
                for property, params in rels.items():
                    if property in ['title', 'name'] and params.get('type', None) == 'embedsOne' and params.get('model', None) == 'L10nString':
                        html = "{{$item.%s | l10n: forceLang}}" % property
                        break
            if not html and not name:
                continue
            representations[m.__name__] = OrderedDict()
            representations[m.__name__]['app'] = m.__module__
            if name:
                representations[m.__name__]['name'] = name
            if html:
                representations[m.__name__]['html'] = html
            for prop in ['namePlural', 'placeholder', 'cssClass', 'cssIconClass']:
                if r.get(prop, None):
                    representations[m.__name__][prop] = r[prop]

        if 'Group' not in representations:
            representations['Group'] = {
                'app': 'auth',
                'modelName': 'GroupPermissions'
            }
        if 'CmsTranslations' not in representations:
            representations['CmsTranslations'] = {
                'app': 'drf_lbcms',
                'modelName': 'CmsTranslations'
            }

        if ext in ['.js', '.ts']:
            file_data = render_to_string(template_path + ext, {
                'module_name': options.get('modulename', 'helpers'),
                'models': models,
                'representations': dump_json(representations, indent=4, separators=(',', ':'))
            })

        elif ext == '.json':
            file_data = dump_json(
                {
                    'models': models,
                    'representation': representations
                },
                indent=4,
                separators=(',', ':')
            )

        with open(file_path, 'w') as f:
            f.write(file_data)
