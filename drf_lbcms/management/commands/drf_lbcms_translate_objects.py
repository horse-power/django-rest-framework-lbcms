# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.apps import apps
import six



def is_str(text):
    return isinstance(text, six.text_type) or isinstance(text, six.string_types)




class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument('--models', type=str)
        parser.add_argument('--fromLang', type=str)
        parser.add_argument('--proxyLang', type=str)
        parser.add_argument('--toLang', type=str)



    def handle(self, *args, **options):
        models = []
        model_names = options.get('--models=', None) or options.get('models', None) or ''
        model_names = model_names.split(',')
        base_langs = options.get('--fromLang=', None) or options.get('fromLang', None) or ''
        base_langs = base_langs.split(',')
        to_lang = options.get('--toLang=', None) or options.get('toLang', None) or ''
        proxy_lang = options.get('--proxyLang', None) or options.get('proxyLang', None)
        for m in apps.get_models():
            if m.__name__ in model_names:
                models.append(m)
        if not models or not base_langs or not to_lang:
            return

        # pip install google-api-python-client
        from googleapiclient.discovery import build
        service = build(
            'translate',
            'v2',
            developerKey='AIzaSyCQ894_ck_WREvoqZwo6SEFW-sq-Jgw45M'
        )

        def translate(text, fromLang, toLang, proxyLang=None):
            if toLang == 'cz':
                toLang = 'cs'
            if proxyLang:
                text = translate(text, fromLang, proxyLang)
                return translate(text, proxyLang, toLang)
            r = service.translations().list(
              source=fromLang,
              target=toLang,
              q=[text],
              format='html'
            )
            r.headers['referer'] = 'http://localhost:8000'
            response = r.execute()
            return response['translations'][0]['translatedText']

        def deep_translate(obj):
            if isinstance(obj, dict):
                for lang in base_langs:
                    if obj.get(to_lang, None):
                        # уже переведено
                        return obj
                    if obj.get(lang, None) and is_str(obj[lang]):
                        # базовый язык найден, переводим
                        obj[to_lang] = translate(obj[lang], lang, to_lang)
                        return obj
                # проходимся по свойствам, т.к. язык не найден
                for k,v in obj.items():
                    if isinstance(v, (dict, list)):
                        obj[k] = deep_translate(v)
            if isinstance(obj, list):
                i = 0
                for item in obj:
                    obj[i] = deep_translate(item)
                    i += 1
            return obj


        for model in models:
            # print 'Translate - ' + model.__name__
            if model.__name__ == 'CmsTranslations':
                if not model.objects.filter(lang=to_lang).count():
                    continue
                for item in model.objects.filter(lang=to_lang).iterator():
                    i = 0
                    for m in item.msgs:
                        if not m.get('msgStr', None) or not m['msgStr'][0]:
                            if len(m['msgStr']) == 0:
                                m['msgStr'] = ['']
                            m['msgStr'][0] = translate(
                                m['msgId'],
                                base_langs[0],
                                to_lang,
                                proxyLang=proxy_lang
                            )
                        item.msgs[i] = m
                        i+= 1
                    item.save()
                continue
            fields = model._meta.fields
            for item in model.objects.all().iterator():
                # ищем ключи
                # print 'Id - ' + str(item.id)
                for field in fields:
                    v = getattr(item, field.name)
                    if isinstance(v, (dict, list)):
                        setattr(item, field.name, deep_translate(v))
                item.save()
