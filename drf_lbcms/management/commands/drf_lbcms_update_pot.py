# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.utils import timezone
from datetime import datetime
from drf_lbcms.helpers import gettext
from drf_lbcms.models import CmsTranslations
from difflib import SequenceMatcher
import re, copy, json


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def replace_ng_to_vue_variables(ng_translated_text, vue_text):
    variables = re.findall(r"\{\{.*?\}\}", ng_translated_text)
    vue_variables = re.findall(r"\%\{.*?\}", vue_text)
    if len(variables) != len(vue_variables):
        print("FAILED TO REPLACE VARIABLES: ", ng_translated_text)
        raise
    i = 0
    for v in vue_variables:
        ng_translated_text = ng_translated_text.replace(variables[i], v)
        i += 1
    return ng_translated_text


def find_similar_msg(msgs_arr, msg_to_find=None):
    if not msg_to_find:
        return None
    for msg in msgs_arr:
        if msg.get('msgId', '') == msg_to_find.get('msgId', '') and \
            msg.get('msgCtxt', '') == msg_to_find.get('msgCtxt', ''):
            return msg

    has_variables = '%{' in msg_to_find["msgId"]
    # сравниваем тогда "похожесть" строк
    possible_msgs = []
    for msg in msgs_arr:
        if msg.get('msgCtxt', '') != msg_to_find.get('msgCtxt', '') or '{{' not in msg.get('msgId', ''):
            continue
        if msg_to_find['msgId'][0].isupper() != msg['msgId'][0].isupper():
            continue
        if has_variables and "{{" not in msg["msgId"]:
            continue
        if not has_variables and "{{" in msg["msgId"]:
            continue
        ratio = similar(msg['msgId'], msg_to_find['msgId'])
        if ratio > 0.65:
            possible_msgs.append({"ratio": ratio, "msg": msg})

    if not possible_msgs:
        return
    sorted(possible_msgs, key=lambda x: x['ratio'] )
    msg = copy.deepcopy(possible_msgs[-1]['msg'])
    for i in range(len(msg['msgStr'])):
        try:
            msg['msgStr'][i] = replace_ng_to_vue_variables(
                msg["msgStr"][i],
                msg_to_find["msgId"]
            )
        except Exception as e:
            return None

    return msg




def migrate_ng_to_vue3_translations(tr_obj, migrate_for_vue3_from_langs):
    lang = tr_obj.lang.split(':')[0]
    if lang == 'cs' and lang not in migrate_for_vue3_from_langs and 'cz' in migrate_for_vue3_from_langs:
        lang = 'cz'
    if lang not in migrate_for_vue3_from_langs:
        return
    orig_obj = CmsTranslations.objects.get(lang=lang)
    found = 0
    not_found = 0
    for i in range(len(tr_obj.msgs)):
        msg = tr_obj.msgs[i]
        orig_msg = find_similar_msg(orig_obj.msgs, msg)
        if not orig_msg:
            not_found += 1
            print("FAILED TO FIND", msg)
        else:
            tr_obj.msgs[i]['msgStr'] = orig_msg["msgStr"]
            found += 1
    print("TOTAL STRINGS = %s, FOUND = %s, NOT FOUND = %s" % (
        len(tr_obj.msgs), found, not_found
    ))





class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument('--langs', nargs='+', type=str)
        parser.add_argument('--rm-langs', nargs='+', type=str)
        parser.add_argument('--podir', action='append', type=str)
        parser.add_argument('--import-po', nargs='+', type=str)
        parser.add_argument('--migrate-for-vue3-from-langs', type=str)

    def handle(self, *args, **options):
        _langs = options.get('langs', None)
        langs = ""
        if isinstance(_langs, list):
            for l in _langs:
                langs = langs + ',' + l
        else:
            langs = _langs
        if not langs:
            if hasattr(settings, 'APP_CONTEXT'):
                langs = settings.APP_CONTEXT['CMS_ALLOWED_LANGS']
                if settings.APP_CONTEXT['CMS_DEFAULT_LANG'] in langs:
                    langs.remove(settings.APP_CONTEXT['CMS_DEFAULT_LANG'])
                if not langs:
                    return
        else:
            langs = langs.split(',')

        migrate_for_vue3_from_langs = []
        if options.get('migrate_for_vue3_from_langs', None):
            migrate_for_vue3_from_langs = options.get('migrate_for_vue3_from_langs', None).split(',')

        gettextMsgs = gettext.pot_to_json(
            options['podir'],
            options={'withReferences': True}
        )['msgs']

        importedGettextMsgs = {}
        for impf in options.get('import_po', []) or []:
            impf = impf.split(':')
            importedGettextMsgs[impf[0]] = gettext.pot_to_json(impf[1], options={'withReferences': True, 'withMsgStr': True})['msgs']

        existed_langs = list(CmsTranslations.objects.filter(lang__in=langs).values_list('lang', flat=True))
        for l in langs:
            if l and l not in existed_langs:
                CmsTranslations.objects.create(lang=l, publicationDate=timezone.now())

        for trObj in CmsTranslations.objects.all():
            if trObj.lang in migrate_for_vue3_from_langs:
                continue
            if trObj.lang not in langs:
                trObj.isPublished = False
                trObj.publicationDate = None
                trObj.save()
                continue
            if len(trObj.msgs):
                trObj.msgs = gettext.merge_translation_msgs(
                    trObj.msgs or [],
                    gettextMsgs
                )
                if importedGettextMsgs.get(trObj.lang, None):
                    for item in importedGettextMsgs[trObj.lang]:
                        for i in range(len(trObj.msgs)):
                            msg = trObj.msgs[i]
                            if msg['msgId'] == item['msgId'] and msg.get('msgCtxt', '') == item.get('msgCtxt', ''):
                                trObj.msgs[i]['msgStr'] = item['msgStr']
            else:
                trObj.msgs = gettextMsgs
            trObj.isPublished = True
            trObj.publicationDate = trObj.publicationDate or datetime.now()
            if migrate_for_vue3_from_langs:
                migrate_ng_to_vue3_translations(trObj, migrate_for_vue3_from_langs)
            trObj.save()

        if options.get('rm_langs', None):
            CmsTranslations.objects.filter(lang__in=options['rm_langs'].split(',')).delete()
