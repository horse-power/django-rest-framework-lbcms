
from django.apps import apps
import os, json
from django.core.management.base import CommandError

from rest_framework.management.commands.generateschema import Command as BaseCommand

from drf_ng_generator.openapi.schemas import SchemaGenerator as BaseSchemaGenerator
from drfs.serializers.openapi import EmbeddedOpenapiSchemaGenerator


def collect_all_embedded_models():
    models = []
    def collect(basedir, dirpath, is_builtin):
        for file in os.listdir(dirpath):
            fpath = os.path.join(dirpath, file)
            if os.path.isdir(fpath):
                collect(basedir, fpath, is_builtin)
                continue
            if not os.path.isfile(fpath) or os.path.splitext(fpath)[-1].lower() != '.json':
                continue
            if file == 'L10nBaseString.json':
                continue
            name = fpath.replace(basedir, '')[1:-5]
            if is_builtin:
                name = 'builtin/' + name
            with open(fpath) as f:
                try:
                    models.append({
                        'name': name,
                        'model': json.load(f)
                    })
                except Exception as e:
                    raise Exception("Unable to parse {file}. {error}".format(file=fpath, error=e))



    for app in apps.get_app_configs():
        dpath = os.path.join(app.path, 'embedded_models.json')
        if not os.path.isdir(dpath):
            continue
        collect(dpath, dpath, app.name in ['drf_lbcms', 'lbcms'])
    return models



def nested_set(dic, keys, value):
    for key in keys[:-1]:
        dic[key] = dic.get(key, None) or {}
        dic = dic[key]
    dic[keys[-1]] = value


def fix_components(components):
    for name in list(components.keys()):
        if '/' not in name:
            continue
        orig_component = components[name]
        del components[name]
        nested_set(components, name.split('/'), orig_component)
    return components



class SchemaGenerator(BaseSchemaGenerator):
    def get_schema(self, *args, **kwargs):
        schema = super().get_schema(*args, **kwargs)
        for path in schema['paths']:
            for method in schema['paths'][path]:
                if 'x-django' in schema['paths'][path][method]:
                    del schema['paths'][path][method]['x-django']

        # теперь выгребаем все embedded модели и их описания
        embedded_models = collect_all_embedded_models()
        components = {}
        for model in embedded_models:
            generator = EmbeddedOpenapiSchemaGenerator(model['name'], model['model'])
            components[model['name']] = generator.get_model_schema()

        schema['components']['schemas']['embedded'] = fix_components(components)
        schema['components']['schemas']['node_modules'] = {
            "vue3-bootstrap5": {
                "Bootstrap5": {
                    "interfaces":{
                        "TranslatableString": {
                            "type": "node_module"
                        },
                        "GeoPoint": {
                            "type": "node_module"
                        }
                    }
                }
            },
            "vue3-drf-lbcms": {
                "CmsApp": {
                    "interfaces": {
                        "L10nFile": {
                            "type": "node_module"
                        }
                    }
                }
            }
        }
        return schema


class Command(BaseCommand):
    
    
    def get_generator_class(self):
        return SchemaGenerator