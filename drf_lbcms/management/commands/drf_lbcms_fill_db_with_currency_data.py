from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from datetime import datetime, timedelta
from currency_converter import CurrencyConverter, RateNotFoundError

from drf_lbcms.models import CmsCurrencyData
from drf_lbcms.helpers import date


def arg_to_date(a):
    if a == 'now':
        return datetime.now().date()
    return date.parse_date(a)



class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument('--todate', type=str)
        parser.add_argument('--fromdate', type=str)
        parser.add_argument('--currencies', type=str)


    def handle(self, *args, **options):
        from_date = arg_to_date(options['--fromdate'].split('=')[-1])
        to_date = arg_to_date(options['--todate'].split('=')[-1])
        currencies = options['--currencies'].split('=')[-1].split(',')
        if from_date > to_date:
            to_date, from_date = (from_date, to_date)

        converter = CurrencyConverter('http://www.ecb.int/stats/eurofxref/eurofxref-hist.zip')
        d = from_date - timedelta(days=1)

        while d < to_date:
            d += timedelta(days=1)
            currencyData = {'date': d}
            for c in currencies:
                if c == 'EUR':
                    continue
                try:
                    currencyData[c] = converter._get_rate(c, date=d)
                except RateNotFoundError:
                    pass

            if currencyData.keys() == ['date']:
                print('skip', d)
                continue
            print('save currency data', currencyData)
            CmsCurrencyData.objects.create(**currencyData)
