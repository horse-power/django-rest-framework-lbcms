import os, six, math
from django.core.management.base import BaseCommand, CommandError
from django.core.files.storage import default_storage as storage
from drf_lbcms.helpers.l10nFile import save_image_as_webp, ALLOWED_IMG_EXT_FOR_WEBP
from PIL import Image


def humanize_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])




class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--report', type=str)
        parser.add_argument('--delete-exisisting-webp', type=str)

    def generate_webp(self, filename, ext):
        if six.PY2:
            img = Image.open(storage.open(filename))
        else:
            img = Image.open(storage.open(filename).file)
        save_image_as_webp(img, filename, storage, is_gif=ext=='.gif')

    def generate_report(self, filepath, statistics):
        import csv
        thumbs = []
        origs = []
        filepath = os.path.splitext(filepath)[0]
        for k,v in statistics.items():
            p = round(
                100 * v['webpSize'] / float(v['origSize']),
                2
            )
            p = str(p).replace('.', ',')
            if '.thumb.' in k:
                thumbs.append([
                    k, v['origSize'], v['webpSize'], humanize_size(v['origSize']), humanize_size(v['webpSize']), p
                ])
            else:
                origs.append([
                    k, v['origSize'], v['webpSize'], humanize_size(v['origSize']), humanize_size(v['webpSize']), p
                ])


        with open(filepath + '-orig.csv', 'w', newline='') as csvfile:
            writer = csv.writer(
                csvfile, delimiter=';', quoting=csv.QUOTE_ALL
            )
            writer.writerow([
                "Filename", "Original size (bytes)", "WebP size (bytes)", "Original size", "WebP size", "Compress percentage",
            ])
            writer.writerow([])
            for row in origs:
                writer.writerow(row)

        with open(filepath + '-thumbs.csv', 'w', newline='') as csvfile:
            writer = csv.writer(
                csvfile, delimiter=';', quoting=csv.QUOTE_ALL
            )
            writer.writerow([
                "Filename", "Original size (bytes)", "WebP size (bytes)", "Original size", "WebP size", "Compress percentage"
            ])
            writer.writerow([])
            for row in thumbs:
                writer.writerow(row)


    def handle(self, *args, **options):
        i = 0
        report = options.get('report', None)
        delete_exisisting_webp = options.get('delete_exisisting_webp', False)
        report_data = {}
        files = storage.listdir('')[1]

        if delete_exisisting_webp == 'only':
            for f in files:
                if os.path.splitext(f)[-1].lower() == '.webp':
                    self.stdout.flush()
                    storage.delete(f)
                    print("Deleting WebP file '%s'" % f, end='\r')
            print("All WebP files deleted")
            return

        files = [
            f
            for f in files if os.path.splitext(f)[-1].lower() in self.IMG_EXT_FOR_WEBP
        ]
        total = len(files)

        for filename in files:
            ext = os.path.splitext(filename)[-1].lower()
            i+=1

            if '.thumb.' in filename:
                thumbSize = filename.split('.thumb.')[-1].split('.')[0]
                thumbSize = {
                    'width': int(thumbSize.split('x')[0]),
                    'height': int(thumbSize.split('x')[1])
                }
                if thumbSize['width'] <= 80 and thumbSize['height'] <= 80:
                    continue

            webpname = os.path.splitext(filename)[0] + '.webp'
            if delete_exisisting_webp == 'true' and storage.exists(webpname):
                storage.delete(webpname)
            elif storage.exists(webpname):
                self.stdout.flush()
                print("Ignoring %s of %s files" % (i, total), end='\r')
                continue
            try:
                self.generate_webp(filename, ext)
                self.stdout.flush()
                #self.stdout.write("\rConverted to WebP %s of %s files" % (i, total))
                print("Converted to WebP %s of %s files" % (i, total), end='\r')
                if report:
                    report_data[filename] = {
                        'origSize': storage.size(filename),
                        'webpSize': storage.size(webpname)
                    }
            except:
                self.stderr.write("Failed to generate file for '%s'" % filename)
                if report:
                    report_data[filename] = {
                        'origSize': 0,
                        'webpSize': 0
                    }


        print("Converted to WebP %s of %s files" % (i, total))
        if report:
            self.generate_report(report, report_data)
