from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string
from django.apps import apps
import os, re, json
from collections import OrderedDict

from drf_ng_generator.schemas import SchemaGenerator
from drf_ng_generator.converter import SchemaConverter




class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument('--filepath=', nargs='+', type=str)
        parser.add_argument('--ignoremodels=', type=str)
        parser.add_argument('--weakrelations=', type=str)
        parser.add_argument('--ignorerelations=', type=str)
        parser.add_argument('--fakerelations=', type=str)
        parser.add_argument('modulename', type=str)


    def normalize_field(self, params):
        definition = OrderedDict()
        for k in ['field', 'type', 'model', 'alias', 'weakAlias']:
            if k in params:
                definition[k] = params[k]
        return definition

    def get_model_relaions(self, model, ignore_models=[], weak_relations={}):
        relations = []
        for field in model._meta.get_fields():
            if not field.is_relation:
                continue
            if field.related_model.__name__ in ignore_models:
                continue
            if not field.auto_created or field.concrete:
                if field.one_to_one:
                    relations.append(self.normalize_field({
                        'field': field.name,
                        'type': 'hasOne',
                        'model': field.related_model.__name__,
                        'alias': "$" + field.name
                    }))
                if field.many_to_one:
                    relations.append(self.normalize_field({
                        'field': field.name,
                        'type': 'belongsTo',
                        'model': field.related_model.__name__,
                        'alias': "$" + field.name
                    }))
                if field.many_to_many or field.one_to_many:
                    relations.append(self.normalize_field({
                        'field': field.name,
                        'type': 'hasMany',
                        'model': field.related_model.__name__,
                        'alias': "$" + field.name
                    }))
                # weak relations
                rmodel = field.related_model
                wr = weak_relations.get(model.__name__, {})
                if field.name not in wr:
                    continue
                for rfield in rmodel._meta.get_fields():
                    if not rfield.is_relation or rfield.name not in wr[field.name]:
                        continue
                    if field.many_to_many or rfield.one_to_many:
                        field_path = field.name + "[X]." + rfield.name
                    else:
                        field_path = field.name + "." + rfield.name
                    if rfield.one_to_one:
                        relations.append(self.normalize_field({
                            'field': field_path,
                            'type': 'weakHasOne',
                            'model': rfield.related_model.__name__,
                            'weakAlias': "$" + rfield.name
                        }))
                    if rfield.many_to_one:
                        relations.append(self.normalize_field({
                            'field': field_path,
                            'type': 'weakBelongsTo',
                            'model': rfield.related_model.__name__,
                            'weakAlias': "$" + rfield.name
                        }))
                    if rfield.many_to_many or rfield.one_to_many:
                        relations.append(self.normalize_field({
                            'field': field_path,
                            'type': 'weakHasMany',
                            'model': rfield.related_model.__name__,
                            'weakAlias': "$" + rfield.name
                        }))
        return relations

    def handle(self, *args, **options):
        generator = SchemaGenerator()
        schema = generator.get_schema()
        converter = SchemaConverter()
        schema, base_api_url = converter.convert(schema, ignore_viewset_names=True)
        relations = OrderedDict()

        ignore_models = options.get('--ignoremodels=', "") or options.get('ignoremodels=', "")
        ignore_models = ignore_models.split(',')
        weak_relations = {}
        wr = options.get('--weakrelations=', "") or options.get('weakrelations=', "")
        if wr == 'None':
            wr = None
        if wr:
            for p in wr.split(','):
                if p.count('.') != 2:
                    raise ValueError("Invalid weak relation '%s'. Format is: 'Model1.relation2.relation3'" % p)
                model_name, relation1, relation2 = p.split('.')
                weak_relations[model_name] = weak_relations.get(model_name, {})
                weak_relations[model_name][relation1] = weak_relations[model_name].get(relation1, [])
                if relation2 not in weak_relations[model_name][relation1]:
                    weak_relations[model_name][relation1].append(relation2)


        fr = options.get('--fakerelations=', "") or options.get('fakerelations=', "")
        if fr == 'None':
            fr = None
        if fr:
            for p in fr.split(','):
                conf = p.split('--')
                if len(conf) != 2:
                    raise ValueError("Invalid fake relation '%s'. Format is: 'Model1.field1(fakeFieldName1)--[relationType]Model2'" % p)
                relationType = conf[1].split(']')[0].replace('[', '')
                modelName2 = conf[1].split(']')[-1]
                modelName1 = conf[0].split('.')[0]
                fieldName = conf[0].split('.')[-1].split('(')[0]
                fakeFieldName1 = conf[0].split('.')[-1].split('(')[-1].replace(')', '') or fieldName
                if not fakeFieldName1.startswith('$'):
                    fakeFieldName1 = "$%s" % fakeFieldName1

                if not relationType or not modelName2 or not modelName1 or not fieldName:
                    raise ValueError("Invalid fake relation '%s'. Format is: 'Model1.field1(fakeFieldName1)--[relationType]Model2'" % p)


                relations[modelName1] = relations.get(modelName1, [])
                relations[modelName1].append(self.normalize_field({
                    'field':fieldName,
                    'type': relationType,
                    'model': modelName2,
                    'alias': fakeFieldName1
                }))


        for resource_name in schema:
            for point_name in schema[resource_name]['api']:
                point = schema[resource_name]['api'][point_name]
                if not point.get('view', None) or not hasattr(point['view'], 'queryset'):
                    continue
                model = point['view'].queryset.model
                if model.__name__ in ignore_models:
                    continue
                model_relations = self.get_model_relaions(
                    model,
                    ignore_models=ignore_models,
                    weak_relations=weak_relations
                )
                relations[model.__name__] = relations.get(model.__name__, [])
                for r in model_relations:
                    has_relation = False
                    for r2 in relations[model.__name__]:
                        if r.get('alias', None) == r2.get('alias', None) and r2.get('alias', None):
                            has_relation = True
                            break
                        if r.get('weakAlias', None) == r2.get('weakAlias', None) and r2.get('weakAlias', None) and r['field'] == r2['field']:
                            has_relation = True
                            break
                    if not has_relation:
                        relations[model.__name__].append(r)


        # clean up relations
        ignore_relations = options.get('--ignorerelations=', "") or options.get('ignorerelations=', "")
        if ignore_relations == 'None':
            ignore_relations = None
        if ignore_relations:
            for p in ignore_relations.split(','):
                model_name, relation1 = p.split('.')
                if model_name in relations:
                    relations[model_name] = [
                        f
                        for f in relations[model_name] if f['field'] != relation1
                    ]
        cleaned_relations = OrderedDict()
        for k in sorted(relations.keys()):
            if not relations[k]:
                continue
            cleaned_relations[k] = sorted(relations[k], key=lambda x: x['field'])


        for fpath in options.get('--filepath=', []) or options.get('filepath=', []):
            ext = os.path.splitext(fpath)[-1].lower()
            if ext != '.js':
                fpath += '.js'
                ext = '.js'
            relations_dump = json.dumps(cleaned_relations, indent=4, separators=(',', ':'))
            relations_dump = '},{'.join(relations_dump.split('},\n        {'))
            dump = [
                "    " + l
                for l in relations_dump.split('\n')
            ]
            file_data = render_to_string(
                'drf_lbcms/commands/ng_models_relations' + ext,
                {
                    'module_name': options.get('modulename', 'helpers'),
                    'relations_dump': '\n'.join(dump)[4:]
                }
            )
            with open(fpath, 'w') as f:
                f.write(file_data)
