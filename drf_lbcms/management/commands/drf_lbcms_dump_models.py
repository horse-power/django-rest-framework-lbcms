from django.core.management.base import BaseCommand, CommandError
from django.apps import apps
import os
import json


class Command(BaseCommand):
    help = ''


    def add_arguments(self, parser):
        parser.add_argument('--with-definition', nargs='+', type=str)

    def handle(self, *args, **options):
        model_list = []
        definitions = {}
        for model in apps.get_models():
            model_list.append(model.__name__)
            if hasattr(model, 'DRFS_MODEL_DEFINITION'):
                definitions[model.__name__] = model.DRFS_MODEL_DEFINITION
        embedded_model_list = []

        def collect_embedded_models(orig_dir, base_dir):
            for name in os.listdir(base_dir):
                fpath = os.path.join(base_dir, name)
                if os.path.isdir(fpath):
                    collect_embedded_models(orig_dir, fpath)
                    continue
                try:
                    model_name = name.replace('.json', '')
                    with open(fpath) as f:
                        definitions[model_name] = json.load(f)
                        definitions[model_name]["path"] = fpath.replace(orig_dir, '').replace('.json', '')
                    embedded_model_list.append(model_name)
                except:
                    continue


        for app in apps.get_app_configs():
            d = os.path.join(app.path, 'embedded_models.json')
            if not os.path.isdir(d):
                continue
            collect_embedded_models(d, d)
            '''
            for name in os.listdir(d):
                fpath = os.path.join(d, name)
                try:
                    with open(fpath) as f:
                        definitions[name.replace('.json', '')] = json.load(f)
                    embedded_model_list.append(name.replace('.json', ''))
                except:
                    continue
            '''
        models = {
            'models': sorted(model_list),
            'embeddedModels': sorted(embedded_model_list),
        }
        if options.get('with_definition', None):
            models['definitions'] = definitions
        #print(json.dumps(models, indent=4))
        if options.get('return_json', False):
            return models
        else:
            print(json.dumps(models, indent=4, ensure_ascii=False))
