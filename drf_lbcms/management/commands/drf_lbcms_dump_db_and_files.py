# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.core.management.commands.dumpdata import Command as DumpDataCommand
from django.core.files.storage import DefaultStorage, FileSystemStorage
from django.apps import apps
from django.conf import settings as djsettings
from django.db.migrations.loader import MigrationLoader
from django.db import DEFAULT_DB_ALIAS, connections
from datetime import datetime
from multiprocessing import Pool
from drf_lbcms.helpers import fs as fs_helpers
from drf_lbcms.helpers.response import file_to_response
import tempfile, os, json



def get_migrations_info():
    """
    Shows a list of all migrations on the system, or only those of
    some named apps.
    """
    # Load migrations from disk/DB
    connection = connections[DEFAULT_DB_ALIAS]
    loader = MigrationLoader(connection, ignore_no_migrations=True)
    graph = loader.graph
    # Show all apps in alphabetic order
    app_names = sorted(loader.migrated_apps)
    # For each app, print its migrations in order from oldest (roots) to
    # newest (leaves).
    info = {}
    for app_name in app_names:
        shown = set()
        info[app_name] = []
        for node in graph.leaf_nodes(app_name):
            for plan_node in graph.forwards_plan(node):
                if plan_node not in shown and plan_node[0] == app_name:
                    # Give it a nice title if it's a squashed one
                    title = plan_node[1]
                    if graph.nodes[plan_node].replaces:
                        title += " (%s squashed migrations)" % len(graph.nodes[plan_node].replaces)
                    # Mark it as applied/unapplied
                    if plan_node in loader.applied_migrations:
                        info[app_name].append({
                            'status': 'applied',
                            'name': title
                        })
                    else:
                        info[app_name].append({
                            'status': 'unapplied',
                            'name': title
                        })
                    shown.add(plan_node)
    return info


def is_local_storage(storage):
    try:
        fpath = storage.path('text.file')
    except:
        return False
    return fpath.startswith('/')


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    lst = list(lst)
    for i in range(0, len(lst), n):
        yield lst[i:i + n]



def dump_file_to_local(params):
    print('DUMP FILE (remote)', params['filename'])
    storage = DefaultStorage()
    with storage.open(params['filename']) as r:
        with open(os.path.join(params['output_dir'], params['filename']), 'wb') as w:
            w.write(r.read())



class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument(
            'args', nargs='*',
            help='Type of data: db, files',
        )
        parser.add_argument(
            '-e', '--exclude', dest='exclude', action='append', default=[],
            help='An app_label or app_label.ModelName to exclude '
                 '(use multiple --exclude to exclude multiple apps/models).',
        )
        parser.add_argument(
            '-o', '--output-dir', dest='output_dir', action='store', default=None,
            help='An output dir for files dump',
        )


    def count_media_files(self):
        storage = DefaultStorage()
        r = storage.listdir('')
        if len(r):
            return len(r[-1])
        return 0


    def is_default_storage_local(self):
        storage = DefaultStorage()
        return is_local_storage(storage)


    def is_dump_possible(self, *args):
        MAX_MEDIA_FILES_TO_EXPORT_WITHOUT_CELERY = 250
        res = {
            'emailRequired': False,
            'isPossible': True
        }
        if 'files' in args:
            res['mediaFilesCount'] = self.count_media_files()
            res['emailRequired'] = res['mediaFilesCount'] >= MAX_MEDIA_FILES_TO_EXPORT_WITHOUT_CELERY
            res['isPossible'] = res['mediaFilesCount'] < MAX_MEDIA_FILES_TO_EXPORT_WITHOUT_CELERY or self.is_default_storage_local()
        if 'db' in args and res['isPossible']:
            res['isPossible'] = 'sqlite3' in djsettings.DATABASES['default'].get('ENGINE', '')
        return res


    def handle(self, *args, **options):
        max_parallel_files = options.get('max_parallel_files', 0) or 10
        output_dir = os.path.abspath(options.get('output_dir', None) or tempfile.mkdtemp())
        save_files_on_copy = bool(options.get('output_dir', None))

        excluded_models = options.get('exclude', None) or []
        excluded_models += [
            'drf_lbcms.CmsCeleryResult',
            'drf_lbcms.CmsGdprRequest',
            'sessions.Session',
            'authtoken.Token'
        ]
        if 'dj_gmap' in djsettings.INSTALLED_APPS:
            # кеш гуглекарт не отправляем в дамп, т.к. там может быть много данных
            excluded_models += [
                'dj_gmap.GMapPointCache',
                'dj_gmap.GMapDirectionCache'
            ]

        if 'db' in args:
            # дамп бд
            print('DUMP DB')
            c = DumpDataCommand()
            c.handle(
                database=DEFAULT_DB_ALIAS,
                output=os.path.join(output_dir, 'db.json'),
                exclude=excluded_models,
                indent=2,
                format='json',
                traceback=False,
                use_natural_foreign_keys=False,
                use_natural_primary_keys=False,
                use_base_manager=False,
                primary_keys=None,
                verbosity=0
            )

        # сохраняем настройки дампа
        dump_settings = {
            'output': args,
            'excludedModels': excluded_models,
            'cmsDbBucket': getattr(djsettings, 'CMS_DB_BUCKET', 'unknown'),
            'migrations': get_migrations_info()
        }
        with open(os.path.join(output_dir, 'dumpSettings.json'), 'w') as f:
            json.dump(dump_settings, f, indent=2)

        # архивируем
        archive_name = options.get('archive_name', None)
        if not archive_name:
            timestamp = datetime.now().isoformat().split(".")[0]
            timestamp = '-'.join(timestamp.split(':'))
            archive_name = "{dbBucket}-dump-{timestamp}.7z".format(
                dbBucket=getattr(djsettings, 'CMS_DB_BUCKET', 'unknown'),
                timestamp=timestamp
            )
        r = fs_helpers.run_shell_command(
            "cd {outputDir}; 7z a -sdel {archiveName} *.json;".format(
                outputDir=output_dir,
                archiveName=archive_name
            ),
            shell=True
        )


        if 'files' in args:
            # дамп файлов
            storage = DefaultStorage()
            r = storage.listdir('')
            if len(r) == 2:
                if is_local_storage(storage):
                    # локальные файлы сразу добавляем в архив
                    print('DUMP FILES (local)', storage.path(''))
                    r = fs_helpers.run_shell_command(
                        "cd {storageDir}; 7z a -r -t7z {outputDir}/{archiveName} {storageDirName} -xr!dump -xr!client; 7z rn {outputDir}/{archiveName} {storageDirName} files".format(
                            outputDir=output_dir,
                            archiveName=archive_name,
                            storageDir=os.path.dirname(storage.path('')),
                            storageDirName=os.path.basename(storage.path('')),
                        ),
                        shell=True
                    )
                else:
                    # файлы с удаленного сервера нужно сначала скопировать локально
                    output_files_dir = os.path.join(output_dir, 'files')
                    fs_helpers.mkdir(output_files_dir)
                    files_batch = chunks(r[1], max_parallel_files)

                    for filenames in files_batch:
                        files_for_multiprocessing = []
                        for filename in filenames:
                            if '/' in filename or filename.startswith('language'):
                                continue
                            if os.path.exists(os.path.join(output_files_dir, filename)):
                                continue
                            files_for_multiprocessing.append({
                                'filename': filename,
                                'output_dir': output_files_dir
                            })
                        if not files_for_multiprocessing:
                            continue
                        pool = Pool(processes=3)
                        pool.map(dump_file_to_local, files_for_multiprocessing)
                        if not save_files_on_copy:
                            for filename in filenames:
                                # добавляем файл в архив
                                r = fs_helpers.run_shell_command(
                                    "cd {outputDir}; 7z a -t7z -sdel {archiveName} files/{filename};".format(
                                        outputDir=output_dir,
                                        archiveName=archive_name,
                                        filename=filename
                                    ),
                                    shell=True
                                )
                                # удаляем файл
                                fs_helpers.delete_file_or_dir(os.path.join(output_files_dir, filename))
                    # удаляем директорию с временными файлами
                    if not save_files_on_copy:
                        fs_helpers.delete_file_or_dir(output_files_dir)


        if options.get('as_http_response', False):
            with open(os.path.join(output_dir, archive_name), 'rb') as f:
                response = file_to_response(file_data=f.read(), file_name=archive_name, as_bytes=True)
            # удаляем директорию с архивом
            fs_helpers.delete_file_or_dir(output_dir)
            return response
        return os.path.join(output_dir, archive_name)
