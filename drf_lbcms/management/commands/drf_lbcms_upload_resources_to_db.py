# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import os, json
from drfs import get_model
from drf_lbcms.models import CmsSmsNotification, CmsMailNotification, CmsPdfData
from drf_lbcms.helpers.django import get_base_dir_as_string
try:
    from django.contrib.sites.models import Site

    sites = Site.objects.all()
    if not sites:
        sites = [None]
except:
    sites = [None]


def safe_update(instance, field, value, force_replace=False):
    if not isinstance(value, dict) or force_replace:
        setattr(instance, field, value)
        return
    # если это словарь, то обновляем только пустые значения
    origValue = getattr(instance, field, None) or {}
    for k,v in value.items():
        if not origValue.get(k, None):
            origValue[k] = v
    setattr(instance, field, origValue)






class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument('--fields', nargs='+', type=str)
        parser.add_argument('--type', nargs='+', type=str)
        parser.add_argument('--force', action='store_true', default=False)

    def handle(self, *args, **options):
        allowed_fields = options.get('fields', [])
        allowed_types = options.get('type', [])
        force_replace = False
        if allowed_fields:
            allowed_fields = allowed_fields[0].split(',')
        if allowed_types:
            allowed_types = allowed_types[0].split(',')
        force_replace = options.get('force', False)
        BASE_DIR = get_base_dir_as_string()



        def load_notify(dir_path):
            if not os.path.exists(os.path.join(dir_path, 'description.json')):
                return None
            with open(os.path.join(dir_path, 'description.json')) as f:
                data = json.load(f)

            for filename in os.listdir(dir_path):
                if filename == 'description.json':
                    continue
                file_path = os.path.join(dir_path, filename)
                ext = os.path.splitext(filename)[-1]
                if ext.lower() not in ['.html', '.txt']:
                    continue
                if not os.path.isfile(file_path):
                    continue
                with open(file_path) as f:
                    content = f.read()
                if content and content[-1]=='\n':
                    content = content[:-1]
                name = filename.split('.')
                data[name[0]] = data.get(name[0], {})
                data[name[0]][name[1]] = content.strip().strip('\n')
            return data


        if os.path.isdir(os.path.join(BASE_DIR, 'resources', 'mail')) and (not allowed_types or 'mail' in allowed_types):
            for dir_name in os.listdir(os.path.join(BASE_DIR, 'resources', 'mail')):
                dir_path = os.path.join(BASE_DIR, 'resources', 'mail', dir_name)
                notify = load_notify(dir_path)
                if not notify:
                    continue
                notify['identifier'] = dir_name
                if not notify.get('fromEmail', None) and hasattr(settings, 'HOSTNAME'):
                    notify['fromEmail'] = "noreply@%s" % settings.HOSTNAME.replace('_', '-')
                for site in sites:
                    instance, created = CmsMailNotification.objects.get_or_create(identifier=notify['identifier'], _site=site)
                    for k,v in notify.items():
                        if allowed_fields and k not in allowed_fields:
                            continue
                        safe_update(instance, k, v, force_replace=force_replace)
                    instance.save()


        if os.path.isdir(os.path.join(BASE_DIR, 'resources', 'sms')) and (not allowed_types or 'sms' in allowed_types):
            for dir_name in os.listdir(os.path.join(BASE_DIR, 'resources', 'sms')):
                dir_path = os.path.join(BASE_DIR, 'resources', 'sms', dir_name)
                notify = load_notify(dir_path)
                if not notify:
                    continue
                notify['identifier'] = dir_name
                for site in sites:
                    instance, created = CmsSmsNotification.objects.get_or_create(identifier=notify['identifier'], _site=site)
                    for k,v in notify.items():
                        if allowed_fields and k not in allowed_fields:
                            continue
                        safe_update(instance, k, v, force_replace=force_replace)
                    instance.save()


        if os.path.isdir(os.path.join(BASE_DIR, 'resources', 'pdf')) and (not allowed_types or 'pdf' in allowed_types):
            for dir_name in os.listdir(os.path.join(BASE_DIR, 'resources', 'pdf')):
                dir_path = os.path.join(BASE_DIR, 'resources', 'pdf', dir_name)
                notify = load_notify(dir_path)
                if not notify:
                    continue
                notify['identifier'] = dir_name
                notify['runningTitles'] = {
                    'footer': notify.get('footer', None) or {},
                    'header': notify.get('header', None) or {},
                    'footerHeight': 10,
                    'headerHeight': 10,
                    'isFooterActive': False,
                    'isHeaderActive': False
                }
                if notify.get('body', None):
                    notify['content'] = notify['body']
                if notify['runningTitles']['footer']:
                    notify['runningTitles']['isFooterActive'] = True
                if notify['runningTitles']['header']:
                    notify['runningTitles']['isHeaderActive'] = True
                for site in sites:
                    instance, created = CmsPdfData.objects.get_or_create(identifier=notify['identifier'], _site=site)
                    for k,v in notify.items():
                        if allowed_fields and k not in allowed_fields:
                            continue
                        safe_update(instance, k, v, force_replace=force_replace)
                    instance.save()

        # custom models
        for name in os.listdir(os.path.join(BASE_DIR, 'resources')):
            if name in ['pdf', 'sms', 'mail']:
                continue
            if not os.path.isdir(os.path.join(BASE_DIR, 'resources', name)):
                continue
            # ищем модель
            model_class = get_model(name[0].upper() + name[1:])
            try:
                model_class = get_model(name[0].upper() + name[1:])
                if not model_class:
                    model_class = get_model('Cms'+name[0].upper() + name[1:])
            except:
                continue
            for dir_name in os.listdir(os.path.join(BASE_DIR, 'resources', name)):
                dir_path = os.path.join(BASE_DIR, 'resources', name, dir_name)
                notify = load_notify(dir_path)
                if not notify:
                    continue
                notify['identifier'] = dir_name
                for site in sites:
                    instance, created = model_class.objects.get_or_create(identifier=notify['identifier'], _site=site)
                    for k,v in notify.items():
                        if allowed_fields and k not in allowed_fields:
                            continue
                        safe_update(instance, k, v, force_replace=force_replace)
                    instance.save()
