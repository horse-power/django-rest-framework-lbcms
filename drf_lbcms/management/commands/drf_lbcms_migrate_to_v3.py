# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
import os, json, re, drfs, collections
from shutil import copyfile
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings
from drf_lbcms.helpers import dump_to_file
from django.db.models.fields import NOT_PROVIDED
from django.contrib.auth.hashers import make_password


MONGO_DB_MISSING_L10N_FILE_ID = -1
FONT_AWESOME_MIGRATION = [
    ['.fa.fa-sign-out', ".fas.sign-out-alt"],
    ['.fa.fa-bus', ".fas.fa-bus"],
    ['.fa.fa-star', ".fas.fa-star"],
    ['.fa.fa-check-circle-o', ".far.fa-check-circle"],
    ['.fa.fa-check-square-o', ".far.fa-check-square"],
    ['.fa.fa-square-o', ".far.fa-square"],
    ['.fa.fa-check', ".fas.fa-check"],
    ['.fa.fa-bolt', ".fas.fa-bolt"],
    ['.fa.fa-map-marker', ".fas.fa-map-marker-alt"],
    ['.fa.fa-plus-circle', ".fas.fa-plus-circle"],
    ['.fa.fa-plus', ".fas.fa-plus"],
    ['.fa.fa-clock-o', ".far.fa-clock"],
    ['.fa.fa-refresh', ".fas.fa-sync-alt"],
    ['.fa.fa-pencil', ".fas.fa-pencil-alt"],
    ['.fa.fa-edit', ".fas.fa-pencil-alt"],
    ['.fa.fa-calendar', ".fas.fa-calendar-alt"],
    ['.fa.fa-times', ".fas.fa-times"],
    ['.fa.fa-money', ".fas.fa-money-bill-alt"],
    ['.fa.fa-map-o', ".far.fa-map"],
    ['.fa.fa-address-card-o', ".far.fa-address-card"],
    ['.fa.fa-trash-o', ".fas.fa-trash-alt"],
    ['.fa.fa-user', ".fas.fa-user"],
    ['.fa.fa-search', ".fas.fa-search"],
    ['.fa.fa-phone', ".fas.fa-phone"],
    ['.fa.fa-bar-chart', ".fas.fa-chart-bar"],
    ['.fa.fa-list', ".fas.fa-list"],
    ['.fa.fa-save', ".fas.fa-save"],
    ['.fa.fa-floppy-o', ".fas.fa-save"],

    ['.fa.fa-arrow-left', ".fas.fa-arrow-left"],
    ['.fa.fa-bars', ".fas.fa-bars"],
    ['.fa.fa-times-circle-o', ".far.fa-times-circle"],
    ['.fa.fa-filter', ".fas.fa-filter"],
    ['.fa.fa-columns', ".fas.fa-columns"],
    ['.fa.fa-camera', ".fas.fa-camera"],
    ['.fa.fa-cloud-upload', ".fas.fa-cloud-upload-alt"],
    ['.fa.fa-exclamation-triangle', ".fas.fa-exclamation-triangle"],
    ['.fa.fa-usd', ".fas.fa-dollar-sign"],
    ['.fa.fa-lock', ".fas.fa-lock"],
    ['.fa.fa-angle-left', ".fas.fa-angle-left"],
    ['.fa.fa-angle-right', ".fas.fa-angle-right"],
    ['.fa.fa-file-pdf-o', ".fas.fa-file-pdf"],
    ['.fa.fa-file-excel-o', ".fas.fa-file-excel"],
    ['.fa.fa-file-o', ".fas.fa-file-alt"],
    ['.fa.fa-table', ".fas.fa-table"],
    ['.fa.fa-mobile', ".fas.fa-mobile-alt"],
    ['.fa.fa-calculator', ".fas.fa-calculator"],
    ['', ""],

    #['', ""],

    ['fa-file-excel-o', "fa-file-excel"],
    ['fa-file-pdf-o', "fa-file-pdf"],
]



class LoopackJsFieldConverter:
    l10n_files = []

    def _is_l10nfile(self, item):
        if not isinstance(item, dict):
            return False
        for k,v in item.items():
            if isinstance(v, dict) and v.get('name', None) and v.get('meta', None):
                return True
        return False

    def _clean_value(self, value):
        if isinstance(value, list):
            return [
                self._clean_value(v)
                for v in value
            ]
        if not isinstance(value, dict):
            return value
        for k,v in list(value.items()):
            if v is None or v == '<p><br></p>':
                del value[k]
                continue
            if isinstance(v, dict):
                value[k] = self._clean_value(v)
        return value

    def convert(self, item):
        converted = {}
        for k,v in item.items():
            newk, newv, is_l10n_file = self.convert_field(k, v)
            #if newv == 'None':
            #    newv = None
            if newv is None:
                continue
            if is_l10n_file:
                if isinstance(newv, list):
                    self.l10n_files += newv
                    newv = [
                        v['id']
                        for v in newv
                    ]
                else:
                    self.l10n_files.append(newv)
                    newv = newv['id']
            #print('----------------------')
            #print(k, newk, newv)
            if newk:
                converted[newk] = self._clean_value(newv)
        return converted

    def convert_l10nfile(self, item):
        #print('l10nfile', item)
        if not item['default']:
            for k,v in item.items():
                if isinstance(v, dict) and v.get('name', None):
                    item['default'] = k
                    break
        converted = {
            'id': str(item['id']),
            'meta_data': item[item['default']]['meta'].copy(),
            'title': {},
            'description': {},
            'file_data': item[item['default']]['name'],
        }
        if not item['id']:
            global MONGO_DB_MISSING_L10N_FILE_ID
            converted['id'] = MONGO_DB_MISSING_L10N_FILE_ID
            MONGO_DB_MISSING_L10N_FILE_ID -= 1
        del converted['meta_data']['id']
        if item[item['default']].get('thumbs', None):
            converted['thumbs'] = item[item['default']]['thumbs'].copy()
            if '70x70' in converted['thumbs']:
                converted['thumbs'].remove('70x70')
            #if '80x80' in converted['thumbs']:
            #    converted['thumbs'].remove('80x80')
        if converted['meta_data']['width'] is None:
            del converted['meta_data']['width']
        if converted['meta_data']['height'] is None:
            del converted['meta_data']['height']
        for k,v in item.items():
            for field in ['title', 'description']:
                if isinstance(v, dict) and v.get(field, None):
                    converted['title'][k] = v[field]
        return converted

    def convert_field(self, field_name, value):
        if field_name == '_id' or field_name == 'id':
            return 'id', value, False
        if field_name == 'ownerId':
            return 'owner', value, False
        if isinstance(value, list) and value:
            if self._is_l10nfile(value[0]):
                # обрабатываем файлы
                return field_name, [
                    self.convert_l10nfile(v)
                    for v in value if self.convert_l10nfile(v)
                ], True
        if isinstance(value, dict) and self._is_l10nfile(value):
            # одиночный файл
            return field_name, self.convert_l10nfile(value), True
        if field_name.startswith('_'):
            return None, None, False
        # другие обычные поля
        if field_name == 'categoryId':
            field_name = 'category'
        return field_name, value, False



def replace_mongo_id_in_object(item, original_id, new_id):
    def replace_in_data(data):
        if data == original_id:
            return new_id
        if isinstance(data, list):
            return [
                replace_in_data(v)
                for v in data
            ]
        if isinstance(data, dict):
            for k,v in data.items():
                data[k] = replace_in_data(v)
        if isinstance(data, str):
            # возможный html текст
            data = re.sub("&quot;{id}&quot;".format(id=original_id), str(new_id), data)
            data = re.sub('"{id}"'.format(id=original_id), '"{id}"'.format(id=new_id), data)
            #data = re.sub(str(original_id), str(new_id), data)
        return data
    return replace_in_data(item)


def normalize_db_item_dump(item, model_class):
    fields = model_class._meta.get_fields(include_hidden=True)
    normalized = {}
    keys = list(item.keys())
    all_fields = []

    if model_class.__name__ == 'CmsUser':
        item['password'] = make_password('1')
        if item['userRole'] == 'admin':
            item['userRole'] = 'a'
        if item['userRole'] in ['user', 'regular']:
            item['userRole'] = 'u'
        item['preferredLang'] = item.get('preferredLang', 'ru')
        item['firstName'] = item.get('firstName', None) or item['username']

    if model_class.__name__ == 'CmsTranslations':
        for i in range(len(item['msgs'])):
            for f in ['id', '_source']:
                if f in item['msgs'][i]:
                    del item['msgs'][i][f]
        from drf_lbcms.helpers.gettext import model_to_gettext_json

        item['_msgsCache'] = model_to_gettext_json(
            collections.namedtuple('CmsTranslations', ['msgs', 'lang'])(
                msgs=item['msgs'],
                lang=item['lang']
            )
        )

    for field in fields:
        all_fields.append(field.name)
        if field.name in item:
            normalized[field.name] = item[field.name]
        elif hasattr(field, 'default') and field.default != NOT_PROVIDED:
            if callable(field.default):
                normalized[field.name] = field.default()
            else:
                normalized[field.name] = field.default
    removed = [
        f
        for f in keys if f not in all_fields
    ]
    if removed:
        print("REMOVED FIELDS FOR: {model_class} - {removed}".format(model_class=model_class.__name__, removed=removed))
    return normalized



def ng_fix_slider(data):
    for k,v in data.items():
        if k == 'slides' and isinstance(v, list) and v:
            for i in range(len(v)):
                slide = v[i]
                if isinstance(slide.get('template', None), str):
                    slide['template'] = {
                        settings.APP_CONTEXT['CMS_DEFAULT_LANG']: slide['template']
                    }
                if slide.get('htmlCaption', None):
                    slide['template'] = slide['htmlCaption']
                    del slide['htmlCaption']
                if slide.get('title', None) and not slide.get('htmlCaption', None):
                    template = {}
                    for lang in settings.APP_CONTEXT['CMS_ALLOWED_LANGS']:
                        template[lang] = ""
                        if slide.get('title', {}).get(lang, None):
                            template[lang] += "<h3>{title}</h3>".format(title=slide['title'][lang])
                        if slide.get('subTitle', {}).get(lang, None):
                            template[lang] += "<p>{subTitle}</p>".format(subTitle=slide['subTitle'][lang])
                    del slide['title']
                    if 'subTitle' in slide:
                        del slide['subTitle']
                    if 'link' in slide:
                        del slide['link']
                    if 'linkTarget' in slide:
                        del slide['linkTarget']
                data['slides'][i] = slide

        if isinstance(v, dict):
            data[k] = ng_fix_slider(v)
    return data


def ng_fix_cards(data):
    for k,v in data.items():
        if k == 'cards' and isinstance(v, list) and v:
            for i in range(len(v)):
                card = v[i]
                if card.get('l10nLink', None):
                    card['link'] = {
                        'type': 'href',
                        'data': card['l10nLink']
                    }
                if card.get('link', None):
                    card['link'] = {
                        settings.APP_CONTEXT['CMS_DEFAULT_LANG']: card['link']
                    }
                data['cards'][i] = card
        if isinstance(v, dict):
            data[k] = ng_fix_cards(v)
    return data



class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument('--filepath', nargs='+', type=str)
        parser.add_argument('--client-dir', nargs='+', type=str)
        parser.add_argument('--lbjs-path-to-dump-dir', nargs='+', type=str)
        parser.add_argument('--lbjs-bucket-name', nargs='+', type=str)


    def migrate_ckeditor_html(self, value):
        if isinstance(value, str):
            return re.sub("ui-sref=", "href uic-sref-for-ckeditor=", value)
        if isinstance(value, list):
            return [
                self.migrate_ckeditor_html(v)
                for v in value
            ]
        if isinstance(value, dict):
            for k,v in value.items():
                value[k] = self.migrate_ckeditor_html(v)
        return value


    def migrate_drf_lbcms(self, db_data):
        for i in range(len(db_data)):
            item = db_data[i]
            for k in item['fields'].keys():
                if isinstance(item['fields'][k], str):
                    try:
                        value = json.loads(item['fields'][k])
                        db_data[i]['fields'][k] = value
                    except:
                        pass
            if item['model'].startswith('drf_lbcms.l10nfile'):
                if not item['fields']['title']:
                    db_data[i]['fields']['title'] = {}
                else:
                    db_data[i]['fields']['title'] = {settings.APP_CONTEXT['CMS_DEFAULT_LANG']: item['fields']['title']}
                if not item['fields']['description']:
                    db_data[i]['fields']['description'] = {}
                else:
                    db_data[i]['fields']['description'] = {settings.APP_CONTEXT['CMS_DEFAULT_LANG']: item['fields']['description']}
            if item['model'] == 'drf_lbcms.cmsmailnotification':
                if '_cache' in item['fields']:
                    del db_data[i]['fields']['_cache']
            if item['model'].endswith('cmsarticle'):
                if 'categoryId' in item['fields']:
                    db_data[i]['fields']['category'] = item['fields']['categoryId']
                    del db_data[i]['fields']['categoryId']
                #if 'owner' not in item['fields']:
                #    db_data[i]['fields']['owner'] = None
            if item['model'].endswith('cmssettings'):
                item['fields']['navbarConf'] = item['fields'].get('navbarConf', None) or {}
                fields = item['fields']
                if fields.get('data', {}).get('navbarConf', None):
                    item['fields']['navbarConf']['main'] = fields.get('data', {}).get('navbarConf', None) or {}
                    del fields['data']['navbarConf']
                if fields.get('data', {}).get('navbarConfSecondary', None):
                    item['fields']['navbarConf']['secondary'] = fields.get('data', {}).get('navbarConfSecondary', None) or {}
                    del fields['data']['navbarConfSecondary']
                if 'indexView' in fields.get('data', {}):
                    item['fields']['viewsData'] = item['fields'].get('viewsData', None) or {}
                    item['fields']['viewsData']['index'] = item['fields']['viewsData'].get('index', None) or {}
                    current_index_data = ng_fix_slider(ng_fix_cards(item['fields']['viewsData']['index']))
                    item['fields']['viewsData']['index'] = ng_fix_slider(ng_fix_cards(fields['data']['indexView']))
                    item['fields']['viewsData']['index'].update(current_index_data)
                    del fields['data']['indexView']

        return self.migrate_ckeditor_html(db_data)

    def migrate_loopbackjs(self, db_data):
        converted = []
        lbc = LoopackJsFieldConverter()
        for item in db_data:
            converted.append(lbc.convert(item))
        return converted, lbc.l10n_files


    def migrate_font_awesome_lib(self, filepath):
        with open(filepath) as f:
            data = f.read()

        for r in FONT_AWESOME_MIGRATION:
            orig = r[0]
            replacement = r[1]
            data = replacement.join(data.split(orig))
            orig = ' '.join(orig.split('.')).strip()
            replacement = ' '.join(replacement.split('.')).strip()
            data = replacement.join(data.split(orig))
        with open(filepath, 'w') as f:
            f.write(data)


    def handle(self, *args, **options):
        if options.get('client_dir', None):
            from pathlib import Path
            for cdir in options.get('client_dir', []):
                for fpath in list(Path(cdir).rglob("*.coffee")):
                    self.migrate_font_awesome_lib(fpath)
                for fpath in list(Path(cdir).rglob("*.pug")):
                    self.migrate_font_awesome_lib(fpath)
                for fpath in list(Path(cdir).rglob("*.less")):
                    self.migrate_font_awesome_lib(fpath)
            return
        if options.get('filepath', None):
            for fpath in options.get('filepath', []):
                ext = os.path.splitext(fpath)[-1].lower()
                if ext not in ['.json']:
                    continue
                with open(fpath) as f:
                    data = json.load(f)
                data = self.migrate_drf_lbcms(data)
                dump_to_file(data, fpath+'.migrated.json', indent=2)
            return

        # иначе делаем импорт из дампа loopback.js цмс-ки
        try:
            import bson
        except ImportError:
            print("Install pymongo!")
            return
        base_dir = options.get('lbjs_path_to_dump_dir', [])[0]
        bucket = options.get('lbjs_bucket_name', [])[0]
        converted_items = []
        converted_l10n_files = []

        print("PARSING BSON...")
        for fname in os.listdir(os.path.join(base_dir, 'cms')):
            ext = os.path.splitext(fname)[-1].lower()
            if ext not in ['.bson'] or bucket + '.' not in fname:
                continue
            model_name = fname.split('_'+bucket)[0]
            if model_name in ['AccessToken', 'CmsRole', 'CmsRoleMapping']:
                continue
            with open(os.path.join(base_dir, 'cms', fname),'rb') as f:
                db_data = bson.decode_all(f.read())

            items, l10n_files = self.migrate_loopbackjs(db_data)
            if model_name == 'CmsPage':
                print('MOVE: CmsPage to CmsArticle - %s items' % len(items))
                model_name = 'CmsArticle'
            if model_name == 'CmsNews':
                print('MOVE: CmsNews to CmsArticle - %s items' % len(items))
                model_name = 'CmsArticle'
            for item in items:
                fields = item.copy()
                del fields['id']
                model_class = drfs.get_model(model_name + '.json')
                if model_class:
                    model = '{module_name}.{class_name}'.format(module_name=model_class.__module__.split('.')[0], class_name=model_class.__name__).lower()
                else:
                    print("ERROR: Unable to find model class '%s'. Create this django model in your app" % model_name)
                    model = 'noname_app.' + model_name.lower()
                    continue

                if model_name == 'CmsSettings':
                    fields['navbarConf'] = fields.get('navbarConf', None) or {}
                    if fields.get('data', {}).get('navbarConf', None):
                        fields['navbarConf']['main'] = fields.get('data', {}).get('navbarConf', None) or {}
                        del fields['data']['navbarConf']
                    if fields.get('data', {}).get('navbarConfSecondary', None):
                        fields['navbarConf']['secondary'] = fields.get('data', {}).get('navbarConfSecondary', None) or {}
                        del fields['data']['navbarConfSecondary']
                    if 'indexView' in fields.get('data', {}):
                        fields['viewsData'] = {
                            'index': ng_fix_slider(ng_fix_cards(fields['data']['indexView']))
                        }
                        del fields['data']['indexView']
                    for field in ['permissions', 'currentPrincipals', 'currentUser', 'pdfData', 'paymentMethods', 'deliveryMethods']:
                        if field in fields:
                            del fields[field]


                if model_name != 'CmsArticle' and 'urlId' in item:
                    item['identifier'] = item['urlId']
                    del item['urlId']

                fields = normalize_db_item_dump(fields, model_class)
                converted_items.append({
                    'model': model,
                    'pk': item['id'],
                    'fields': fields
                })
            for item in l10n_files:
                fields = item.copy()
                del fields['id']
                if model_name == 'CmsSettings':
                    if item['id'] == converted_items[-1]['fields'].get('watermarkImg', None):
                        converted_l10n_files.append({
                            'model': 'drf_lbcms.l10nfilelocal',
                            'pk': item['id'],
                            'fields': fields
                        })
                        continue
                converted_l10n_files.append({
                    'model': 'drf_lbcms.l10nfile',
                    'pk': item['id'],
                    'fields': fields
                })

            #break
        print("CONVERTING TO DJANGO ID...")
        # теперь проходимся по всем документам и заменяем ссылки на файлы
        new_id = 0
        for l10nfile in converted_l10n_files:
            original_id = l10nfile['pk']
            new_id = new_id +1
            l10nfile['pk'] = new_id
            if isinstance(original_id, int) and original_id < 0:
                # неверные сломанные id
                for item in converted_items:
                    for k in item['fields'].keys():
                        if item['fields'][k] == original_id:
                            item['fields'][k] = new_id
                continue
            # а вот теперь тут мы заменяем все вхождения как в строковых переменных, так и массивах/объектах
            for i in range(len(converted_items)):
                converted_items[i]['fields'] = replace_mongo_id_in_object(
                    converted_items[i]['fields'],
                    original_id,
                    new_id
                )

        # теперь проходимся по документам и собираем mongoid и конвертим их в int
        last_ids = {}
        mongo_ids = {}
        for item in converted_items:
            if item['pk'] in mongo_ids:
                print('Already exists', item)
                return
            last_ids[item['model']] = last_ids.get(item['model'], 0) + 1
            mongo_ids[item['pk']] = last_ids[item['model']]

        # проходимся по всем докам и заменяем ссылки
        for i in range(len(converted_items)):
            for k,v in mongo_ids.items():
                converted_items[i] = replace_mongo_id_in_object(converted_items[i], k, v)
                converted_items[i] = replace_mongo_id_in_object(converted_items[i], str(k), v)

        print('STRIPPING INVALID MONGO ID...')
        def remove_mongo_id(data):
            if not data:
                return data
            if isinstance(data, list):
                return [
                    remove_mongo_id(v)
                    for v in data
                ]
            if isinstance(data, dict):
                for k,v in data.items():
                    data[k] = remove_mongo_id(v)

            if isinstance(data, bson.objectid.ObjectId):
                print('remove - ', data)
                return None
            return data

        converted_items = remove_mongo_id(converted_items)

        # теперь проходимся по CmsSettings и заменяем ссылки на статьи с urlId на id
        def replace_url_id_to_id(data):
            if isinstance(data, list):
                return [
                    replace_url_id_to_id(v)
                    for v in data
                ]
            if not isinstance(data, dict):
                return data
            if data.get('type', None) == 'ui-sref:app.articles.details':
                for item in converted_items:
                    if 'cmsarticle' in item['model'] and item['fields']['urlId'] == data['data']:
                        data['data'] = item['pk']
            if data.get('items', None):
                data['items'] = replace_url_id_to_id(data['items'])
            return data


        for i in range(len(converted_items)):
            item = converted_items[i]
            if 'cmssettings' in item['model']:
                converted_items[i]['fields']['navbarConf']['main']['right'] = replace_url_id_to_id(item['fields']['navbarConf']['main']['right'])
            # заменяем также ссылки сгенеренные ckeditor
            converted_items[i] = self.migrate_ckeditor_html(converted_items[i])


        # сохраняем результат в json-файл
        class LazyEncoder(DjangoJSONEncoder):
            def default(self, obj):
                if isinstance(obj, bson.objectid.ObjectId):
                    return str(obj)
                return super().default(obj)

        with open(os.path.join(settings.DATABASES_ROOT, 'db.lbjs-migrated.json'), 'w') as f:
            json.dump(
                converted_l10n_files + converted_items,
                f,
                indent=2,
                cls=LazyEncoder,
                ensure_ascii=False
            )

        #return
        # теперь копируем файлы
        print("COPY MEDIA...")
        dirpath = os.path.join(base_dir, 'gcloud', 'lbcms-container-' + bucket)
        def copy_media(fpath):
            if not os.path.exists(os.path.join(dirpath, fpath)):
                print('ignore ', fpath)
                return
            copyfile(os.path.join(dirpath, fpath), os.path.join(settings.MEDIA_ROOT, fpath))

        for l10nfile in converted_l10n_files:
            copy_media(l10nfile['fields']['file_data'])
            for thumb in l10nfile['fields'].get('thumbs', []):
                copy_media(l10nfile['fields']['file_data'] + '.thumb.' + thumb + '.jpg')
