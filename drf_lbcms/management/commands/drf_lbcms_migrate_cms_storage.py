from django.core.management.base import BaseCommand, CommandError
from django.apps import apps
import re
from drf_lbcms.helpers.l10nFile import resolve_l10nfile_uri

ALLOWED_STORAGE_TYPES = ['local', 'google', 'amazon', 'digitaloceanSpaces']





class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--old-storage-type', type=str)
        parser.add_argument('--old-storage-url', type=str)
        parser.add_argument('--old-storage-bucket', type=str)


    def replace_storage_urls(self, value, old_prefix=None, new_prefix=None):
        if isinstance(value, str):
            return re.sub(old_prefix, new_prefix, value)
        if isinstance(value, list):
            return [
                self.replace_storage_urls(v, old_prefix=old_prefix, new_prefix=new_prefix)
                for v in value
            ]
        if isinstance(value, dict):
            for k,v in value.items():
                value[k] = self.replace_storage_urls(v, old_prefix=old_prefix, new_prefix=new_prefix)
        return value


    def handle(self, *args, **options):
        old_storage_type = options.get('old_storage_type', None)
        old_storage_url = options.get('old_storage_url', None) or ''
        old_storage_bucket = options.get('old_storage_bucket', None) or ''

        if old_storage_type not in ALLOWED_STORAGE_TYPES:
            print("Error: old_storage_type is invalid. Use one of " + str(ALLOWED_STORAGE_TYPES))
            return
        if not old_storage_bucket:
            print("Error: old_storage_bucket is empty")
            return
        if not old_storage_url and old_storage_url != 'local':
            print("Error: old_storage_url is empty")
            return

        old_prefix = resolve_l10nfile_uri('/', force_cms_storage={
            'type': old_storage_type,
            'server': old_storage_url
        }, force_cms_db_bucket=old_storage_bucket)
        new_prefix = resolve_l10nfile_uri('/')
        print(old_prefix)
        print(new_prefix)

        for model in apps.get_models():
            if not model.__name__.startswith('Cms'):
                continue
            print("MIGRATE: " + model.__name__)
            for item in model.objects.all():
                for field in item._meta.get_fields():
                    value = getattr(item, field.name, None)
                    if not isinstance(value, (dict, list, str)):
                        continue
                    value = self.replace_storage_urls(
                        value,
                        old_prefix=old_prefix,
                        new_prefix=new_prefix
                    )
                    setattr(item, field.name, value)
                item.save()
