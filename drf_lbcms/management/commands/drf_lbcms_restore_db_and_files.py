# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.core.management.commands.loaddata import Command as LoadDataCommand
from django.db import DEFAULT_DB_ALIAS, connections
from django.core.files.storage import DefaultStorage
from drf_lbcms.helpers import fs as fs_helpers
from django.core.files.base import ContentFile
import tempfile, os, json



class Command(BaseCommand):
    help = ''
    def add_arguments(self, parser):
        parser.add_argument(
            'args', nargs='*',
            help='Path to 7z-file with dump',
        )

    def handle(self, *args, **options):
        output_dir = tempfile.mkdtemp()
        input_file = None
        if args:
            input_file = os.path.abspath(args[0])

        if options.get('in_memory_file', None):
            input_file = os.path.join(output_dir, 'archive.7z')
            with open(input_file, 'wb') as f:
                f.write(options['in_memory_file'].read())

        r = fs_helpers.run_shell_command(
            "cd {outputDir}; 7z x {inputFile}; ls -la".format(
                outputDir=output_dir,
                inputFile=input_file
            ),
            shell=True
        )

        if options.get('in_memory_file', None):
            # если мы брали файл из ОЗУ, то удаляем его с диска
            fs_helpers.delete_file_or_dir(input_file)

        if r.err:
            raise Exception(r.err)

        # восстанавливаем файлы
        files_dir = os.path.join(output_dir, 'files')
        if os.path.exists(files_dir) and os.path.isdir(files_dir):
            storage = DefaultStorage()
            for name in os.listdir(files_dir):
                fpath = os.path.join(files_dir, name)
                # восстанавливаем только те, которые НЕ существуют
                if os.path.isfile(fpath) and not storage.exists(name):
                    with open(fpath, 'rb') as r:
                        storage.save(name, r)

        # восстанавливаем бд
        db_path = os.path.join(output_dir, 'db.json')
        if os.path.isfile(db_path):
            # раскоментить нижние строки, если проводится миграция с sqlite на postgres

            #from django.contrib.contenttypes.models import ContentType
            #ContentType.objects.all().delete()

            c = LoadDataCommand()
            c.handle(
                db_path,
                database=DEFAULT_DB_ALIAS,
                app_label=None,
                traceback=False,
                ignore=False,
                exclude=[],
                verbosity=0,
                format=None
            )

        fs_helpers.delete_file_or_dir(output_dir)
