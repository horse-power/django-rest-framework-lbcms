# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.conf import settings as djsettings
from drfs.helpers import import_class
from .models import CmsSmsNotification, CmsMailNotification, CmsTranslations
from .helpers.cms import notify_user


UserModel = get_user_model()
SKIP_MODEL_NAMES = [
    "Migration", "ContentType", "L10nFile", "L10nFileLocal",
    "CmsSettings", "CmsMailNotification", "CmsSmsNotification", "CmsTranslations"
]



def find_user_prop_from_identifier(instance, identifier):
    identifier = ''.join(identifier.split(' '))
    identifier = identifier.split('=>')
    if len(identifier) > 1 and identifier[-1]:
        return getattr(instance, identifier[-1], None)
    if instance.__class__ == UserModel:
        return instance
    return getattr(instance, 'owner', None)


def is_hook(identifier, hook):
    identifier = ''.join(identifier.split(' '))
    identifier = identifier.split('=>')
    identifier = identifier[0]
    return identifier.split('.')[-1] == hook


def model_name_to_identifier(name):
    identifier = ""
    if name.startswith('Cms'):
        identifier = "cms."
        name = name[3:]
    name = name[0].lower() + name[1:]
    return identifier + name


def find_identifiers(starts_with):
    identifiers = list(CmsSmsNotification.objects.filter(
        identifier__startswith=starts_with
    ).values_list('identifier', flat=True))
    identifiers += list(CmsMailNotification.objects.filter(
        identifier__startswith=starts_with
    ).values_list('identifier', flat=True))
    # скрываем дубликаты
    return list(set(identifiers))


def notify_user_wrapper(user, context={}, identifier=None):
    '''
        функция обертка для возможности отправлять уведомления по
        пользовательским шаблонам
    '''
    fname = getattr(djsettings, 'DRF_LBCMS', {}).get('validate_identifier_for_notify_in_signal', None)
    if fname:
        fn = import_class(fname)
        identifier = fn(context['instance'], identifier)
    if not identifier:
        return
    notify_user(user, context=context, identifier=identifier)



"""
    Правила именования идентификаторов уведомлений, для автоподхватывания
    через сигналы:
    пример для модели CmsUser
        - cms.user.$new - создание нового объекта
        - cms.user.$update - обновление
        - cms.user.$delete - удаление
        - cms.user.username.$diff - обновление частных полей
    пример для модели CmsSomeModel
        - все теже $new, $update, $delete, $diff
        * уведомление будет отослано пользователю, только если модель CmsSomeModel
    имеет ссылку owner на пользователя. Если поле с ссылкой на пользователя
    называется как-то по другому, то нужно это указать вот так:
        - cms.someModel.myField$diff=>fieldWithOwner
        - cms.someModel.$update=>fieldWithOwner
"""
@receiver(post_save)
def send_notify_to_user_on_save(sender, instance, **kwargs):
    if sender.__name__ in SKIP_MODEL_NAMES:
        return
    # преобразуем название модели в начальный кусочек идентификатора и
    identifier_starts_with = model_name_to_identifier(sender.__name__)
    # находим все идентификаторы уведомлений которые начинаются с:
    identifiers = find_identifiers(identifier_starts_with)
    #
    # отсылаем уведомление всем кому можем
    #
    if kwargs.get('created', False):
        # объект был только создан
        for id in identifiers:
            if not is_hook(id, '$new'):
                continue
            if id.startswith('cms.user.$new'):
                # о новом созданном пользователе следует уведомлять ТОЛЬКО
                # во вьюхах, т.к. в сигналах сырой пароль не доступен
                continue
            user = find_user_prop_from_identifier(instance, id)
            notify_user_wrapper(user, context={'instance': instance}, identifier=id)
        return

    # это обычное обновление модели
    # сначала просто пытаемся найти общие уведомления
    for id in identifiers:
        if not is_hook(id, '$update'):
            continue
        user = find_user_prop_from_identifier(instance, id)
        notify_user_wrapper(user, context={'instance': instance}, identifier=id)

    # теперь пытаемся найти частные уведомления по обновлению
    if not hasattr(instance, 'changes'):
        return
    changes = instance.changes()
    for field in changes:
        if field.endswith('_id'):
            field = field[:-3]
        for id in identifiers:
            if not id.startswith(identifier_starts_with + '.' + field+'.$diff'):
                continue
            if '$diff-' in id:
                user = find_user_prop_from_identifier(instance.previous_instance(), id)
            else:
                user = find_user_prop_from_identifier(instance, id)
            notify_user_wrapper(user, context={'instance': instance}, identifier=id)



@receiver(post_delete)
def send_notify_to_user_on_delete(sender, instance, **kwargs):
    if sender.__name__ in SKIP_MODEL_NAMES:
        return
    # преобразуем название модели в начальный кусочек идентификатора и
    identifier_starts_with = model_name_to_identifier(sender.__name__)
    # находим все идентификаторы уведомлений которые начинаются с:
    identifiers = find_identifiers(identifier_starts_with)
    #
    # отсылаем уведомление всем кому можем
    #
    for id in identifiers:
        if not id.startswith(identifier_starts_with + '.$delete'):
            continue
        user = find_user_prop_from_identifier(instance.previous_instance(), id)
        notify_user_wrapper(
            user,
            identifier=id,
            context={'instance': instance.previous_instance()}
        )

'''
@receiver(post_save, sender=CmsTranslations)
def generate_gettext_dump_and_upload(sender, instance, **kwargs):
    from drf_lbcms.helpers.gettext import generate_gettext_dump_and_upload as gen
    gen(instance)
'''
