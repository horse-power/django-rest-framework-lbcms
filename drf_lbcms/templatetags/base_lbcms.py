# https://www.djangosnippets.org/snippets/967/
from django import template
from django.template import Library, Node, VariableDoesNotExist
from django.template.defaultfilters import stringfilter
from django.templatetags import static as static_templatetag
from django.conf import settings
from django.core.exceptions import SuspiciousOperation

register = Library()


@register.tag(name="switch")
def do_switch(parser, token):
    """
    The ``{% switch %}`` tag compares a variable against one or more values in
    ``{% case %}`` tags, and outputs the contents of the matching block.  An
    optional ``{% else %}`` tag sets off the default output if no matches
    could be found::

        {% switch result_count %}
            {% case 0 %}
                There are no search results.
            {% case 1 %}
                There is one search result.
            {% else %}
                Jackpot! Your search found {{ result_count }} results.
        {% endswitch %}

    Each ``{% case %}`` tag can take multiple values to compare the variable
    against::

        {% switch username %}
            {% case "Jim" "Bob" "Joe" %}
                Me old mate {{ username }}! How ya doin?
            {% else %}
                Hello {{ username }}
        {% endswitch %}
    """
    bits = token.contents.split()
    tag_name = bits[0]
    if len(bits) != 2:
        raise template.TemplateSyntaxError("'%s' tag requires one argument" % tag_name)
    variable = parser.compile_filter(bits[1])

    class BlockTagList(object):
        # This is a bit of a hack, as it embeds knowledge of the behaviour
        # of Parser.parse() relating to the "parse_until" argument.
        def __init__(self, *names):
            self.names = set(names)
        def __contains__(self, token_contents):
            name = token_contents.split()[0]
            return name in self.names

    # Skip over everything before the first {% case %} tag
    parser.parse(BlockTagList('case', 'endswitch'))

    cases = []
    token = parser.next_token()
    got_case = False
    got_else = False
    while token.contents != 'endswitch':
        nodelist = parser.parse(BlockTagList('case', 'else', 'endswitch'))

        if got_else:
            raise template.TemplateSyntaxError("'else' must be last tag in '%s'." % tag_name)

        contents = token.contents.split()
        token_name, token_args = contents[0], contents[1:]
        if token_name == 'case':
            tests = list(map(parser.compile_filter, token_args))
            case = (tests, nodelist)
            got_case = True
        else:
            # The {% else %} tag
            case = (None, nodelist)
            got_else = True
        cases.append(case)
        token = parser.next_token()

    if not got_case:
        raise template.TemplateSyntaxError("'%s' must have at least one 'case'." % tag_name)

    return SwitchNode(variable, cases)



class SwitchNode(Node):
    def __init__(self, variable, cases):
        self.variable = variable
        self.cases = cases

    def __repr__(self):
        return "<Switch node>"

    def __iter__(self):
        for tests, nodelist in self.cases:
            for node in nodelist:
                yield node

    def get_nodes_by_type(self, nodetype):
        nodes = []
        if isinstance(self, nodetype):
            nodes.append(self)
        for tests, nodelist in self.cases:
            nodes.extend(nodelist.get_nodes_by_type(nodetype))
        return nodes

    def render(self, context):
        try:
            value = self.variable.resolve(context, True)
        except VariableDoesNotExist:
            if self.cases and self.cases[-1][0] is None:
                # {% else %}
                return self.cases[-1][1].render(context)
            return ""

        for tests, nodelist in self.cases:
            if tests is None:
                # {% else %}
                return nodelist.render(context)
            # {% case x %}
            for test in tests:
                test_value = test.resolve(context, True)
                if value == test_value:
                    return nodelist.render(context)
        return ""









BUILD_DATA = getattr(settings, 'BUILD_DATA', {'date': None})
if settings.PLATFORM == 'prod':
    class StaticExtraNode(static_templatetag.StaticNode):
        def url(self, context):
            path = self.path.resolve(context)
            if 'staticFilesPrefix' in BUILD_DATA:
                path = settings.BUILD_DATA['staticFilesPrefix'] + '/' + path
            try:
                path = self.handle_simple(path)
            except SuspiciousOperation:
                # ошибка в boto3, который падает при попытке получить ресурс c / в начале
                path = self.handle_simple(path[1:])
            # если хостим с домена, то указываем полный путь, т.к. иногда такие ссылки вставляются и в почту
            if path.startswith('/'):
                path = "https://{hostname}{path}".format(
                    hostname=settings.HOSTNAME,
                    path=path
                )
            return path

        def render(self, context):
            return super().render(context) + '?build=%s' % BUILD_DATA['date']
else:
    static_prefix = settings.STATIC_URL[1:]
    class StaticExtraNode(static_templatetag.StaticNode):
        def url(self, context):
            path = self.path.resolve(context)
            if path.startswith(static_prefix):
                path = path[len(static_prefix):]
            return self.handle_simple(path)


@register.tag('static')
def do_static(parser, token):
    """
    Join the given path with the STATIC_URL setting.
    Usage::
        {% static path [as varname] %}
    Examples::
        {% static "myapp/css/base.css" %}
        {% static variable_with_path %}
        {% static "myapp/css/base.css" as admin_base_css %}
        {% static variable_with_path as varname %}
    """
    return StaticExtraNode.handle_token(parser, token)


# https://github.com/django/django/blob/a7c73b944f51d6c92ec876fd7e0a171e7c01657d/django/template/defaultfilters.py#L63
@register.filter(is_safe=False)
def add(value, arg):
    """Add the arg to the value."""
    try:
        return int(value) + int(arg)
    except (ValueError, TypeError):
        try:
            return value + arg
        except Exception:
            pass
    try:
        return str(value) + str(arg)
    except:
        return ''

@register.filter
@stringfilter
def suffix(value, args):
    return value + str(args) if value else value
