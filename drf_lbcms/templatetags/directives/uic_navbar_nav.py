# -*- coding: utf-8 -*-
import six
from django.utils.html import conditional_escape
from django.template import Template, Context
from urllib.parse import quote
from drfs import get_model
from . import helpers
from .helpers import get_site_settings
from drf_lbcms.helpers.l10n import l10n_filter




def slugify(text):
    table = {ord(char): None for char in ";/?:@&=+$|,!{}\^[]`<>#%"}
    table[ord(' ')] = ord('-')
    return text.translate(table)



class Menuitem:
    def __init__(self, **kwargs):
        for k,v in kwargs.items():
            if k == 'liAttrs' and getattr(v, 'class', None):
                self.liClass = v.safe_value(getattr(v, 'class'))
                v.rm('class')
            setattr(self, k, v)


class Attrs(object):
    def __init__(self):
        self._attr_names = []

    def add(self, name, value):
        setattr(self, name, value)
        if name not in self._attr_names:
            self._attr_names.append(name)

    def append(self, name, value):
        if not hasattr(self, name):
            setattr(self, name, [])
        value_list = getattr(self, name)
        if not isinstance(value_list, list):
            value_list = [value_list]
        value_list.append(value)
        setattr(self, name, value_list)


    def rm(self, name):
        if name in self._attr_names:
            self._attr_names.remove(name)
            delattr(self, name)

    def items(self):
        for name in self._attr_names:
            yield [name, getattr(self, name, None)]

    def safe_value(self, value):
        if isinstance(value, six.string_types) or isinstance(value, six.text_type):
            return value
        if isinstance(value, list):
            return ' '.join([
                self.safe_value(v)
                for v in value
            ])
        if value == None:
            return ""
        return str(value)

    def __str__(self):
        r = []
        for name in self._attr_names:
            value = self.safe_value(getattr(self, name, None))
            r.append("%s=\"%s\"" % (
                name,
                conditional_escape(value)
            ))
        return ' '.join(r)

    def __unicode__(self):
        return u"" + self.__str__()







class NavConfParser:
    def __init__(self, navbarConf, context={}, options={}):
        self.navbarConf = navbarConf
        self.context = context
        self.request = context['request']
        self.current_lang = helpers.get_current_lang(context)
        self.options = options
        self.options['bsVersion'] = options.get('bsVersion', None) or 3
        self.options['useDropdowns'] = options.get('useDropdowns', True)

    def is_current_url(self, href):
        eq = (href == self.request.path) or (href == quote(self.request.path.encode('utf-8')))
        return eq

    def render_to_string(self, template, context):
        if not template:
            return ''
        context['LANGUAGE_CODE'] = self.current_lang
        t = Template("{% load lbcms %}"+template)
        c = Context(context)
        return t.render(c)


    def is_empty(self, obj):
        if isinstance(obj, str):
            return not len(obj)
        if isinstance(obj, dict):
            if not obj.keys():
                return True
        else:
            if not obj:
                return False
            return True
        for k,v in obj.items():
            if v:
                return False
        return True


    def get_title(self, item):
        template = self.navbarConf.get('itemInnerTemplate', None)  or  '{{title}}'
        if item.get('html', None):
            if item['html'].get('pre', None):
                template = item['html']['pre'] + template
            if item['html'].get('post', None):
                template = template + item['html']['post']
        title = ''
        if item.get('title', None):
            title = l10n_filter(item['title'], lang=self.current_lang)
        if template == '{{title}}':
            return title
        return self.render_to_string(template, {'title': title})


    def get_attrs(self, item):
        attrs = Attrs()
        if self.options['bsVersion'] >= 5:
            attrs.add('class', 'nav-link')
        if not item.get('attrs', None):
            attrs.add('target', '_self')
            return attrs
        for k,v in item['attrs'].items():
            attrs.add(k, v)
        if not getattr(attrs, 'target', None):
            attrs.add('target', '_self')
        return attrs


    def get_hide_item_attrs(self, item):
        if not item:
            return ''
        css_classes = []
        if item.get('hideForLangs', None):
            if item['hideForLangs'].get(self.current_lang, False):
                return True, None
        for screen_size in item.get('hideForScreenSize', {}):
            if item['hideForScreenSize'][screen_size]:
                css_classes.append("hidden-" + screen_size)
        if self.options['bsVersion'] >= 5:
            css_classes.append('nav-item')
        attrs = Attrs()
        if css_classes:
            attrs.add('class', ' '.join(css_classes))
        return False, attrs


    def href_to_menuitem(self, item):
        href = item['data']
        if not href:
            return None
        hide, liAttrs = self.get_hide_item_attrs(item)
        if hide:
            return
        attrs = self.get_attrs(item)

        if not href.startswith('http://') and not href.startswith('https://') and href[0]!='/' and href[0]!='#':
            href = u"http://"+href
        if href[0] not in ['#', '/']:
            attrs.add('rel', 'nofollow')
        return Menuitem(
            type="href",
            href=href,
            title=self.get_title(item),
            attrs=attrs,
            liAttrs=liAttrs,
            isActive=self.is_current_url(href)
        )


    def uisrefAnchor_to_menuitem(self, item):
        hide, liAttrs = self.get_hide_item_attrs(item)
        if hide:
            return
        attrs = self.get_attrs(item)
        view = None
        if isinstance(item['data'], str):
            data = item['data'].split('#')
            if len(data) == 2:
                view, anchor = data
        if not view:
            return

        scrollOffset = self.options.get('navbarHeight', None) or 0
        if not self.options.get('navbarFixedTop', False):
            scrollOffset = 0

        return Menuitem(
            type="uisrefAnchor",
            uisref=view,
            title=self.get_title(item),
            attrs=attrs,
            liAttrs=liAttrs,
            anchor=anchor,
            scrollOffset=scrollOffset
        )



    def uisref_to_menuitem(self, item, view=''):
        hide, liAttrs = self.get_hide_item_attrs(item)
        if hide:
            return

        title = self.get_title(item)
        attrs = self.get_attrs(item)
        href = None
        ModelClass = None

        if 'app.articles.details' in view:
            try:
                ModelClass = get_model('CmsArticle.json', latest=True)
            except:
                return None
        if 'app.galleries.details' in view:
            try:
                ModelClass = get_model('CmsPictureAlbum.json', latest=True)
            except:
                return None

        if not ModelClass:
            if isinstance(item['data'], str):
                kw = {'lang': self.current_lang}
                href = helpers.reverse(item['data'], kwargs=kw)
            else:
                return None
        else:
            id = item['data']
            if isinstance(id, str):
                id = id.split('-')[0]
            try:
                obj = ModelClass.objects.get(id=id, isPublished=True)
            except ModelClass.DoesNotExist:
                return
            if self.is_empty(title):
                _item = item.copy()
                _item['title'] = obj.title
                title = self.get_title(_item)
            try:
                href = obj.get_absolute_url(lang=self.current_lang)
            except:
                return None
        return Menuitem(
            type="uisref",
            href=href,
            title=title,
            attrs=attrs,
            liAttrs=liAttrs,
            isActive=self.is_current_url(href),
        )


    def uisrefGenerator_to_menuitem(self, item, _type):
        if not item['data'] or not self.options['useDropdowns']:
            return None
        if _type not in ['article-category']:
            return None
        hide, liAttrs = self.get_hide_item_attrs(item)
        if hide:
            return

        try:
            CmsCategoryForArticle = get_model('CmsCategoryForArticle.json', latest=True)
        except:
            return None
        id = item['data']
        if isinstance(id, str):
            id = id.split('-')[0]
        try:
            obj = CmsCategoryForArticle.objects.get(id=id, isPublished=True)
        except CmsCategoryForArticle.DoesNotExist:
            return None

        title = self.get_title(item)
        if self.is_empty(title):
            _item = item.copy()
            _item['title'] = obj.title
            title = self.get_title(_item)

        childs = []
        for article in obj.articles_by_category.filter(isPublished=True):
            try:
                href = article.get_absolute_url(lang=self.current_lang)
            except:
                continue
            article_attrs = Attrs()
            article_liAttrs = Attrs()
            if self.options['bsVersion'] >= 5:
                article_attrs.add('class', 'nav-link')
                article_liAttrs.add('class', 'nav-item')
            childs.append(Menuitem(
                type="href",
                href=href,
                title=self.render_to_string("{% trans title %}", {'title': article.title}),
                attrs=article_attrs,
                liAttrs=article_liAttrs,
                isActive=self.is_current_url(href)
            ))
        if not childs:
            return None
        return Menuitem(
            type='dropdown',
            title=title,
            childs=childs,
            attrs=self.get_attrs(item),
            liAttrs=liAttrs,
            isActive=False
        )



    def file_to_menuitem(self, item):
        hide, liAttrs = self.get_hide_item_attrs(item)
        if hide:
            return

        from drf_lbcms.models import L10nFile
        l10nFile = L10nFile.objects.filter(id=item['data']).first()
        if not l10nFile:
            return None
        attrs = self.get_attrs(item)
        attrs.add('download', '')
        attrs.add('target', '_blank')
        menuitem = Menuitem(
            type="href",
            href=l10nFile.resolve_file_uri(),
            title=self.get_title(item),
            attrs=attrs,
            liAttrs=liAttrs,
            isActive=False
        )
        return menuitem


    def action_to_menuitem(self, item):
        hide, liAttrs = self.get_hide_item_attrs(item)
        if hide:
            return

        attrs = self.get_attrs(item)
        attrs.rm('target')
        menuitem = Menuitem(
            type="action",
            ngClick="$cms.collapseNavbar();$cms.runSiteAction('%s')" % item['data'],
            title=self.get_title(item),
            attrs=attrs,
            liAttrs=liAttrs,
            isActive=False
        )
        return menuitem


    def directive_to_menuitem(self, item):
        if not item['data']:
            return None
        hide, liAttrs = self.get_hide_item_attrs(item)
        if hide:
            return

        if item['data'] == 'ui-lang-picker-for-navbar':
            return Menuitem(
                type="directive",
                htmlAttr='',
                template=self.render_to_string(
                    "{% ui_lang_picker_for_navbar css_class=cssClass bs_version=bsVersion %}",
                    {
                        'request': self.request,
                        'cssClass': getattr(liAttrs, 'class', ''),
                        'bsVersion': self.options.get('bsVersion', None) or 3
                    }
                ),
                isActive=False
            )
        return Menuitem(
            type="directive",
            htmlAttr=item['data'],
            liAttrs=liAttrs,
            template="",
            isActive=False
        )


    def contacts_to_menuitem(self, item):
        hide, liAttrs = self.get_hide_item_attrs(item)
        if hide:
            return

        settings = get_site_settings(self.context)
        attrs = self.get_attrs(item)
        menuitems = []
        for contact in settings.publicContacts or []:
            if item['data'] == 'tel' and contact['type'] != 'tel':
                continue
            if item['data'] == 'email' and contact['type'] != 'email':
                continue
            if item['data'] in ['tel-skype-whatsapp', 'tel-skype-messenger'] and \
                contact['type'] not in ['tel', 'skype', 'whatsapp-tel', 'whatsapp', 'messenger-tel', 'messenger']:
                continue
            menuitems.append(Menuitem(
                type="contact",
                template=self.render_to_string(
                    "{% uic_contact contact color='fa' %}",
                    {'request': self.request, 'contact': contact}
                ),
                attrs=attrs,
                liAttrs=liAttrs
            ))
        return menuitems


    def parse(self, item):
        _type = item.get('type', None)
        if _type == 'href':
            return self.href_to_menuitem(item)
        if _type == 'ui-sref':
            return self.uisref_to_menuitem(item)
        if _type == 'ui-sref:generator:article-category':
            return self.uisrefGenerator_to_menuitem(item, 'article-category')
        if _type =='ui-sref:app.articles.details':
            return self.uisref_to_menuitem(item, 'app.articles.details')
        if _type =='ui-sref:app.galleries.details':
            return self.uisref_to_menuitem(item, 'app.galleries.details')
        if _type == 'ui-sref:anchor':
            return self.uisrefAnchor_to_menuitem(item)
        if _type == 'file':
            return self.file_to_menuitem(item)
        if _type == 'action':
            return self.action_to_menuitem(item)
        if _type == 'directive':
            return self.directive_to_menuitem(item)
        if _type == 'contacts':
            return self.contacts_to_menuitem(item)
        if item.get('items', None) and self.options['useDropdowns']:
            childs = []
            for child in item['items']:
                _child = self.parse(child)
                if _child:
                    childs.append(_child)
            if not childs:
                return None
            return Menuitem(
                type='dropdown',
                title=self.get_title(item),
                childs=childs,
                attrs=self.get_attrs(item),
                isActive=False
            )
        return None






def uic_navbar_nav_item(context, menuitem, bs_version=3):
    return {
        'menuitem': menuitem,
        'request': context.get('request', None),
        'settings': context.get('settings', None),
        'site': context.get('site', None),
        'bsVersion': bs_version
    }


def uic_navbar_nav(context, navbar_conf={}, bs_version=3, use_dropdowns=True):
    new_context = {
        'menuConf': {},
        'request': context.get('request', None),
        'settings': context.get('settings', None),
        'site': context.get('site', None),
        'bsVersion': bs_version,
        'useDropdowns': use_dropdowns
    }
    if not navbar_conf:
        return new_context

    parser = NavConfParser(navbar_conf, context=context, options={'bsVersion': bs_version, 'useDropdowns': use_dropdowns})
    for positionType, items in navbar_conf.items():
        if not items:
            continue
        new_context['menuConf'][positionType] = []
        for item in items:
            menuitem = parser.parse(item)
            if not menuitem:
                continue
            if not isinstance(menuitem, list):
                menuitem = [menuitem]
            new_context['menuConf'][positionType] += menuitem
        if not new_context['menuConf'][positionType]:
            del new_context['menuConf'][positionType]

    return new_context
