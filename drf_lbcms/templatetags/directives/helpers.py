# -*- coding: utf-8 -*-
from django.conf import settings as djsettings
from django.utils import translation
from drf_lbcms.helpers import get_site_settings as _get_site_settings
try:
    from django.core.urlresolvers import reverse as djreverse, resolve, NoReverseMatch
except ImportError:
    from django.urls import reverse as djreverse, resolve, NoReverseMatch



LANG_MAP = {
    'en': u"English",
    'ru': u"Русский",
    'cz': u"Čeština",
    'cs': u"Čeština",
    'ua': u"Українська",
    'uk': u"Українська",
    'de': u"Deutsch",
    'es': u'Español',
    'da': u'Dansk',
}
LANG_CODE_TO_COUNTRY_CODE_MAPPING = {
    'en': 'gb',
    'cz': 'cs',
    'da': 'dk',
    'uk': 'ua'
}


def get_view(context):
    view = resolve(context['request'].path)
    return view, context['request']


def get_current_lang(context, current_lang=None):
    if current_lang:
        return current_lang
    lang = translation.get_language() or djsettings.APP_CONTEXT['CMS_DEFAULT_LANG'] or djsettings.LANGUAGE_CODE
    if 'LANGUAGE_CODE' in context:
        lang = context['LANGUAGE_CODE']
    if 'LANGUAGE' in context:
        lang = context['LANGUAGE']
    if '$LANGUAGE_CODE' in context:
        lang = context['$LANGUAGE_CODE']
    return lang


def get_site_settings(context):
    if 'settings' in context:
        return context['settings']
    return _get_site_settings()


def reverse(view_name, args=[], kwargs={}):
    try:
        return djreverse(view_name, args=args, kwargs=kwargs)
    except NoReverseMatch:
        _kwargs = kwargs.copy()
        try:
            _kwargs['_0'] = ''
            return djreverse(view_name, args=args, kwargs=_kwargs)
        except NoReverseMatch:
            # удаляем язык если он есть
            if 'lang' in _kwargs:
                del _kwargs['lang']
            if _kwargs['_0'] == '':
                del _kwargs['_0']
            return djreverse(view_name, args=args, kwargs=_kwargs)
