# -*- coding: utf-8 -*-
from django.db.models.query import QuerySet

def _is_localhost(context):
    if 'request' not in context:
        return False
    return 'localhost' in context['request'].META.get('HTTP_HOST', '')

def l10nfile_to_url(context, l10nFile, size=None, file_id=None):
    if isinstance(l10nFile, QuerySet):
        # пытаемся найти по id
        try:
            l10nFile = l10nFile.get(id=file_id)
        except:
            l10nFile = None

    if not l10nFile:
        return ""
    return l10nFile.resolve_file_uri(
        thumb_size=size,
        force_cms_db_bucket=context.get('CMS_DB_BUCKET', None)
    )


def uic_file_download(context, l10nFile):
    # директива, которая рендерит кусочек html
    if not l10nFile:
        return {}
    return {
        'url': l10nFile.resolve_file_uri(
            force_cms_db_bucket=context.get('CMS_DB_BUCKET', None)
        ),
        'originalName': l10nFile.resolve_file_orig_name()
    }


def uic_picture(context, l10nFile, file_id=None, size=None, css_class=None):
    # директива, которая рендерит кусочек html
    if isinstance(l10nFile, QuerySet):
        # пытаемся найти по id
        try:
            l10nFile = l10nFile.get(id=file_id)
        except:
            l10nFile = None
    if not l10nFile:
        return {}
    return {
        'url': l10nFile.resolve_file_uri(thumb_size=size, force_cms_db_bucket=context.get('CMS_DB_BUCKET', None)),
        'title': l10nFile.title,
        'description': l10nFile.description,
        'cssClass': css_class or ''
    }


def site_logo_img_url(context, size=None):
    from drf_lbcms.helpers.l10nFile import resolve_l10nfile_uri

    if size:
        size = "."+str(size)
    else:
        size = ""
    #if _is_localhost(context):
    #    return "/client/brand/logo%s.png" % size
    return resolve_l10nfile_uri(
        "/client/brand/logo%s.png" % size,
        force_cms_db_bucket=context.get('CMS_DB_BUCKET', None)
    )


def site_favicon_img_url(context, size=None):
    from drf_lbcms.helpers.l10nFile import resolve_l10nfile_uri
    if not size:
        size = '32x32'

    #if _is_localhost(context):
    #    return "/client/brand/favicon.%s.png" % size
    return resolve_l10nfile_uri(
        "/client/brand/favicon.%s.png" % size,
        force_cms_db_bucket=context.get('CMS_DB_BUCKET', None)
    )
