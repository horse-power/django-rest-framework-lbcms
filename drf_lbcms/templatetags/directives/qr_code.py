# -*- coding: utf-8 -*-
import base64, six
from django.utils.safestring import mark_safe

if six.PY2:
    from StringIO import StringIO as BytesIO
else:
    from io import BytesIO


def qr_code(context, text, size=None, border=4):
    try:
        import qrcode
        import qrcode.image.svg
    except ImportError:
        import warnings
        warnings.warn("Install https://pypi.org/project/qrcode/ package so qr_code function from drf_lbcms.templatetags could work!")
        return '<img />'

    qr = qrcode.QRCode(box_size=size or 2, border=border)
    qr.add_data(text)
    img = qr.make_image(fill_color="black", back_color="white")
    output = BytesIO()
    img.save(output)

    img = base64.b64encode(output.getvalue())
    if not six.PY2:
        img = img.decode('utf-8')

    return mark_safe("<img src='data:image/png;base64,{img}' />".format(
        img=img
    ))
