# -*- coding: utf-8 -*-
from django.utils import translation
from . import helpers





def generate_context(context, langs=[], current_lang=None, css_class=None, bs_version=3):
    view, request = helpers.get_view(context)
    if current_lang and '-' in current_lang:
        current_lang = current_lang.split('-')[0]
    if current_lang not in langs:
        langs = [current_lang] + langs

    def get_country_flag_code(lang):
        return helpers.LANG_CODE_TO_COUNTRY_CODE_MAPPING.get(lang, None) or lang

    def get_language_name(lang):
        if lang in helpers.LANG_MAP:
            return helpers.LANG_MAP[lang]
        if lang in helpers.LANG_CODE_TO_COUNTRY_CODE_MAPPING:
            return helpers.LANG_MAP[helpers.LANG_CODE_TO_COUNTRY_CODE_MAPPING[lang]]
        return lang

    directive_context = {
        'currentLang':{
            'flagCode': get_country_flag_code(current_lang),
            'code': current_lang,
            'name': get_language_name(current_lang)
        },
        'langsList': [],
        'cssClass': css_class or '',
        'bsVersion': bs_version
    }
    special_kwargs = {}
    for _dict in context:
        for k,v in _dict.items():
            if k.startswith('$kwargsGettext-'):
                special_kwargs[k.replace('$kwargsGettext-', '')] = v

    orig_lang = translation.get_language()
    for lang_code in langs:
        kwargs = view.kwargs.copy()
        kwargs['lang'] = lang_code
        for k in kwargs:
            if special_kwargs.get(k, {}).get(lang_code, None):
                kwargs[k] = special_kwargs[k][lang_code]
        # параметр 'lang' в i18n_patterns в джанге не изменяется через kwargs
        translation.activate(lang_code)
        try:
            _url = helpers.reverse(view.view_name, args=view.args, kwargs=kwargs)
        except:
            _url = helpers.reverse('app.index', args=view.args, kwargs=kwargs)
        directive_context['langsList'].append({
            'url': request.build_absolute_uri(_url),
            'code': lang_code,
            'isActive': lang_code == current_lang,
            'flagCode': get_country_flag_code(lang_code),
            'name': get_language_name(lang_code),
        })
    translation.activate(orig_lang)
    return directive_context



def ui_lang_picker_for_navbar(context, langs=[], css_class=None, bs_version=3):
    if not langs:
        settings = helpers.get_site_settings(context)
        if settings:
            langs = settings.cmsLangCodes or []
    if not langs:
        return {}
    return generate_context(
        context,
        langs=langs,
        current_lang=helpers.get_current_lang(context),
        css_class=css_class,
        bs_version=bs_version
    )
