# -*- coding: utf-8 -*-





def uic_site_brand_a(context, ngModel, style=None):
    # директива, которая рендерит кусочек html
    if not ngModel:
        return ""
    allowed_style = ['grey', 'circle']
    if not style:
        style = "circle/"
    elif style == 'squire':
        style = ''
    elif style not in allowed_style:
        style = ''
    else:
        style = style+'/'

    def get_site_brand_img(url):
        url = url.lower()
        if 'facebook.com/' in url:
            return 'facebook'
        if 'youtube.com/' in url:
            return 'youtube'
        if 'vk.com/' in url  or  'new.vk.com/' in url or 'vkontakte.com/' in url:
            return 'vkontakte'
        if 'odnoklassniki.ru/' in url or 'ok.ru/' in url:
            return 'odnoklassniki'
        if 'twitter.com/' in url:
            return 'twitter'
        if 'tripadvisor' in url:
            return 'tripadvisor'
        if 'booking.com/' in url:
            return 'booking'
        if 'airbnb.com' in url:
            return 'airbnb'
        if 'instagram.com/' in url:
            return 'instagram'
        return 'default'

    def get_href(url):
        if url.find('http://') != 0 and url.find('https//') != 0:
            return "http://" + url
        return url

    icon_type = get_site_brand_img(ngModel)
    return {
        'href': get_href(ngModel),
        'iconType': icon_type,
        'icon': "/drf_lbcms/brand/%s%s.png" % (style, icon_type)
    }
