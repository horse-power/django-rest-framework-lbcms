# -*- coding: utf-8 -*-
import tempfile, subprocess, os, six, base64, uuid
from django.template import Node, Variable
from django.core.files.storage import DefaultStorage
import logging

from drf_lbcms.helpers.html import img_to_base64
from drf_lbcms.helpers.l10nFile import resolve_l10nfile_uri

storage = DefaultStorage()
logger = logging.getLogger("drf_lbcms")

try:
    from bs4 import BeautifulSoup
except ImportError:
    BeautifulSoup = None



def to_int(value):
    value = value.replace("'", '').replace("'", '').replace('"', '').replace('"', '')
    try:
        return int(value)
    except:
        return None

def to_str(value):
    value = value.replace("'", '').replace("'", '').replace('"', '').replace('"', '')
    return value



def uic_block_to_img(parser, token):
    bits = token.split_contents()
    height = None
    width = None
    name = None
    alt = None
    img_type = None
    for b in bits:
        if b.startswith('size'):
            size = b.split('=')[-1]
            width = to_int(size.split('x')[0])
            height = to_int(size.split('x')[-1])
        if b.startswith('name'):
            name = Variable(b.split('=')[-1])
        if b.startswith('type'):
            img_type = to_str(b.split('=')[-1])
        if b.startswith('alt'):
            if '=' not in b:
                alt = True
            else:
                alt = Variable(b.split('=')[-1])


    nodelist = parser.parse(('end_uic_block_to_img',))
    parser.delete_first_token()
    return UicBlockToImgNode(nodelist, height=height, width=width, name=name, alt=alt, img_type=img_type)




class UicBlockToImgNode(Node):
    def __init__(self, nodelist, height=None, width=None, name=None, alt=None, img_type=None):
        self.nodelist = nodelist
        self.height = height
        self.width = width
        self.name = name
        self.alt = alt
        if img_type not in ['jpg', 'png', None]:
            self.img_type = 'png'
        else:
            self.img_type = img_type or 'png'

    def render(self, context):
        from drf_lbcms.pdf_render.helpers import FONTS

        # генерируем html для верстки которую переведем в картинку
        fonts = ""
        for k,v in FONTS.items():
            fonts += v


        html = self.nodelist.render(context).strip()
        if html.startswith('<!--'):
            html = html[4:]
        if html.endswith('-->'):
            html = html[:-3]

        html = img_to_base64(img_to_base64("""
        <html>
          <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <style type="text/css">
                """ + fonts + """
                body{
                    padding: 0;
                    margin: 0;
                }
                *{
                    font-family: "Roboto", "sans-serif";
                }
            </style>
          </head>
          <body>
            """ + html + """
          </body>
        </html>
        """))


        if isinstance(self.alt, Variable):
            self.alt = self.alt.resolve(context)
        elif self.alt:
            # генерим текст из верстки
            if not BeautifulSoup:
                logger.warning("Please install BeautifulSoup v4 for generation of alt-text inside uic_block_to_img directive")
                self.alt = None
            else:
                soup = BeautifulSoup(html, "html.parser")
                if soup.style:
                    soup.style.decompose()
                if soup.script:
                    soup.script.decompose()
                self.alt = ' '.join(soup.stripped_strings)


        f, tmpfilename_in = tempfile.mkstemp()
        f, tmpfilename_out = tempfile.mkstemp()
        os.remove(tmpfilename_in)
        process_args = ['wkhtmltoimage']

        tmpfilename_in += '.html'
        tmpfilename_out += '.' + self.img_type
        with open(tmpfilename_in, 'w') as f:
            f.write(html)

        # рендерим картинку
        if self.height:
            process_args.append('--height')
            process_args.append(str(self.height))
        if self.width:
            process_args.append('--disable-smart-width')
            process_args.append('--width')
            process_args.append(str(self.width))

        process_args.append(tmpfilename_in)
        process_args.append(tmpfilename_out)
        p = subprocess.Popen(
            process_args,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        out, err = p.communicate()
        #print out, err
        if os.path.getsize(tmpfilename_out) <= 1500:
            # картинка маленькая.
            # преобразовываем ее в base64
            with open(tmpfilename_out, 'rb') as f:
                data = f.read()
            os.remove(tmpfilename_in)
            os.remove(tmpfilename_out)

            prefix = 'data:image/%s;base64,' % self.img_type
            if six.PY2:
                data = prefix + base64.b64encode(data)
            else:
                data = prefix + base64.b64encode(data).decode('utf-8')
            return u"<img src='{data}' alt='{alt}' style='width: 100%;' />".format(
                data=data,
                alt=self.alt or ''
            )


        # картинка большая. грузим ее в сторадж.
        # генерируем имя, под которым мы затолкаем этот файл в бакет
        if isinstance(self.name, Variable):
            name = self.name.resolve(context)
        else:
            name = self.name
        if not name:
            name = str(uuid.uuid1())
        name = 'embedded-for-email/{name}.{ext}'.format(name=os.path.splitext(name)[0], ext=self.img_type)

        with open(tmpfilename_out, 'rb') as f:
            storage.delete(name)
            storage.save(name, f)

        os.remove(tmpfilename_in)
        os.remove(tmpfilename_out)

        # вставляем ссылку на файл в верстку
        return u"<img src='{url}' alt='{alt}' style='width:100%' />".format(
            url=resolve_l10nfile_uri(name),
            alt=self.alt or ''
        )
