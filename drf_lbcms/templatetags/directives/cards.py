from drfs import get_model
from .helpers import reverse, get_current_lang


def get_model_class(name):
    if 'app.articles.details' in name:
        try:
            return get_model('CmsArticle.json', latest=True)
        except:
            return None
    if 'app.galleries.details' in name:
        try:
            return get_model('CmsPictureAlbum.json', latest=True)
        except:
            return None

def normalize_url(url):
    if not url:
        return url
    if not url.startswith('http'):
        return "https://" + url
    return url



def uic_card_link(context, card):
    if not card or not card.get('link', None) or not card['link'].get('type', None):
        return ''

    if card['link']['type'] == 'href':
        return normalize_url(card['link'].get('data', None) or '')

    ModelClass = get_model_class(card['link']['type'])
    if not ModelClass:
        return ''
    try:
        obj = ModelClass.objects.get(id=card['link']['data'], isPublished=True)
    except ModelClass.DoesNotExist:
        return ''

    lang = get_current_lang(context)
    try:
        return obj.get_absolute_url(lang=lang)
    except:
        return ''
