from django.conf import settings as djsettings
from django.utils import translation
from drf_lbcms.helpers.l10n import l10n_filter

class EmptyCmsSettings:
    siteTitle = None
    siteDescription = None

def get_lang_and_settings(context):
    if not context or 'settings' not in context:
        raise AttributeError("CmsSettings object not found in context")
    lang = context.get('LANGUAGE_CODE', None) or translation.get_language()
    settings = context.get('settings', None) or EmptyCmsSettings()
    return settings, lang


def get_metatag_value(db_object, field):
    if not djsettings.DRF_LBCMS.get('seo', {}).get('useDataMetatags', False) or isinstance(db_object, dict):
        return None
    if hasattr(db_object, 'data') and db_object.data and db_object.data.get('metatags', None):
        return db_object.data['metatags'].get(field, None)
    return None


def get_field_value(db_object_or_dict, field):
    if isinstance(db_object_or_dict, dict):
        return db_object_or_dict
    return getattr(db_object_or_dict, field, None)




def site_page_title(context, subtitle_or_object=None):
    settings, lang = get_lang_and_settings(context)
    if not subtitle_or_object:
        return l10n_filter(settings.siteTitle, lang)

    # если перед нами объект, то пытаемся найти метатег заголовка
    metatag_title = get_metatag_value(subtitle_or_object, 'title')
    if metatag_title:
        title = l10n_filter(metatag_title, lang)
        if title:
            return title

    # просто собираем текст заголовка
    title = l10n_filter(settings.siteTitle, lang) or ''
    subtitle = get_field_value(subtitle_or_object, 'title')
    if callable(subtitle):
        subtitle = subtitle()
    if isinstance(subtitle, dict):
        subtitle = l10n_filter(subtitle, lang)
    if not subtitle:
        return title
    return subtitle + " | " + title



def site_page_description(context, description_or_object=None):
    settings, lang = get_lang_and_settings(context)
    if not description_or_object:
        return l10n_filter(settings.siteDescription, lang) or ''

    # если перед нами объект, то пытаемся найти метатег заголовка
    metatag_description = get_metatag_value(description_or_object, 'description')
    if metatag_description:
        description = l10n_filter(metatag_description, lang)
        if description:
            return description
    #
    if isinstance(description_or_object, dict):
        description = l10n_filter(description_or_object, lang)
        if description:
            return description

    return l10n_filter(settings.siteDescription, lang) or ''
