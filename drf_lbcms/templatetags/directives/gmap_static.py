


def gmap_static_map_marker(marker, size=None, color='red'):
    from dj_gmap.gmap.gmap_static import GMapStaticClient

    G = GMapStaticClient()
    canvas = G.new_map_canvas()

    if isinstance(marker, (list, tuple)):
        place = {'lat': marker[0], 'lng': marker[1]}
    else:
        place = marker
    if not size:
        width = 640
        height = 640
    else:
        size = size.split('x')
        if len(size) == 1:
            width = size[0]
            height = size[0]
        else:
            width = size[0]
            height = size[1]
    canvas.add_marker(place, color=color)
    return {
        'url': G.map_canvas_to_url(canvas, width=int(width), height=int(height))
    }
