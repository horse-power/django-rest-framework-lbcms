# -*- coding: utf-8 -*-
import re
import json
from django.utils.translation import pgettext_lazy


ICONS = {
    'email': 'fa-envelope-o',
    'tel': 'fa-phone',
    'skype': 'fa-skype',
    'fax': 'fa-fax',
    'messenger': 'fa-phone',
    'messenger-tel': 'fa-phone',
}

ICONS.update({
    'whatsapp-tel': 'fa-phone', #DEPRECATED
    'whatsapp': 'fa-phone', #DEPRECATED
})

TEL_TYPE = ['tel', 'fax', 'messenger', 'messenger-tel'] + [
    'whatsapp', 'whatsapp-tel' #DEPRECATED
]

# не удалять ниже. переводы на клиенте
pgettext_lazy('open chat', 'Open chat in WhatsUp')
pgettext_lazy('open chat', 'Open chat in Viber')
pgettext_lazy('open chat', 'Open chat in Telegram')
pgettext_lazy('call phone', "Call to %(value)s")




def get_href(contact):
    if not contact['type'] or not contact.get('value', None):
        return ''
    if contact['type'] in TEL_TYPE:
        tel = contact['value']
        tel = re.sub(r'/\s+/', '', tel)
        tel = re.sub(r'/\(+/', '', tel)
        tel = re.sub(r'/\)+/', '', tel)
        tel = re.sub(r'/-/', '', tel)
        return "tel:" + tel
    if contact['type'] == 'email':
        return "mailto:" + contact['value']
    if contact['type'] == 'skype':
        return "skype:" + contact['value'] + "?call"
    return contact['value']


def get_provider_href(value, provider=None):
    cleaned = value.replace(' ', '').replace('(', '').replace(')', '').replace('-', '').strip()
    cleaned = cleaned.replace('+', '').replace('(', '').replace(')', '').replace(' ', '').strip()
    if cleaned.isdigit():
        value = cleaned
    if provider == 'whatsapp':
        return "https://wa.me/" + value
    if provider == 'viber':
        return "viber://chat?number=" + value
    if provider == 'telegram':
        if not value.startswith('+') and cleaned.isdigit():
            value = '+' + value
        return "https://t.me/" + value
        #return "tg://resolve?domain=" + value
    return "tel:" + value





def old_uic_contact(context, ngModel, size=14, color='black'):
    # директива, которая рендерит кусочек html
    size = size or 14
    if color not in ['white', 'black', 'fa']:
        color = 'black'
    if ngModel['type'] not in ICONS.keys():
        ngModel['type'] = 'skype'

    return {
        'href': get_href(ngModel),
        'icon': "/drf_lbcms/fa/%s--%s.png" % (ICONS[ngModel['type']], color),
        'faIcon': ICONS[ngModel['type']],
        'ngModel': ngModel,
        'size': size,
        'color': color
    }




def uic_contact(context, ngModel, size=14, color='black', bs_version=None):
    if not bs_version:
        context = old_uic_contact(context, ngModel, size=size, color=color)
        context['bsVersion'] = None
        return context
    
    providers = ngModel.get('providers', None) or {}
    variants = []
    if 'messenger' in ngModel['type']:
        for provider in ['whatsapp', 'viber', 'telegram']:
            if not providers.get(provider, None):
                continue
            variants.append({
                'href': get_provider_href(ngModel['value'], provider),
                'faIcon': 'fab fa-'+provider,
                'target': '_blank',
            })

    if 'tel' in ngModel['type']:
        variants.append({
            'href': get_provider_href(ngModel['value'], None),
            'faIcon': 'fas fa-phone',
            'target': '',
        })
    
    target = ''
    if len(variants) == 1:
        href = variants[0]['href']
        target = variants[0]['target']
    else:
        href = get_href(ngModel)

    return {
        'bsVersion': bs_version,
        'href': href,
        'target': target,
        'faIcon': ICONS[ngModel['type']],
        'ngModel': ngModel,
        'variants': variants,
        'ngModelAsString': json.dumps(ngModel)
    }
