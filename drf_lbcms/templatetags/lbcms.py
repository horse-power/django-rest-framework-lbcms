# -*- coding: utf-8 -*-
from django import template
from django.template import Node, TemplateSyntaxError, Variable
from django.template.base import render_value_in_context
from django.utils.safestring import SafeData, mark_safe
from django.utils.translation import gettext
from django.conf import settings as djsettings
from django.templatetags.i18n import translation
from django.utils import formats
from django.utils.dateformat import format
from django.template.defaultfilters import floatformat
from django.urls import translate_url as django_translate_url

import datetime as _datetime
import json, six, math, random, sys

from drf_lbcms.helpers.date import str_to_date, str_to_datetime

from .directives.uic_navbar_nav import uic_navbar_nav, uic_navbar_nav_item
from .directives.ui_lang_picker_for_navbar import ui_lang_picker_for_navbar
from .directives.uic_contact import uic_contact
from .directives.uic_site_brand_a import uic_site_brand_a
from .directives.uic_file import uic_file_download, uic_picture, l10nfile_to_url, site_logo_img_url, site_favicon_img_url
from .directives.uic_block_to_img import uic_block_to_img
from .directives.qr_code import qr_code
from .directives.helpers import get_current_lang
from .directives import gmap_static
from .directives.seo import site_page_title, site_page_description
from .directives.cards import uic_card_link


from drf_lbcms.helpers import slugify

register = template.Library()


class TranslationsCache:
    def __init__(self):
        from drf_lbcms.models import CmsTranslations
        self.tr = {}
        self.model = CmsTranslations

    def translate(self, text, to_lang=None, context=None):
        if not to_lang:
            return text
        if to_lang not in self.tr:
            try:
                obj = self.model.objects.get(lang=to_lang)
                self.tr[to_lang] = obj._msgsCache or {}
            except self.model.DoesNotExist:
                self.tr[to_lang] = {}
                # return text
        context = context or ''
        translated_text = self.tr.get(to_lang, {}).get(text, None)
        if not translated_text:
            return gettext(text)
        if isinstance(translated_text, six.text_type):
            return translated_text
        if translated_text.get(context, None):
            return translated_text[context]
        return text

translations_cache = TranslationsCache()


class TranslateNode(Node):
    def __init__(self, filter_expression, noop, asvar=None,
                 message_context=None, asSlug = False):
        self.noop = noop
        self.asvar = asvar
        self.message_context = message_context
        self.asSlug = asSlug
        self.filter_expression = filter_expression
        if isinstance(self.filter_expression.var, six.string_types):
            self.filter_expression.var = Variable("'%s'" %
                                                  self.filter_expression.var)
        self.filter_expression.var.translate = False

    def render(self, context):
        self.filter_expression.var.translate = not self.noop
        if self.message_context:
            self.filter_expression.var.message_context = (
                self.message_context.resolve(context))
        self.filter_expression.var.translate = False
        output = self.filter_expression.resolve(context)
        value = render_value_in_context(output, context)
        # Restore percent signs. Percent signs in template text are doubled
        # so they are not interpreted as string format flags.
        is_safe = isinstance(value, SafeData)
        lang = djsettings.LANGUAGE_CODE or translation.get_language()

        if 'LANGUAGE_CODE' in context:
            lang = context['LANGUAGE_CODE']
        if '$LANGUAGE_CODE' in context:
            lang = context['$LANGUAGE_CODE']

        # TODO: разобраться что за дебилизм происходит ниже и почему контекст стал вложенным
        if context.get('context', None):
            try:
                if context['context'].get('LANGUAGE_CODE', None):
                    lang = context['context']['LANGUAGE_CODE']
                if context['context'].get('$LANGUAGE_CODE', None):
                    lang = context['context']['$LANGUAGE_CODE']
            except:
                pass


        if isinstance(output, dict):
            value = output
            lang = lang.split('-')[0]
            tr = value.get(lang, None)
            if tr in ['<p><br/></p>', '<p><br/></p>\n']:
                tr = None
            if not tr:
                tr = value.get(djsettings.LANGUAGE_CODE, None)
            if not tr and value.keys():
                tr = value[list(value.keys())[0]]
            elif not tr:
                tr = ''
            value = tr
        else:
            value = value.replace('%%', '%')
            tr = translations_cache.translate(value, to_lang=lang, context=self.message_context)
            if tr:
                value = tr
        value = mark_safe(value) if is_safe else value
        if self.asSlug:
            value = slugify(value)
        if self.asvar:
            context[self.asvar] = value
            return ''
        return value




def base_trans(parser, token):
    bits = token.split_contents()
    if len(bits) < 2:
        raise TemplateSyntaxError("'%s' takes at least one argument" % bits[0])
    message_string = parser.compile_filter(bits[1])
    remaining = bits[2:]

    noop = False
    asvar = None
    message_context = None
    seen = set()
    invalid_context = {'as', 'noop'}
    asSlug = False

    while remaining:
        option = remaining.pop(0)
        if option in seen:
            raise TemplateSyntaxError(
                "The '%s' option was specified more than once." % option,
            )
        elif option == 'noop':
            noop = True
        elif option == 'context':
            try:
                value = remaining.pop(0)
            except IndexError:
                msg = "No argument provided to the '%s' tag for the context option." % bits[0]
                six.reraise(TemplateSyntaxError, TemplateSyntaxError(msg), sys.exc_info()[2])
            if value in invalid_context:
                raise TemplateSyntaxError(
                    "Invalid argument '%s' provided to the '%s' tag for the context option" % (value, bits[0]),
                )
            message_context = parser.compile_filter(value)
        elif option == 'as':
            try:
                value = remaining.pop(0)
            except IndexError:
                msg = "No argument provided to the '%s' tag for the as option." % bits[0]
                six.reraise(TemplateSyntaxError, TemplateSyntaxError(msg), sys.exc_info()[2])
            asvar = value
        elif option == 'slugify' or option == 'slug':
            asSlug = True
        else:
            raise TemplateSyntaxError(
                "Unknown argument for '%s' tag: '%s'. The only options "
                "available are 'noop', 'context' \"xxx\", and 'as VAR'." % (
                    bits[0], option,
                )
            )
        seen.add(option)
    return TranslateNode(message_string, noop, asvar, message_context, asSlug)


@register.tag(name='trans')
def _trans(parser, token):
    return base_trans(parser, token)

@register.tag(name='translate')
def _translate(parser, token):
    return base_trans(parser, token)



class LanguageNode(Node):
    def __init__(self, nodelist, language):
        self.nodelist = nodelist
        self.language = language

    def render(self, context):
        context['$LANGUAGE_CODE'] = self.language.var
        with translation.override(self.language.resolve(context)):
            output = self.nodelist.render(context)
        if '$LANGUAGE_CODE' in context:
            del context['$LANGUAGE_CODE']
        return output


@register.tag
def language(parser, token):
    """
    This will enable the given language just for this block.
    Usage::
        {% language "de" %}
            This is {{ bar }} and {{ boo }}.
        {% endlanguage %}
    """
    bits = token.split_contents()
    if len(bits) != 2:
        raise TemplateSyntaxError("'%s' takes one argument (language)" % bits[0])
    language = parser.compile_filter(bits[1])
    nodelist = parser.parse(('endlanguage',))
    parser.delete_first_token()
    return LanguageNode(nodelist, language)



@register.simple_tag(takes_context=True)
def translate_current_url(context, lang_code):
    '''
        usage:
        {% translate_url lang_code %}
    '''
    path = context.get('request').get_full_path()
    return django_translate_url(path, lang_code)


@register.filter
def batch(iterable, n=1):
    try:
        l = iterable.count()
    except:
        l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]

@register.filter
def split(iterable, n=1):
    try:
        l = iterable.count()
    except:
        l = len(iterable)
    f = l / n
    r = l % n

    i = 0
    j = r
    if l < n:
        j = 0
        f = 1
    for ndx in range(0, n):
        j += f
        yield iterable[i:j]
        i = j

@register.filter
def chunks(iterable, size=1, mode=None):
    try:
        length = iterable.count()
    except:
        length = len(iterable)
    if not length:
        return iterable
    if size < 1:
        return []
    if length < size:
        return [iterable]

    if mode not in ['horiz', 'vert']:
        mode = 'horiz'

    if mode == 'horiz':
        i = 0
        while i < length:
            yield iterable[i: i + size]
            i += size
    elif mode == 'vert':
        items_per_chunk = math.floor(length/float(size))
        if items_per_chunk != length/float(size):
            # остаток говорит о том, что поделить красиво у нас не получится
            # добавляем +1 элемент. тогда последняя колонка у нас получится короче
            # всех
            items_per_chunk += 1
        i = 0
        while i < size:
            yield iterable[
                i * items_per_chunk:
                i * items_per_chunk + items_per_chunk
            ]
            i += 1


def obj_to_json(obj):
    try:
        return mark_safe(json.dumps(obj))
    except:
        return mark_safe(obj)

@register.filter(name='json')
def to_json(value, fields=''):
    if isinstance(value, (dict, list)):
        return obj_to_json(value)
    _type = None
    if hasattr(value, 'model') and hasattr(value, 'query'):
        # queryset
        _fields = value.model._meta.get_fields()
        data = []
        only_fields = []
        if fields:
            only_fields = fields.split(',')
        for v in value:
            obj = {}
            for f in _fields:
                if only_fields and f.name not in only_fields:
                    continue
                vv = getattr(v, f.name)
                obj[f.name] = vv
            data.append(obj)
    else:
        try:
            _fields = value._meta.get_fields()
            _type = 'model'
        except:
            return obj_to_json(value)
        data = {}
        only_fields = []
        if fields:
            only_fields = fields.split(',')
        for f in _fields:
            if only_fields and f.name not in only_fields:
                continue
            v = getattr(value, f.name)
            data[f.name] = v
    return obj_to_json(data)


@register.filter(expects_localtime=True, is_safe=False)
def date(value, arg=None):
    # замена рагульному фильтру из джанги, который отказывается понимать строки как даты
    # взято из django/template/defaultfilters.py
    if value in (None, ''):
        return ''
    if not isinstance(value, (_datetime.datetime, _datetime.date)):
        # парсим с дату
        if 'T' in value:
            value = str_to_datetime(value)
        else:
            value = str_to_date(value)
    try:
        return formats.date_format(value, arg)
    except AttributeError:
        try:
            return format(value, arg)
        except AttributeError:
            return ''

@register.filter(is_safe=True)
def human_currency(value, currencySymbol, fractionSize=None):
    value = float(value or 0)
    if value == int(value):
        if fractionSize is None:
            fractionSize = 0
    if fractionSize is None:
        fractionSize = 2
    if currencySymbol == 'CZK':
        currencySymbol = 'Kč'
    if currencySymbol == 'USD':
        currencySymbol = '$'
    if currencySymbol == 'EUR':
        currencySymbol = '€'
    if currencySymbol:
        return floatformat(value, fractionSize) +' '+ currencySymbol
    return floatformat(value, fractionSize)


@register.filter(name='lookup')
def lookup(value, arg):
    if not value:
        return ''
    if isinstance(value, dict):
        return value.get(arg, '')
    if isinstance(arg, int) and isinstance(value, (list, tuple)):
        return value[arg]
    if hasattr(value, arg):
        return getattr(value, arg, '')
    return ''



@register.simple_tag
def filter_array(array, property=None, operator="=", values=[]):
    if not array or not property or operator != '=':
        return []
    if not values:
        return []
    if isinstance(values, str):
        values = values.split(',')

    def comparator(val):
        if not val or not isinstance(val, dict) or property not in val:
            return False
        return val[property] in values

    return [
        v
        for v in array if comparator(v)
    ]


@register.simple_tag
def disable_email_trim():
    return mark_safe(
        """<div style="color: transparent; visibility: hidden; display: none; width: 0; height: 0; background: transparent; font-size: 0.1px;">#%s</div>""" %
        random.randint(0, 10000000)
    )






register.tag(name='uic_block_to_img')(uic_block_to_img)

register.inclusion_tag('drf_lbcms/tags/langs_list_for_head.html', takes_context=True, name='langs_list_for_head')(
    ui_lang_picker_for_navbar
)

register.inclusion_tag('drf_lbcms/tags/uic_navbar_nav_item.html', takes_context=True)(
    uic_navbar_nav_item
)

register.inclusion_tag('drf_lbcms/tags/uic_navbar_nav_item_mobile.html', takes_context=True, name='uic_navbar_nav_item_mobile')(
    uic_navbar_nav_item
)

register.inclusion_tag('drf_lbcms/tags/uic_navbar_nav.html', takes_context=True)(
    uic_navbar_nav
)

register.inclusion_tag('drf_lbcms/tags/uic_navbar_nav_mobile.html', takes_context=True, name='uic_navbar_nav_mobile')(
    uic_navbar_nav
)

register.inclusion_tag('drf_lbcms/tags/uic_picture.html', takes_context=True)(
    uic_picture
)

register.inclusion_tag('drf_lbcms/tags/uic_file_download.html', takes_context=True)(
    uic_file_download
)

register.simple_tag(takes_context=True)(l10nfile_to_url)


register.inclusion_tag('drf_lbcms/tags/ui_lang_picker_for_navbar.html', takes_context=True)(
    ui_lang_picker_for_navbar
)

register.inclusion_tag('drf_lbcms/tags/ui_langs_list.html', takes_context=True, name='ui_langs_list')(
    ui_lang_picker_for_navbar
)

#
@register.simple_tag(takes_context=True, name='country_code_to_text')
def country_code_to_text(context, code):
    from drf_lbcms.helpers.countries import country_code_to_text as cctt
    return cctt(code, force_lang=get_current_lang(context))


register.simple_tag(takes_context=True, name='qr_code')(
    qr_code
)

register.inclusion_tag('drf_lbcms/tags/gmap_static_map_marker.html')(
    gmap_static.gmap_static_map_marker
)

register.inclusion_tag('drf_lbcms/tags/uic_contact.html',takes_context=True)(
    uic_contact
)
register.inclusion_tag('drf_lbcms/tags/uic_site_brand_a.html', takes_context=True)(
    uic_site_brand_a
)

register.simple_tag(takes_context=True)(uic_card_link)

register.simple_tag(takes_context=True)(site_logo_img_url)
register.simple_tag(takes_context=True)(site_favicon_img_url)
register.simple_tag(takes_context=True)(site_page_title)
register.simple_tag(takes_context=True)(site_page_description)
