#from django.contrib.auth import get_user_model
import logging
from django.conf import settings
try:
    from uwsgidecorators import get_free_signal
    import uwsgi
except:
    get_free_signal = lambda: None
    uwsgi = None

logger = logging.getLogger(__name__)




def celery_result(func):
    from .models import CmsCeleryResult
    #UserModel = get_user_model()
    orig_delay = func.delay

    def wrapper_delay(*args, **kwargs):
        result = CmsCeleryResult.objects.create(
            ownerId=kwargs.get('owner_id', 0) or 0,
            params={
                'args': args,
                'kwargs': kwargs
            }
        )
        args = [result.id] + list(args)
        orig_delay(*args, **kwargs)
        return result.id

    func.delay = wrapper_delay
    return func






class call_with_timeout(object):
    def __init__(self, timeout=None, **kwargs):
        timeout = timeout or 0
        if timeout < 0:
            timeout = 0

        self.num = kwargs.get('signum', get_free_signal())
        self.secs = timeout
        self.target = kwargs.get('target', '')

    def __call__(self, fn):
        self.is_allowed_call = False
        if uwsgi:
            def wrapper(*args, **kwargs):
                if not self.is_allowed_call:
                    self.is_allowed_call = True
                    return
                #if uwsgi and len(args):
                #    args = args[1:]
                logger.warning("\033[93mcall_with_timeout:\033[0m execute {function}".format(function=fn))
                fn()
            if self.secs:
                uwsgi.register_signal(self.num, self.target, wrapper)
                # uwsgi.add_rb_timer(signum, seconds[, iterations=0])
                uwsgi.add_rb_timer(self.num, self.secs, 1)
            else:
                self.is_allowed_call = True
            return wrapper

        # fallback
        logger.error("Can't import uwsgi module. Function call_with_timeout will run without timeout")

        def wrapper(*args, **kwargs):
            logger.warning("\033[93mcall_with_timeout:\033[0m execute {function}".format(function=fn))
            fn()

        return wrapper



if getattr(settings, 'CELERY_BROKER_URL', None) == 'drf-lbcms://uwsgi-spooler':
    try:
        from uwsgidecorators import spool
    except:
        spool = None

    def shared_task(orig_fn):
        # https://pythonise.com/series/learning-flask/exploring-uwsgi-decorators
        if spool:
            fn = spool(pass_arguments=True)(orig_fn)
        else:
            logger.warning("\033[93mshared_task:\033[0m can't import spool from uwsgidecorators")

        def run_with_spooler(*args, **kwargs):
            if spool:
                logger.warning("\033[93mshared_task:\033[0m execute async with uwsgi spooler {function}, args = {args}, kwargs = {kwargs}".format(
                    function=orig_fn,
                    args=args,
                    kwargs=kwargs
                ))
                return fn.spool(*args, **kwargs)
            logger.warning("\033[93mshared_task:\033[0m execute async without uwsgi spooler {function}, args = {args}, kwargs = {kwargs}".format(
                function=orig_fn,
                args=args,
                kwargs=kwargs
            ))
            return orig_fn(*args, **kwargs)

        def wrapper(*args, **kwargs):
            return orig_fn(*args, **kwargs)
        wrapper.delay = run_with_spooler
        wrapper.apply_async = run_with_spooler
        return wrapper

else:
    try:
        from celery import shared_task
    except:
        def shared_task(fn):
            def install_celery_exception(*args, **kwargs):
                raise Exception('drf_lbcms: Install celery!')
            def wrapper(*args, **kwargs):
                return fn(*args, **kwargs)
            wrapper.delay = install_celery_exception
            wrapper.apply_async = install_celery_exception
            return wrapper
