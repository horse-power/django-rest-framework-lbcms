from drfs import routers
from drfs import generate_viewset
from .views import HelpersView
from .rest.urls import urlpatterns as rest_urlpatterns

router = routers.SimpleRouter()
router.register(r'Helpers', HelpersView, base_name='Helpers')


app_name = 'drf_lbcms'
urlpatterns =  router.urls + rest_urlpatterns
