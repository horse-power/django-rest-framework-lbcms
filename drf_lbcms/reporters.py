import os, django
from pathlib import Path
from django.views.debug import ExceptionReporter
from django.template import Context, Engine
from django.conf import settings

CURRENT_DIR = os.path.dirname(__file__)
DEBUG_ENGINE = Engine(
    debug=True,
    libraries={'i18n': 'django.templatetags.i18n'},
)


class DrfLbcmsExceptionReporter(ExceptionReporter):
    if django.VERSION >= (3,2,0):
        html_template_path = Path(__file__).parent / 'templates' / 'technical_500.html'
        text_template_path = Path(__file__).parent / 'templates' / 'technical_500.txt'
    else:
        html_template_path = os.path.join(CURRENT_DIR, 'templates', 'technical_500.html')
        text_template_path = os.path.join(CURRENT_DIR, 'templates', 'technical_500.txt')

    def get_traceback_html(self):
        """Return HTML version of debug 500 HTTP error page."""
        with open(self.html_template_path) as f:
            t = DEBUG_ENGINE.from_string(f.read())
        context = self.get_traceback_data()
        context['PLATFORM'] = settings.PLATFORM
        c = Context(context, use_l10n=False)
        return t.render(c)
