import polib
import drfs, json, os
from django.test import TestCase
from drf_lbcms.helpers import gettext

BASE_DIR = os.path.dirname(__file__)
RESOURCE_DIR = os.path.join(BASE_DIR, 'resources')



def read_json(path):
    with open(os.path.join(RESOURCE_DIR, path)) as f:
        data = json.load(f)
    return data


class GettextTest(TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_pot_to_cmstranslations(self):
        res = gettext.pot_to_json([
            os.path.join(RESOURCE_DIR, 'po/**/*.pot'),
            os.path.join(RESOURCE_DIR, 'po/*.pot'),
        ])
        expectedRes = read_json('po/res.json')

        self.assertDictEqual(
            res,
            expectedRes
        )

        res2 = {'msgs': []}
        for p in ['po/some_dir/template.pot', 'po/template.pot',]:
            with open(os.path.join(RESOURCE_DIR, p)) as f:
                catalog = polib._pofile_or_mofile(f.read(), 'pofile')
                r = gettext.pot_file_to_json(catalog)
                res2['msgs'] += r['msgs']

        self.assertDictEqual(
            res2,
            expectedRes
        )


    def test_merge(self):
        _original = read_json('translations/originalMsgs.json')
        _new = read_json('translations/newMsgs.json')
        _merged = read_json('translations/mergedMsgs.json')

        res = gettext.merge_translation_msgs(_original['msgs'], _new['msgs'])
        self.assertListEqual(
            sorted(res, key=lambda x: x['msgId']),
            sorted(_merged, key=lambda x: x['msgId']),
        )
