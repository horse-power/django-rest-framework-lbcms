# -*- coding: utf-8 -*-
from django.test import TestCase
from drf_lbcms.management.commands.drf_lbcms_dump_db_and_files import Command as DumpCommand
from drf_lbcms.management.commands.drf_lbcms_restore_db_and_files import Command as RestoreCommand
from drf_lbcms.helpers import fs as fs_helpers
from .models import TestModel

class Test(TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_dump(self):
        c = DumpCommand()
        filepath = c.handle('db', 'files', archive_name='dump.7z')
        r = fs_helpers.run_shell_command("7z l -ba {filepath}".format(filepath=filepath), shell=True)

        # всеряемся, что все нужные файлы на месте
        required_files = ['db.json', 'dumpSettings.json', 'files/.gitignore']
        required_dirs = ['files']
        for line in r.out.split('\n'):
            if ' D....' in line:
                for d in required_dirs:
                    if d in line:
                        required_dirs.remove(d)
            if '....A' in line:
                for f in required_files:
                    if f in line:
                        required_files.remove(f)

        self.assertEqual(required_dirs, [])
        self.assertEqual(required_files, [])


    def test_restore(self):
        # проверяем, что до импорта таблица пуста
        self.assertEqual(TestModel.objects.all().count(), 0)

        # восстанавливаем из архива
        c = RestoreCommand()
        c.handle('./tests/resources/dump.7z')

        # проверяем, что появилась новая запись в бд
        self.assertEqual(TestModel.objects.all().count(), 1)
        obj = TestModel.objects.all()[0]
        self.assertEqual(
            obj.string_field,
            'imported from dump'
        )
