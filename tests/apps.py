import sys
from django.apps import AppConfig as AppConfigBase




class AppConfig(AppConfigBase):
    name = 'tests'
    def ready(self):
        if sys.argv != ['uwsgi']:
            return

        from drf_lbcms.decorators import call_with_timeout
        from .tasks import simple

        print(simple.delay("Hello?"))

        @call_with_timeout(timeout=3)
        def special(*args, **kwargs):
            print("SPECIAL", args, kwargs)

        print("RUN SPECIAL")
        special()
