from drf_lbcms.tasks import shared_task



@shared_task
def simple(*args, **kwargs):
    print("\033[92m{text}\033[0m".format(text="uwsgi spooler is alive in tests/tasks.py =) Args/Kwargs:"), args, kwargs)
    return "alive", {'args': args, 'kwargs': kwargs}
