# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core import mail
from rest_framework import exceptions
from rest_framework.test import APIRequestFactory
import json, os

from drfs import generate_viewset, generate_model

from drf_lbcms.models import CmsPdfData, CmsMailNotification
from drf_lbcms.pdf_render.render import PdfRender
from tests.models import TestModelForCmsPdfData, CmsSettings, CmsUser, FakeL10nFile




class TestPdf(TestCase):
    def setUp(self):
        self.cmi = CmsMailNotification.objects.create(
            identifier='test.identifierForEmailPdf',
            emailTemplateName='BlueWave',
            to=['test@cmi.com'],
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "Test subject for pdf",
                'ru': u"Тестовая тема"
            },
            body={
                'en': "Test body for pdf",
                'ru': u"Тестовое тело для pdf"
            }
        )
        self.user = CmsUser.objects.create(
            username='test_user',
            email='test_user_for_pdf@mail.com'
        )
        TestModelForCmsPdfData.objects.create()
        CmsSettings.objects.create()

    '''
    def test_simple(self):
        pdf_data = CmsPdfData(
            title={
                'en': 'Some title for file'
            },
            content={
                'en': """
                    <h1 class='text-center'>{{string_from_context}}</h1>
                    <p>regular string</p>
                """
            }
        )

        context = {
            'string_from_context': 'Hello world!'
        }
        renderer = PdfRender(context=context, pdf_data=pdf_data)
        renderer.render_to_file('./simple.pdf')
        #print(renderer.render_to_buffer())


    def test_mixin(self):
        CmsPdfData.objects.create(
            title={
                'en': 'Some title for file with identifier'
            },
            identifier='my.id',
            content={
                'en': """
                    <h1 class='text-center'>{{string_from_context}}</h1>
                    <p>regular string for CmsPdfData with identifier</p>
                """
            }
        )
        request = APIRequestFactory().get('')
        viewset = generate_viewset(generate_model('TestModelForCmsPdfData.json'))
        renderPdf = viewset.as_view({'get': 'render_pdf'})

        response = renderPdf(request, pk=1, identifier="my.id")
        #print(response.data)


        # email with pdf file
        outbox = len(mail.outbox)
        request = APIRequestFactory().get('?emailIdentifier=test.identifierForEmailPdf&fileName=myFFile.pdf')
        request._user = self.user
        viewset = generate_viewset(generate_model('TestModelForCmsPdfData.json'))
        renderPdf = viewset.as_view({'get': 'render_pdf_to_user_email'})
        response = renderPdf(request, pk=1, identifier="my.id")

        self.assertEqual(response.data, None)
        self.assertEqual(len(mail.outbox), outbox+1)
        self.assertIn(
            'Test body for pdf',
            mail.outbox[-1].body
        )
        self.assertEqual(
            mail.outbox[-1].attachments[0][0],
            'myFFile.pdf'
        )
    '''

    def test_image(self):
        pdf_data = CmsPdfData.objects.create(
            title={
                'en': 'Some title for file with identifier'
            },
            identifier='my.idWithImage',
            content={
                'en': """
                    <h1 class='text-center'>{{string_from_context}}</h1>
                    <p>image from media root</p>
                    <img src="/storage/38da8b62-7c5c-11e8-a3d5-a7474210c913.png" />
                    <p>image from database - not found (tag)</p>
                    {% uic_picture fakeImg %}
                    <p>image from web</p>
                    <img src="https://www.python.org/static/community_logos/python-logo-master-v3-TM.png" />
                    <p>qr code</p>
                    {% qr_code 'Hello world!' size='3'%}
                """
            },
            runningTitles={
                'isHeaderActive': True,
                'isFooterActive': True,
                'headerHeight': 10,
                'footerHeight': 10,
                'header': {
                    'en': "<h3>This is header <b style='color: red;'>COPY {{ COPY_INDEX }}</b></h3>"
                },
                'footer': {
                    'en': "<b>This is footer</b>"
                }
            }
        )

        fake_img = FakeL10nFile()
        fake_img.file_data = '38da8b62-7c5c-11e8-a3d5-a7474210c913.png'
        context = {
            'string_from_context': 'Hi image!',
            'fakeImg': fake_img
        }
        renderer = PdfRender(context=context, pdf_data=pdf_data)
        renderer.render_to_file('./with-image.pdf')


        # рендерим несколько объединенных файлов
        renderer = PdfRender(context=context, pdf_data=pdf_data)
        renderer.render_to_file('./with-image-2.pdf', copy_count=3)
