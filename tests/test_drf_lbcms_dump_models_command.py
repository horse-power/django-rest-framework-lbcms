# -*- coding: utf-8 -*-
from django.test import TestCase
from drf_lbcms.management.commands.drf_lbcms_dump_models import Command


class Test(TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_command(self):
        c = Command()
        models = c.handle(return_json=True)
        try:
            from rest_framework.authtoken.models import TokenProxy
        except ImportError:
            TokenProxy = False

        model_names = [
            "CmsArticle",
            "CmsCategoryForArticle",
            "CmsCategoryForPictureAlbum",
            "CmsCeleryResult",
            "CmsCurrencyData",
            "CmsGdprRequest",
            "CmsKeyword",
            "CmsMailNotification",
            "CmsPdfData",
            "CmsPictureAlbum",
            "CmsSettings",
            "CmsSiteReview",
            "CmsSmsNotification",
            "CmsSomeModel",
            "CmsTranslations",
            "CmsUser",
            "ContentType",
            "Group",
            "L10nFile",
            "L10nFileLocal",
            "Permission",
            "Session",
            "Site",
            "TestModel",
            "TestModelForCmsPdfData",
            "TestModelL10nFile",
            "TestModelL10nFileList",
            "TestModelL10nFileLocal",
            "Token",
        ]
        if TokenProxy:
            model_names.append('TokenProxy')

        self.assertDictEqual(
            models, {
                "models": model_names,
                "embeddedModels": [
                    "L10nBaseString",
                    "L10nUrl",
                    "SiteRedirection",
                    "VariableDescription"
                ]
        })
