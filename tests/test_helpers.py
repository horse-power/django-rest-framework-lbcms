# -*- coding: utf-8 -*-
from django.test import TestCase
from django.conf import settings
from rest_framework import exceptions
import json, os
from drf_lbcms.helpers import download_file
from drf_lbcms.helpers import response
from drf_lbcms.helpers.l10n import django_choices_field_to_text
from datetime import datetime
import csv
import xlwt
import xlrd
import six
from io import StringIO

from .models import CmsSomeModel



class TestHelpers(TestCase):
    def test_download_file(self):
        url = "https://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&language=en&key=AIzaSyDzv9YYZYaZtyXcY5PIresdMErzCSXVgUY&size=500x500&markers=color:blue|label:1|48.464717,35.046183&markers=color:red|label:2|50.4501,30.5234"
        ffile, meta = download_file(url)
        self.assertEqual(
            meta['originalName'],
            'staticmap.png'
        )
        self.assertEqual(
            meta['type'],
            'image/png'
        )
        self.assertTrue(len(ffile) > 0)

    def test_csv_to_response(self):
        now_dt = datetime.now()
        now_d = datetime.now().date()
        data = [
            [1,3,None, 'hello'],
            [192, -1.2, 0.28821, None],
            [now_d, now_dt, True, False]
        ]
        def test_rows(rows, file_type='csv'):
            # None
            self.assertEqual(rows[0][2],'')
            # float
            if file_type == 'csv':
                self.assertEqual(rows[1][2], '0.29')
            else:
                self.assertEqual(rows[1][2], 0.29)
            # date
            self.assertEqual(rows[2][0], now_dt.strftime('%d/%m/%Y'))
            # datetime
            self.assertEqual(rows[2][1], now_dt.strftime('%d/%m/%Y, %H:%M'))
            # custom formatter for bool
            self.assertEqual(rows[2][2], 'Yes')
            self.assertEqual(rows[2][3], 'No')

        # CSV
        r = response.csv_to_response(
            file_name='test.csv',
            rows_data=data,
            formatters={
                'bool': lambda x: 'Yes' if x else 'No'
            }
        )
        if six.PY2:
            generated = u''.join(r._container)
            reader = csv.reader(StringIO(generated))
        else:
            generated = b''.join(r._container)
            reader = csv.reader(StringIO(generated.decode("utf-8")))
        test_rows(list(reader), file_type='csv')

        # XLS
        r = response.csv_to_response(
            file_name='test.xls',
            rows_data=data,
            formatters={
                'bool': lambda x: 'Yes' if x else 'No'
            }
        )
        if six.PY2:
            generated = ''.join(r._container)
        else:
            generated = b''.join(r._container)
        book = xlrd.open_workbook(file_contents=generated)
        sheet = book.sheet_by_index(0)
        rows = []
        for r in range(0, sheet.nrows):
            rows.append(sheet.row_values(r))
        test_rows(rows, file_type='xls')

    def test_django_choices_field_to_text(self):
        test = CmsSomeModel.objects.create()
        field = None
        for f in CmsSomeModel._meta.fields:
            if f.name == 'choices_field':
                field = f
                break
        value = django_choices_field_to_text(
            field,
            test.choices_field
        )
        self.assertEqual(value, u'Новый')
