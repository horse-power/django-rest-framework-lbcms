# -*- coding: utf-8 -*-
from django.test import TestCase
import json, os, pytz
from datetime import datetime


from drf_lbcms.models import CmsSmsNotification
from drf_lbcms.helpers import sms

from .models import CmsSettings, CmsUser



class TestHelpers(TestCase):
    def setUp(self):
        sms.outbox = []
        self.csm = CmsSmsNotification.objects.create(
            identifier='test.identifier',
            to=['+1111111111'],
            send=True,
            body={
                'en': "Test body {{phone}}",
                'ru': u"Тестовое тело {{phone}}"
            }
        )
        self.user = CmsUser.objects.create(
            username='test_user',
            email='test_user@mail.com',
            preferredLang='en'
        )
        self.user.accountData = {
            'phones':[
                {
                    'value': '+2222222',
                    'type': 'm'
                }
            ]
        }
        self.settings = CmsSettings.objects.create(
            siteTitle={
                'en': 'Test-site-name'
            },
            serverIntegrations={
                'useSmsNotifications': True,
                'useEmailNotifications': True,
                'smsapicom':{
                    'username': 'test',
                    'password': 'test',
                    'senderName': 'Jack'
                }
            }
        )

    def test_send_sms(self):
        self.assertEqual(len(sms.outbox), 0)
        self.csm.send_user_notification(self.user)
        self.assertEqual(len(sms.outbox), 1)

        self.assertEqual(
            sms.outbox[0].from_phone,
            'Jack'
        )
        self.assertEqual(
            sms.outbox[0].to[0],
            '+2222222'
        )

        self.assertIn(
            'Test body +2222222',
            sms.outbox[0].body
        )




        # проверка поддержки часовых поясов
        self.csm.body['ru'] = '{{date | date:"Y-m-d H:i:s"}}'
        dt = datetime.now()
        dt = dt.replace(tzinfo=pytz.timezone('UTC'))
        kiev_dt = dt.astimezone(pytz.timezone('Europe/Kiev'))
        prague_dt = dt.astimezone(pytz.timezone('Europe/Prague'))

        self.assertNotEqual(
            str(dt),
            str(kiev_dt)
        )

        # kiev
        self.user.timezone = 'Europe/Kiev'
        self.csm.send_user_notification(self.user, preferred_lang='ru', context={'date': dt})

        self.assertEqual(len(sms.outbox), 2)
        self.assertEqual(
            sms.outbox[-1].from_phone,
            'Jack'
        )
        self.assertEqual(
            sms.outbox[-1].to[0],
            '+2222222'
        )

        self.assertIn(
            kiev_dt.strftime("%Y-%m-%d %H:%M:%S"),
            sms.outbox[-1].body
        )
        # prague
        self.user.timezone = 'Europe/Prague'
        self.csm.send_user_notification(self.user, preferred_lang='ru', context={'date': dt})

        self.assertEqual(len(sms.outbox), 3)
        self.assertEqual(
            sms.outbox[-1].from_phone,
            'Jack'
        )
        self.assertEqual(
            sms.outbox[-1].to[0],
            '+2222222'
        )

        self.assertIn(
            prague_dt.strftime("%Y-%m-%d %H:%M:%S"),
            sms.outbox[-1].body
        )
