import django, sys, os
from django.conf import settings

BASE_DIR = os.path.dirname(__file__)
SECRET_KEY = '--'
ALLOWED_HOSTS = ['*']
APP_CONTEXT = {
    'CMS_DB_BUCKET': 'test_bucket',
    'CMS_STORAGE': {
        'type': 'google',
        'server': 'https://storage.googleapis.com'
    },
    'CMS_DEFAULT_LANG': 'en',
    'CMS_ALLOWED_LANGS': ['ru', 'en']
}

AUTH_USER_MODEL = 'tests.CmsUser'

DEBUG=True
DATABASES={
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
    }
}
PLATFORM='prod'
ROOT_URLCONF='tests.urls'
INSTALLED_APPS=[
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'widget_tweaks',
    'drf_ng_generator',
    'drf_loopback_js_filters',
    'rest_framework',
    'rest_framework.authtoken',
    'drfs',
    'django_gcs',

    'drf_lbcms',
    'lbcms',
    'tests'
]
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(os.path.dirname(__file__), 'db.sqlite3'),
    }
}
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'builtins': ['drf_lbcms.templatetags.base_lbcms']
        }
    }
]
LANGUAGES = [
    ['en', 'en'],
    ['ru', 'ru']
]
#USE_TZ = True
TIME_ZONE = 'UTC'

MEDIA_ROOT=os.path.join(BASE_DIR, 'storage-tests')
if sys.argv == ['uwsgi']:
    pass
elif sys.argv[1] == 'makemigrations':
    MEDIA_ROOT = '/media_data'
MEDIA_URL='storage/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'resources')
]
STATIC_ROOT = os.path.join(BASE_DIR, 'static-root')
STATIC_URL = '/static/'


#DJANGO_GCS_BUCKET='ffffaaaaaaake-bucket'
#DJANGO_GCS_PRIVATE_KEY_PATH=os.path.join(BASE_DIR, 'key.json')
#DJANGO_GCS_BLOB_MAKE_PUBLIC=True
#DEFAULT_FILE_STORAGE='django_gcs.storage.GoogleCloudStorage'

DJANGO_GC_MAP_API_KEY = 'AIzaSyDzv9YYZYaZtyXcY5PIresdMErzCSXVgUY'


EMAIL_BACKEND = "sgbackend.SendGridBackend"
SENDGRID_API_KEY = "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"

DRF_LBCMS = {
    'models_for_sitemap': ['CmsSomeModel', 'TestModel'],
    'use_wkhtml': True
}

CELERY_BROKER_URL = 'drf-lbcms://uwsgi-spooler'
