# -*- coding: utf-8 -*-
from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
from datetime import datetime
import json, os, six

from drf_lbcms.models import CmsMailNotification
from .models import CmsUser
from drf_lbcms.views import ResetPasswordAndPhonesView




class TestReset(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        CmsMailNotification.objects.create(
            identifier='cms.user.$resetPasswordr',
            emailTemplateName='BlueWave',
            to=[],
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "Link for password reset",
            },
            body={
                'en': "Use link {{resetPasswordHref}}",
            }
        )
        self.user = CmsUser.objects.create(
            username='test_user',
            email='test@mail.com'
        )
        self.user.set_password('1')
        self.user.save()


    def test_reset_password(self):
        class RPView(ResetPasswordAndPhonesView):
            template_name = 'drf_lbcms/reset_password/form.html'
            dead_link_url = '/dead'
            success_url = '/ok'

        view = RPView.as_view()

        # отправляем запрос, который точно не будет обработан
        request = self.factory.get('')
        request.user = self.user
        response = view(request)
        self.assertEqual(response.url, '/dead')

        # отправляем запрос с валидной ссылкой
        self.user.hiddenAccountData['resetPassword'] = datetime.now().isoformat()
        self.user.save()
        from rest_framework.authtoken.models import Token
        token, created = Token.objects.get_or_create(user=self.user)

        request = self.factory.get('/?access_token={token}&username={username}&control={control}'.format(
            token=str(token),
            username=self.user.username,
            control=self.user.hiddenAccountData['resetPassword']
        ))
        request.user = self.user
        response = view(request)
        response.render()

        content = response.content
        if six.PY2:
            content = response.content
        else:
            content = response.content.decode('utf-8')
        html = content.split('<fieldset>')[-1].split('</fieldset>')[0]
        self.assertTrue(
            'type="password" name="new_password1" required class="form-control" id="id_new_password1"' in html or \
            'type="password" name="new_password1" autocomplete="new-password" class="form-control" required id="id_new_password1"' in html or \
            'type="password" name="new_password1" class="form-control" required id="id_new_password1"' in html
        )
        self.assertTrue(
            'type="password" name="new_password2" required class="form-control" id="id_new_password2"' in html or \
            'type="password" name="new_password2" autocomplete="new-password" class="form-control" required id="id_new_password2"' in html or \
            'type="password" name="new_password2" class="form-control" required id="id_new_password2"' in html
        )
        self.assertNotIn(
            'type="text" name="phones"',
            html
        )

        # теперь отсылаем запрос с неравными паролями
        request = self.factory.post('/?access_token={token}&username={username}&control={control}'.format(
            token=str(token),
            username=self.user.username,
            control=self.user.hiddenAccountData['resetPassword']
        ), {'new_password1': '1', 'new_password2': '2'})
        request.user = self.user
        response = view(request)
        response.render()

        content = response.content
        if six.PY2:
            content = response.content
        else:
            content = response.content.decode('utf-8')
        html = content.split('<fieldset>')[-1].split('</fieldset>')[0]

        from drf_lbcms.forms import ResetPasswordForm

        self.assertTrue(
            str(ResetPasswordForm.error_messages['password_mismatch']) in html or \
            str(ResetPasswordForm.error_messages['password_mismatch']).replace("'", "&#39;") in html
        )

        self.assertFalse(self.user.check_password('new'))
        # теперь заменяем пароли
        request = self.factory.post('/?access_token={token}&username={username}&control={control}'.format(
            token=str(token),
            username=self.user.username,
            control=self.user.hiddenAccountData['resetPassword']
        ), {'new_password1': 'new', 'new_password2': 'new'})
        request.user = self.user
        response = view(request)
        self.assertEqual(response.url, '/ok')
        # сверяемся, что больше по этому url мы не зайдем:
        self.user.refresh_from_db()
        self.assertEqual(
            self.user.hiddenAccountData.get('resetPassword', ''),
            ''
        )
        # сверяемся, что пароль обновлен
        self.assertTrue(self.user.check_password('new'))


        # теперь мы делаем смену пароля и телефонов
        self.user.hiddenAccountData['resetPassword'] = datetime.now().isoformat()
        self.user.loginProtection = 's'
        self.user.accountData['phones'] = [{
            'type': 'm',
            'value': '000000'
        },{
            'type': 'a',
            'value': '1111111'
        }]
        self.user.save()
        from rest_framework.authtoken.models import Token
        token, created = Token.objects.get_or_create(user=self.user)

        request = self.factory.get('/?access_token={token}&username={username}&control={control}'.format(
            token=str(token),
            username=self.user.username,
            control=self.user.hiddenAccountData['resetPassword']
        ))
        request.user = self.user
        response = view(request)
        response.render()

        html = str(response.content).split('<fieldset>')[-1].split('</fieldset>')[0]
        self.assertTrue(
            'type="password" name="new_password1" required class="form-control" id="id_new_password1"' in html or \
            'type="password" name="new_password1" autocomplete="new-password" class="form-control" required id="id_new_password1"' in html or \
            'type="password" name="new_password1" class="form-control" required id="id_new_password1"' in html
        )
        self.assertTrue(
            'type="password" name="new_password2" required class="form-control" id="id_new_password2"' in html or \
            'type="password" name="new_password2" autocomplete="new-password" class="form-control" required id="id_new_password2"' in html or \
            'type="password" name="new_password2" class="form-control" required id="id_new_password2"' in html
        )
        self.assertIn(
            'type="text" name="phones"',
            html
        )

        # отсылаем форму с новыми основными номерами
        request = self.factory.post('/?access_token={token}&username={username}&control={control}'.format(
            token=str(token),
            username=self.user.username,
            control=self.user.hiddenAccountData['resetPassword']
        ), {'new_password1': 'new2', 'new_password2': 'new2', 'phones': "233456, 456787"})
        request.user = self.user
        response = view(request)
        self.assertEqual(response.url, '/ok')

        # сверяемся, что больше по этому url мы не зайдем:
        self.user.refresh_from_db()
        self.assertEqual(
            self.user.hiddenAccountData.get('resetPassword', ''),
            ''
        )
        # сверяемся, что пароль обновлен
        self.assertTrue(self.user.check_password('new2'))
        # сверяемся, что номера телефонов обновлены
        self.assertEqual(
            self.user.accountData['phones'], [
                {u'type': u'm', u'value': u'233456'},
                {u'type': u'm', u'value': u'456787'},
                {u'type': u'a', u'value': u'1111111'}
            ]
        )
