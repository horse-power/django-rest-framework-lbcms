# -*- coding: utf-8 -*-
from django.test import TestCase
from django.conf import settings
from rest_framework import exceptions
from drf_lbcms.helpers.tests import APIRequestFactoryWrapper
from drfs import generate_viewset
from .models import CmsUser
import json, os



class TestValidate(TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_valid(self):
        from drf_lbcms.helpers.templates import validate_template

        isValid, errors = validate_template("Hello {{name}}!")
        self.assertTrue(isValid)
        self.assertEqual(errors, [])


    def test_invalid(self):
        from drf_lbcms.helpers.templates import validate_template

        isValid, errors = validate_template("""
        Hello {{name}}!
        {% for v in values %}
            {{v}}
        """)
        self.assertFalse(isValid)
        self.assertListEqual(errors, [{
            "type": "parsing",
            "description": {
                "after": "\n",
                "during": "{% for v in values %}",
                "message": "Unclosed tag on line 3: 'for'. Looking for one of: empty, endfor.",
                "total": 6,
                "line": 3,
                "before": "        ",
                "source_lines": [
                    [1, "{% load lbcms %}{% load l10n %}{% load tz %}\n"],
                    [2, "        Hello {{name}}!\n"],
                    [3, "        {% for v in values %}\n"],
                    [4, "            {{v}}\n"],
                    [5, "        "]
                ],
                "end": 98,
                "name": "<unknown source>",
                "bottom": 6,
                "top": 1,
                "start": 77
            }
        }])


        isValid, errors = validate_template("""
        Hello {{name!
        {%
        """)
        self.assertFalse(isValid)
        self.assertListEqual(errors, [
            {
                "type": "general",
                "description": {
                    "message": 'Found impaired {{ and }} ("{{" - 1, "}}" - 0 count)'
                }
            },
            {
                "type": "general",
                "description": {
                    "message": 'Found impaired {% and %} ("{%" - 1, "%}" - 0 count)'
                }
            }
        ])


class TestRestValidators(TestCase):
    def setUp(self):
        self.viewsetMail = generate_viewset('CmsMailNotification.json')
        self.viewsetSms = generate_viewset('CmsSmsNotification.json')
        self.factory = APIRequestFactoryWrapper()
        self.maxDiff = None
        self.admin = CmsUser.objects.create(username='admin', userRole='a')

    def test_mail(self):
        create = self.viewsetMail.as_view({'post': 'create'})
        request = self.factory.post('', data={
            'identifier': 'test',
            'description': {},
            'body': {'ru': "\nHello {% error %}\n Some text", 'en': 'Hello!'},
            'subject': {'cz': "Forgotten {{"}
        }, user=self.admin)
        response = create(request)

        self.assertEqual(response.status_code, 400)

        response_data = json.loads(json.dumps(response.data))
        self.assertDictEqual(response_data, {
            "body": {
                "ru": [
                    {
                        "type": "parsing",
                        "description": {
                            "source_lines": [
                                ["1", "{% load lbcms %}{% load l10n %}{% load tz %}\n"],
                                ["2", "Hello {% error %}\n"],
                                ["3", " Some text"]
                            ],
                            "end": "62",
                            "name": "<unknown source>",
                            "bottom": "4",
                            "top": "1",
                            "after": "\n",
                            "start": "51",
                            "during": "{% error %}",
                            "message": "Invalid block tag on line 2: 'error'. Did you forget to register or load this tag?",
                            "total": "4",
                            "line": "2",
                            "before": "Hello "
                        }
                    }
                ]
            },
            "subject": {
                "cz": [
                    {
                        "type": "general",
                        "description": {
                            "message": 'Found impaired {{ and }} ("{{" - 1, "}}" - 0 count)'
                        }
                    }
                ]
            }
        })


    def test_sms(self):
        create = self.viewsetSms.as_view({'post': 'create'})
        request = self.factory.post('', data={
            'identifier': 'test',
            'description': {},
            'body': {'ru': "\nHello {% error %}\n Some text", 'en': 'Hello!'},
            'subject': {'cz': "Forgotten {{"}
        }, user=self.admin)
        response = create(request)
        self.assertEqual(response.status_code, 400)

        response_data = json.loads(json.dumps(response.data))
        # поля subject у CmsSmsNotification нет
        self.assertDictEqual(response_data, {
            "body": {
                "ru": [
                    {
                        "type": "parsing",
                        "description": {
                            "source_lines": [
                                ["1", "{% load lbcms %}{% load l10n %}{% load tz %}\n"],
                                ["2", "Hello {% error %}\n"],
                                ["3", " Some text"]
                            ],
                            "end": "62",
                            "name": "<unknown source>",
                            "bottom": "4",
                            "top": "1",
                            "after": "\n",
                            "start": "51",
                            "during": "{% error %}",
                            "message": "Invalid block tag on line 2: 'error'. Did you forget to register or load this tag?",
                            "total": "4",
                            "line": "2",
                            "before": "Hello "
                        }
                    }
                ]
            }
        })
