from rest_framework import routers
from django.conf.urls import url, include
from django.views.generic.base import View


urlpatterns = [
    url('^api/v1/', include(('tests.rest.urls', 'app'), namespace='app')),
    url('^blog/list/$', View.as_view(), name='app.blog.list'),
    url(r'^articles/(?P<category_url_id>[\w\-]+)/(?P<article_url_id>[\w\-]+)$', View.as_view(), name='app.articles.details')
]
