# -*- coding: utf-8 -*-
from django.test import TestCase
from django.conf import settings
from rest_framework import exceptions
import json, os


from drf_lbcms.models import CmsMailNotification, CmsSmsNotification
from drf_lbcms.helpers import email
from django.core import mail
from drf_lbcms.helpers import sms
from drf_lbcms.helpers.cms import notify_user

from .models import CmsSettings, CmsSomeModel, CmsUser



class TestNotifyByModelSignal(TestCase):
    def setUp(self):
        CmsSettings.objects.create(
            siteTitle={
                'en': 'Test-site-name'
            },
            serverIntegrations={
                'useSmsNotifications': True,
                'useEmailNotifications': True,
                'smsapicom':{
                    'username': 'test',
                    'password': 'test',
                    'senderName': 'Jack'
                }
            }
        )
        CmsMailNotification.objects.create(
            identifier='cms.someModel.$new',
            emailTemplateName='BlueWave',
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "New"
            },
            body={
                'en': "Object {{instance.id}} created"
            }
        )
        CmsMailNotification.objects.create(
            identifier='cms.someModel.$new=>otherUser',
            emailTemplateName='BlueWave',
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "New for otherUser"
            },
            body={
                'en': "Object {{instance.id}} created"
            }
        )
        CmsMailNotification.objects.create(
            identifier='cms.someModel.$update',
            emailTemplateName='BlueWave',
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "Update"
            },
            body={
                'en': "Object {{instance.id}} updated"
            }
        )
        CmsMailNotification.objects.create(
            identifier='cms.someModel.name.$diff',
            emailTemplateName='BlueWave',
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "Update field 'name'"
            },
            body={
                'en': "Object {{instance.id}} with field 'name' updated"
            }
        )
        CmsMailNotification.objects.create(
            identifier='cms.someModel.$delete',
            emailTemplateName='BlueWave',
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "Delete"
            },
            body={
                'en': "Object {{instance.id}} deleted"
            }
        )
        self.user = CmsUser.objects.create(
            username='test_user',
            email='test_user@mail.com',
            preferredLang='en'
        )
        self.user2 = CmsUser.objects.create(
            username='test_user2',
            email='test_user2@mail.com',
            preferredLang='en'
        )

    def test_new(self):
        self.assertEqual(len(mail.outbox), 0)

        # новый объект, но т.к. поле owner не установлено, то сообщение никому не
        # отправится
        m = CmsSomeModel.objects.create(name="hello")
        self.assertEqual(len(mail.outbox), 0)

        # обновляем объект и устанавливаем поле owner
        m.name = "goodbye"
        m.owner = self.user
        m.save()
        # прийдет 2 письма. один от $update, второй от $diff
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(
            mail.outbox[0].to[0],
            self.user.email
        )
        self.assertEqual(
            mail.outbox[0].subject,
            'Update'
        )
        self.assertIn(
            'Object {instance.id} updated'.format(instance=m),
            mail.outbox[0].body
        )

        self.assertEqual(
            mail.outbox[1].to[0],
            self.user.email
        )
        self.assertEqual(
            mail.outbox[1].subject,
            "Update field 'name'"
        )
        self.assertIn(
            "Object {instance.id} with field 'name' updated".format(instance=m),
            mail.outbox[1].body
        )

        # удаляем объект
        id = m.id
        m.delete()
        self.assertEqual(len(mail.outbox), 3)
        self.assertEqual(
            mail.outbox[-1].to[0],
            self.user.email
        )
        self.assertEqual(
            mail.outbox[-1].subject,
            'Delete'
        )
        self.assertIn(
            'Object {id} deleted'.format(id=id),
            mail.outbox[-1].body
        )


        # устанавливаем otherUser в self.user2 и ожидаем сообщение для него о новом
        # объекте
        m = CmsSomeModel.objects.create(name="hello", otherUser=self.user2)
        self.assertEqual(len(mail.outbox), 4)
        self.assertEqual(
            mail.outbox[-1].to[0],
            self.user2.email
        )
        self.assertEqual(
            mail.outbox[-1].subject,
            'New for otherUser'
        )
        self.assertIn(
            'Object {id} created'.format(id=m.id),
            mail.outbox[-1].body
        )

class TestNotify(TestCase):
    def setUp(self):
        self.cmi = CmsMailNotification.objects.create(
            identifier='test.identifier',
            emailTemplateName='BlueWave',
            to=['test@cmi.com'],
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "Test subject",
                'ru': u"Тестовая тема"
            },
            body={
                'en': "Test body {{email}}",
                'ru': u"Тестовое тело {{email}}"
            }
        )
        self.csm = CmsSmsNotification.objects.create(
            identifier='test.identifier',
            to=['+1111111111'],
            send=True,
            body={
                'en': "Test body {{phone}}",
                'ru': u"Тестовое тело {{phone}}"
            }
        )
        self.user = CmsUser.objects.create(
            username='test_user',
            email='test_user@mail.com',
            preferredLang='en'
        )
        self.user.accountData = {
            'phones':[
                {
                    'value': '+2222222',
                    'type': 'm'
                }
            ]
        }
        self.settings = CmsSettings.objects.create(
            siteTitle={
                'en': 'Test-site-name'
            },
            serverIntegrations={
                'useSmsNotifications': True,
                'useEmailNotifications': True,
                'smsapicom':{
                    'username': 'test',
                    'password': 'test',
                    'senderName': 'Jack'
                }
            }
        )

    def test_notify(self):
        def test_email_data(i):
            self.assertEqual(len(mail.outbox), i+1)
            self.assertEqual(
                mail.outbox[i].subject,
                'Test subject'
            )
            self.assertIn(
                'Test body test_user@mail.com',
                mail.outbox[i].body
            )

        def test_sms_data(i):
            self.assertEqual(len(sms.outbox), SMS_OUTBOX_INITIAL_LEN+i+1)
            self.assertEqual(
                sms.outbox[i].from_phone,
                'Jack'
            )
            self.assertEqual(
                sms.outbox[SMS_OUTBOX_INITIAL_LEN+i].to[0],
                '+2222222'
            )

            self.assertIn(
                'Test body +2222222',
                sms.outbox[SMS_OUTBOX_INITIAL_LEN+i].body
            )

        # проверяем что сообщение ушло, если нет настроек уведомлений
        # ни у сайта ни у пользователя
        SMS_OUTBOX_INITIAL_LEN = len(sms.outbox)
        self.assertEqual(len(mail.outbox), 0)

        notify_user(self.user, context={}, identifier='test.identifier')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(len(sms.outbox), SMS_OUTBOX_INITIAL_LEN+1)
        test_email_data(0)
        test_sms_data(0)

        # отключаем для пользователя возможность получать почтовые сообщения по
        # идентификатору
        self.user.userRole = 'testRole'
        self.settings.userNotifySettings = {
            'email':{
                'testRole': {
                    'enabled': []
                }
            }
        }
        self.settings.save()
        notify_user(self.user, context={}, identifier='test.identifier')
        # смс получил, а почту нет
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(len(sms.outbox), SMS_OUTBOX_INITIAL_LEN+2)
        test_sms_data(1)

        # отключаем для пользователя возможность получать почтовые и смс
        # сообщения идентификатору
        self.settings.userNotifySettings = {
            'email':{
                'testRole': {
                    'enabled': []
                }
            },
            'sms':{
                'testRole':{
                    'enabled': []
                }
            }
        }
        self.settings.save()
        notify_user(self.user, context={}, identifier='test.identifier')
        # ни смс ни почту он не получил
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(len(sms.outbox), SMS_OUTBOX_INITIAL_LEN+2)


        # открываем возможность получить почту по идентификатору для всех пользователей
        self.settings.userNotifySettings = {
            'email':{
                'testRole': {
                    'enabled': ['test.identifier']
                }
            },
            'sms':{
                'testRole':{
                    'enabled': []
                }
            }
        }
        self.settings.save()
        notify_user(self.user, context={}, identifier='test.identifier')
        # получил только почту
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(len(sms.outbox), SMS_OUTBOX_INITIAL_LEN+2)
        test_email_data(1)


        '''
                проверяем настройки пользователя для получения уведомлений
        '''
        # разрешаем все уведомления для сайта, но отключаем в профиле пользователя
        # получение сообщений
        self.settings.userNotifySettings = {
            'email':{
                'testRole': {
                    'enabled': ['test.identifier']
                }
            },
            'sms':{
                'testRole':{
                    'enabled': ['test.identifier']
                }
            }
        }
        self.settings.save()
        self.user.userNotifySettings = {
            'email':{
                'test.identifier': False
            },
            'sms':{
                'test.identifier': False
            }
        }
        notify_user(self.user, context={}, identifier='test.identifier')
        # ничего нового не пришло
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(len(sms.outbox), SMS_OUTBOX_INITIAL_LEN+2)


        # разрешаем все уведомления для сайта, но отключаем в профиле пользователя
        # только почту
        self.settings.userNotifySettings = {
            'email':{
                'testRole': {
                    'enabled': ['test.identifier']
                }
            },
            'sms':{
                'testRole':{
                    'enabled': ['test.identifier']
                }
            }
        }
        self.settings.save()
        self.user.userNotifySettings = {
            'email':{
                'test.identifier': False
            },
            'sms':{
                'test.identifier': True
            }
        }
        notify_user(self.user, context={}, identifier='test.identifier')
        # пришла только смс
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(len(sms.outbox), SMS_OUTBOX_INITIAL_LEN+3)
        test_sms_data(2)


        # разрешаем все уведомления для сайта, но отключаем в профиле пользователя
        # только смс, а настройки для почты вообще удаляем
        self.settings.userNotifySettings = {
            'email':{
                'testRole': {
                    'enabled': ['test.identifier']
                }
            },
            'sms':{
                'testRole':{
                    'enabled': ['test.identifier']
                }
            }
        }
        self.settings.save()
        self.user.userNotifySettings = {
            'sms':{
                'test.identifier': False
            }
        }
        notify_user(self.user, context={}, identifier='test.identifier')
        # пришла только почта
        self.assertEqual(len(mail.outbox), 3)
        self.assertEqual(len(sms.outbox), SMS_OUTBOX_INITIAL_LEN+3)
        test_email_data(2)
