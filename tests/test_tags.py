# -*- coding: utf-8 -*-
from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
from django.template import Context, Template
import json, os, six, bs4

from drf_lbcms.models import CmsTranslations, L10nFile
from lbcms.models import CmsArticle, CmsCategoryForArticle


from .models import CmsSettings, FakeL10nFile

WKHTML_VERSION = os.popen('wkhtmltoimage --version').read().split(' ')[1]




class TestTags(TestCase):
    def setUp(self):
        self.maxDiff = None
        self.factory = RequestFactory()
        self.cmsSettings = CmsSettings.objects.create(
            siteTitle={
                'en': 'Test-site-name'
            },
            publicContacts=[
                {
                    "type": "whatsapp-tel",
                    "value": "+000000000000"
                },
                {
                    "type": "email",
                    "value": "booking@noreply.com"
                }
            ]
        )

    def render_template(self, string, context=None):
        string = "{% load lbcms %}{% load i18n %}" + string
        context = context or {}
        context = Context(context)
        return Template(string).render(context)


    def prettify_html(self, html):
        return bs4.BeautifulSoup(html, "html.parser").prettify()




    def test_uic_contact(self):
        static_url = "https://test-google.com"
        with self.settings(STATICFILES_STORAGE='storages.backends.s3boto3.S3Boto3Storage', STATIC_URL=static_url):
            r = self.render_template(
                "{% uic_contact contact 10 %}\n{% uic_contact contact None 'white' %}\n{% uic_contact contact2 %}\n{% uic_contact contact3 %}",
                {
                    'contact': {'type':"tel", 'value':"+000"},
                    'contact2': {'type': "email", 'value': "s@m.com"},
                    'contact3': {'type': "skype", 'value': "sada"}
                }
            )
        expected = u"""
<a href="tel:+000" style="color:black;">
    <img src="{static_url}/drf_lbcms/fa/fa-phone--black.png?build=None" style="height:10px; vertical-align:middle;"/>
    +000
</a>

<a href="tel:+000" style="color:white;">
    <img src="{static_url}/drf_lbcms/fa/fa-phone--white.png?build=None" style="height:14px; vertical-align:middle;"/>
    +000
</a>

<a href="mailto:s@m.com" style="color:black;">
    <img src="{static_url}/drf_lbcms/fa/fa-envelope-o--black.png?build=None" style="height:14px; vertical-align:middle;"/>
    s@m.com
</a>

<a href="skype:sada?call" style="color:black;">
    <img src="{static_url}/drf_lbcms/fa/fa-skype--black.png?build=None" style="height:14px; vertical-align:middle;"/>
    sada
</a>
""".format(static_url=static_url)

        self.assertEqual(
            r.strip(),
            expected[1:].strip()
        )




    def test_uic_site_brand_a(self):
        static_url = "https://test-google.com"
        with self.settings(STATICFILES_STORAGE='storages.backends.s3boto3.S3Boto3Storage', STATIC_URL=static_url):
            r = self.render_template(
                "{% uic_site_brand_a myHrefVk %}",
                {
                    'myHrefVk': "vk.com/bla-bla"
            })
            expected = """<a href='http://vk.com/bla-bla' target="_blank" style="color:transparent; text-decoration:none;"><img src="{static_url}/drf_lbcms/brand/circle/vkontakte.png?build=None" /></a>""".format(static_url=static_url)
            self.assertEqual(
                r[:-1],
                expected
            )

            r = self.render_template(
                "{% uic_site_brand_a myHrefVk %}",
                {
                    'myHrefVk': "unknown.site/ffff"
            })
            expected = """<a href='http://unknown.site/ffff' target="_blank" style="color:transparent; text-decoration:none;"><img src="{static_url}/drf_lbcms/brand/circle/default.png?build=None" /></a>""".format(static_url=static_url)
            self.assertEqual(
                r[:-1],
                expected
            )



    def test_uic_picture(self):


        l10nFile = FakeL10nFile()
        l10nFile.file_data = "cz-589861e19a0a336d28f3355f.jpg"
        l10nFile.meta_data = {
            "size": 39806,
            "type": "image/jpeg",
            "originalName": "3.jpg",
            "width": 600,
            "height": 600
        }
        l10nFile.thumbs = [
             "70x70",
             "350x350",
             "440x250"
        ]

        # regular
        r = self.render_template(
            "{% uic_picture file %}",
            {
                'file': l10nFile,
                'CMS_DB_BUCKET': 'MY_TEST_BUCKET'
        })
        self.assertEqual(
            r,
            "<img src='https://storage.googleapis.com/lbcms-container-MY_TEST_BUCKET/cz-589861e19a0a336d28f3355f.jpg' style='width: 100%;' class=''/>\n"
        )

        # thumb
        r = self.render_template(
            "{% uic_picture file size='70x70' %}",
            {
                'CMS_DB_BUCKET': 'MY_TEST_BUCKET',
                'file': l10nFile
        })
        self.assertEqual(
            r,
            "<img src='https://storage.googleapis.com/lbcms-container-MY_TEST_BUCKET/cz-589861e19a0a336d28f3355f.jpg.thumb.70x70.jpg' style='width: 100%;' class=''/>\n"
        )


    def test_site_logo_img_url(self):
        r = self.render_template(
            "{% site_logo_img_url %}",
            {
                'CMS_DB_BUCKET': 'MY_TEST_BUCKET'
        })
        self.assertEqual(
            r,
            'https://storage.googleapis.com/lbcms-container-MY_TEST_BUCKET/client/brand/logo.png'
        )

    def test_uic_navbar_nav(self):
        navbarConf = {
            'right': [
                {'type': 'ui-sref', 'data': 'app:cmsuser-list', 'title':{'en': "Index", 'ru': u"Главная", 'ua': u"Головна", 'cz': u"Hlavní" }},
                {'type': 'href', 'data': 'google.com', 'title':{'en': "Some link with auto nofollow", 'ru': u"Ссылка с auto nofollow", 'ua': u"Лiнк з auto nofollow", 'cz': u"Link + auto nofollow"}, 'attrs':{'target': '_blank'}},
                {'type': 'directive', 'data': 'ui-lang-picker-for-navbar'},
                {
                    'type': 'dropdown',
                    'title': {'en': 'Dropdown with items'},
                    'items':[
                        {'type': 'ui-sref', 'data': 'app:cmsuser-list', 'title':{'en': "Index", 'ru': u"Главная", 'ua': u"Головна", 'cz': u"Hlavní" }},
                        {'type': 'href', 'data': 'google.com', 'title':{'en': "Some link with auto nofollow", 'ru': u"Ссылка с auto nofollow", 'ua': u"Лiнк з auto nofollow", 'cz': u"Link + auto nofollow"}, 'attrs':{'target': '_blank'}},
                        {'type': 'action', 'data': 'MyAction', 'title': {'en': 'Hello'}}
                    ]
                }
            ]
        }
        navbarConf = {
            "right": [
                {
                    "type": "contacts",
                    "attrs": {
                        "target": "_blank"
                    },
                    "data": "tel-skype-whatsapp",
                    "html": {},
                    "title": {},
                    "hideForLangs": {
                        "ru": True
                    },
                    "hideForScreenSize": {
                        "xs": True,
                        "lg": False
                    }
                },
                {
                    "title": {
                        "cz": "Náš blog",
                        "ru": "Блог",
                        "en": "Our blog"
                    },
                    "attrs": {},
                    "data": "app.blog.list",
                    "type": "ui-sref",
                    "hideForLangs": {
                        "cz": True,
                        "ru": False,
                        "en": True
                    },
                    "html": {}
                },
                {
                    "type": "ui-sref:app.articles.details",
                    "attrs": {
                        "target": "_blank"
                    },
                    "data": 10,
                    "html": {},
                    "title": {
                        "ru": "О нас",
                        "en": "About us"
                    }
                },
                {
                    "type": "",
                    "attrs": {},
                    "data": "",
                    "html": {},
                    "title": {
                        "cz": "Kontaktujte nás",
                        "ru": "Связаться с нами?",
                        "en": "Contact us"
                    },
                    "items": [
                        {
                            "title": {
                                "ru": "Google"
                            },
                            "type": "href",
                            "data": "google.com",
                            "attrs": {
                                "target": "_blank"
                            }
                        },{
                            "type": "file",
                            "title": {
                                "ru": "Файл",
                                "en": "File"
                            },
                            "data": 100
                        }
                    ]
                },
                {
                    "type": "ui-sref:generator:article-category",
                    "data": 3,
                    "title": {},
                    "attrs": {
                        "target": "_blank"
                    },
                    "html": {},
                    "hideForScreenSize": {
                        "xs": True
                    }
                },
                {
                    "type": "directive",
                    "attrs": {
                        "target": "_blank"
                    },
                    "data": "ui-lang-picker-for-navbar",
                    "html": {},
                    "title": {},
                    "hideForScreenSize": {
                        "lg": True
                    }
                },
                {
                    "title": {},
                    "attrs": {
                        "target": "_blank"
                    },
                    "data": "h-cart-for-navbar",
                    "type": "directive",
                    "hideForLangs": {
                        "cz": True,
                        "ru": True,
                        "en": False
                    },
                    "html": {}
                }
            ]
        }
        category = CmsCategoryForArticle.objects.create(
            id=3,
            title={'en': 'Category for article!'},
            isPublished=True
        )
        for i in [1,2,3]:
            CmsArticle.objects.create(
                title={'en': 'Article # ' + str(i)},
                category=category,
                isPublished=True
            )
        for i in [4,5,6]:
            CmsArticle.objects.create(
                title={'en': 'Article # ' + str(i)},
                isPublished=True
            )

        CmsArticle.objects.create(
            id=10,
            title={'en': 'Article about us'},
            isPublished=True
        )
        from django.core.files.base import ContentFile
        l10nFile = L10nFile(id=100)
        l10nFile.save(ignore_processing=True)
        l10nFile.file_data.save('file.txt', ContentFile('A string with the file content'), save=False)
        l10nFile.save(ignore_processing=True)

        request = self.factory.get('/api/v1/UsersWithLb/')
        r = self.render_template(
            "{% uic_navbar_nav navbarConf %}",{
                'navbarConf': navbarConf,
                'request': request,
                'settings': self.cmsSettings,
                'LANGUAGE_CODE': 'en'
        })
        with open('tests/uic_navbar_rendered_en.html') as f:
            fixture = f.read()
        #with open('tests/uic_navbar_rendered.html', 'w') as f:
        #    f.write(r)
        self.assertEqual(
            self.prettify_html(r).strip(), 
            fixture.strip()
        )


        r = self.render_template(
            "{% uic_navbar_nav navbarConf%}",{
                'navbarConf': navbarConf,
                'request': request,
                'LANGUAGE_CODE': 'ru',
                'wft': 1,
                'settings': self.cmsSettings
        })
        l10nFile.file_data.delete()
        with open('tests/uic_navbar_rendered_ru.html') as f:
            fixture = f.read()
        #with open('tests/uic_navbar_rendered.html', 'w') as f:
        #    f.write(r)
        self.assertEqual(
            self.prettify_html(r).strip(), 
            fixture.strip()
        )






'''
class TestTranslateTags(TestCase):
    def render_template(self, string, context=None):
        string = "{% load i18n %}{% load lbcms %}" + string
        context = context or {}
        context = Context(context)
        return Template(string).render(context)

    def test_translate(self):
        CmsTranslations.objects.create(
            lang='ru',
            msgs=[{
                'msgId': 'One apple',
                'msgStr': [u'Одно яблоко']
            }]
        )
        r = self.render_template(
            "{% trans 'One apple' %}"
        )
        self.assertEqual(
            r,
            'One apple'
        )
        # russian
        r = self.render_template(
            """{% language 'ru'%}{% trans 'One apple' %}{% endlanguage %}"""
        )
        self.assertEqual(
            r,
            u'Одно яблоко'
        )

    def test_country_code_to_text(self):
        r = self.render_template(
            "{% country_code_to_text 'UA' %}"
        )
        self.assertEqual(
            r,
            'Ukraine'
        )
        r = self.render_template(
            "{% language 'ru'%}{% country_code_to_text 'UA' %}{% endlanguage %}"
        )
        self.assertEqual(
            r,
            u'Украина'
        )
        r = self.render_template(
            "{% language 'ru'%}{% country_code_to_text code %}{% endlanguage %}",
            {'code': 'de'}
        )
        self.assertEqual(
            r,
            u'Германия'
        )


    def test_qr_code(self):
        r = self.render_template(
            "{% qr_code 'Hello' %}"
        )
        self.assertEqual(
            r,
            "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6AQAAAADJwHeFAAAArElEQVR4nJ2QMQrCQBBF/zrbCII5hGAlGLRc2VxF8C6ewdvMYhUQVkibQyRouWFsLGZSZqvhMe9/Zp0AAJBW+L+lg/Opdo0mcYgkZmdszvBzneeBfWPJlicuhqRqf3GGSEEW1qT2j1F3QYoEekZWO19/P0zK8qD21pGxeKAgyoKIxJCFleWopWvWVvycCoTN7esuQHcB7zrPbqfjDjbnRWS6UrUJk8kR9ZnLhx9gcjzt7hSjoAAAAABJRU5ErkJggg==' />"
        )


    def test_uic_block_to_img(self):
        # генерация base-64 простой картинки
        r = self.render_template(
            """{% uic_block_to_img size='17x20' name=nameForFile alt %}
                {% uic_contact contact 10 %}
                <p>Hello World!</p>
            {% end_uic_block_to_img %}""",
            {
                'contact': {'type':"tel", 'value':"+000"},
                'nameForFile': 'hello.png'
            }
        )
        if WKHTML_VERSION == '0.12.5':
            self.assertEqual(
                r,
                "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAUCAYAAABroNZJAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALiQAAC4kBN8nLrQAABW9JREFUOBEBZAWb+gDx8fH/uLi4/+rq6v///////////////////////////////////////////////////////////////////////////wMjIyOAZWVlAPz8/ABDQ0MAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcHBz/AAAA/x0dHf/Hx8f//////////////////////////////////////////////////////////////////////wIQEBAAAAAAADw8PAAfHx8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACVlZX/AwMD/7+/v////////////////////////////////////////////////////////////////////////////wDW1tb/LCws/01NTf/r6+v//////////////////////////////////////////////////////////////////////wD/////r6+v/woKCv9ZWVn/6+vr///////m5ub/x8fH//7+/v///////////////////////////////////////////wD//////v7+/4GBgf8KCgr/TU1N/7+/v/9ZWVn/HR0d/3h4eP/q6ur//////////////////////////////////////wD///////////7+/v+vr6//LCws/wMDA/8AAAD/AAAA/w4ODv+4uLj//////////////////////////////////////wD/////////////////////1tbW/5WVlf8sLCz/HBwc/5ubm//x8fH//////////////////////////////////////wH/////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJOTkwAjIyMABwcHACQkJACVlZUAAAAAAAD/////////////////////////////////////////////////////pKSk/wAAAP9xcXH/tbW1/25ubv8AAAD/qKio/wD////////////////b29v/V1dX/9jY2P//////////////////////OTk5/0dHR/////////////////9JSUn/PDw8/wIAAAAAAAAAAAAAAADs7OwAqampAOvr6wAAAAAAAAAAAAAAAAD+/v4Azs7OAERERAAAAAAAAAAAAAAAAABCQkIAzc3NAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADw8PAA+fn5ABcXFwAAAAAAAAAAAAAAAAAVFRUA9/f3AAT29vYA7u7uAAAAAADr6+sAAAAAAOvr6wDk5OQAAAAAABEREQD6+voAAAAAAAUFBQAAAAAAAAAAAAAAAAADAwMAAAAAAACbm5v/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/5eXl//n5+f/AAAA/6enp/////////////////+jo6P/AAAA/3PAqu70MHOOAAAAAElFTkSuQmCC' alt='+000 Hello World!' style='width: 100%;' />"
            )
        else:
            self.assertEqual(
                r,
                "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAUCAYAAABroNZJAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALiQAAC4kBN8nLrQAABW9JREFUOBEBZAWb+gDx8fH/uLi4/+rq6v///////////////////////////////////////////////////////////////////////////wMjIyOAZWVlAPz8/ABDQ0MAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcHBz/AAAA/x0dHf/Hx8f//////////////////////////////////////////////////////////////////////wIQEBAAAAAAADw8PAAfHx8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACVlZX/AwMD/7+/v////////////////////////////////////////////////////////////////////////////wDW1tb/LCws/01NTf/r6+v//////////////////////////////////////////////////////////////////////wD/////r6+v/woKCv9ZWVn/6+vr///////m5ub/x8fH//7+/v///////////////////////////////////////////wD//////v7+/4GBgf8KCgr/TU1N/7+/v/9ZWVn/HR0d/3h4eP/q6ur//////////////////////////////////////wD///////////7+/v+vr6//LCws/wMDA/8AAAD/AAAA/w4ODv+4uLj//////////////////////////////////////wD/////////////////////1tbW/5WVlf8sLCz/HBwc/5ubm//x8fH//////////////////////////////////////wH/////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD7+/sAg4ODAAD////////////////////////////////w8PD/+vr6//////////////////////////////////////9qamr/BQUF/wD///////////////////////////////8LCwv/o6Oj/////////////////////////////////97e3v8CAgL/fn5+/wIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALS0tAD+/v4AXV1dAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANXV1QAHBwcAIyMjAAQAAAAA+/v7APHx8QAAAAAAAAAAAAAAAAAAAAAA9PT0AOzs7AAAAAAAAAAAAAYGBgAODg4AAAAAAO7u7gAQEBAAAQEBAAD/////s7Oz/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/0dHR////////////1RUVP8YGBj//////0KunxRnoa4UAAAAAElFTkSuQmCC' alt='+000 Hello World!' style='width: 100%;' />"
            )
        # теперь генерим в jpg
        r = self.render_template(
            """{% uic_block_to_img size='17x20' type='jpg' name=nameForFile alt %}
                {% uic_contact contact 10 %}
                <p>Hello World!</p>
            {% end_uic_block_to_img %}""",
            {
                'contact': {'type':"tel", 'value':"+000"},
                'nameForFile': 'hello.png'
            }
        )
        if WKHTML_VERSION == '0.12.5':
            self.assertEqual(
                r,
                "<img src='data:image/jpg;base64,/9j/4AAQSkZJRgABAQEASwBLAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAUABEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7k/4JJ/tDN+yL8PPjJ8Of2j/jPNP8SfAPi3UNZ1VvF1xHZpDpN1MGtLy0kdsS2kwZW+U4ikkaLC4Gf0O0TW7LxLo9rqGnXdrqFhexLPb3NtKssNxGwyro6khlIOQQcGvE/wBsr9hz4A/t3yaT4U+Mng3wl4x1FIZbrS7e8nNvqiQoyea8EkTpcCMM0YfY2351B+9zwXxJ/wCCgPwy/YTv9Q+F/hb4afEXxBofwg0C0u/EQ8EaDFd6Z4D0542aAXO+eNyfIjaXyrdJpBGNxXBGQD6zor5y/wCHuv7NH/RZ/BP/AIFn/wCJooA/nv8A2rv+Dmr9oHwj/wAFIL/xha+H/hW2qfCl/EXgjR0k0m9ME1jcX0G951F2C0w+ww4ZSq/M/wApyMfsr+xd4mn+I3iT9ujXdRjgN7rk+jXNyEUiPL+BtKcoASTsBdsAk8GiigD+Q/7bN/z1l/76NFFFAH//2Q==' alt='+000 Hello World!' style='width: 100%;' />"
            )
        else:
            self.assertEqual(
                r,
                "<img src='data:image/jpg;base64,/9j/4AAQSkZJRgABAQEASwBLAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAUABEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7k/4JJ/tDN+yL8PPjJ8Of2j/jPNP8SfAPi3UNZ1VvF1xHZpDpN1MGtLy0kdsS2kwZW+U4ikkaLC4Gf0O0TW7LxLo9rqGnXdrqFhexLPb3NtKssNxGwyro6khlIOQQcGvDP2+P2L/2ef2r/h9bS/tA+FPB2saHpU8NtbalrVydPeyluJ44Yoo7tHjkjMszxRhFcb2dVwS2D6PIfAv7Hv7Ps0gTSvBXw7+HWivM4jTy7TSNPtYizEAZIVI0JwMk47mgDtqK+Af+Ij/4Ff8AQnfH7/w3N/8A4UUAfmF/wX+/4OCvjX4B/a1+K/7OdloPw2k8C+FPEei3VndzabeNqjvZTWGqxB5BdCMg3EKq2IxlCQMH5h6P4F/4LG/FP/grb/wQ4/bK1L4k6X4L0e48HWuj6dZjw3ZXNos0N3dATCXzriYnIQAbSvDNnOeCigD94v8AhDNH/wCgVpv/AIDJ/hRRRQB//9k=' alt='+000 Hello World!' style='width: 100%;' />"
            )


        # генерация base-64 простой картинки с alt-текстом из строки
        r = self.render_template(
            """{% uic_block_to_img size='100x10' type='jpg' name=nameForFile alt='Test text' %}
                {% uic_contact contact 10 %}
                <p>Hello World!</p>
            {% end_uic_block_to_img %}""",
            {
                'contact': {'type':"tel", 'value':"+000"},
                'nameForFile': 'hello.png'
            }
        )
        if WKHTML_VERSION == '0.12.5':
            self.assertEqual(
                r,
                "<img src='data:image/jpg;base64,/9j/4AAQSkZJRgABAQEASwBLAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAKAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD92Phd8YvCnxt0K71Twh4h0jxLp1jfz6XcXOnXSXEcN1A5SaFmUkB0YYI7V0lflh/waN6ZbWP/AATi8VSw28EMlx4+1UyukYVpNpRV3EdcAADPQCvrH/gtH431r4bf8Ep/jzr3h3V9U0DXNK8I3dxZajpt1Ja3dnIAMPHLGQ6MOxUg0Aev/CX9pLw38aPHfxH8O6Ob5b/4W63HoGtm5hEcYuXsre9BibJ3p5NzHkkDncMcZr47+En/AAU2+M/ibwf8IfjPr/h74aw/Aj42eMbPwxpOl2aXq+KNEttSuntNLv7i5eQ203mS+QZIUhjMazjDuVNfgX/wQp/bD+Lnif8A4KzfDHTdS+KfxG1DTvGniCS48Q2tz4lvZYddkWwkjV7tGkKzsEiiUGQMQsaDoox+yngT/lAj+w7/ANjt8Mf/AE/WVAHr/wAV/wDgpv8AGbw54P8Ai78aNB8PfDWX4D/BTxjeeGNV0y8S9bxPrdrpt0lpqeoW9ykgtofLl88xwvC5kWA5dSwrsPjx+2n8dfEXxr+NukfBXRvhdL4f/Z70+zl1seKkvprzxVfz6eNSays5LeVEtQtrJAPOlWbMk2NgCk183ePP+UA37bf/AGOnxN/9P97Xu37M/wDyV/8A4KBf9hvTv/UL0ugD69/Z1+NOnftI/s/eBviJpEMtvpXjzw/YeIbOKUgyRQ3dvHOiNj+ILIAfcGuyr52/4JD/APKKf9mz/smHhz/02W9fRNABRRRQAUUUUAFFFFAH/9k=' alt='Test text' style='width: 100%;' />"
            )
        else:
            self.assertEqual(
                r,
                "<img src='data:image/jpg;base64,/9j/4AAQSkZJRgABAQEASwBLAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAKAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD92Phd8YvCnxt0K71Twh4h0jxLp1jfz6XcXOnXSXEcN1A5SaFmUkB0YYI7V0lflh/waN6ZbWP/AATi8VSw28EMlx4+1UyukYVpNpRV3EdcAADPQCv1PoA8/wDjn+0bovwB8QfDnTtXtNUupvib4ri8IaY1nHGy291JZXl4JJtzqRF5dlKCVDNuZPlwSRyX/BQH9rK7/Y6/Z8HiDRdFt/EvjDxFremeEvCukXFwbeDUdX1K7jtbVJZACUiVpDI5AzsjYDkiv5If2/v29Pjncfts/Eizk+NHxYktPCPxC1i40KBvF2oGPRZYrq6gjktl83ELJDJJGpj2lUdlGASK/U//AIJp/G/xp+0B/wAElv2dNe8eeL/FHjbXF/a88NWo1HX9Vn1K7EKzW5WPzZmZ9gLMQucDcfWgD9Tfgf8Ate/E74dftZ3nwc+PkXw8bUtQ8GzeOfD/AIk8IW95ZaddWtrPHBf2k1vdSzSLNAZoJA6yFZI5CdqFSteReD/+CofxqT4LfD79onxP4U+HVj+zx8R/ENhp0el24vB4q8PaVqV6tlp+q3Nw0htpd0ktu8lukKlEnGJHZSKvf8FEf+Unnwy/7Ij8RP5aXXkPxp/5VTvhN/2K/wANf/T1odAH038UP2s/jp8Xv2pvij8PPgFpPwwS3+Cljp/9vXvjNL2Y69qt7bG8i020+zSxi3VLcwl7iTzMNcKBEdrGvbf2Iv2p9N/ba/ZO8CfFTSrC40m28Z6Wl5Jp8775NOuAzR3FuzYG4xTJJHuwM7M4GcV4X/wTo/5P6/bf/wCyg6L/AOo1ptJ/wb//APKJj4Yf9fev/wDp/wBRoA+yaKKKACiiigAooooA/9k=' alt='Test text' style='width: 100%;' />"
            )


        # генерация base-64 простой картинки с alt-текстом из переменной, которая переводится на др язык
        r = self.render_template(
            """{% trans altVar as altTr %}{% uic_block_to_img size='100x10' type='jpg' name=nameForFile alt=altTr %}
                {% uic_contact contact 10 %}
                <p>Hello World!</p>
            {% end_uic_block_to_img %}""",
            {
                'contact': {'type':"tel", 'value':"+000"},
                'nameForFile': 'hello.png',
                'altVar':{
                    'ru': u"Альт-текст"
                }
            }
        )
        if WKHTML_VERSION == '0.12.5':
            self.assertEqual(
                r,
                u"<img src='data:image/jpg;base64,/9j/4AAQSkZJRgABAQEASwBLAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAKAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD92Phd8YvCnxt0K71Twh4h0jxLp1jfz6XcXOnXSXEcN1A5SaFmUkB0YYI7V0lflh/waN6ZbWP/AATi8VSw28EMlx4+1UyukYVpNpRV3EdcAADPQCvrH/gtH431r4bf8Ep/jzr3h3V9U0DXNK8I3dxZajpt1Ja3dnIAMPHLGQ6MOxUg0Aev/CX9pLw38aPHfxH8O6Ob5b/4W63HoGtm5hEcYuXsre9BibJ3p5NzHkkDncMcZr47+En/AAU2+M/ibwf8IfjPr/h74aw/Aj42eMbPwxpOl2aXq+KNEttSuntNLv7i5eQ203mS+QZIUhjMazjDuVNfgX/wQp/bD+Lnif8A4KzfDHTdS+KfxG1DTvGniCS48Q2tz4lvZYddkWwkjV7tGkKzsEiiUGQMQsaDoox+yngT/lAj+w7/ANjt8Mf/AE/WVAHr/wAV/wDgpv8AGbw54P8Ai78aNB8PfDWX4D/BTxjeeGNV0y8S9bxPrdrpt0lpqeoW9ykgtofLl88xwvC5kWA5dSwrsPjx+2n8dfEXxr+NukfBXRvhdL4f/Z70+zl1seKkvprzxVfz6eNSays5LeVEtQtrJAPOlWbMk2NgCk183ePP+UA37bf/AGOnxN/9P97Xu37M/wDyV/8A4KBf9hvTv/UL0ugD69/Z1+NOnftI/s/eBviJpEMtvpXjzw/YeIbOKUgyRQ3dvHOiNj+ILIAfcGuyr52/4JD/APKKf9mz/smHhz/02W9fRNABRRRQAUUUUAFFFFAH/9k=' alt='Альт-текст' style='width: 100%;' />"
            )
        else:
            self.assertEqual(
                r,
                u"<img src='data:image/jpg;base64,/9j/4AAQSkZJRgABAQEASwBLAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAKAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD92Phd8YvCnxt0K71Twh4h0jxLp1jfz6XcXOnXSXEcN1A5SaFmUkB0YYI7V0lflh/waN6ZbWP/AATi8VSw28EMlx4+1UyukYVpNpRV3EdcAADPQCv1PoA8/wDjn+0bovwB8QfDnTtXtNUupvib4ri8IaY1nHGy291JZXl4JJtzqRF5dlKCVDNuZPlwSRyX/BQH9rK7/Y6/Z8HiDRdFt/EvjDxFremeEvCukXFwbeDUdX1K7jtbVJZACUiVpDI5AzsjYDkiv5If2/v29Pjncfts/Eizk+NHxYktPCPxC1i40KBvF2oGPRZYrq6gjktl83ELJDJJGpj2lUdlGASK/U//AIJp/G/xp+0B/wAElv2dNe8eeL/FHjbXF/a88NWo1HX9Vn1K7EKzW5WPzZmZ9gLMQucDcfWgD9Tfgf8Ate/E74dftZ3nwc+PkXw8bUtQ8GzeOfD/AIk8IW95ZaddWtrPHBf2k1vdSzSLNAZoJA6yFZI5CdqFSteReD/+CofxqT4LfD79onxP4U+HVj+zx8R/ENhp0el24vB4q8PaVqV6tlp+q3Nw0htpd0ktu8lukKlEnGJHZSKvf8FEf+Unnwy/7Ij8RP5aXXkPxp/5VTvhN/2K/wANf/T1odAH038UP2s/jp8Xv2pvij8PPgFpPwwS3+Cljp/9vXvjNL2Y69qt7bG8i020+zSxi3VLcwl7iTzMNcKBEdrGvbf2Iv2p9N/ba/ZO8CfFTSrC40m28Z6Wl5Jp8775NOuAzR3FuzYG4xTJJHuwM7M4GcV4X/wTo/5P6/bf/wCyg6L/AOo1ptJ/wb//APKJj4Yf9fev/wDp/wBRoA+yaKKKACiiigAooooA/9k=' alt='Альт-текст' style='width: 100%;' />"
            )



        # генерация большой картинки с alt-текстом из переменной, которая переводится на др язык
        r = self.render_template(
            """{% trans altVar as altTr %}{% uic_block_to_img size='100x70' name=nameForFile alt=altTr %}
                {% uic_contact contact 10 %}
                <p>Hello World!</p>
            {% end_uic_block_to_img %}""",
            {
                'contact': {'type':"tel", 'value':"+000"},
                'nameForFile': 'hello.png',
                'altVar':{
                    'ru': u"Альт-текст"
                }
            }
        )
        self.assertEqual(
            r,
            u"<img src='https://storage.googleapis.com/lbcms-container-test_bucket/embedded-for-email/hello.png' alt='Альт-текст' style='width:100%' />"
        )
'''
