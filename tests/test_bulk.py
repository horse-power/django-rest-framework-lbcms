from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
import json, os

from drf_lbcms.helpers.bulk import Bulk
from .models import TestModel, CmsUser

ERROR_MSGS = Bulk.error_msgs



class TestBulk(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.maxDiff = None
        for n in [1,2,3]:
            CmsUser.objects.create(username='user'+str(n))

        for m in [1,2,3,4,5]:
            TestModel.objects.create(int_field=m*2, string_field='string '+str(m))

        self.userData = [
            {'id':u.pk, 'username': u.username, 'email': u.email}
            for u in CmsUser.objects.all()
        ]
        self.testModelData = [
            {'id': t.id, 'int_field': t.int_field, 'string_field': t.string_field, 'user': t.user, 'manyUsers': []}
            for t in TestModel.objects.all()
        ]

    def test_bulk_simple(self):
        bulk = Bulk()
        bulk_query = {
            'Users': {
                'list': {}
            }
        }

        request = self.factory.get('', {'data': json.dumps(bulk_query)})

        response = bulk.bulk_view(request)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, {
            'data': {
                'Users':{
                    'list': self.userData
                }
            },
            'error': {}
        })


    def test_bulk_complex(self):
        bulk = Bulk()
        bulk_query = {
            'Users': {
                'list': {}
            },
            'TestModels':{
                'list': {}
            }
        }

        request = self.factory.get('', {'data': json.dumps(bulk_query)})

        response = bulk.bulk_view(request)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, {
            'data': {
                'Users':{
                    'list': self.userData
                },
                'TestModels':{
                    'list': self.testModelData
                }
            },
            'error': {}
        })

        bulk_query = {
            'Users': {
                'find': {}
            },
            'TestModels':{
                'retrieve': {
                    'id': 1
                },
                'findById': {
                    'id': 2
                }
            }
        }

        request = self.factory.get('', {'data': json.dumps(bulk_query)})

        response = bulk.bulk_view(request)
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, {
            'data': {
                'Users':{
                    'find': self.userData
                },
                'TestModels':{
                    'retrieve': self.testModelData[0],
                    'findById': self.testModelData[1]
                }
            },
            'error': {}
        })

    def test_lb(self):
        bulk = Bulk()
        bulk_query = {
            'UsersWithLb': {
                'list': {
                    'filter':{
                        'where':{
                            'username':{
                                'inq': ['user1', 'user3', 'nouser4']
                            }
                        }
                    }
                }
            },
            'TestModels':{
                'list': {}
            }
        }

        request = self.factory.get('', {'data': json.dumps(bulk_query)})

        response = bulk.bulk_view(request)
        self.assertEqual(
            response.data['data']['UsersWithLb']['list'],
            [
                {'id': 1, 'username': 'user1', 'email':''},
                {'id': 3, 'username': 'user3', 'email':''},
            ]
        )


    def test_fail(self):
        bulk = Bulk()

        request = self.factory.get('', {'data':'----'})
        self.assertRaisesMessage(
            exceptions.ParseError,
            ERROR_MSGS['malformed_json'].format(
                property='data'
            ),
            bulk.bulk_view,

            request
        )

        request = self.factory.get('', {'data': '[]'})
        self.assertRaisesMessage(
            exceptions.ParseError,
            ERROR_MSGS['invalid_type'].format(
                property='data',
                type=type([])
            ),
            bulk.bulk_view,

            request
        )


        request = self.factory.get('', {'data': json.dumps({'Users':1})})
        self.assertRaisesMessage(
            exceptions.NotAcceptable,
            ERROR_MSGS['invalid_type'].format(
                property='data.Users',
                type=type(1)
            ),
            bulk.bulk_view,

            request
        )


        request = self.factory.get('', {'data': json.dumps({'NoModel':{}})})
        self.assertRaisesMessage(
            exceptions.NotAcceptable,
            ERROR_MSGS['no_such_rest_point'].format(
                rest_point='NoModel'
            ),
            bulk.bulk_view,

            request
        )



        bulk_query = {
            'Users': {
                'noSuchRestPoint!': {}
            }
        }

        request = self.factory.get('', {'data': json.dumps(bulk_query)})
        response = bulk.bulk_view(request)

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, {
            'data': {},
            'error': {
                'Users':{
                    'noSuchRestPoint!': ERROR_MSGS['no_such_rest_action'].format(
                        rest_point='Users',
                        rest_action='noSuchRestPoint!'
                    )
                }
            }
        })
