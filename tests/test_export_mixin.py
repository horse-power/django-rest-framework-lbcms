# -*- coding: utf-8 -*-
from django.contrib.auth.models import User as UserModel
from django.test import TestCase
from django.db.models import Sum
import drfs, json, os
from rest_framework.test import APIRequestFactory
from rest_framework.viewsets import ModelViewSet
from io import StringIO
import csv, six

from .models import TestModel
from drf_lbcms.mixins import ExportMixin, ExportByIdMixin

BASE_DIR = os.path.dirname(__file__)
RESOURCE_DIR = os.path.join(BASE_DIR, 'resources')



class ExportViewset(ExportMixin, ModelViewSet):
    queryset = TestModel.objects.all()
    serializer_class = str


class ExportWithFooterViewset(ExportMixin, ModelViewSet):
    queryset = TestModel.objects.all()
    serializer_class = str
    def perform_export(self, queryset, preferred_lang='en', **kwargs):
        rows_data = [[]]
        fields = []
        choices_fields = {}
        # демо-функция экспорта
        for field in queryset.model._meta.get_fields():
            fields.append(field.name)
            rows_data[0].append(field.name)
            # если поле у нас с choices, то нужен перевод в строку этого поля
            if getattr(field, 'choices', None):
                choices_fields[field.name] = field
        for item in queryset.iterator():
            rows_data.append([])
            for field in fields:
                value = getattr(item, field, None)
                rows_data[-1].append(value)
        return rows_data, {'FOOTER': {
            'startRowIndex': 1,
            'columns': {
                1: sum,
                3: len
            },
            'formatters': {
                0: "total",
                1: "summ = %s"
            }
        }}

class ExportByIdViewset(ExportByIdMixin, ModelViewSet):
    queryset = TestModel.objects.all()
    serializer_class = str





class TestExportMixin(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.maxDiff = None

        for m in [1,2,3,4,5]:
            TestModel.objects.create(int_field=m*2, string_field='string '+str(m))

    def test_mixin(self):
        export = ExportViewset.as_view({'get': 'export'})
        exportWithFooter = ExportWithFooterViewset.as_view({'get': 'export'})
        exportById = ExportByIdViewset.as_view({'get': 'export_by_id'})

        # wrong format
        request = self.factory.get('')
        response = export(request, fileFormat='fff')
        self.assertEqual(
            response.data['detail'],
            ExportMixin.export_error_msgs['invalid_format'].format(
                fileFormat='fff'
            )
        )
        self.assertEqual(
            response.status_code,
            406
        )
        # wrong format
        request = self.factory.get('')
        response = exportById(request, fileFormat='fff', pk=1)
        self.assertEqual(
            response.data['detail'],
            ExportMixin.export_error_msgs['invalid_format'].format(
                fileFormat='fff'
            )
        )
        self.assertEqual(
            response.status_code,
            406
        )




        # export models
        def test_fields(fields):
            item = TestModel.objects.get(id=fields[0])
            self.assertEqual(
                str(item.int_field),
                fields[1]
            )
            self.assertEqual(
                item.string_field,
                fields[2]
            )

        # all objects
        response = export(request, fileFormat='csv')
        if six.PY2:
            generated = u''.join(response._container)
            reader = csv.reader(StringIO(generated))
        else:
            generated = b''.join(response._container)
            reader = csv.reader(StringIO(generated.decode('utf-8')))

        for row in list(reader)[1:]:
            test_fields(row)

        # all objects with footer
        response = exportWithFooter(request, fileFormat='csv')
        if six.PY2:
            generated = u''.join(response._container)
            reader = csv.reader(StringIO(generated))
        else:
            generated = b''.join(response._container)
            reader = csv.reader(StringIO(generated.decode('utf-8')))

        i = 0
        for row in list(reader)[1:]:
            if i == 5:
                # footer
                self.assertEqual(row[0], 'total')
                self.assertEqual(
                    row[1],
                    'summ = %s' % TestModel.objects.aggregate(Sum('int_field'))['int_field__sum']
                )
                self.assertEqual(row[2], '')
                self.assertEqual(row[3], str(TestModel.objects.count()))
            else:
                test_fields(row)
            i += 1


        # single object
        response = exportById(request, fileFormat='csv', pk=2)
        if six.PY2:
            generated = u''.join(response._container)
            reader = csv.reader(StringIO(generated))
        else:
            generated = b''.join(response._container)
            reader = csv.reader(StringIO(generated.decode('utf-8')))

        self.assertEqual(len(list(reader)), 2)
        for row in list(reader)[1:]:
            self.assertEqual(row[0], '2')
            test_fields(row)
