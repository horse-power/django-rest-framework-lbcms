from rest_framework import viewsets, generics
from drf_loopback_js_filters import LoopbackJsFilterBackend
from drfs.decorators import action

from . import serializers
from ..models import TestModel, CmsUser



class UserViewset(viewsets.ModelViewSet):
    queryset = CmsUser.objects.all()
    serializer_class = serializers.UserSerializer

    @action(methods=['get'], detail=False)
    def list_route_decorator(self, *args, **kwargs):
        return self.list(*args, **kwargs)[:2]

    @action(methods=['put'], detail=True)
    def put_detail_route_decorator(self, *args, **kwargs):
        return self.update(*args, **kwargs)

    @action(methods=['get'], detail=True)
    def get_detail_route_decorator(self, *args, **kwargs):
        return self.retrieve(*args, **kwargs)


class UserViewsetWithLbFilter(viewsets.ModelViewSet):
    queryset = CmsUser.objects.all()
    serializer_class = serializers.UserSerializer
    filter_backends = (LoopbackJsFilterBackend,)


class CustomViewset(viewsets.ViewSet):
    def get_serializer_class(self, *args, **kwargs):
        return str

    def destroy(self, request, *args, **kwargs):
        pass

    @action(methods=['get'], detail=False)
    def test_list_route_decorator(self, request):
        return

    @action(methods=['patch'], detail=True)
    def test_detail_route_decorator(self, request):
        return


class TestModelViewset(viewsets.ModelViewSet):
    queryset = TestModel.objects.all()
    serializer_class = serializers.TestModelSerializer
