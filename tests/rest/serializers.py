from rest_framework import serializers
from ..models import TestModel, CmsUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CmsUser
        fields = ['id', 'username', 'email']



class TestModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestModel
        fields = '__all__'
