from rest_framework import routers
from django.conf.urls import url, include
from . import viewsets

router = routers.SimpleRouter()
router.register(r'Users', viewsets.UserViewset)
router.register(r'UsersWithLb', viewsets.UserViewsetWithLbFilter)
router.register(r'TestModels', viewsets.TestModelViewset)

urlpatterns = router.urls
