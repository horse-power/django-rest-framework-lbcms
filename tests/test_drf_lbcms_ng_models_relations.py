# -*- coding: utf-8 -*-
from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
import json, os, tempfile

from drf_lbcms.management.commands.drf_lbcms_ng_models_relations import Command

RESOURCES_DIR = os.path.join(settings.BASE_DIR, 'resources')




class Test(TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_full(self):
        tmp_file_path = os.path.join(tempfile.mkdtemp(), 'relations.js')
        c = Command()
        kwargs={
            '--filepath=': [tmp_file_path],
            '--ignorerelations=': 'TestModel.manyUsers'
        }
        c.handle(**kwargs)

        with open(tmp_file_path) as f:
            generated = f.read()

        os.remove(tmp_file_path)
        os.removedirs(os.path.dirname(tmp_file_path))

        with open(os.path.join(RESOURCES_DIR, 'relations_full.js')) as f:
            constants = f.read()
        self.assertEqual(generated, constants)

    def test_ignore_model(self):
        tmp_file_path = os.path.join(tempfile.mkdtemp(), 'relations.js')
        c = Command()
        kwargs={
            '--filepath=': [tmp_file_path],
            '--ignoremodels=': 'Permission'
        }
        c.handle(**kwargs)

        with open(tmp_file_path) as f:
            data = f.read()

        os.remove(tmp_file_path)
        os.removedirs(os.path.dirname(tmp_file_path))

        with open(os.path.join(RESOURCES_DIR, 'relations_no_permission.js')) as f:
            constants = f.read()

        self.assertEqual(data, constants)


    def test_weak_relations(self):
        tmp_file_path = os.path.join(tempfile.mkdtemp(), 'weak_relations.js')
        c = Command()
        kwargs={
            '--filepath=': [tmp_file_path],
            '--weakrelations=': 'TestModel.user.groups,TestModel.manyUsers.groups',
            '--ignorerelations=': 'TestModel.user,TestModel.manyUsers,CmsUser.groups,CmsUser.user_permissions'
        }
        c.handle(**kwargs)

        with open(tmp_file_path) as f:
            data = f.read()

        os.remove(tmp_file_path)
        os.removedirs(os.path.dirname(tmp_file_path))

        with open(os.path.join(RESOURCES_DIR, 'relations_weak_and_ignore.js')) as f:
            constants = f.read()

        self.assertEqual(data, constants)
