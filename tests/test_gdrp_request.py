# -*- coding: utf-8 -*-
from django.test import TestCase
from django.db.models import Sum
import drfs, json, os
from django.core import mail

from drf_lbcms.models import CmsGdprRequest, L10nFile
from drf_lbcms.helpers import sms
from drf_lbcms.helpers.tests import APIRequestFactoryWrapper

from .models import CmsSomeModel, CmsSettings, CmsUser

BASE_DIR = os.path.dirname(__file__)
RESOURCE_DIR = os.path.join(BASE_DIR, 'resources')





class TestExportMixin(TestCase):
    def setUp(self):
        self.viewset = drfs.generate_viewset('CmsGdprRequest.json')
        self.factory = APIRequestFactoryWrapper()
        self.maxDiff = None
        self.mailboxCount = len(mail.outbox)
        self.smsboxCount = len(sms.outbox)
        self.settings = CmsSettings.objects.create(
            siteTitle={
                'en': 'Test-site-name'
            },
            serverIntegrations={
                'useSmsNotifications': True,
                'useEmailNotifications': True,
                'smsapicom':{
                    'username': 'test',
                    'password': 'test',
                    'senderName': 'Jack'
                }
            }
        )
        self.admin = CmsUser.objects.create(
            username='admin',
            email="admin@nomail.com",
            preferredLang='en',
            userRole='a',
            is_superuser=True
        )

        for m in [1,2,3,4,5]:
            CmsSomeModel.objects.create(
                name='myemail-'+str(m)+"@mail.com and other text",
            )
        sm = CmsSomeModel.objects.get(id=3)
        sm.json = {'key': {'deep': 'dadadada +0001234567'}}
        sm.save()

    def test_mixin(self):
        create = self.viewset.as_view({'post': 'create'})
        send_verification_code = self.viewset.as_view({'put': 'send_verification_code'})
        verify_code = self.viewset.as_view({'put': 'verify_code'})
        send_request = self.viewset.as_view({'put': 'send_request'})
        collect_user_data = self.viewset.as_view({'put': 'collect_user_data'})
        anonimize_user_data = self.viewset.as_view({'put': 'anonimize_user_data'})
        delete_user_data = self.viewset.as_view({'put': 'delete_user_data'})

        # information request
        request = self.factory.post('', data={'preferredLang': 'ru', 'type': 'i'})
        response = create(request)
        self.assertEqual(
            response.data['preferredLang'],
            'ru'
        )
        self.assertEqual(
            response.data['type'],
            'i'
        )
        requestId = response.data['id']
        self.assertEqual(len(mail.outbox), self.mailboxCount)



        # добавляем почтовый ящик в запрос. сервер должен отослать письмо с кодом
        test_email = 'myemail-2@mail.com'

        request = self.factory.put('', data={'email': test_email})
        response = send_verification_code(request, pk=requestId)

        self.assertEqual(len(mail.outbox), self.mailboxCount+1)
        self.assertEqual(
            mail.outbox[-1].to,
            [test_email]
        )
        self.assertEqual(
            mail.outbox[-1].subject,
            u'Код подтверждения'
        )
        gdprRequest = CmsGdprRequest.objects.get(pk=requestId)
        self.assertIn(
            "<b>%s</b>" % gdprRequest.verificationCodes[test_email],
            mail.outbox[-1].body
        )

        # вводим неверный код
        self.assertEqual(
            gdprRequest.emails,
            []
        )
        request = self.factory.put('', data={'email': test_email, 'code': "INVALID1"})
        response = verify_code(request, pk=requestId)
        self.assertEqual(
            response.data['validated'],
            False
        )

        # вводим код и подтверждаем мыло
        self.assertEqual(
            gdprRequest.emails,
            []
        )
        request = self.factory.put('', data={'email': test_email, 'code': gdprRequest.verificationCodes[test_email]})
        response = verify_code(request, pk=requestId)
        # теперь код должен был приняться и почтовый ящик добавиться
        self.assertEqual(
            response.data['validated'],
            True
        )
        gdprRequest.refresh_from_db()
        self.assertEqual(
            gdprRequest.emails,
            [test_email]
        )

        #
        # тестируем подтверждение по номеру телефона
        #

        # добавляем № телефора в запрос. сервер должен отослать смс с кодом
        test_phone = '+0001234567'

        request = self.factory.put('', data={'phone': test_phone})
        response = send_verification_code(request, pk=requestId)

        self.assertEqual(len(sms.outbox), self.smsboxCount+1)
        self.assertEqual(
            sms.outbox[-1].to,
            [test_phone]
        )
        gdprRequest = CmsGdprRequest.objects.get(pk=requestId)
        self.assertEqual(
            sms.outbox[-1].body,
            u'Код подтверждения - %s' % gdprRequest.verificationCodes[test_phone]
        )

        # вводим неверный код
        self.assertEqual(
            gdprRequest.phones,
            []
        )
        request = self.factory.put('', data={'phone': test_phone, 'code': "INVALID1"})
        response = verify_code(request, pk=requestId)
        self.assertEqual(
            response.data['validated'],
            False
        )

        # вводим код и подтверждаем телефон
        self.assertEqual(
            gdprRequest.phones,
            []
        )
        request = self.factory.put('', data={'phone': test_phone, 'code': gdprRequest.verificationCodes[test_phone]})
        response = verify_code(request, pk=requestId)
        # теперь код должен был приняться и телефон добавиться
        self.assertEqual(
            response.data['validated'],
            True
        )
        gdprRequest.refresh_from_db()
        self.assertEqual(
            gdprRequest.phones,
            [test_phone]
        )


        # теперь отправляем почту админу с этим запросом
        request = self.factory.put('')
        response = send_request(request, pk=requestId)
        self.assertEqual(len(mail.outbox), self.mailboxCount+2)
        self.assertEqual(
            mail.outbox[-1].to,
            [self.admin.email]
        )
        self.assertEqual(
            mail.outbox[-1].subject,
            u'The Site Visitor has requested information about himself/herself'
        )
        gdprRequest.refresh_from_db()
        for e in gdprRequest.emails:
            self.assertIn(
                e,
                mail.outbox[-1].body
            )
        for p in gdprRequest.phones:
            self.assertIn(
                p,
                mail.outbox[-1].body
            )



        #
        #       действия от имени админа
        #
        # запускаем сбор инфы
        request = self.factory.put('?sendEmail=true', user=self.admin)
        response = collect_user_data(request, pk=requestId)
        self.assertEqual(response.data['status'], 'a')
        self.assertEqual(
            response.data['infoFile'],
            1
        )
        l10nfile = L10nFile.objects.get(id=1)
        data = l10nfile.file_data.read()
        data = json.loads(data)
        self.assertEqual(
            data, {
            "CmsArticle": [],
            "CmsCategoryForPictureAlbum": [],
            "CmsPictureAlbum": [],
            "CmsSiteReview": [],
            "CmsKeyword": [],
            "CmsCategoryForArticle": [],
            "CmsUser": [],
            "CmsSomeModel": [
                {
                    "id": 2,
                    "choices_field": "n",
                    "model": "",
                    "json": {},
                    "name": "myemail-2@mail.com and other text",
                    "manufacturer": "",
                    "owner": None,
                    "otherUser": None
                },
                {
                    "id": 3,
                    "choices_field": "n",
                    "model": "",
                    "json": {
                        "key": {
                            "deep": "dadadada +0001234567"
                        }
                    },
                    "name": "myemail-3@mail.com and other text",
                    "manufacturer": "",
                    "owner": None,
                    "otherUser": None
                }
            ]
        })
        # посетителю должно было отправиться письмо
        self.assertEqual(len(mail.outbox), self.mailboxCount+3)
        self.assertEqual(
            mail.outbox[-1].to,
            [test_email]
        )
        self.assertEqual(
            mail.outbox[-1].subject,
            u'Ответ на запрос о получении данных с сайта'
        )
        # проверяем что есть ссылка на файл
        self.assertIn(
            "<a href=\"https://storage.googleapis.com/lbcms-container-test_bucket/",
            mail.outbox[-1].body
        )
        #print mail.outbox[-1].body




        # анонимизируем данные
        request = self.factory.put('?sendEmail=true', user=self.admin)
        response = anonimize_user_data(request, pk=requestId)

        self.assertEqual(response.data['status'], 'a')
        # сверяемся, что CmsSomeModel не содержит больше запрещенный текст
        sm2 = CmsSomeModel.objects.get(id=2)
        self.assertEqual(
            sm2.name,
            "protected-by-gdpr@mail.com and other text"
        )
        sm3 = CmsSomeModel.objects.get(id=3)
        self.assertEqual(
            sm3.json,{
                "key": {
                    "deep": "dadadada +phone-protected-by-gdpr"
                }
            }
        )
        # посетителю должно было отправиться письмо
        self.assertEqual(len(mail.outbox), self.mailboxCount+4)
        self.assertEqual(
            mail.outbox[-1].to,
            [test_email]
        )
        self.assertEqual(
            mail.outbox[-1].subject,
            u'Ответ на запрос об анонимизации данных с сайта'
        )


        # тестируем удаление
        sm2.name = "myemail-2@mail.com and other text"
        sm2.save()
        sm3.json['key']['deep'] = "dadadada +0001234567"
        sm3.save()
        request = self.factory.put('?sendEmail=true', user=self.admin)
        response = delete_user_data(request, pk=requestId)

        self.assertEqual(response.data['status'], 'a')
        # сверяемся, что объектов больще нет
        self.assertEqual(
            CmsSomeModel.objects.filter(id=2).count(),
            0
        )
        self.assertEqual(
            CmsSomeModel.objects.filter(id=3).count(),
            0
        )
        # посетителю должно было отправиться письмо
        self.assertEqual(len(mail.outbox), self.mailboxCount+5)
        self.assertEqual(
            mail.outbox[-1].to,
            [test_email]
        )
        self.assertEqual(
            mail.outbox[-1].subject,
            u'Ответ на запрос об удалении данных с сайта'
        )
