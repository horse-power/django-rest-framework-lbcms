from django.contrib.auth.models import User as UserModel
from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
from django.core.files.images import ImageFile
import json, os
from PIL import Image

from drf_lbcms.models import L10nFileLocal as L10nFileModelClass

from drf_lbcms.helpers import l10nFile

BASE_DIR = os.path.dirname(__file__)
RESOURCE_DIR = os.path.join(BASE_DIR, 'resources')
STORAGE_DIR = os.path.join(BASE_DIR, 'storage-tests')

class TestWatermark(TestCase):
    def setUp(self):
        self.watermarkImg = Image.open(
            os.path.join(RESOURCE_DIR, 'watermark.png')
        )
        w_width, w_height = self.watermarkImg.size
        self.watermarkImgSize = {
            'width': w_width,
            'height': w_height
        }
        self.img = Image.open(
            os.path.join(RESOURCE_DIR, 'django_logo.png')
        )
        i_width, i_height = self.img.size
        self.imgSize = {
            'width': i_width,
            'height': i_height
        }


    def test_watermark(self):
        for pos in ['top left', 'top right', 'bottom right', 'bottom left', 'center']:
            self.img = Image.open(
                os.path.join(RESOURCE_DIR, 'django_logo.png')
            )
            img_with_watermark = l10nFile.draw_watermark_on_img(
                self.img,
                self.watermarkImg,
                pos,
                img_size=self.imgSize,
                watermark_size=self.watermarkImgSize
            )
            img_with_watermark.save(
                os.path.join(STORAGE_DIR,
                'TEST WATERMARK '+pos+'.png'),
                format=img_with_watermark.format
            )



class TestL10nWatermark(TestCase):
    def setUp(self):
        ffile = ImageFile(open(os.path.join(RESOURCE_DIR, 'watermark.png'), 'rb'))
        ffile.name = os.path.basename(ffile.name)
        meta = {
            'size': ffile.size,
            'originalName': ffile.name,
            'type': 'image/png'
        }
        self.watermark  = L10nFileModelClass(file_data=ffile, meta_data=meta)
        self.watermark.save()

    def test_watermark(self):
        ffile = ImageFile(open(os.path.join(RESOURCE_DIR, 'django_logo.png'), 'rb'))
        ffile.name = os.path.basename(ffile.name)
        meta = {
            'size': ffile.size,
            'originalName': ffile.name,
            'type': 'image/png'
        }
        l10nFile = L10nFileModelClass(file_data=ffile, meta_data=meta)
        l10nFile.save(options={
            'watermark':{
                'img': self.watermark.file_data,
                'position': 'bottom right'
            }
        })
