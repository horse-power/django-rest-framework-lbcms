# -*- coding: utf-8 -*-
from django.test import TestCase, RequestFactory
from django.conf import settings
import os





class Test(TestCase):
    def test_ng_constants(self):
        from drf_lbcms.management.commands.drf_lbcms_collectstatic import Command
        c = Command()
        c.handle(
            interactive=False,
            verbosity=True,
            link=False,
            clear=True,
            dry_run=False,
            ignore_patterns=["*/translations/*.json", "*/po/*.json", "*/rest_framework/*", "*.pdf", "*.pot", "*.xls", "*.xlsx", "*.csv"],
            use_default_ignore_patterns=False,
            post_process=True,
            skip_checks=False
        )
        files = os.listdir(settings.STATIC_ROOT)
        files.sort()
        self.assertListEqual(
            files,
            [
                'constants.js', 'django_logo.png', 'drf_lbcms', 'dump.7z', 'mail', 'python-zen.txt',
                'relations_full.js', 'relations_no_permission.js',
                'relations_weak_and_ignore.js', 'test_pass', 'watermark.png'
            ]
        )
