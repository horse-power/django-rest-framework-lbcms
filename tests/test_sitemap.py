# -*- coding: utf-8 -*-
from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
from rest_framework.test import APIRequestFactory
import json, os
from .models import CmsSomeModel, TestModel, CmsSettings


def pretty_xml(text):
    from bs4 import BeautifulSoup
    bs = BeautifulSoup(text, 'xml')
    return bs.prettify()



class TestNotifyByModelSignal(TestCase):
    def setUp(self):
        self.factory = RequestFactory(SERVER_NAME="sometest.tt")
        self.maxDiff = None
        CmsSettings.objects.create(
            siteTitle={
                'en': 'Test-site-name'
            },
            cmsLangCodes=['ru', 'en', 'de']
        )
        for m in [1,2,3,4,5]:
            TestModel.objects.create(int_field=m*3, string_field='string '+str(m))
        for m in [1,2,3,4,5]:
            CmsSomeModel.objects.create(
                name='text-'+str(m)+" and other text",
            )

    def test_sitemap(self):
        from drf_lbcms.views import SitemapView
        get_sitemap = SitemapView.as_view()
        request = self.factory.get('/sitemap.xml')
        response = get_sitemap(request)
        data = response.render()
        #print('----------------------------')
        #print(data._container[0])
        #print('----------------------------')
        xml = pretty_xml(data._container[0].decode('utf=8'))
        result = """<?xml version="1.0" encoding="utf-8"?>
<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
 <url>
  <loc>
   http://sometest.tt/en/
  </loc>
  <changefreq>
   weekly
  </changefreq>
  <priority>
   1
  </priority>
  <xhtml:link href="http://sometest.tt/ru/" hreflang="ru" rel="alternate"/>
  <xhtml:link href="http://sometest.tt/en/" hreflang="en" rel="alternate"/>
  <xhtml:link href="http://sometest.tt/de/" hreflang="de" rel="alternate"/>
 </url>
 <url>
  <loc>
   http://sometest.tt/en/test-model/1/string_1
  </loc>
  <xhtml:link href="http://sometest.tt/ru/test-model/1/string_1" hreflang="ru" rel="alternate"/>
  <xhtml:link href="http://sometest.tt/en/test-model/1/string_1" hreflang="en" rel="alternate"/>
 </url>
 <url>
  <loc>
   http://sometest.tt/en/test-model/2/string_2
  </loc>
  <xhtml:link href="http://sometest.tt/ru/test-model/2/string_2" hreflang="ru" rel="alternate"/>
  <xhtml:link href="http://sometest.tt/en/test-model/2/string_2" hreflang="en" rel="alternate"/>
 </url>
 <url>
  <loc>
   http://sometest.tt/en/test-model/3/string_3
  </loc>
  <xhtml:link href="http://sometest.tt/ru/test-model/3/string_3" hreflang="ru" rel="alternate"/>
  <xhtml:link href="http://sometest.tt/en/test-model/3/string_3" hreflang="en" rel="alternate"/>
 </url>
 <url>
  <loc>
   http://sometest.tt/en/test-model/4/string_4
  </loc>
  <xhtml:link href="http://sometest.tt/ru/test-model/4/string_4" hreflang="ru" rel="alternate"/>
  <xhtml:link href="http://sometest.tt/en/test-model/4/string_4" hreflang="en" rel="alternate"/>
 </url>
 <url>
  <loc>
   http://sometest.tt/en/test-model/5/string_5
  </loc>
  <xhtml:link href="http://sometest.tt/ru/test-model/5/string_5" hreflang="ru" rel="alternate"/>
  <xhtml:link href="http://sometest.tt/en/test-model/5/string_5" hreflang="en" rel="alternate"/>
 </url>
</urlset>"""


        self.assertEqual(
            pretty_xml(data._container[0].decode('utf=8')).strip(),
            result.strip()
        )
