# -*- coding: utf-8 -*-
from django.test import TestCase
from django.conf import settings
from rest_framework import exceptions
import json, os
from datetime import datetime,timedelta
from currency_converter import CurrencyConverter

from drf_lbcms.helpers.date import str_to_date
from drf_lbcms.helpers import currency

orig_converter = CurrencyConverter('http://www.ecb.int/stats/eurofxref/eurofxref-hist.zip')


class TestConverter(TestCase):
    def test_date(self):
        today = datetime.now().date()
        czk = currency.convert_ecb(100, 'EUR', 'CZK', date=today)[0]
        self.assertEqual(
            czk,
            orig_converter.convert(100, 'EUR', 'CZK')
        )
        august = str_to_date('2017-08-01')
        czk = currency.convert_ecb(100, 'EUR', 'CZK', date=august)[0]
        self.assertEqual(
            round(czk, 3),
            2613.2
        )

    def test_human_currency(self):
        october = str_to_date('2017-10-22')
        czk = currency.human_currency(100, from_currency='EUR', to_currency='CZK', date=october)
        self.assertEqual(czk, u'2,569.0 Kč')
        czk = currency.human_currency(100.4, from_currency='EUR', to_currency='CZK', fraction_size=1, date=october)
        self.assertEqual(czk, u'2,579.3 Kč')
        czk = currency.human_currency(100.4, from_currency='EUR', to_currency='CZK', fraction_size=2, date=october)
        self.assertEqual(czk, u'2,579.28 Kč')




class DbTestConverter(TestCase):
    def setUp(self):
        from drf_lbcms.models import CmsCurrencyData
        today = datetime.now().date()
        CmsCurrencyData.objects.create(
            date=today,
            CZK= 27.022
        )
        CmsCurrencyData.objects.create(
            date=today-timedelta(days=1),
            USD= 1.0927
        )
        CmsCurrencyData.objects.create(
            date=today-timedelta(days=2),
            CZK= 26.022,
        )

    def test_date(self):
        self.assertEqual(
            100*27.022,
            currency.convert_db(100, 'EUR', 'CZK')[0]
        )
        self.assertEqual(
            122*27.022,
            currency.convert_db(122, 'EUR', 'CZK')[0]
        )
        self.assertEqual(
            122 * 26.022,
            currency.convert_db(122, 'EUR', 'CZK', date=datetime.now().date()-timedelta(days=1))[0]
        )
        self.assertEqual(
            122 * (1/26.022),
            currency.convert_db(122, 'CZK', 'EUR', date=datetime.now().date()-timedelta(days=1))[0]
        )


class CacheTestConverter(TestCase):
    def test_date(self):
        cache = {
            '2017-10-01':{
                'EUR': 1,
                'CZK': 20
            }
        }
        czk = currency.convert(10, 'EUR', 'CZK', date=str_to_date('2017-10-01'), currencyData=cache)
        self.assertEqual(
            czk,
            200
        )
        self.assertEqual(
            len(cache.keys()),
            1
        )
        czk2 = currency.convert(10, 'EUR', 'CZK', date=str_to_date('2017-12-01'), currencyData=cache)
        self.assertEqual(czk2, 255.24)
        # обновление внешнего кеша
        self.assertEqual(
            len(cache.keys()),
            2
        )
        self.assertDictEqual(
            cache['2017-12-01'],
            {
                'EUR': 1.0,
                'CZK': 25.524
            }
        )
