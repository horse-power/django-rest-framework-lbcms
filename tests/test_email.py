# -*- coding: utf-8 -*-
from django.test import TestCase
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from rest_framework.test import APIRequestFactory
from rest_framework import exceptions
import json, os, pytz
from datetime import datetime

import drfs

from drf_lbcms.models import CmsMailNotification
from drf_lbcms.helpers import email
from django.core import mail

from .models import CmsSettings, CmsUser


class TestHelpers2(TestCase):
    def test_html_to_text(self):
        from drf_lbcms.helpers.email import html_to_text
        text = """

  <!DOCTYPE html><html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>h2 &gt; a:hover {text-decoration:underline !important}
a.link:hover {text-decoration:underline !important}
.btn.btn-primary:hover {background:#0065ff !important}</style>
  </head>
  <body>
    <div class="text-left" style="font-family:Roboto, RobotoDraft, Helvetica, Arial, sans-serif; text-align:left" align="left"><h1 style="font-family:Roboto, RobotoDraft, Helvetica, Arial, sans-serif; color:#0052cc; font-size:20px; font-weight:400; line-height:24px; margin:10px 0">To continue working with the plugin, you need to confirm your email</h1>
<div class="spacing-big" style="font-family:Roboto, RobotoDraft, Helvetica, Arial, sans-serif; display:block; height:22px; width:100%" height="22" width="100%"/>
<div class="text-left" style="font-family:Roboto, RobotoDraft, Helvetica, Arial, sans-serif; text-align:left" align="left">
    <a class="btn btn-primary" href="http://9251ef3f5e60.ngrok.io/prod/notificationAccounts/confirm/4?token=30fbd71c-4526-11eb-bc94-0242ac160003&amp;atrf=e156a9f3b031bf4491346fe9c2db6f36d5fc8f63&amp;preferredLang=ru" style="border-radius:3px; border-width:0; box-sizing:border-box; color:#fff; display:inline-flex; font-family:-apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Droid, Helvetica, sans-serif; font-size:14px; font-style:normal; height:2.285714em; line-height:2.285714em; margin:0; outline:none; padding:0 38px; text-align:center; text-decoration:none; vertical-align:middle; white-space:nowrap; background:#0052cc" height="2.285714em" align="center" valign="middle"> Confirm this email</a>
</div>
      <div class="spacing" style="font-family:Roboto, RobotoDraft, Helvetica, Arial, sans-serif; display:block; height:12px; width:100%" height="12" width="100%"/>
      <div class="hr" style="font-family:Roboto, RobotoDraft, Helvetica, Arial, sans-serif; background:#dfe1e6; height:2px; margin:0; margin-bottom:12px; margin-top:12px; padding:0" height="2"/>
      <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; border-spacing:0; margin:0; width:100%">
        <tbody style="border-bottom:0">
          <tr>
            <td style="padding:0">
              <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; border-spacing:0; margin:0; width:100%">
                <tbody style="border-bottom:0">
                  <tr>
                    <td style="padding:0"><small style="color:#707070;font-size:12px;line-height:1.3333334;color:#707070;font-size:12px;font-weight:normal;line-height:17px">You have received this email because your email address has been subscribed to the notifications in the <b style="font-weight:600">TEST: Jira notifications for email, telegram, viber</b> plugin for JIRA</small></td>
                  </tr>
                  <tr>
                    <td style="padding:0">
                      <div style="font-family:Roboto, RobotoDraft, Helvetica, Arial, sans-serif; font-size:1px; height:4px; line-height:4px" height="4"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding:0; line-height:15px"><small style="color:#707070;font-size:12px;line-height:1.3333334;color:#707070;font-size:12px;font-weight:normal;line-height:17px"><a class="link" href="https://lbcmstest.atlassian.net/plugins/servlet/ac/eu.glsplugins.jira-notifications.test/app-notificationChannels-list" target="_blank" style="color:#0052cc; text-decoration:none">Notification settings</a></small><small style="color:#707070;font-size:12px;line-height:1.3333334;color:#707070;font-size:12px;font-weight:normal;line-height:17px">  •  </small><small style="color:#707070;font-size:12px;line-height:1.3333334;color:#707070;font-size:12px;font-weight:normal;line-height:17px"><a class="link" href="http://jira-notifications.glsplugins.eu/prod/privacy-policy" target="_blank" style="color:#0052cc; text-decoration:none">Privacy &amp; Policy</a></small></td>
                  </tr>
                </tbody>
              </table>
            </td>
            <td width="20" style="padding:0; display:block">   </td>
            <td style="padding:0; text-align:right" align="right">
              <div class="logo" style="font-family:Roboto, RobotoDraft, Helvetica, Arial, sans-serif"><img src="/client/brand/logo.min.png" style="border:0;border:0;height:32px" height="32"/></div>
            </td>
          </tr>
        </tbody>
      </table><div style="color: transparent; visibility: hidden; display: none; width: 0; height: 0; background: transparent; font-size: 0.1px;">#327690</div>
    </div>
  </body>
</html>
        """
        #print(html_to_text(text))


class TestConnections(TestCase):
    def setUp(self):
        self.settings = CmsSettings.objects.create(
            siteTitle={
                'en': 'Test-site-name'
            },
            serverIntegrations={
                'emailBackend': 'sendgrid',
                'sendgrid': {
                    'apiKey': "INVALID"
                },
                'gmail':{
                    'username': 'noname@gmail.com',
                    'password': 'invalid'
                }
            }
        )

    def test_sendgrid(self):
        from drf_lbcms.helpers.email import get_connection
        from sendgrid import SendGridClientError
        connection = get_connection(self.settings, fail_silently=False)

        djemail = EmailMultiAlternatives(
            subject='test',
            body='hello world!',
            from_email='noreply@mail.com',
            to=['gerasev.kirill@gmail.com'],
            connection=connection
        )
        self.assertRaisesMessage(
            SendGridClientError,
            '{"errors":["The provided authorization grant is invalid, expired, or revoked"],"message":"error"}',

            djemail.send
        )

        """
        self.settings.serverIntegrations['sendgrid']['apiKey'] = ""
        connection = get_connection(self.settings, fail_silently=False)
        djemail = EmailMultiAlternatives(
            subject='test #2: sendgrid',
            body='hello <b>world</b>!',
            from_email='noreply@mail.com',
            to=['gerasev.kirill@gmail.com'],
            connection=connection
        )
        djemail.send()
        """


    def test_gmail(self):
        from drf_lbcms.helpers.email import get_connection
        from smtplib import SMTPAuthenticationError

        self.settings.serverIntegrations['emailBackend'] = 'gmail'
        connection = get_connection(self.settings, fail_silently=False)

        djemail = EmailMultiAlternatives(
            subject='test #3: gmail',
            body='hello <b>world</b>!',
            from_email='noreply@mail.com',
            to=['gerasev.kirill@gmail.com'],
            connection=connection
        )
        self.assertRaisesRegexp(
            SMTPAuthenticationError,
            "Username and Password not accepted*",

            djemail.send
        )

        """
        self.settings.serverIntegrations['gmail']['username'] = ""
        self.settings.serverIntegrations['gmail']['password'] = ""
        connection = get_connection(self.settings, fail_silently=False)
        djemail = EmailMultiAlternatives(
            subject='test #4: gmail',
            body='hello <b style="color: red;">world</b>!',
            from_email='noreply@mail.com',
            to=['gerasev.kirill@gmail.com'],
            connection=connection
        )
        djemail.attach_alternative('hello <b style="color: red;">world</b>!', "text/html")
        djemail.attach(
            'Test file.txt',
            'Nocontent',
            'text/plain'
        )
        djemail.send()
        """





class TestHelpers(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.cmi = CmsMailNotification.objects.create(
            identifier='test.identifier',
            emailTemplateName='BlueWave',
            to=['test@cmi.com'],
            fromEmail="test@drflbcms.com",
            send=True,
            subject={
                'en': "Test subject",
                'ru': u"Тестовая тема"
            },
            body={
                'en': "Test body {{email}}. Variable - {{variable}}",
                'ru': u"Тестовое тело {{email}}. Variable - {{variable}}"
            },
            formResponseSuccess={
                'en': 'Response for success. Hello!'
            }
        )
        self.user = CmsUser.objects.create(
            username='test_user',
            email='test_user@mail.com',
            preferredLang='en'
        )
        self.settings = CmsSettings.objects.create(
            serverIntegrations={
                'useEmailNotifications': True
            },
            siteTitle={
                'en': 'Test-site-name'
            }
        )

    def test_send_email(self):
        self.assertEqual(len(mail.outbox), 0)

        self.cmi.send_user_notification(self.user)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].subject,
            'Test subject'
        )

        self.assertIn(
            'Test body test_user@mail.com',
            mail.outbox[0].body
        )
        i = 1
        for tn in ['BlueWave', 'OrangeWave', 'SimpleGrey']:
            self.cmi.emailTemplateName = tn
            self.cmi.send_user_notification(self.user, preferred_lang='ru')
            self.assertEqual(len(mail.outbox), i+1)
            self.assertEqual(
                mail.outbox[i].subject,
                u'Тестовая тема'
            )

            self.assertIn(
                u'Тестовое тело test_user@mail.com',
                mail.outbox[i].body
            )
            self.assertIn(
                tn,
                mail.outbox[i].body
            )
            i += 1

        # проверка поддержки часовых поясов
        self.cmi.body['ru'] = '{{date | date:"Y-m-d H:i:s"}}'
        dt = datetime.now()
        dt = dt.replace(tzinfo=pytz.timezone('UTC'))
        kiev_dt = dt.astimezone(pytz.timezone('Europe/Kiev'))
        prague_dt = dt.astimezone(pytz.timezone('Europe/Prague'))

        self.assertNotEqual(
            str(dt),
            str(kiev_dt)
        )


        # kiev
        self.user.timezone = 'Europe/Kiev'
        self.cmi.send_user_notification(self.user, preferred_lang='ru', context={'date': dt})
        self.assertEqual(len(mail.outbox), i+1)
        self.assertEqual(
            mail.outbox[i].subject,
            u'Тестовая тема'
        )

        self.assertIn(
            kiev_dt.strftime("%Y-%m-%d %H:%M:%S"),
            mail.outbox[i].body
        )

        # prague
        self.user.timezone = 'Europe/Prague'
        self.cmi.send_user_notification(self.user, preferred_lang='ru', context={'date': dt})
        self.assertEqual(len(mail.outbox), i+2)
        self.assertEqual(
            mail.outbox[i+1].subject,
            u'Тестовая тема'
        )

        self.assertIn(
            prague_dt.strftime("%Y-%m-%d %H:%M:%S"),
            mail.outbox[i+1].body
        )
        self.assertNotIn(
            dt.strftime("%Y-%m-%d %H:%M:%S"),
            mail.outbox[i+1].body
        )
        self.assertNotIn(
            kiev_dt.strftime("%Y-%m-%d %H:%M:%S"),
            mail.outbox[i+1].body
        )


    def test_send_email_with_attachment(self):
        outbox = len(mail.outbox)
        self.cmi.send_user_notification(self.user, attachments=[
            {
                'name': 'hello.txt',
                'content': 'My file',
                'mime': 'text/plain'
            }
        ])
        self.assertEqual(len(mail.outbox), outbox+1)
        self.assertEqual(
            mail.outbox[-1].subject,
            'Test subject'
        )
        attachments = mail.outbox[-1].attachments
        self.assertEqual(len(attachments), 1)
        self.assertEqual(attachments[0][0], 'hello.txt')
        self.assertEqual(attachments[0][1], 'My file')


    def test_rest_points(self):
        outbox = len(mail.outbox)
        CmsMailNotificationViewset = drfs.generate_viewset('CmsMailNotification.json')

        # обычное письмо
        send = CmsMailNotificationViewset.as_view({'put': 'send'})

        request = self.factory.put('', data={'email': 'usertest@myemail.ua', 'variable': 1234567}, format='json')
        response = send(request, 'test.identifier')
        self.assertEqual(len(mail.outbox), outbox+1)
        self.assertEqual(
            mail.outbox[-1].subject,
            'Test subject'
        )
        self.assertIn(
            "Test body usertest@myemail.ua. Variable - 1234567",
            mail.outbox[-1].body
        )
        self.assertEqual(
            mail.outbox[-1].to,
            ["usertest@myemail.ua"]
        )
        self.assertDictEqual(
            response.data,
            {'en': 'Response for success. Hello!'}
        )

        # письмо с файлом
        send_multipart = CmsMailNotificationViewset.as_view({'put': 'send_multipart'})

        with open(os.path.join(settings.BASE_DIR, 'resources', 'django_logo.png'), 'rb') as f:
            request = self.factory.put('', data={
                'email': 'usertest22@myemail.ua',
                'variable': 999999,
                'myFile': f
            }, format='multipart')
        response = send_multipart(request, 'test.identifier')
        self.assertEqual(len(mail.outbox), outbox+2)
        self.assertEqual(
            mail.outbox[-1].subject,
            'Test subject'
        )
        self.assertIn(
            "Test body usertest22@myemail.ua. Variable - 999999",
            mail.outbox[-1].body
        )
        self.assertEqual(
            mail.outbox[-1].to,
            ["usertest22@myemail.ua"]
        )
        self.assertDictEqual(
            response.data,
            {'en': 'Response for success. Hello!'}
        )
        self.assertEqual(
            len(mail.outbox[-1].attachments),
            1
        )
        self.assertEqual(
            mail.outbox[-1].attachments[0][0],
            'django_logo.png'
        )
