# -*- coding: utf-8 -*-
from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
import json, os, tempfile







class Test(TestCase):
    def test(self):
        from drf_lbcms.management.commands.drf_lbcms_translate_objects import Command
        from .models import CmsSomeModel
        o1 = CmsSomeModel.objects.create(json={
            'en': 'hello world!'
        })
        o2 = CmsSomeModel.objects.create(json={
            'en': '<p>Hello world with tags! Other text</p>'
        })
        o3 = CmsSomeModel.objects.create(json=[{
            'deep':{
                'en': '<p>hello world with tags for deep path!</p>'
            }
        }])
        c = Command()
        kwargs={
            '--models=': 'CmsSomeModel',
            '--fromLang=': 'en',
            '--toLang=': 'ru'
        }
        c.handle(**kwargs)

        o1.refresh_from_db()
        o2.refresh_from_db()
        o3.refresh_from_db()
        # print o1.json['ru']
        # print o2.json['ru']
        # print o3.json[0]['deep']['ru']
        self.assertEqual(
            o2.json['ru'],
            u'<p>Привет, мир с тегами! Другой текст</p>'
        )
        self.assertEqual(
            o3.json[0]['deep']['ru'],
            u'<p>привет, мир с тегами для глубокого пути!</p>'
        )
