# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core import mail
from rest_framework import exceptions
from rest_framework.test import APIRequestFactory
import json, os

from drfs import generate_viewset, generate_model, generate_serializer
from rest_framework.viewsets import ModelViewSet
from drf_lbcms.models import CmsPdfData, CmsSmsNotification
from drf_lbcms.mixins import CmsUserManagementMixin
from drfs.mixins import UserRegisterLoginLogoutMixin
from tests.models import TestModelForCmsPdfData, CmsSettings
from drf_lbcms.helpers import sms

from .models import CmsUser



class TestUserManagement(TestCase):
    def setUp(self):
        CmsSmsNotification.objects.create(
            identifier='cms.user.$verificationCodeForLogin',
            send=True,
            body={
                'en': "Your code for login: {{user.hiddenAccountData.smsProtection.smsCode}}",
            }
        )
        self.user = CmsUser.objects.create(
            username='test_user',
            email='test_user1@mail.com',
            loginProtection='s',
            is_active=True,
            accountData={
                'phones':[{
                    'type': 'm',
                    'value': '1111111111111'
                }]
            }
        )
        self.user.set_password('123')
        self.user.save()
        self.user2 = CmsUser.objects.create(
            username='test_user2',
            email='test_user2@mail.com',
            loginProtection='',
            is_active=True,
            accountData={
                'phones':[{
                    'type': 'a',
                    'value': '1111111111111'
                }]
            }
        )
        self.user2.set_password('123456')
        self.user2.save()
        CmsSettings.objects.create()

        class UserViewset(generate_viewset('CmsUser.json')):
            serializer_class = generate_serializer('CmsUser.json', hidden_fields=[
                'one_test_model', 'many_test_model', 'sModel', 'timezone', 'accountData',
                'cmssomemodel'
            ])
        self.viewset = UserViewset


    def test_permissions(self):
        meView = self.viewset.as_view({'get': 'me'})
        updateMeView = self.viewset.as_view({'put': 'update_me'})
        # обычное поведение
        request = APIRequestFactory().get('')
        response = meView(request)
        self.assertEquals(response.status_code, 401)

        # залогиненный пользователь
        request.user = self.user
        response = meView(request)
        self.assertEqual(response.status_code, 200)
        # пустые permissions
        self.assertEqual(response.data['allPermissions'], set())
        self.assertTrue('user_permissions' not in response.data)
        self.assertTrue('groups' not in response.data)


        # залогиненный пользователь с 1 permission
        from django.contrib.auth.models import Permission
        can_view_user = Permission.objects.get(codename='view_cmsuser')
        self.user.user_permissions.add(can_view_user)
        request.user = CmsUser.objects.get(username='test_user')

        response = meView(request)
        self.assertEqual(response.status_code, 200)
        # есть 1 permission
        self.assertEqual(response.data['allPermissions'], set(['tests.view_cmsuser']))
        self.assertTrue('user_permissions' not in response.data)
        self.assertTrue('groups' not in response.data)

        # проверяем, что сам пользователь не может изменить свои permissions
        first_permission = Permission.objects.exclude(codename='view_cmsuser').first()
        request = APIRequestFactory().put('', {"permissions": [first_permission.id]}, format='json')
        request.user = CmsUser.objects.get(username='test_user')
        response = updateMeView(request)
        user = CmsUser.objects.get(username='test_user')
        self.assertEquals(
            list(user.user_permissions.all().values_list('id', flat=True)),
            list(Permission.objects.filter(codename='view_cmsuser').values_list('id', flat=True))
        )

    '''
    def test_login_with_sms(self):
        ERRORS = self.viewset.management_errors
        loginView = self.viewset.as_view({'post': 'login'})
        smsCount = len(sms.outbox)

        # обычное поведение
        request = APIRequestFactory().post('', {'username':'test_user', 'password': '1'}, format='json')
        response = loginView(request)
        self.assertEqual(
            response.status_code,
            400
        )
        self.assertEqual(
            response.data['non_field_errors'],
            [u"Unable to log in with provided credentials."]
        )


        self.user.refresh_from_db()
        # проверяем, что сведения о смс нет
        self.assertDictEqual(self.user.hiddenAccountData, {})

        request = APIRequestFactory().post('', {'username':'test_user', 'password': '123'}, format='json')
        response = loginView(request)
        # проверяем, что в стеке есть новое отправленное смс
        self.assertEqual(smsCount+1, len(sms.outbox))
        # проверяем, что это последнее смс - пользователю который хочет залогиниться
        self.assertEqual(
            sms.outbox[-1].to[0],
            self.user.accountData['phones'][0]['value']
        )

        self.user.refresh_from_db()
        self.assertEqual(
            sms.outbox[-1].body,
            "Your code for login: {smsCode}".format(
                smsCode=self.user.hiddenAccountData['smsProtection']['smsCode']
            )
        )
        self.assertEqual(
            response.status_code,
            406
        )
        self.assertEqual(
            response.data['detail'],
            ERRORS['provide_sms_code_for_login']
        )


        # теперь пытаемся залогиниться с НЕВЕРНЫМ смс-кодом
        request = APIRequestFactory().post('', {'username':'test_user', 'password': '123', 'smsCode': 'bla-bla'}, format='json')
        response = loginView(request)
        # проверяем, что в стеке НЕТ новых отправленных смс
        self.assertEqual(smsCount+1, len(sms.outbox))
        self.assertEqual(
            response.status_code,
            400
        )
        self.assertEqual(
            response.data['detail'],
            ERRORS['invalid_sms_code_for_login']
        )


        # пытаемся залогиниться с ВЕРНЫМ смс-кодом
        request = APIRequestFactory().post('', {'username':'test_user', 'password': '123', 'smsCode': self.user.hiddenAccountData['smsProtection']['smsCode']}, format='json')
        response = loginView(request)
        # проверяем, что в стеке НЕТ новых отправленных смс
        self.assertEqual(smsCount+1, len(sms.outbox))
        # проверяем, что пользователь действительно залогинился
        self.assertEqual(
            response.status_code,
            200
        )
        self.assertEqual(
            response.data['userId'],
            self.user.id
        )

        self.user.refresh_from_db()
        # проверяем, что сведения о смс больше нет
        self.assertDictEqual(self.user.hiddenAccountData, {'smsProtection': {}})
    '''
