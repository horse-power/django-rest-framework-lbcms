# -*- coding: utf-8 -*-
from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
import json, os, tempfile

from drf_lbcms.management.commands.drf_lbcms_ng_constants import Command

RESOURCES_DIR = os.path.join(settings.BASE_DIR, 'resources')




class Test(TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_ng_constants(self):
        tmp_file_path = os.path.join(tempfile.mkdtemp(), 'constants.js')
        c = Command()
        c.handle(
            filepath=[
                tmp_file_path
            ]
        )
        with open(tmp_file_path) as f:
            data = f.read()
        os.remove(tmp_file_path)
        os.removedirs(os.path.dirname(tmp_file_path))

        with open(os.path.join(RESOURCES_DIR, 'constants.js')) as f:
            constants = f.read()
        #print('------------')
        #print(data)
        #print('------------')

        self.assertEqual(data, constants)
