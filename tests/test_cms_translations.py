# -*- coding: utf-8 -*-
from django.contrib.auth.models import User as UserModel
from django.test import TestCase, RequestFactory
from django.conf import settings
from rest_framework import exceptions
import json, os

from drf_lbcms.helpers.bulk import Bulk
from drf_lbcms.models import CmsTranslations

from drf_lbcms.helpers import l10n, gettext
from drf_lbcms.management.commands.drf_lbcms_update_pot import Command

BASE_DIR = os.path.dirname(__file__)
RESOURCE_DIR = os.path.join(BASE_DIR, 'resources')


def read_json(path):
    with open(os.path.join(RESOURCE_DIR, path)) as f:
        data = json.load(f)
    return data


origMSGS = [
    {'msgId': u'One apple', 'msgIdPlural': u'{{count}} apples', 'msgCtxt': u'My context text', 'msgStr': [], 'references': ['client/index_app/index/View.html:1']},
    {'msgComm': u'Some commentary string', 'msgId': u'Translations', 'msgStr': [], 'references': ['client/index_app/index/View.html:1']},
    {'msgId': u'Unsafe &copy; string &plusmn;', 'msgStr': [], 'references': ['client/index_app/index/View.html:1']}
]
updatedMSGS = read_json('po/resMerged.json')['msgs']


class TestTranslations(TestCase):
    def test_create_and_update(self):
        # create
        kw = {
            'podir': os.path.join(RESOURCE_DIR, 'po/*.pot'),
            'langs': 'ru,en'
        }
        c = Command()
        c.handle(**kw)
        self.assertEqual(
            sorted(list(CmsTranslations.objects.all().values_list('lang', flat=True))),
            ['en', 'ru']
        )
        for obj in CmsTranslations.objects.all():
            self.assertListEqual(
                sorted(obj.msgs, key=lambda x: x['msgId']),
                sorted(origMSGS, key=lambda x: x['msgId'])
            )

        # update
        kw = {
            'podir': os.path.join(RESOURCE_DIR, 'po/some_dir/*.pot'),
            'langs': 'ru'
        }
        c = Command()
        c.handle(**kw)
        self.assertEqual(
            sorted(list(CmsTranslations.objects.all().values_list('lang', flat=True))),
            ['en', 'ru']
        )
        self.assertListEqual(
            CmsTranslations.objects.get(lang='en').msgs,
            origMSGS
        )
        self.assertListEqual(
            CmsTranslations.objects.get(lang='ru').msgs,
            updatedMSGS
        )

        # dump to storage
        obj = CmsTranslations.objects.get(lang='ru')
        obj.msgs[0]['msgStr'] = [
            u"{{count}} яблоко",
            u"{{count}} яблока",
            u"{{count}} яблок"
        ]
        obj.save()

        dumped_data = gettext.model_to_gettext_json(obj)
        self.assertDictEqual(
            dumped_data, {
                "ru": {
                    "One apple": {
                        "My context text": [
                            u"{{count}} яблоко",
                            u"{{count}} яблока",
                            u"{{count}} яблок"
                        ],
                        "My other context": []
                    }
                }
        })
        gettext.generate_gettext_dump_and_upload(obj)
