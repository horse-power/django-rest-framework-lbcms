# -*- coding: utf-8 -*-
from django.test import TestCase
from django.db.models import Sum
import drfs, json, os
from django.core import mail

from drf_lbcms.models import CmsMailNotification, CmsSmsNotification
from .models import CmsUser
from drf_lbcms.helpers.tests import APIRequestFactoryWrapper

BASE_DIR = os.path.dirname(__file__)
RESOURCE_DIR = os.path.join(BASE_DIR, 'resources')





class TestExportMixin(TestCase):
    def setUp(self):
        self.viewsetMail = drfs.generate_viewset('CmsMailNotification.json')
        self.viewsetSms = drfs.generate_viewset('CmsSmsNotification.json')
        self.factory = APIRequestFactoryWrapper()
        self.maxDiff = None


    def test_mail(self):
        user = CmsUser.objects.create(username='somename', userRole='u', email='t@c.com')
        tinstance = CmsMailNotification.objects.create(
            identifier='test',
            description={},
            subject={
                'en': 'Some subject'
            },
            body={
                'ru': 'Some text'
            }
        )
        tinstance2 = CmsMailNotification.objects.create(
            identifier='test-without-initial',
            description={},
            subject={
                'en': 'Some subject'
            },
            body={
                'ru': 'Some text'
            }
        )
        tinstance_child = CmsMailNotification.objects.create(
            identifier='custom-1|test',
            description={},
            subject={
                'en': 'Some subject'
            },
            body={
                'ru': 'Some text'
            },
            owner=1000
        )
        # сверяемся, что только 2 может взять пользователь
        request = self.factory.get('', user=user)
        response = self.viewsetMail.as_view({'get': 'list'})(request)
        self.assertEqual(len(response.data), 2)
        # теперь все 3
        tinstance_child.owner = user.id
        tinstance_child.save()
        request = self.factory.get('', user=user)
        response = self.viewsetMail.as_view({'get': 'list'})(request)
        self.assertEqual(len(response.data), 3)
        # берем текст из папки проекта
        get_initial = self.viewsetMail.as_view({'get': 'get_initial'})
        request = self.factory.get('', user=user)
        response = get_initial(request, pk=tinstance.id)
        self.assertDictEqual(response.data, {
            "body": {
                u"ru": u"<b>Изначальный текст в шаблоне</b>"
            },
            "echoSubject": {},
            "echoBody": {},
            "subject": {
                u"en": u"Initial subject"
            }
        })

        # берем текст из родительского объекта
        get_initial = self.viewsetMail.as_view({'get': 'get_initial'})
        request = self.factory.get('', user=user)
        response = get_initial(request, pk=tinstance_child.id)
        self.assertDictEqual(response.data, {
            "body": {
                "ru": "Some text"
            },
            "echoSubject": {},
            "echoBody": {},
            "subject": {
                "en": "Some subject"
            }
        })

        # для данного объекта не должно быть начальных данных
        get_initial = self.viewsetMail.as_view({'get': 'get_initial'})
        request = self.factory.get('', user=user)
        response = get_initial(request, pk=tinstance2.id)
        self.assertDictEqual(response.data, {
            "body": {},
            "echoSubject": {},
            "echoBody": {},
            "subject": {}
        })
