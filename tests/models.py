from django.db import models
import drfs





CmsUser = drfs.generate_model('CmsUser.json')


class TestModel(models.Model):
    int_field = models.IntegerField()
    string_field = models.CharField(max_length=50)
    user = models.ForeignKey(CmsUser, null=True, related_name='one_test_model', on_delete=models.CASCADE)
    manyUsers = models.ManyToManyField(CmsUser, related_name='many_test_model')


    def get_sitemap_item(self, lang=None):
        if not lang or lang == 'de':
            return None
        return {
            'location': "/{lang}/test-model/{id}/{string_field}".format(
                lang=lang,
                id=self.id,
                string_field='_'.join(self.string_field.split(' '))
            )
        }



TestModelL10nFile = drfs.generate_model('TestModelL10nFile.json')
TestModelL10nFileLocal = drfs.generate_model('TestModelL10nFileLocal.json')
TestModelL10nFileList = drfs.generate_model('TestModelL10nFileList.json')
TestModelForCmsPdfData = drfs.generate_model('TestModelForCmsPdfData.json')
CmsSettings = drfs.generate_model('CmsSettings.json')
CmsSomeModel = drfs.generate_model('CmsSomeModel.json')


class FakeL10nFile:
    file_data = None
    meta_data = {}
    thumbs = []
    title = {}
    description = {}
    def resolve_file_uri(self, **kwargs):
        from drf_lbcms.helpers.l10nFile import resolve_l10nfile_uri
        return resolve_l10nfile_uri(self.file_data, **kwargs)
