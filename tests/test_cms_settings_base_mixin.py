from django.test import TestCase
import drfs, json, os
from django.core.files import File
from django.core.files.images import ImageFile
from django.core.files.storage import default_storage
from rest_framework.test import APIRequestFactory
import datetime


from .models import CmsSettings

BASE_DIR = os.path.dirname(__file__)
RESOURCE_DIR = os.path.join(BASE_DIR, 'resources')


class TestMixin(TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_parse_file(self):
        viewset = drfs.generate_viewset(CmsSettings)
        factory = APIRequestFactory()

        parseFile = viewset.as_view({'post': 'parse_file'})
        request = factory.post('', {'file': open(os.path.join(RESOURCE_DIR, 'file1.csv'), 'rb')}, format='multipart')

        response = parseFile(request)
        self.assertEqual(
            response.data, {
            "data": {
                "sheets": [
                    {
                        "rows": [
                            [
                                "Integer",
                                "Float",
                                "Date",
                                "Time",
                                "String"
                            ],
                            [
                                "1",
                                "10.99",
                                "04/24/19",
                                "03:14:35 PM",
                                "Hello world!"
                            ]
                        ],
                        "name": "default"
                    }
                ]
            },
            "contentType": "text/csv"
        })



        data = {
            "sheets": [
                {
                    "rows": [
                        [
                            "Integer",
                            "Float",
                            "Date",
                            "Time",
                            "String"
                        ],
                        [
                            1.0,
                            10.99,
                            datetime.datetime(2019, 4, 24),
                            datetime.time(15, 14, 35),
                            "Hello world!"
                        ]
                    ],
                    "name": "Sheet1"
                }
            ]
        }

        request = factory.post('', {'file': open(os.path.join(RESOURCE_DIR, 'file1.xls'), 'rb')}, format='multipart')

        response = parseFile(request)
        self.assertEqual(response.data['contentType'], 'application/vnd.ms-excel')
        self.assertEqual(response.data['data'], data)


        request = factory.post('', {'file': open(os.path.join(RESOURCE_DIR, 'file1.xlsx'), 'rb')}, format='multipart')

        response = parseFile(request)
        self.assertEqual(response.data['contentType'], 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        self.assertEqual(response.data['data'], data)
