# -*- coding: utf-8 -*-
from django.contrib.auth.models import User as UserModel
from django.test import TestCase
from django.db.models import Sum
import drfs, json, os
from rest_framework.test import APIRequestFactory
from rest_framework.viewsets import ModelViewSet
from io import StringIO
import csv, six

from .models import TestModel
from drf_lbcms.mixins import ExportMixin, ExportByIdMixin
from drf_loopback_js_filters import LoopbackJsFilterBackend

BASE_DIR = os.path.dirname(__file__)
RESOURCE_DIR = os.path.join(BASE_DIR, 'resources')



class TestModelViewset(ModelViewSet):
    queryset = TestModel.objects.all()
    serializer_class = str





class TestQuickFilterMixin(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.maxDiff = None

        for m in [1,2,3,4,5]:
            TestModel.objects.create(int_field=m*2, string_field='string '+str(m))
        TestModel.objects.create(int_field=100, string_field='other')

    def test_mixin(self):
        viewset = drfs.generate_viewset(
            TestModel,
            filter_backends=[LoopbackJsFilterBackend],
            mixins=['drf_lbcms.mixins.QuickFilterMixin']
        )
        qf = viewset.as_view({'get': 'list'})

        # пустое значение
        request = self.factory.get('?quickFilter=')
        response = qf(request)
        self.assertEqual(
            len(response.data),
            6
        )
        self.assertEqual(
            response.status_code,
            200
        )

        # поиск по строке
        request = self.factory.get('?quickFilter=4')
        response = qf(request)
        # print json.dumps(response.data, indent=4)
        # должно быть найдено 2 эл-та: 1 с int_field==4 и 2 с string_field=='string 4'
        self.assertEqual(
            len(response.data),
            2
        )
        self.assertEqual(
            response.data[0]['id'],
            2
        )
        self.assertEqual(
            response.data[1]['id'],
            4
        )
        self.assertEqual(
            response.status_code,
            200
        )

        # поиск по строке
        request = self.factory.get('?quickFilter=string')
        response = qf(request)
        #print json.dumps(response.data, indent=4)
        # должно быть найдено 5 эл-тов: с string_field=='string *'
        self.assertEqual(
            len(response.data),
            5
        )
        for item in response.data:
            self.assertIn(
                'string',
                item['string_field'],
            )
        self.assertEqual(
            response.status_code,
            200
        )
